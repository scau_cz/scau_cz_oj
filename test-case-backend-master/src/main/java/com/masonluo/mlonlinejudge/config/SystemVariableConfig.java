package com.masonluo.mlonlinejudge.config;

import com.masonluo.mlonlinejudge.utils.StringUtils;
import org.apache.catalina.connector.Connector;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class SystemVariableConfig {
//
//    private final String testCaseDir = "/home/OnlineJudgeDeploy/data/backend/test_case";
//
//    private final String tmpDir = "/home/mlojDir/mlojHome/tmp";
//    @Bean("mlojHome")
//    public String getHome() {
//        String mlHome = System.getenv("MLOJ_HOME");
//        if (StringUtils.isBlank(mlHome)) {
//           // throw new IllegalStateException("MLOJ_HOME does not found, please set the system environment");
//            mlHome = "//home/mlojDir/mlojHome";
//        }
//        // TODO 修改回来原来的代码
//
//        return mlHome;
//    }
//
//    @Bean("testCaseDir")
//    public String getTestCaseDir() {
//        // TODO 修改回原来的代码
//        String dataHome = System.getenv("MLOJ_DATA_HOME");
//        if (StringUtils.isBlank(dataHome)) {
//            dataHome = "/home/OnlineJudgeDeploy/data";
//            throw new IllegalStateException("MLOJ_HOME does not found, please set the system environment");
//        }
//        return dataHome + "/backend/test_case";
//    }
//
//    @Bean("tmpDir")
//    public String getTmpDir() {
//        String mlHome = getHome();
//        return mlHome + "/tmp";
//    }

    /**
     * @author: zw
     * @create: 2019-06-27 11:19
     **/
    @Configuration
    public class TomcatConfig {

        @Bean
        public TomcatServletWebServerFactory webServerFactory() {
            TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
            factory.addConnectorCustomizers((Connector connector) -> {
                connector.setProperty("relaxedPathChars", "\"<>[\\]^`{|}");
                connector.setProperty("relaxedQueryChars", "\"<>[\\]^`{|}");
            });
            return factory;
        }
    }
}
