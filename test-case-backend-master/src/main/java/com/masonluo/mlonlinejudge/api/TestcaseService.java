package com.masonluo.mlonlinejudge.api;

import com.masonluo.mlonlinejudge.model.dto.TestCaseUploadDto;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface TestcaseService {
    TestCaseUploadDto uploadTestCase(MultipartFile zipFile) throws IOException;

    void downloadAllAttachment(String filePath, HttpServletRequest request, HttpServletResponse response) throws Exception;

    // boolean deleteAllFile(String filePath);

    void deleteTestCase(String testCaseId) throws Exception;
}
