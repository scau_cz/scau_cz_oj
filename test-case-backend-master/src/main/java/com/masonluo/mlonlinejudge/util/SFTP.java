package com.masonluo.mlonlinejudge.util;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.util.Properties;

public class SFTP {

    private Session session;//会话
    private Channel channel;//连接通道
    private ChannelSftp sftp;// sftp操作类


    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public ChannelSftp getSftp() {
        return sftp;
    }

    public void setSftp(ChannelSftp sftp) {
        this.sftp = sftp;
    }


    public static void getConnect(SFTP s) throws Exception {
        /** 密钥的密码  */
//      String privateKey ="key";
//        /** 密钥文件路径  */
//      String passphrase ="path";
        /** 主机 */
        // String host = "119.23.104.65";
        // TODO 修改IP地址
        String host = "106.53.210.134";
        /** 端口 */
        int port = 22;
        /** 用户名 */
        String username = "root";
        /** 密码 */
        // TODO 修改连接虚拟机的密码
        // String password = "37rmT!954";
        String password = "yourPassword";
        Session session = null;
        Channel channel = null;
        ChannelSftp sftp = null;// sftp操作类
        JSch jsch = new JSch();
        //设置密钥和密码
        //支持密钥的方式登陆，只需在jsch.getSession之前设置一下密钥的相关信息
//      if (privateKey != null && !"".equals(privateKey)) {
//             if (passphrase != null && "".equals(passphrase)) {
//              //设置带口令的密钥
//                 jsch.addIdentity(privateKey, passphrase);
//             } else {
//              //设置不带口令的密钥
//                 jsch.addIdentity(privateKey);
//             }
//      }
        session = jsch.getSession(username, host, port);
        session.setPassword(password);
        Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no"); // 不验证 HostKey
        config.put("PreferredAuthentications", "password");
        session.setConfig(config);
        try {
            session.connect(5000);
        } catch (Exception e) {
            if (session.isConnected())
                session.disconnect();
        }
        channel = session.openChannel("sftp");
        try {
            channel.connect(10000);
        } catch (Exception e) {
            if (channel.isConnected())
                channel.disconnect();
        }
        sftp = (ChannelSftp) channel;
        s.setChannel(channel);
        s.setSession(session);
        s.setSftp(sftp);
    }

    /*断开连接*/
    public static void disConn(Session session, Channel channel, ChannelSftp sftp) throws Exception {
        if (null != sftp) {
            sftp.disconnect();
            sftp.exit();
            sftp = null;
        }
        if (null != channel) {
            channel.disconnect();
            channel = null;
        }
        if (null != session) {
            session.disconnect();
            session = null;
        }
    }
}