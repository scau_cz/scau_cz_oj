import request from "@/utils/request"
export function addClass(data) {
    return request({
        url: "class/addClass",
        method: "post",
        headers: {
            "content-type": "application/json"
        },
        data: data
    })
}
export function findByTeacherId(data) {
    return request({
        url: "class/findByTeacherId/" + data.teacherId,
        method: "get",
        params: data
    })
}
export function pageClassBySchool(data) {
    return request({
        url: "class/pageClassBySchool/" + data.current + '/' + data.limit + '/' + data.id,
        method: "post",
        params: data
    })
}

export function ClassByTeacher(data) {
    return request({
        url: "/class/findByTeacherId/" + data,
        method: "get",
        params: data
    })
}
export function updateClass(data) {
    return request({
        url: "class/updateClass",
        method: "post",
        data: data,
        headers: {
            "content-type": "application/json"
        },
    })
}
export function deleteClass(data) {
    return request({
        url: encodeURI("class/deleteClass/" + data),
        method: "DELETE",
        data: data
    })
}
export function getUserBySchoolId(data) { //export function delete (data){
    return request({
        url: "/user/getUserBySchoolId",
        method: "get",
        headers: {
            "content-type": "application/json"
        },
        params: data
    })
}
