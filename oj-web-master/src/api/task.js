import request from "@/utils/request"
//获取任务列表信息
export function getTaskList(data){
    return request({
      url: "/task/taskList",
      method: "get",
      headers:{
        "content-type": "application/json"
    },
      params:data
    })
  }
  //根据班级获取任务
  export function getTaskListByClass(data){
    return request({
      url: "/task/classTask/"+data.classId,
      method: "get",
      headers:{
        "content-type": "application/json"
    },
      params:data
    })
  }
   //根据任务详情
   export function getTaskDetail(data){
    return request({
      url: "/task/getTaskInfo/"+data,
      method: "get",
      headers:{
        "content-type": "application/json"
    },
      params:data
    })
  }

//获取试卷详情
export function showPaperDetail(data) {
  return request({
      url: "/testpaper/showPaperDetail",
      method: "GET",
      headers: {
          "content-type": "application/json"
      },
      params: data
  })
}

  //提交试卷
  export function submitTask(data){
    return request({
      url: "/task/submit",
      method: "POST",
      headers:{
        "content-type": "application/json"
    },
      data:data
    })
  }
//删除任务
export function taskDel(data){
    return request({
      url: "/task/deleteTask/"+data,
      method: "DELETE",
      headers:{
        "content-type": "application/json"
    },
      params:data
    })
  }
  //创建任务
export function taskBuild(data){
    return request({
      url: "/task/createTask",
      method: "post",
      headers:{
        "content-type": "application/json"
    },
      data:data
    })
  }
//编辑任务
export function taskUpdate(data) {
  return request({
    url: "/task/editTask",
    method: "PATCH",
    headers:{
      "content-type": "application/json"
  },
    data:data
  })

}

  //获取班级的任务列表
export function getClassTaskList(data){
    return request({
      url: "/task/classTask/"+data,
      method: "get",
      headers:{
        "content-type": "application/json"
    },
      params:data
    })
  }
  //获取任务具体信息
export function getTaskInfo(data){
    return request({
      url: "/task/getTaskInfo/"+data,
      method: "get",
      headers:{
        "content-type": "application/json"
    },
      params:data
    })
  }

  //修改考试状态
  export function taskChangeStatus(data){
    return request({
      url: "/task/changeStatus",
      method: "PATCH",
      headers:{
        "content-type": "application/json"
    },
      data:data
    })
  }

  //修改考试时间
  export function taskChangeTime(data){
    return request({
      url: "/task/changeTime",
      method: "PATCH",
      headers:{
        "content-type": "application/json"
    },
      data:data
    })
  }

  // 查看考试答卷
  export function getTestAnswer(data){
    return request({
      url: "/task/getAnswerContent",
      method: "get",
      headers:{
        "content-type": "application/json"
    },
      params:data
    })
  }

  //缓存答卷
  export function storeExamAnswer(data){
    return request({
      url: "/task/storeExamAnswerSheet",
      method: "post",
      headers:{
        "content-type": "application/json"
    },
      data:data
    })
  }

//获取缓存答卷
export function getExamAnswerCache(data){
  return request({
    url: "/task/getExamAnswerSheetCache/" + data.userId + "/" + data.examId,
    method: "post",
    headers:{
      "content-type": "application/json"
  },
    data:data
  })
}

// 获取状态信息
export function getExamSituation(data){
  return request({
    url: "/task/getExamProblemResponseSituation",
    method: "get",
    headers:{
      "content-type": "application/json"
  },
    params:data
  })
}
