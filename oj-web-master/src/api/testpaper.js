import request from "@/utils/request"
//分页获取试卷
export function showPapers(data) {
    return request({
        url: "/testpaper/pageTestPaper",
        method: "post",
        headers: {
            "content-type": "application/json"
        },
        data: data
    })
}

//添加试卷
export function addTestPaper(data) {
    return request({
        url: "/testpaper/addTestPaper",
        method: "post",
        headers: {
            "content-type": "application/json"
        },
        data: data
    })
}

//获取试卷详情
export function showPaperDetail(data) {
    return request({
        url: "/testpaper/showPaperDetail",
        method: "GET",
        headers: {
            "content-type": "application/json"
        },
        params: data
    })
}

//删除试卷
export function deleteTestPaper(data) {
    return request({
        url: "/testpaper/deleteTestPaper/"+data,
        method: "DELETE",
        headers: {
            "content-type": "application/json"
        },
        data: data
    })
}

//修改试卷
export function updateTestPaper(data) {
    return request({
        url: "/testpaper/updateTestPaper",
        method: "post",
        headers: {
            "content-type": "application/json"
        },
        data: data
    })
}