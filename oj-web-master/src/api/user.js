import request from "@/utils/request"
export function addUserList(data) {
    return request({
        url: "user/addUserList",
        method: "post",
        headers: {
            "content-type": "application/json"
        },
        data: data
    })
}
export function getUserInfoByRole(data) { //获取学生/教师/校管理员信息列表
    return request({
        url: "user/getUserInfoByRole",
        method: "post",
        headers: {
            "content-type": "application/json"
        },
        data: data
    })
}
export function update(data) {
    return request({
        url: "user/update",
        method: "post",
        data: data
    })
}
export function deleteManage(data) {
    return request({
        url: "user/delete",
        method: "post",
        data: data
    })
}
export function getUSerInfoByNickOrName(data) { //export function delete (data){
    return request({
        url: "user/getUSerInfoByNickOrName",
        method: "post",
        headers: {
            "content-type": "application/json"
        },
        data: data
    })
}

export function getUserBySchoolId(data) { //export function delete (data){
    return request({
        url: "/user/getUserBySchoolId",
        method: "get",
        headers: {
            "content-type": "application/json"
        },
        params: data
    })
}