import request from "@/utils/request"
export function login(data) {
  return request({
    url: "user/login",
    method: "post",
    headers:{
        "content-type": "application/json"
    },
    data:data
  })
}
export function schools(data) {
    return request({
      url: "schools",
      method: "get",
      params:data
    })
  } 