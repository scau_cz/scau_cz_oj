import request from "@/utils/request"
function programPOST(data){
    return request({
      url: "problems",   //url: "class/findAll"+data.id
      method: "post",  
      headers:{
        "content-type": "application/json"
    },  
      data:data
    }) 
  }
  export {programPOST}