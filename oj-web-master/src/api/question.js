import request from "@/utils/request"
//根据标签/标题/题型获取题目
const questionGlobalConfig = {
  timeout: 20000,
}; // 该文件下
export function getAllQuestion(data) {
  return request({
    url: "problems/listAllProblem",
    method: "get",
    ...questionGlobalConfig,
    params:data
  })
}
//获取所有程序题
export function getAllPGQuestion(data) {
  return request({
    url: "/problems/list",
    method: "get",
    ...questionGlobalConfig,
    params:data
  })
}
//根据难度获取程序题
export function getPGQuestionByDifficulty(data) {
  return request({
    url: "/problems//listByDifficulty",
    method: "get",
    headers:{
      "content-type": "application/x-www-form-urlencoded"
  },
    params:data
  })
}
//上传编程题创建测试文件
export function testCase(data) {
  return request({
    url: "problems/testCase",
    method: "post",
    headers:{
      "content-type": "multipart/form-data"
  },
  ...questionGlobalConfig,
    data:data
  })
}
//编辑程序题
export function editPGProblem(data) {
  return request({
    url: "/problems/updateProblemById?userId="+data.userId+"&problemId="+data.id,
    method: "PATCH",
    headers:{
      "content-type": "application/json"
  },
  ...questionGlobalConfig,
    data:data
  })
}
//删除程序题
export function deletePGProblem(data) {
  return request({
    url: "/problems/"+data,
    method: "DELETE",
    headers:{
      "content-type": "application/json"
  },
  ...questionGlobalConfig,
    params:data
  })
}
//查找程序题
export function findPGProblem(data) {
  return request({
    url: "/problems/"+data,
    method: "GET",
    headers:{
      "content-type": "application/json"
  },
  ...questionGlobalConfig,
    params:data
  })
}
//上传编程题
export function uploadPGProblem(data) {
  return request({
    url: "problems",
    method: "post",
    headers:{
      "content-type": "application/json"
  },
  ...questionGlobalConfig,
    data:data
  })
}
//上传选择题判断题
export function uploadSLProblem(data) {
  return request({
    url: "/select/add",
    method: "post",
    headers:{
      "content-type": "application/json"
  },
    data:data
  })
}
//删除选择题判断题
export function deleteSLProblem(data) {
  return request({
    url: "/select/"+data,
    method: "DELETE",
    headers:{
      "content-type": "application/json"
  },
  ...questionGlobalConfig,
    params:data
  })
}
//编辑选择题判断题
export function editSLProblem(data) {
  return request({
    url: "/select/updateSelectProblemById?userId="+data.userId,
    method: "PATCH",
    headers:{
      "content-type": "application/json"
  },
  ...questionGlobalConfig,
    data:data
  })
}
//查找选择题判断题
export function findSLProblem(data) {
  return request({
    url: "/select/getSelectProblemById",
    method: "GET",
    headers:{
      "content-type": "application/json"
  },
  ...questionGlobalConfig,
    data:data
  })
}
//根据标签/标题/题型搜索题
export function findQuestion(data) {
  return request({
    url: "problems/getProblemByOthers?others="+data,
    method: "get",
    headers:{
      "content-type": "application/json"
  },
  timeout: 30000,
    data:data
  })
}
//列出所有tag
export function getAllTag(data) {
  return request({
    url: "tag/listAllTag",
    method: "POST",
    headers:{
      "content-type": "application/json"
  },
  ...questionGlobalConfig,
    data:data
  })
}

//下载测试用例  axios里面要设置一个responseType为blob,不然无法压缩
export function downloadAttachment(data) {
  return request({
    url: "problems/downloadTestCase/",
    method: "POST",
    headers:{
      "content-type": "application/json"
    },
    responseType: "blob",
    ...questionGlobalConfig,
    params:data,
  })
}
