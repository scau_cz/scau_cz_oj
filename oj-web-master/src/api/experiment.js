import request from "@/utils/request"

//根据学校Id返回实验列表(分页)
function getExperimentBySchool(data){
  return request({
    url: "experiments",  
    method: "get",
    params:data
  })
}

//根据实验Id返回实验
function getExperimentById(data){
  return request({
    url: "experiments/" + data,  
    method: "get",
    params:data
  })
}

//获取所有实验列表
function getAllExperiment(data){
  return request({
    url: "experiments/list",   
    method: "get",
    headers:{
      "content-type": "application/json"
  },
    params:data
  })
}
//超级管理员添加实验
function addExperiment(data){
  return request({
    url: "experiments/add",  
    method: "POST",
    headers:{
      "content-type": "application/json"
  },
    data:data
  })
}
//校管理员添加实验
function addExperimentByschool(data,userId){
  return request({
    url: "experiments/admin/add?userId="+userId,  
    method: "POST",
    headers:{
      "content-type": "application/json"
  },
  data:data
  })
}
//调整实验顺序
function sortExperiment(data,schoolId){
  return request({
    url: "experiments/sortExperiment?schoolId="+schoolId,  
    method: "POST",
    headers:{
      "content-type": "application/json"
  },
    data:data
  })
}
//获取该学校未关联的实验列表
function findNotRelatedSchool(data){
  return request({
    url: "experiments/admin/findNotRelatedSchool",   
    method: "get",
    headers:{
      "content-type": "application/json"
  },
    params:data
  })
}
//校管理员关联实验
function linkExperiment(data){
  return request({
    url: "experiments/admin/relatedSchool",  
    method: "GET",
    headers:{
      "content-type": "application/json"
  },
    params:data
  })
}

//校管理员设置实验状态
function setExperimentStatus(data){
  return request({
    url: "experiments/admin/setExperimentStatus",  
    method: "GET",
    headers:{
      "content-type": "application/json"
  },
    params:data
  })
}

//校管理员设置实验限制时间
function setExperimentDateTime(data){
  return request({
    url: "experiments/admin/setExperimentDateTime",  
    method: "GET",
    headers:{
      "content-type": "application/json"
  },
    params:data
  })
}

//删除实验
function delExperiment(data){
  return request({
    url: "experiments/"+data.experimentId,  
    method: "DELETE",
    headers:{
      "content-type": "application/json"
  },
    params:{userId:data.userId}
  })
}
//编辑实验
function updateExperiment(data,userId){
  return request({
    url: "experiments/admin/updateExperimentById?userId="+userId,  
    method: "PATCH",
    headers:{
      "content-type": "application/json"
  },
    data:data
  })
}

//获取实验列表实验
function getExperimentBySchoolId(data){
  return request({
    url: "experiments/listBySchoolIdAndStatus",  
    method: "GET",
    headers:{
      "content-type": "application/json"
  },
  params:data
  })
}

//获取实验列表实验
function getExperimentByexperimentIdAndUserId(data){
  return request({
    url: "experiments/listByExperimentIdAndUserId",  
    method: "GET",
    headers:{
      "content-type": "application/json"
  },
  params:data
  })
}

//根据实验id获取实验信息
function getExperimentByexperimentId(data){
  return request({
    url: "experiments/"+data,  
    method: "GET",
    headers:{
      "content-type": "application/json"
  },
  params:data
  })
}

export {getExperimentByexperimentId,getExperimentByexperimentIdAndUserId,getExperimentBySchoolId,getExperimentById,sortExperiment,findNotRelatedSchool,setExperimentStatus,setExperimentDateTime,linkExperiment,getExperimentBySchool,getAllExperiment,addExperiment,updateExperiment,delExperiment,addExperimentByschool}