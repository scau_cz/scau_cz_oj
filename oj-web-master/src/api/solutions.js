import request from "@/utils/request";
//提交解决方案
export function submitQuestion(data) {
  return request({
    url: "solutions",
    method: "post",
    headers: {
      "content-type": "application/json"
    },
    data: data
  });
}
//查找结果id
export function getResultId(data) {
  return request({
    url: "/solutions/findsolution/" + data,
    method: "get",
    headers: {
      "content-type": "application/json"
    },
    params: data
  });
}
//根据结果id查找结果
export function getResult(data) {
  return request({
    url: "/results/" + data.resultId + "/" + data.problemId,
    method: "get",
    headers: {
      "content-type": "application/json"
    },
    params: data
  });
}

//根据当前登录者的Id返回所有解决方案
//查找结果id
export function getSolutions(data) {
  return request({
    url: "/solutions",
    method: "get",
    headers: {
      "content-type": "application/json"
    },
    params: data
  });
}
//分页查询该用户的结果列表
export function getResults(data) {
  console.log(data);
  return request({
    url: "/results/findResult",
    method: "post",
    headers: {
      "content-type": "application/json"
    },
    data: data
  });
}

export function getSourceCode(data) {
  return request({
    url: "/results/getSourceCode/" + data.resultId,
    method: "get",
    headers: {
      "content-type": "application/json"
    },
    params: data
  });
}

// 根据用户id，实验id，题目id查看该实验的该题的最后一次提交代码
export function getFinallyCode(data) {
  return request({
    url:
      "solutions/findsolution/" +
      data.experimentId +
      "/" +
      data.problemId +
      "/" +
      data.userId,
    method: "get",
    params: data
  });
}

export function judgeProblemStatus(data) {
  return request({
    url:
      "/solutions/judgeProblemStatus/" +
      data.experimentId +
      "/" +
      data.problemId +
      "/" +
      data.userId,
    method: "get",
    headers: {
      "content-type": "application/json"
    },
    params: data
  });
}
