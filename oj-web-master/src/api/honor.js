import request from "@/utils/request"
export function getRankBySchoolId(data) {
    return request({
        url: "/user/getRankBySchoolId",
        method: "get",
        params: data
    })
}