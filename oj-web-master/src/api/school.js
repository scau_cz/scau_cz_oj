import request from "@/utils/request"
export function addSchool(data) {
    return request({
        url: "/schools",
        method: "post",
        headers: {
            "content-type": "application/json"
        },
        data: data
    })
}
export function getSchools(data) { //获取学校信息列表
    return request({
        url: "/schools",
        method: "get",
        params: data
    })
}
export function update(data) {
    return request({
        url: "/schools",
        method: "PATCH",
        headers: {
            "content-type": "application/json"
        },
        data: data
    })
}
export function deleteSchool(data) {
    return request({
        url: "/schools/" + data,
        method: "DELETE",
        params: data
    })
}
export function findBySchoolId(data) { 
    return request({
        // url: "/schools/findBySchoolId/" + data.id,
        url: "/schools/findBySchoolId/" + data,
        method: "get",
        params: data
    })
}
export function findBySchoolName(data) { 
    return request({
        url: "/schools/findBySchoolName",
        method: "POST",
        headers: {
            "content-type": "application/json"
        },
        data: data
    })
}
export function uploadbg(data) {
    return request({
        url: "/imgs/upload/background",
        method: "POST",
        headers: {
            "content-type": "application/json"
        },
        data: data
    })
}