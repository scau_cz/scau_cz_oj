import request from "@/utils/request"
export function addStu(data){
  return request({
    url: "user/addUserList",   
    method: "post",  
    headers:{
      "content-type": "application/json"
  },  
    data:data,
    timeout:10000
  })
}

// 通过用户Id删除用户
export function delStudent(data){
  return request({
    url: "user/delete?userId="+data.userId,   
    method: "POST",  
    headers:{
      "content-type": "application/json"
  },  
    data:data
  })
}

// 根据教师Id查看其管理的班级
export function getClass(data){
  return request({
    url: "class/findByTeacherId/"+data,   
    method: "get",  
    headers:{
      "content-type": "application/json"
  },  
    data:data
  })
}

// 根据班级id教师id分页显示学生信息
export function getStuByClassId(data) {
  return request({
      url: "class/getStudentByClassId/current/limit/"+data.classId+"/"+data.teacherId+"/"+data.current+"/"+data.limit,
      method: "get",
      headers: {
          "content-type": "application/json"
      },
      params: data
  })
}

//查本班学生
export function searchClassStu(data){
  return request({
    url: "class/getInfoByClassIdAndUsernameOrNickname",  
    method: "post",  
    headers:{
      "content-type": "application/json"
  },  
    data:data
  })
}

export function searchStu(data){
  return request({
    url: "user/getUSerInfoByNickOrName",
    method: "post",  
    headers:{
      "content-type": "application/json"
  },  
    data:data
  })
}

  // 更新用户信息
export function updateStu(data){
  return request({
    url: "user/update",   
    method: "post",  
    headers:{
      "content-type": "application/json"
  },  
    data:data
  })
}

    // 判断用户信息是否重复
export function userIsRepeat(data){
  return request({
    url: "user/userIsRepeat",   
    method: "post",  
    headers:{
      "content-type": "application/json"
  },  
    data:data
  })
}