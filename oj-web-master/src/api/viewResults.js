import request from "@/utils/request"
//通过教师id获取班级名称列表
function getClassByTeacherId(data) {
  return request({
    url: "class/findByTeacherId/" + data.teacherId,
    method: "get",
    params: data
  })
}

// 通过教师id和班级id获取学生实验成绩表
function getExperimentScoreByteacherIdAndClassId(data) {
  return request({
    url: "experiments/findScoreByTeacherIdAndClassId",
    method: "post",
    Headers: {
      "content-type": "application/json"
    },
    params: data
  })
}

// 通过教师id和班级id刷新学生实验成绩表
function refreshExperimentScoreByteacherIdAndClassId(data) {
  return request({
    url: "experiments/refreshScoreByTeacherIdAndClassId",
    method: "post",
    Headers: {
      "content-type": "application/json"
    },
    params: data,
    timeout: 50000,
  })
}

//通过教师id和班级id重构学生实验成绩表
function rebuildExperimentScoreByteacherIdAndClassId(data) {
  return request({
    url: "experiments/rebuildScoreByTeacherIdAndClassId",
    method: "post",
    Headers: {
      "content-type": "application/json"
    },
    params: data,
    timeout: 50000,

  })
}

// 根据用户id和实验id返回学生实验题目成绩
function getExperimentProblemResultByUserIdAndExperiment(data) {
  return request({
    url: "experiments/findExperimentProblemResult/",
    method: "post",
    Headers: {
      "content-type": "application/json"
    },
    params: data
  })
}

// 通过教师id获取授课班级及考试名称
function getClassAndExamTitleByTeacherId(data) {
  return request({
    url: "examscore/findExamTitleAndClassByUserId/" + data.userId,
    method: "get",
    params: data
  })
}

//通过学校id和班级id获取实验名称列表
function getExperimentBySchoolIdAndClassId(data) {
  return request({
    url: "experiments/listBySchoolIdAndClassId",
    method: "get",
    params: data
  })
}
//根据学校id和班级id获取考试名称列表
function getExamBySchoolIdAndClassId(data) {
  return request({
    url: "examscore/findExamByClassId/" + data.classId + "/" + data.schoolId,
    method: "get",
    params: data
  })
}
//通过学生id获取学生参与的所有考试成绩
function getStudentScoreByStudentId(data) {
  return request({
    url: "examscore/findByStudentId/" + data.studentId,
    method: "get",

  })
}
//通过考试id和班级id获取班级学生的所有考试成绩
function getExamScoreByExamIdAndClassId(data) {
  return request({
    url: "examscore/findByExamAndClass/" + data.examId + "/" + data.classId + "/" + data.current + "/" + data.limit,
    method: "get",
    params: data
  })
}
//通过实验id和班级id获取班级所以学生的实验成绩
function getExamScoreByExperimentIdAndClassId(data) {
  return request({
    url: "examscore/findByExamAndScore/", //+data.experimentId+"/"+data.classId+"/"+data.current+"/"+data.limit,
    method: "post",
    Headers: {
      "content-type": "application/json"
    },
    params: data
  })
}

// 根据用户id，实验id，题目id查看该实验的该题的最后一次提交代码
function getFinallyCode(data) {
  return request({
    url: "solutions/findsolution/" + data.experimentId + "/" + data.problemId + "/" + data.userId,
    method: "get",
    params: data
  })
}

//分页查询班级学生考试成绩
function getPageExamScore(data) {
  return request({
    url: "examscore/pageExamScore/",
    method: "get",
    params: data
  })
}

//分页查询班级学生考试成绩
function getExperimentProblemResultByproblem(data) {
  return request({
    url: "results/findResultByProblem",
    method: "get",
    params: data
  })
}


export {
  refreshExperimentScoreByteacherIdAndClassId,
  rebuildExperimentScoreByteacherIdAndClassId,
  getExperimentProblemResultByproblem,
  getClassByTeacherId,
  getExperimentBySchoolIdAndClassId,
  getStudentScoreByStudentId,
  getExamScoreByExperimentIdAndClassId,
  getExamScoreByExamIdAndClassId,
  getExamBySchoolIdAndClassId,
  getPageExamScore,
  getExperimentScoreByteacherIdAndClassId,
  getClassAndExamTitleByTeacherId,
  getExperimentProblemResultByUserIdAndExperiment,
  getFinallyCode
}