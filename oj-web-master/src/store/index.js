import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

 const store = new Vuex.Store({
  state: {
    role: null,   //用户角色
  },
  mutations: {
    setRole (state,role) {
      state.role=role;
    }
  }
})
export default store;