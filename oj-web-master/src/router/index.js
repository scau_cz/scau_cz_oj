import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import {
  Message
} from "element-ui"
Vue.use(Router)
const menulist = [
  {
    path: "/main/experimentModule/experiment",
    name: "experimentModule/experiment",
    component: () => import('@/page/experimentModule/experiment/index.vue'),
    children: null,
    meta: {
      title: "上机实验",
      icon: "hhtx-diannao",
      roles: ["student", "teacher", "schoolAdmin", "superAdmin"]
    }
  },{
    path: "/main/experimentModule/experiment/viewHistoryByProblem",
    name: "experimentModule/experiment/viewHistoryByProblem",
    component: () => import('@/page/experimentModule/experiment/viewHistoryByProblem.vue'),
    children: null,
    meta: {
      title: "提交列表",
      "hidden": true,
      "roles": [ "student","teacher", "schoolAdmin", "superAdmin"]
    }
  },
  {
    path: "/main/testModule/test",
    name: "testModule/test",
    component: () => import('@/page/testModule/test/index.vue'),
    children: null,
    meta: {
      title: "上机考试",
      "icon": "hhtx-ziyuan",
      "roles": ["student", "teacher", "schoolAdmin", "superAdmin"]
    }
  },
  {
    path: "/main/experimentModule/experimentLevelChoice",
    name: "experimentModule/experimentLevelChoice",
    component: () => import('@/page/experimentModule/experimentLevelChoice/index.vue'),
    children: null,
    meta: {
      title: "华山论剑",
      "icon": "hhtx-jijian",
      "roles": ["student", "teacher", "schoolAdmin", "superAdmin"]
    }
  },
  {
    path: "/main/honorModule/honor",
    name: "honorModule/honor",
    component: () => import('@/page/honorModule/honor/index.vue'),
    children: null,
    meta: {
      title: "封神榜",
      "icon": "hhtx-bangdan",
      "roles": ["student", "teacher", "schoolAdmin"]
    }
  },
  {
    path: "/main/experimentModule/history",
    name: "experimentModule/history",
    component: () => import('@/page/experimentModule/history/index.vue'),
    children: null,
    meta: {
      title: "查看历史提交",
      "icon": "hhtx-chakan",
      "roles": ["student", "teacher", "schoolAdmin", "superAdmin"]
    }
  },
  {
    path: "/main/testModule/studentViewResults",
    name: "testModule/studentViewResults",
    component: () => import('@/page/testModule/studentViewResults/index.vue'),
    children: null,
    meta: {
      title: "查看成绩（学生）",
      "hidden": true,
      "roles": ["student", "teacher", "schoolAdmin", "superAdmin"]
    }
  }, {
    path: "/main/experimentModule/experimentCompetition",
    name: "experimentModule/experimentCompetition",
    component: () => import('@/page/experimentModule/experimentCompetition/index.vue'),
    children: null,
    meta: {
      title: "华山论剑列表",
      "hidden": true,
      "roles": [ "student", "teacher", "schoolAdmin", "superAdmin"]
    }
  },{
    path: "/main/experimentModule/experimentDetail",
    name: "experimentModule/experimentDetail",
    component: () => import('@/page/experimentModule/experimentDetail/index.vue'),
    children: null,
    meta: {
      title: "题目详情",
      "hidden": true,
      "roles": ["student", "teacher", "schoolAdmin", "superAdmin"]
    }
  }, {
    path: "/main/experimentModule/history",
    name: "experimentModule/history",
    component: () => import('@/page/experimentModule/history/index.vue'),
    children: null,
    meta: {
      title: "历史提交",
      "hidden": true,
      "roles": ["student", "teacher", "schoolAdmin", "superAdmin"]
    }
  }, {
    path: "/main/testModule/viewDetail",
    name: "testModule/viewDetail",
    component: () => import('@/page/testModule/viewDetail/index.vue'),
    children: null,
    meta: {
      title: "实验成绩详情",
      "hidden": true,
      "roles": [ "teacher", "schoolAdmin", "superAdmin"]
    }
  }, {
    path: "/main/testModule/viewAnswer",
    name: "viewAnswer",
    component: () => import('@/page/testModule/viewAnswer/index.vue'),
    children: null,
    meta: {
      title: "查看答卷详情",
      "hidden": true,
      "roles": [ "student","teacher", "schoolAdmin", "superAdmin"]
    }
  },
  {
    path: "0",
    name: null,
    component: ()=>import('@/layout/submenuComponent.vue'),
    children: [
      {
        path: "/main/testModule/viewResults",

        name: "testModule/viewResults",
        component: () => import('@/page/testModule/viewResults/index.vue'),
        children: null,
        meta: {
          title: "查看成绩",
          "icon": "hhtx-chengjichaxun",
          "roles": ["teacher", "schoolAdmin", "superAdmin"]
        }
      },
      {
        path: "/main/testModule/rebuildResults",

        name: "testModule/rebuildResults",
        component: () => import('@/page/testModule/viewResults/rebuildResults.vue'),
        children: null,
        meta: {
          title: "重构/构建",
          "icon": "hhtx-chengjichaxun",
          "roles": ["teacher", "schoolAdmin", "superAdmin"]
        }
      }, {
        path: "/main/userModule/studentManage",
        name: "userModule/studentManage",
        component: () => import('@/page/userModule/studentManage/index.vue'),
        children: null,
        meta: {
          title: "学生管理",
          "icon": "hhtx-yewutubiao_xueshengchaxun",
          "roles": ["teacher"]
        }
      }, {
        path: "/main/userModule/classManage",
        name: "userModule/classManage",
        component: () => import('@/page/userModule/classManage/index.vue'),
        children: null,
        meta: {
          title: "班级管理",
          "icon": "hhtx-banjiguanli",
          "roles": ["teacher", "schoolAdmin", "superAdmin"]
        }
      },
    ],
    meta: {
      title: "授课班级",
      "roles": ["teacher", "schoolAdmin"]
    }
  },
  {
    path: "0",
    name: null,
    component: ()=>import('@/layout/submenuComponent.vue'),
    children: [
      {
        path: "/main/questionModule/questionList",
        name: "questionModule/questionList",
        component: () => import('@/page/questionModule/questionList/index.vue'),
        children: null,
        meta: {
          title: "题库列表",
          "icon": "hhtx-tiku",
          "roles": ["teacher", "schoolAdmin", "superAdmin"]
        }
      }, {
        path: "/main/questionModule/programCreate",
        name: "questionModule/programCreate",
        component: () => import('@/page/questionModule/programCreate/index.vue'),
        children: null,
        meta: {
          title: "编程题创建",
          "icon": "hhtx-Programming",
          "roles": ["teacher", "schoolAdmin", "superAdmin"]
        }
      }, {
        path: "/main/questionModule/choiceCreate",
        name: "questionModule/choiceCreate",
        component: () => import('@/page/questionModule/choiceCreate/index.vue'),
        children: null,
        meta: {
          title: "选择题创建",
          "icon": "hhtx-xuanze",
          "roles": ["teacher", "schoolAdmin", "superAdmin"]
        }
      }, {
        path: "/main/questionModule/judgeCreate",
        name: "questionModule/judgeCreate",
        component: () => import('@/page/questionModule/judgeCreate/index.vue'),
        children: null,
        meta: {
          title: "判断题创建",
          "icon": "hhtx-panduanti2",
          "roles": ["teacher", "schoolAdmin", "superAdmin"]
        }
      },
    ],
    meta: {
      title: "题库管理",
      "roles": ["teacher", "schoolAdmin", "superAdmin"]
    }
  },
  {
    path: "0",
    name: null,
    component: ()=>import('@/layout/submenuComponent.vue'),
    children: [
      {
        path: "/main/experimentModule/experimentList",
        name: "experimentModule/experimentList",
        component: () => import('@/page/experimentModule/experimentList/index.vue'),
        children: null,
        meta: {
          title: "实验列表",
          "icon": "hhtx-shiyanshuju",
          "roles": ["teacher", "schoolAdmin", "superAdmin"]
        }
      }, {
        path: "/main/experimentModule/experimentCreate",
        name: "experimentModule/experimentCreate",
        component: () => import('@/page/experimentModule/experimentCreate/index.vue'),
        children: null,
        meta: {
          title: "创建实验",
          "icon": "hhtx-chuangjianshili",
          "roles": ["teacher", "schoolAdmin", "superAdmin"]
        }
      },
      {
        path: "/main/experimentModule/experimentLink",
        name: "experimentModule/experimentLink",
        component: () => import('@/page/experimentModule/experimentLink/index.vue'),
        children: null,
        meta: {
          title: "关联实验",
          "icon": "hhtx-shiyanshuju",
          "roles": [ "schoolAdmin", "superAdmin"]
        }
      },
    ],
    meta: {
      title: "实验管理",
      "roles": ["teacher", "schoolAdmin", "superAdmin"]
    }
  },
   {
    path: "0",
    name: null,
    component: ()=>import('@/layout/submenuComponent.vue'),
    children: [
      {
        path: "/main/testModule/paperlist",
        name: "testModule/paperlist",
        component: () => import('@/page/testModule/paperlist/index.vue'),
        children: null,
        meta: {
          title: "试卷列表",
          "icon": "hhtx-shijuanguanli",
          "roles":["teacher","schoolAdmin","superAdmin"]
        }
      }, {
        path: "/main/testModule/paperView",
        name: "testModule/paperView",
        component: () => import('@/page/testModule/paperView/index.vue'),
        children: null,
        meta: {
          title: "试卷预览",
          "hidden": true,
          "icon": "hhtx-shijuanguanli",
          "roles":["teacher","schoolAdmin","superAdmin"]
        }
      }, {
        path: "/main/testModule/paperCreate",
        name: "testModule/paperCreate",
        component: () => import('@/page/testModule/paperCreate/index.vue'),
        children: null,
        meta: {
          title: "试卷创建",
          "icon": "hhtx-shijuan",
          "roles":["teacher","schoolAdmin","superAdmin"]
        }
      }, {
        path: "/main/testModule/taskbuild",
        name: "testModule/taskbuild",
        component: () => import('@/page/testModule/taskbuild/index.vue'),
        children: null,
        meta: {
          title: "创建考试",
          "icon": "hhtx-renwuchuangjian",
          "roles":["teacher","schoolAdmin","superAdmin"]
        }
      }, {
        path: "/main/testModule/taskList",
        name: "testModule/taskList",
        component: () => import('@/page/testModule/taskList/index.vue'),
        children: null,
        meta: {
          title: "考试列表",
          "icon": "hhtx-renwuliebiao",
          "roles":["teacher","schoolAdmin","superAdmin"]
        }
      },
    ],
    meta: {
      title: "考试管理",
      "roles": ["teacher", "schoolAdmin", "superAdmin"]
    }
  },
  {
    path: "0",
    name: null,
    component: ()=>import('@/layout/submenuComponent.vue'),
    children: [
      {
        path: "/main/userModule/schoolmanage",
        name: "userModule/schoolmanage",
        component: () => import('@/page/userModule/schoolmanage/index.vue'),
        children: null,
        meta: {
          title: "管理员管理",
          "icon": "hhtx-guanliyuan",
          "roles":["superAdmin"]
        }
      },{
        path: "/main/userModule/teacher",
        name: "userModule/teacher",
        component: () => import('@/page/userModule/teacher/index.vue'),
        children: null,
        meta: {
          title: "教师管理",
          "icon": "hhtx-xuexiao_jiaoshi",
          "roles":["schoolAdmin","superAdmin"]
        }
      },{
        path: "/main/userModule/student",
        name: "userModule/student",
        component: () => import('@/page/userModule/student/index.vue'),
        children: null,
        meta: {
          title: "学生管理",
          "icon": "hhtx-xuexiao_jiaoshi",
          "roles":["schoolAdmin"]
        }
      },{
        path: "/main/userModule/school",
        name: "userModule/school",
        component: () => import('@/page/userModule/school/index.vue'),
        children: null,
        meta: {
          title: "学校管理",
          "icon": "hhtx-xuexiao",
          "roles":["superAdmin","schoolAdmin"]
        }
      },
    ],
    meta: {
      "icon": "hhtx-config-account",
      title: "账号管理",
      "roles":["schoolAdmin","superAdmin"]
    }
  }
]
export const asynsRoutes = [
  {
    path: "/main",
    name: "main",
    component: () => import('@/layout/layout'),
    meta: { title: "首页" },
    redirect: '/main/home',
    children: [
      {
        path: '/main/home',
        name: 'home',
        component: ()=>import('@/page/systemModule/home/index.vue'),
        meta: {
          title: "首页",
          requireAuth: true,
          icon:"hhtx-ziyuan1",
          roles: ["student", "teacher", "schoolAdmin", "superAdmin"]
        },
      }
    ]
  },
  {
    path: '/login',
    name: "login",
    component: () => import('@/page/systemModule/login/index.vue'),
  },
  {
    path: '/testDetail',
    name: "testDetail",
    component: () => import('@/page/testModule/testDetail/index.vue'),
    meta: {
      title: "考试内容",
      requireAuth: true,
      keepAlive: true,
      isBack:true,
      roles: ["student", "teacher", "schoolAdmin", "superAdmin"]
    },
    children: [
      {
        path: '/testDetail/programDetail',
        name: 'programDetail',
        component: () => import('@/page/experimentModule/programDetail/index.vue'),
        meta: {
          title: "题目详情",
        },
      }
    ]
  },
  {
    path: "*",
    redirect: '/login',
  }];

  asynsRoutes[0].children=[... asynsRoutes[0].children,...menulist];

const router = new Router({
  routes: asynsRoutes
})
//全局路由守卫
router.beforeEach((to, from, next) => {
  //压缩二级目录
  handleKeepAlive(to)
  //刷新时保存params
  if (from.name == null) {
    Object.assign(to.params, JSON.parse(sessionStorage.getItem("routeParams")));
  }

  // next();,
  let role = store.state.role;
  if (role == null) {
    if (sessionStorage.getItem('store')) {
      store.replaceState(
        Object.assign(
          {},
          store.state,
          JSON.parse(sessionStorage.getItem('store'))
        )
      )
    }
    role = store.state.role
  }
  if (role == null && to.path != '/login') {
    Message({
      message: "登录信息丢失，请重新登录",
      type: 'warning',
      duration: 5 * 1000
    })
    next({ path: "/login" })
    return;
  }
  if (!to.meta.roles || to.path == '/login' || to.meta.roles.includes(role) || to.path == from.path) {
    next();
  } else {
    Message({
      message: "无权限",
      type: 'warning',
      duration: 5 * 1000
    })
    next(false);
  }
})
//将二级目录压缩，保证keep-alive能正常缓存三级路由
async function handleKeepAlive (to) {
  if (to.matched && to.matched.length > 2) {
    for (let i = 0; i < to.matched.length; i++) {
      const element = to.matched[i]
      if (element.components.default.name === 'submenuComponent') {
        to.matched.splice(i, 1)
        await handleKeepAlive(to)
      }
      // 如果没有按需加载完成则等待加载
      if (typeof element.components.default === 'function') {
        await element.components.default()
        await handleKeepAlive(to)
      }
    }
  }
}
export default router;
