export const columnList1 = [
  {
    label: "考试ID",
    type: "text",
    prop: "examId"
  },
  {
    label: "考试标题",
    type: "text",
    prop: "examName"
  },
  {
    label: "班级名称",
    type: "text",
    prop: "className"
  },
  {
    label: "学号",
    type: "text",
    prop: "userName"
  },
  {
    label: "姓名",
    type: "text",
    prop: "nickName"
  },
  {
    label: "单选题",
    type: "text",
    prop: "singleChoiceScore"
  },
  {
    label: "判断题",
    type: "text",
    prop: "trueFalseQuestionScore"
  },
  {
    label: "编程题",
    type: "text",
    prop: "programQuestionScore"
  },
  {
    label: "总成绩",
    type: "text",
    prop: "totalScore"
  }
];
export const columnList2 = [
  {
    label: "学号",
    type: "text",
    prop: "userId",
    minWidth: "120px"
  },
  {
    label: "姓名",
    type: "text",
    prop: "userName",
    minWidth: "120px"
  }
];
export const columnquestionList = [
  {
    label: "算法初试",
    type: "text",
    prop: "grade1"
  },
  {
    label: "实验三",
    type: "text",
    prop: "grade2"
  },
  {
    label: "实验四---循环结构",
    type: "text",
    prop: "grade3"
  },
  {
    label: "实验一 数据类型与运算符",
    type: "text",
    prop: "grade4"
  },
  {
    label: "实验二 流程控制",
    type: "text",
    prop: "grade5"
  },
  {
    label: "实验三 数组",
    type: "text",
    prop: "grade6"
  },
  {
    label: "实验四 方法",
    type: "text",
    prop: "grade7"
  },
  {
    label: "实验五 面向对象",
    type: "text",
    prop: "grade8"
  },
  {
    label: "实验六字符串与正则表达式",
    type: "text",
    prop: "grade9"
  },
];
