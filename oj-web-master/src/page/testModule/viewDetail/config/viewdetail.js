export const columnList = [
  {
    label: "题目编号",
    type: "text",
    prop: "problemId",
    minWidth: "120px"
  },
  {
    label: "题目名称",
    type: "text",
    prop: "problemTitle",
    minWidth: "120px"
  }
];
