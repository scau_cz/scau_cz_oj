// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import 'element-ui/lib/theme-chalk/index.css';
import ElementUI from 'element-ui';
import App from './App'
import router from './router'
import XLSX from 'xlsx'
import 'lib-flexible'
import '../static/flexible.js'
Vue.use(XLSX)
import store from './store/index.js'
Vue.use(ElementUI);
Vue.config.productionTip = false
/* eslint-disable no-new */

//使用自定义指令封装防抖函数
import { debounce } from '@/utils/common'
// 防抖自定义指令
Vue.directive('debounce', {
    bind(el, binding) {
        const executeFunction = debounce(binding.value, 1000)
        el.addEventListener('click', executeFunction)
    }
})

new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>'
})