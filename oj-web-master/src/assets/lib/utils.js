// 导出学生数组表格
import XLSX from "xlsx";
var dataConversionUtil = {};

// 把文件按照二进制进行读取
export function readFile(file) {
	return new Promise(resolve => {
		let reader = new FileReader();
		reader.readAsBinaryString(file);
		reader.onload = ev => {
			resolve(ev.target.result);
		};
	});
}
// 字段对应表
export let character = {
	// 班级classId
	classId:{
		text:"专业班级",
		type:"string"
	},
	// 性别gender
	gender:{
		text:"性别",
		type:"string"
	},
	// nickname用户名昵称
	nickname:{
		text:"昵称",
		type:"string"
	},
	// 密码password
	password:{
		text:"密码",
		type:"string"
	},
	// phone电话
	phone:{
		text:"电话",
		type:"string"
	},
	// role角色
	role:{
		text:"角色",
		type:"string"
	},
	// 学校
	schoolId:{
		text:"学校",
		type:"string"
	},
	// 状态
	status:{
		text:"状态",
		type:"string"
	},
	// 学生userId
	userId:{
		text:"Id",
		type:"string"
	},
	// username学号
	username:{
		text:"学号",
		type:"string"
	}
};
//导出学生数组表格
// 将数据转换成Excel，单个sheet保存
//fileName文件名,tableHeader表头,dataList表数据,sheet="sheet1"工作表默认名字是sheet1
dataConversionUtil.dataToExcel = function(fileName,tableHeader,dataList,sheet="sheet1"){
  // excel的表头和数据
  let aoa = [];
  // aoa的数据格式：[[],[],[],[]]   数组的第一个子数组可以指定为表头  根据情况而定
  aoa = tableHeader.concat(dataList);
 
  let workSheet = XLSX.utils.aoa_to_sheet(aoa);
  let workBook = XLSX.utils.book_new();
 
  // 把数据写到工作簿中
  XLSX.utils.book_append_sheet(workBook,workSheet,sheet)
  //如果一个工作工作簿中有多个工作表，可以修改参数类型并遍历添加，期中workBook是同一个，workSheet和sheet根据自己的需求添加，
  //比如在此处添加第二个工作表叫‘第二张表’，把数据封装好后，数据格式同上,假如数据叫workSheet2，执行下面代码，工作簿就会多一张工作表叫‘第二张表’
  //XLSX.utils.book_append_sheet(workBook,workSheet2,'第二张表')
 
  //保存
  XLSX.writeFile(workBook, fileName + ".xlsx")
}
 
export { dataConversionUtil }

