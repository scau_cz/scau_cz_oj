【华南农业大学 程序设计语言智能实验管理系统】共有三个仓库，前台、后台和测试用例。 前台 https://gitee.com/scau_cz/oj-web 后台 https://gitee.scau_cz/mloj-backend 测试用例 https://gitee.com/scau_cz/test-case-backend

目标是实现C，java,python等语言的实验课程管理，学生提交的代码可以在线评判，不需要教师手动修改。可以实现在线考试，系统直接评判，不需要教师自己批改学生的代码。