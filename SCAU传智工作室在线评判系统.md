
# SCAU传智工作室在线评判系统

**功能设计文档**

## 文档变更记录表

| 序号  | 变更日期      | 版本号 | 变更内容    | 负责人员    | 备注            |
| --- | --------- | --- | ------- | ------- | ------------- |
| 001 | 2023.7.19 | 1.0 | 前端架构    | 陈培锋     | 添加前端工具介绍      |
| 002 | 2023.7.20 | 1.1 | 需求分析    | 陈培锋     | 修改需求分析文档      |
| 003 | 2023.7.21 | 1.2 | 系统功能展示  | 吴泽铭     | 完善学生端功能演示     |
| 004 | 2023.7.25 | 1.3 | 前端映射    | 陈培锋     | 完善前端各个页面的映射内容 |
| 005 | 2023.8.1  | 1.4 | 后端技术栈介绍 | 陈泽楷、罗家琪 | 完善后端技术栈介绍部分内容 |
|     |           |     |         |         |               |

目录

[SCAU传智工作室在线评判系统 1](#_Toc23324)

[文档变更记录表 1](#文档变更记录表)

[第一章 引言 4](#_Toc9183)

[1.1.1. 编写目的 4](#_Toc5430)

[1.1.2. 读者对象 4](#_Toc9360)

[1.1.3. 参考资料 4](#_Toc6164)

[1.1.4. 系统概述 4](#_Toc13759)

[1.1.5. 系统用户 5](#系统用户)

[第二章 系统需求分析 5](#系统需求分析)

[2.1.系统需求概述： 5](#_Toc28370)

[2.1.1. 系统需求概述 6](#_Toc2216)

[2.1.2. 系统需求详细分析 6](#_Toc18024)

[第三章 系统部署概述 8](#系统部署概述)

[1. 数据库 8](#_Toc26199)

[2. 模块 8](#_Toc9339)

[3. 微服务架构 9](#_Toc22283)

[4. 在线评判流程实现 10](#_Toc29399)

[5. 系统部署 10](#_Toc21932)

[第四章 系统功能展示 11](#系统功能展示-1)

[第五章 前端架构以及开发事项 26](#_Toc9373)

[5.1. 前端技术介绍 26](#_Toc40)

[5.1.1.1. Web概述(HTML+CSS+JavaScript) 26](#web概述htmlcssjavascript)

[5.1.1.2. Vue框架 27](#vue框架)

[5.1.1.3. 前端打包工具 28](#前端打包工具)

[5.2. 前端映射 29](#_Toc6406)

[5.2.1. 上机实验界面 29](#上机实验界面)

[5.2.2. 提交列表界面 29](#提交列表界面)

[5.2.3. 上机考试界面 29](#上机考试界面)

[5.2.4. 华山论剑界面 29](#华山论剑界面)

[5.2.5. 封神榜界面 30](#封神榜界面)

[5.2.6. 查看历史提交界面 30](#查看历史提交界面)

[5.2.7. 查看成绩（学生）界面 30](#查看成绩学生界面)

[5.2.8. 华山论剑列表页面 30](#华山论剑列表页面)

[5.2.9. 实验题目详情页面 31](#实验题目详情页面)

[5.2.10. 历史提交页面 31](#历史提交页面)

[5.2.11. 查看答卷详情界面 31](#查看答卷详情界面)

[5.2.12. 查看成绩界面（教师，管理员） 32](#查看成绩界面教师管理员)

[5.2.13. 重构/构建界面 32](#重构构建界面)

[5.2.14. 学生管理界面 32](#学生管理界面)

[5.2.15. 班级管理界面 33](#班级管理界面)

[5.2.16. 题库列表界面 33](#题库列表界面)

[5.2.17. 编程题创建界面 33](#编程题创建界面)

[5.2.18. 选择题创建界面 33](#选择题创建界面)

[5.2.19. 判断题创建界面 33](#判断题创建界面)

[5.2.20. 实验列表页面（教师） 34](#实验列表页面教师)

[5.2.21. 创建实验界面（教师） 34](#创建实验界面教师)

[5.2.22. 关联实验页面（教师，管理员） 34](#关联实验页面教师管理员)

[5.2.23. 试卷列表页面（教师） 34](#试卷列表页面教师)

[5.2.24. 试卷预览页面（教师） 35](#试卷预览页面教师)

[5.2.25. 试卷创建界面 35](#试卷创建界面)

[5.2.26. 创建考试界面 35](#创建考试界面)

[5.2.27. 考试列表界面（教师） 35](#考试列表界面教师)

[5.2.28. 管理员管理页面（超级管理员） 36](#管理员管理页面超级管理员)

[5.2.29. 教师管理页面（管理员） 36](#教师管理页面管理员)

[5.2.30. 学生管理页面（教师） 36](#学生管理页面教师)

[5.2.31. 学校管理页面（超级管理员） 37](#学校管理页面超级管理员)

[5.2.32. 首页 37](#首页)

[5.2.33. 考试内容界面 37](#考试内容界面)

[5.2.34. 题目详情界面 37](#题目详情界面)

[第六章 后端架构及开发事项 38](#_Toc10903)

[6.1.1. 后端基础技术介绍 38](#_Toc440)

[6.1.1.1. Java语言 38](#java语言)

[6.1.1.2. MySQL 39](#mysql)

[6.1.1.3. Tomcat 41](#tomcat)

[6.1.1.4. Docker 42](#docker)

[6.1.1.5. Springboot 43](#springboot)

[6.1.2. 后端模块开发流程 44](#_Toc9917)

[第七章 数据库设计 44](#_Toc22168)

## 第一章 **引言**

### 编写目的

本用户手册根据对系统的理解，参照需求规格说明书、详细设计、概要设计等资料进行编写，对系统的运行环境、功能、操作方式等进行详细说明，力求内容准确、无歧义。

### 读者对象

1.管理该系统的系统管理员

2.系统测试人员及维护人员

3.使用该系统的相关课程老师

4.使用该系统完成课程任务的同学

### 参考资料

李西明老师B站OJ在线评判系统使用教程：

https://space.bilibili.com/334988827/channel/seriesdetail?sid=2129091

### 系统概述

本系统旨在为想要提升自己编程能力、转变自身的编程思维的计算机从事人员和即将步入企业工作的应聘者提供一个开放的平台，结合现在流行的Spring Boot框架、Vue.js框架和微服务的设计思想，设计出一个容易拓展的、提供了基础评判服务的平台。利于Docker容器化的特性，将应用部署在Docker上面，打包成一个镜像，能够方便快捷的对这些系统进行弹性扩容，横向增加系统的并发性能；并且将传统的系统分层模块化，模块之间可以打包成jar包进行依赖，不需要的jar包可进行卸载，为容器镜像瘦身，部署更便捷。

通过这个平台，用户能够快速提升自己的编程能力，并且从其中发现编程的乐趣，增加自己对编程的兴趣，并且能够在其中发掘自己所擅长的领域，向算法领域进行钻研；并且能够给从事计算机领域、有意愿到企业中去进行计算机系统开发的人，提供一个能够练习的平台，从而降低了入门门槛。

在线评判系统的应用，可以为高校学生和教师提供了便利。该系统的应用把教师从大量重复性劳动中解放出来，从而有更多精力用于对学生进行针对性地指导，提高了学生分析问题、解决问题的能力，提高了教学质量。

### 系统用户

**超级管理员：**可以创建学校和指定校级管理员，拥有后台管理的各项权限。也能够对各类账号进行管理。

**校管理员：**校级管理员具有对本校教师账号的管理权限以及管理题目、实验和考试等方面的功能。

**普通教师：**负责创建班级、导入所教班级的学生名单；获得授权可以创建题目、实验、试卷或考试。

**学生：**可以查看实验和提交实验、参加考试、查看完成情况或成绩；也可以查看系统公开题目的列表。

# 系统需求分析

**2.1.系统需求概述：**

在线评判系统是一款用于评判各种项目的在线系统，可以实现多用户、多角色、多项目、多维度的评判功能。

本系统的基本需求是提供一个用户的管理系统，能够保存用户的提交信息，并且管理一个题库，用户能够从题库中查询到所需要的题目，并且通过这些题目进行答题，并且保存答题记录和判题结果，能够让用户方便的进行查询。

本系统将使用的用户分为管理员、教师、学生/普通用户，不同的用户对应使用系统的不同功能。

该系统旨在简化评判流程，提高评判效率，降低评判成本，提供更为良好的用户交互页面。

### 系统需求概述

在线评判系统是一款用于评判各种项目的在线系统，可以实现多用户、多角色、多项目、多维度的评判功能。核心场景就是对代码的编译、运行、比对测试用例(特定输入检测输出)和反馈。每次提交题目代码，需要新建进程而且规定进程权限、监控进程运行，然后获取进程的一系列输出和测试用例中预期的结果相比对。

本系统的基本需求是提供一个用户的管理系统，能够保存用户的提交信息，并且管理一个题库，用户能够从题库中查询到所需要的题目，并且通过这些题目进行答题，并且保存答题记录和判题结果，能够让用户方便的进行查询。本系统将使用的用户分为管理员、教师、学生/普通用户，不同的用户对应使用系统的不同功能。

### 系统需求详细分析

#### 身份认证系统

不论是哪个身份的用户，都需要进行身份的认证(登录)这一过程。认证系统给用户进行基于用户为单位的信息管理，包括但不限于以下功能：账号录入、登录、个人信息修改等。

#### 题目查询系统

题目查阅系统让用户可以根据自己的需要，进行题目的查阅。题目可以根据知识点、题型分类，同时支持按名字查询。查阅到具体题目的时候，可以点击查看题目的描述、输入描述、输出描述、提示、时间与空间限制等信息，并且可以提交代码作答。

此处衍生出题库查阅、实验查阅、考试查阅等功能，其中题库只有教师能够查阅。

#### 代码提交评判系统

代码提交系统主要是提供给用户进行代码提交、返回判定结果的功能。提交的代码会到后台服务器中运行，实时(在几秒内)返回结果。并且提供代码的语法高亮功能。用户提交代码的评判结果有可能有以下几种情况：通过、答案错误、超时、内存溢出、编译错误。

#### 历史提交记录查询

用户可以查询以往自己提交的代码和评判结果。

#### 用户管理系统

用户主要有三种身份：管理员、学校教师、普通用户。管理员主要是可以维护所有教师的账号信息，学校教师可以录入学生账号、管理班级，而每个用户都可以使用普通用户的功能。

#### 题库管理系统

题库管理系统主要在于题目的上传、删除、修改等，管理员与教师可以录入题目。一个题目的数据包含如下：题目标题、题目描述、题目输入输出描述、时间限制、空间限制。

#### 实验/考试系统

教师能将题库中的题目添加到某个实验/考试之中，然后下发到某个班级的学生。实验一般限定时间单位为日，而考试一般限定时间范围更小。

# 系统部署概述

**1. 数据库**

系统涉及的实体有学校、班级、普通用户、题目、考试和实验等，它们的实体关系（E-R）如图所示。

![](media/75fd4679d0f66bac0e74fcea4dc3013a.png)

**2. 模块**

为了实现在线评判系统和实验管理的功能，划分为不同的模块，如图所示。

![](media/d0af23b38db22594b36d214ac39c5637.png)

**3. 微服务架构**

整个系统运行在Virtual Private Cloud（VPC）上，除了互联网（Internet）、防火墙（firewall）、数据库（MySQL）和云文件存储（Cloud File Storage）之外其它都是微服务，每个微服务都构成各自的集群，微服务之间可以自由通信。系统微服务架构如图所示。

![](media/e45e9c903c849e17a9f8a743bbe1c769.png)

各部件的作用：

（1）Nginx实现动静分离和反向代理、以及解决跨域无法访问的问题。

（2）RocketMQ是Spring AMQP(Advanced Message Queuing Protocol)的默认通信中间件，应用程序对应用程序的异步通信。其中NameServer是微服务注册中心，拥有完整的路由信息，支持Broker的动态注册和发现，维护Topic和Broker之间的关系，以及和其他NameServer之间相互通信。Broker负责把消息队列中的评判消息主动推送到消费者。

（3）CFS（Cloud File Storage, 云文件存储）用来存储所有的微服务的配置文件和测试用例文件，最终挂载到相应的微服务容器上。这样有利于解决文件冗余和不一致性的问题。

（4）MySQL实现持久化数据，采用读写分离的主从结构，主MySQL负责写，从MySQL负责读（查询），可以较好地提高系统的响应速度。

（5）Redis缓存Docker容器目录。从缓存中恢复判题所需的环境，无需再次下载所需组件进行构建，从而提高判题效率。

（6）JudgeServer微服务提供API供后台管理微服务调用，完成评判用户的程序并把评判结果返回。

（7）Testcase微服务面向管理员，实现题目管理。

（8）SpringBoot Application采用SpringBoot框架实现，属于后台管理微服务，是系统的中心枢纽。Producer注册到NameServer上获取Broker的路由信息，Consumer则从Broker上订阅消息。当有消息时，Broker就会将它推送到Consumer，那么Consumer调用评判微服务进行评判，然后将结果返回给Other Modules。

**4. 在线评判流程实现**

在线评判是整个系统的核心功能，流程比较复杂，以时序图形式表示，如图所示。其中ID（提交编号）是由服务端根据本次提交代码、提交用户、时间和题号保存到数据库生成的。提交用户根据返回的ID查询评判结果。

![](media/0fa661be126421366b593016865f2c9f.png)

**5. 系统部署**

①首先需要对后端管理和测试用例服务的redis、rocketMQ、JudgeServer、文件服务等的地址与端口根据服务器以及数据库的信息进行修改。

②Nginx。需要对Nginx的配置文件进行修改。将listen修改为对应访问页面的端口，server_name修改为服务器的地址，root修改为前端静态页面对应的目录，并将proxy_pass反向代理地址设置为 <http://backend:8080/>; 启动Nginx。

③后台管理。将修改好地址的后端项目进行打包，在服务器上通过nohup java -jar ml-online-judge-web-0.0.1-SNAPSHOT.jar & 启动服务。

④评判服务。根据<https://github.com/QingdaoU/OnlineJudgeDeploy中的docker-compose.yml>文件，对文件中的后端与评判服务的地址、端口与Token进行相应的修改，使用docker-compose up -d命令自动部署评判服务。

⑤测试用例服务。将修改好的测试用例项目进行打包，在服务器上通过nohup java -jar ml-online-judge-judge-0.0.1-SNAPSHOT.jar & 启动服务。启动服务后，测试用例的文件存放在 …/OnlineJudgeDeploy/data/backend/test_case 当中。

⑥Redis。修改redis配置文件，将bind 127.0.0.1一行注释，并将protected-mode设置为no，并设置正确对应的redis端口，启动redis。

⑦RocketMQ。修改broker.conf，在其中加入brokerIP1 = 服务器IP(xxx.xxx.xxx.xxx)。

1\. nohup sh mqnamesrv & \#启动nameserver

2\. nohup sh mqbroker -n IPaddress:9876 autoCreateTopicEnable=true -c /usr/local/rocketmq/conf/broker.conf &

命令中的IPaddress（端口不需要更改）修改成服务器的ip地址，以及路径需要修改为你的rocketmq所在的路径。

3\. sh mqadmin updateTopic -n IPaddress:9876 -b 127.0.0.1:10909 -t Judge

命令中的第一个IPaddress（端口不需要更改）修改成服务器的ip地址

# 系统功能展示

**登录界面：**

用户需要在登录界面处输入自己的所属学校、账号和密码以登录系统。账号类别可以分成四种：普通学生、教师、管理员、超级管理员。

![](media/05c1fe08f89788fa74769a6809cbc497.png)

**超级管理员端功能：**

**用户选择界面：**

进入系统后，用户可以在界面左侧的菜单栏中选择相应功能，目前来说可以分成题库管理模块、实验管理模块、考试管理模块、还有账号管理模块。

登录系统后，学生可以自行选择进行考试、查看历史提交、还有参加竞赛。如果用户是教师、管理员等身份，还可以进行其他的一系列操作。

![](media/f2c5034d49a13e313fb39395988d8c75.png)

**题库管理模块：**

题库管理模块的主要内容是添加、下载、预览、删除题目，对题库进行一系列的管理操作。

**题库列表：**

用户可以查看当前列表中存在的习题，并进入测试

![](media/ecb8eb14ed13dc1d279f0fca59d46ce1.png)

**创建编程题：**

用户可以通过这个功能创建编程题

![](media/26362862e200e77e3425ac3dd7fda4cd.png)

**创建选择题：**

![](media/fb3ad85b21dc2a296b1a423e2c62f7c6.png)

用户可以通过这个功能创建选择题

**创建判断题：**

![](media/e795834bb52b381a691881ee36eb1b7f.png)

用户可以通过这个功能创建判断题

**实验管理：**

实验管理模块，可以根据学校、班级的区别进行个性化的习题操作，教师可以通过实验管理模块安排学生具体完成某些习题，并设置相应的截止时间。也可以将其他学校的实验关联到本校。

**实验列表：**

![](media/9de5f158e29c61a39ad7a3ee11a8fb02.png)

用户可以通过这个功能查看、编辑已经发布的实验

**创建实验：**

![](media/4011b0f87c1b11b1f3785514024acba8.png)**.**

用户可以通过这个功能实现对实验的创建，可以设置可选题和必做题、并对实验进行备注

**关联实验：**

![](media/90c08a98a6255467b8290e7f57abeb33.png)

用户可以通过这个功能来实现对实验的关联。可以将当前已经发布的实验关联到本校，来进行题目的共享。

**考试管理：**

考试管理模块主要可以分成创建、浏览两个模块。学生、教师可以查看查看当前已经发布的考试试卷。教师可以进行试卷的创建，进行一系列包括但不限于：创建考试、设置考试班级、管理考试时间、添加考试题目的操作

**试卷列表：**

![](media/2d9215051269f822c3b1ee2b1c74a469.png)

用户可以通过这个功能查看、修改、删除实验

**试卷创建：**

![](media/943fb6e441819f50358499dde6c262b0.png)

用户可以通过这个功能创建一个新的实验，可以设置实验名称、题目以及总分

**创建考试：**

![](media/cece5310faf1be5db20b2fa2f4bd661d.png)

用户可以通过这个功能来发布一个实验，可以设置考试开始、结束时间和参加考试的班级

**考试列表：**

![](media/7cdf7eae2f5377c71e5eee763e723f1e.png)

用户可以通过这个功能来查看已经发布的考试

**账号管理：**

账号管理模块主要用于对管理员、对教师和学校的管理。可以新增管理员、删除管理员、以及编辑管理员的相关信息。

除此以外，还可以对学校、教师等对象进行相同的修改操作。

**管理员管理：**

![](media/4dc1d59f5dcb89b8f8d3b88a0ce2e9e3.png)

用户可以通过这个功能来对管理员进行新增、修改、删除的操作

**教师管理：**

![](media/ddf785b1afca1e149f4d387cfe1da94e.png)

用户可以通过这个功能来对教师进行新增、修改、删除的操作

**学校管理：**

![](media/436f32d1e241a00a8817b2f8ec2f9056.png)

用户可以通过这个功能来对学校进行新增、修改、删除的操作

**学生端功能：**

**登录界面：**

**![](media/32b7181a6ffcd0ec223883d726ba4423.png)**

学生通过输入自己的账号密码进行登录，进入到学生端主界面。

![](media/7a49b8536dd6fa7926b3e9e5328879bc.png)

进入主界面后，左侧一栏为学生端的主要功能。分别为上机实验、上机考试、华山论剑、封神榜和查看历史提交功能。主界面的右上角为退出账号操作。上面还展示了用户的个人等级，此外，用户还可以进行修改密码操作。

**上机实验：**

**![](media/f3e77293d2578a1d7ab13c8f3b1171cd.png)**

![](media/79a5fc5d751f5adcc0fac0b932e67448.png)

学生可以通过此功能查看到实验相关信息，包括题目id、题目名称、提交列表和作答情况等操作。

![](media/115c6fce3ce1f92c079eeb06d6d26280.png)

题目详情以及限制条件。

![](media/fb174ed5e4d25ad095c5e6c5c7413783.png)

学生可以选择适合自己的编程语言、也可切换适合自己的背景主题，在完成习题后，可单击“提交代码”完成提交。

![](media/45cff5eabf0fe808e9d439d24dd5de04.png)

**上机考试：**

**![](media/5bd5faf2cbb82d9bcac198762e5a53e1.png)**

学生可以通过此界面查看考试相关信息包括考试id、试卷id、考试的起止时间等。通过此功能学生可以参加考试。

![](media/514d8a95e6b0b88524d96fe8c5f9b27e.png)

考试详情界面。我们可以选择左侧对应题目题号来快速进行题目切换，也可以按下一页进行顺序切换题目。完成考试后点击“提交试卷”。

**![](media/b08b745cdf518331663d74f0c44218ea.png)**

交卷成功界面。

**![](media/6d0016d4c55af218394b82227ad0e072.png)**

交卷失败界面（提示：重复提交已提交的答卷）。

**华山论剑：**

**![](media/77dbaf434d3296644f10246e4b70a907.png)**

学生可以通过这个功能来完成额外的习题，习题按从左到右难度依次递增划分，答对题多者分数高，用户还可以查看自己在排行榜（即封神榜）中的排名。

![](media/5deea9d4fc0bdc1bb43d160380a99ec0.png)

![](media/e500751ab46ce0cca15f22dfa1875eed.png)

封神榜：

![](media/c005d429d32c2d41428addd6943f5669.png)

封神榜是一种排行榜，分数越高的学生，在封神榜上的排名就越前。用户据此可以估算自己的编程水平。

**查看历史提交：**

![](media/0d5fe60e677fe8a381bf38badffeb593.png)

学生可以通过这个功能来查看自己已经提交过的历史信息。可以通过修改时间来获取对应时间内自己的历史提交记录。

# 前端架构以及开发事项

### 前端技术介绍

#### Web概述(HTML+CSS+JavaScript)

Web即www(World Wide Web,万维网)的Web。Web应用程序主要是指通过网络能直接访问的应用程序。在Web中，每一个系统能够通过一个“统一资源定位符”(URL)标识来访问，而每一项资源又能够通过“统一资源标识符”来唯一标识。

Web这几十年的迅猛发展，让越来越多、越来越丰富的内容呈现于网络之上。一个轻量的浏览器，就可以访问来自世界各地的网站。在Web1.0时代，人们通常只能访问一些以文字为主要内容的门户网站，如微博、论坛等。随着网络、硬件设备条件的进步，网站所能承载的内容也越来越丰富。诸如FaceBook、知乎、哔哩哔哩、淘宝等众多交互性、图片和功能性网站的出现，让人们足不出户便可观看视频、听音乐、看商品。在不久的未来，Web肯定会进一步发展。

而HTML在这其中便扮演着基石的作用。HTML(Hyper Text Markup Language,超文本标记语言)是一种标记语言，它包含一系列标签，用来将网络上的文档形式统一、规定。直至今天，HTML5为最新的HTML标准，它让网页可以呈现更丰富的图像、动画、音频和视频。

如果说HTML是网页的基础，规定了网页的骨架，那么CSS(Cascading Style Sheets,层叠样式表)就是网页的外观。它是一个具有“层叠”概念的语言。所谓层叠，即支持样式根据优先级进行覆盖，不同来源、不同优先级的样式，如用户代理、作者、读者的样式等，最终会被覆盖成一份计算样式呈现在用户面前。相较于传统HTML的基础样式，CSS能够对网页中对象的位置排版具有像素级的精度，支持绝大多数字体字号。优秀的表现能力让他成为目前文本展示最优秀的设计语言。CSS能够根据不同使用者的理解能力，简化或者优化写法，使页面有较强的易读性。

最后，Javascript控制着网页的行为。Javascript前身为LiveScript,是一种单线程的客户端脚本语言，也是使用的最广泛的Web客户端开发语言。它是一门弱类型、单线程的解释型语言。其语言核心规范为ECMAScript,2015年以前，最常用的规范还是ES5,2015年以后规范一年一更新，此后以年份来称呼，如ES2015(也称为ES6)。

#### Vue框架

![](media/615a35aa46ea2d588848ee237377d896.png)

在网页内容日渐丰富的今天，Web应用的复杂度也直线上升。JavaScript当年的设计者恐怕也没想到这门语言会有这么热门的今天。这门语言的缺点在面临复杂逻辑交互时越来越凸显。

在基于原生JavaScript上，尤雨溪(Evan You)写了一款数据驱动思想的前端框架。一经发布，瞬间火爆，时至今日Github上Vue项目的star已经有了19 万，已经是全世界三大前端框架之一。

以往的Javascript要操作某个页面上的数据改变，就需要通过DOM(Document ObjectModel,文档对象模型)来修改一个文档的内容和结构。当网页被加载的时候，浏览器会生成一个树形结构，树的每一个节点都是一个DOM 元素，所以这棵树也被称为DOM树，它用来层次化的描述文档。反复操作DOM 不仅会导致逻辑混乱，还会影响网页的性能。

而本系统选的技术框架：Vue,它是一个用于构建用户界面的渐进式框架。所谓渐进式，就是实际上Vue还有提供目前前端开发要解决的痛点的其他技术，但并不强制使用，开发者可以根据自己的需要增加使用。Vue的核心库只关注视图层，开发者可以避免操作DOM节点，它遵循“数据驱动”的思想。利用简洁的模板语法，声明HTML中所需要的数据，开发者只需要在JavaScript部分操作数据，视图就会自动的更新。在这个过程中Vue内部帮开发者做了模板编译、数据绑定视图、操作DOM等工作。

Vue的出现，降低了前端开发的难度，也易于理解，且有着良好的兼容性。而对于Vue来说，在兼容性方面也比较友好，Vue除了对IE的支持有限，不支持IE8及以下版本外，对于其他浏览器，只要能够兼容ECMAScript5特性，都可以正常运行基于Vue.js的项目(刘亚茹，2022)。因此选为本系统的技术框架。

#### 前端打包工具

在现代前端开发之中，我们常常用到很多新技术来开发，例如typescript、Sass(css的预处理器)等，而他们都是无法直接在浏览器上运行的，需要使用官方的工具进行编译处理、转化成传统可以在浏览器上运行的html、css、js 文件。频繁的手动使用工具进行编译，不仅效率低，还容易出错。而且长期以来前端开发经常需要手动刷新页面以观察代码的更改效果，非常的影响开发体验，于是，许多前端打包工具诞生了。

目前知名的前端打包工具有Webpack、Rollup、Vite等。其中Webpack由于时间的积淀，它的社区生态是最丰富完善的，也许Webpack不一定是最好的，但基本上开发人员的需求在里面都能找到对应的工具。而相对简洁清爽的是Rollup.js,由于原理的不同，Rollup.js生成的最终代码往往都比较清爽、冗余少。最后是Vite.js,以极速的编译被誉为下一代打包工具。

而本论文的在线评判系统，选用了Webpack作为项目的打包工具，它的功能如下：

1. 代码转换。可以将Scss文件编译成css、将typescript文件编译成JavaScript文件 等。
2. 文件优化：压缩JS、CSS、HTML代码，压缩图片。
3. 代码分割：提取多个页面的公共代码，异步加载资源。
4. 模块合并：构建时将模块分类合并成一个文件。
5. 热重载：监听本地代码的变化，自动重新构建、刷新，便于开发。
6. 自动发布：更新代码后，自动构建出线上发布代码并传输给发布系统。

Webpack工作时，会首先读取项目根目录下的webpack.config.js来获取配置项(当然，从4.0.0版本开始可以不用该文件配置，使用默认配置),配置项是一个对象，使用module.export导出，通常可以指定打包的入口文件

(entry)、打包的输出相关(output),如输出文件名、位置等，以及指定文件转换使用的loader。获取配置项后，会对每一个入口进行如下的工作：从入口的模块开始递归解析该模块依赖的所有模块，每找到一个模块，会根据配置的loader进行文件转换(webpack原本只支持对js和json的解析，其他文件需要对应的loader提供支持),然后这些模块以入口为分组，每个入口及其所有依赖的模块会被分到一个组(chunk),而且每一个入口对应一个依赖图。最后将所有chunk转换成文件输出。这是一个正常的webpack工作流程，除此之外，还可以在webpack正常工作流程中使用插件，注入钩子在特定工作流程中对打包结果进行干预。

### 前端映射

### 上机实验界面

1. Views：/main/experimentModule/experiment
   1. Component：/page/experimentModule/experiment/index.vue
   2. Api：/api/experiment.js

### 提交列表界面

1. Views：/main/experimentModule/experiment/viewHistoryByProblem
   1. Component：/page/experimentModule/experiment/viewHistoryByProblem.vue
   2. Api：
      1. /api/solutions
      2. /api/viewResults.js

### 上机考试界面

1. Views：/main/testModule/test
   1. Component：/page/testModule/test/index.vue
   2. Api：/api/task.js

### 华山论剑界面

1. Views：/main/experimentModule/experimentLevelChoice
   1. Component：/page/experimentModule/experimentLevelChoice/index.vue
   2. Api：/api/question.js

### 封神榜界面

1. Views：/main/honorModule/honor
   1. Component：/page/honorModule/honor/index.vue
   2. Api：/api/honor

### 查看历史提交界面

1. Views：/main/experimentModule/history
   1. Component：/page/experimentModule/history/index.vue
   2. Api：
      1. /api/login
      2. /api/solutions

### 查看成绩（学生）界面

1. Views：/main/testModule/studentViewResults
   1. Component：/page/testModule/studentViewResults/index.vue
   2. Api：/api/viewResults

### 华山论剑列表页面

1. Views：/main/experimentModule/experimentCompetition
   1. Component：
      1. /page/experimentModule/experimentCompetition/index.vue
      2. /components/programDetail.vue
   2. Api：/api/question.js

### 实验题目详情页面

1. Views：/main/experimentModule/experimentDetail
   1. Component：
      1. /page/experimentModule/experimentDetail/index.vue
      2. /components/programDetail.vue
      3. /components/codeMirror.vue
      4. /components/codeResult.vue
   2. Api：
      1. /api/question.js
      2. /api/solutions.js

### 历史提交页面

1. Views：/main/testModule/viewDetail
   1. Component：/page/experimentModule/history/index.vue
   2. Api：
      1. /api/solutions
      2. /api/login

### 查看答卷详情界面

1. Views：/main/testModule/viewAnswer
   1. Component：/page/testModule/viewAnswer/index.vue
   2. Api：/api/task

### 查看成绩界面（教师，管理员）

1. Views：/main/testModule/viewResults
   1. Component：/page/testModule/viewResults/index.vue
   2. Api：
      1. /api/class
      2. /api/viewResults

### 重构/构建界面

1. Views：/main/testModule/rebuildResults
   1. Component：/page/testModule/viewResults/rebuildResults.vue
   2. Api：
      1. /api/class
      2. /api/viewResults

### 学生管理界面

1. Views：/main/userModule/studentManage
   1. Component：/page/userModule/studentManage/index.vue
   2. Api：
      1. /api/studentManage
      2. /api/class

### 班级管理界面

1. Views：/main/userModule/classManage
   1. Component：/page/userModule/classManage/index.vue
   2. Api：/api/class

### 题库列表界面

1. Views：/main/questionModule/questionList
   1. Component：/page/questionModule/questionList/index.vue
   2. Api：/api/question.js

### 编程题创建界面

1. Views：/main/questionModule/programCreate
   1. Component：/page/questionModule/programCreate/index.vue
   2. Api：/api/question.js

### 选择题创建界面

1. Views:/main/questionModule/choiceCreate
   1. Component:/page/questionModule/choiceCreate/index.vue
   2. Api：/api/question

### 判断题创建界面

1. Views：/main/questionModule/judgeCreate
   1. Component：/page/questionModule/judgeCreate/index.vue
   2. Api：/api/question

### 实验列表页面（教师）

1. Views：/main/experimentModule/experimentList
   1. Component：/page/experimentModule/experimentList/index.vue
   2. Api：/api/experiment.js

### 创建实验界面（教师）

1. Views：/main/experimentModule/experimentCreate
   1. Component：/page/experimentModule/experimentCreate/index.vue
   2. Api：/api/question.js

### 关联实验页面（教师，管理员）

1. Views：/main/experimentModule/experimentLink
   1. Component：/page/experimentModule/experimentLink/index.vue
   2. Api：/api/experiment.js

### 试卷列表页面（教师）

1. Views：/main/testModule/paperlist
   1. Component：/page/testModule/paperlist/index.vue
   2. Api：/api/testpaper.js

### 试卷预览页面（教师）

1. Views：/main/testModule/paperView
   1. Component：/page/testModule/paperView/index.vue
   2. Api：/api/task.js

### 试卷创建界面

1. Views：/main/testModule/paperCreate
   1. Component：/page/testModule/paperCreate/index.vue
   2. Api：
      1. /api/question.js
      2. /api/testpaper.js

### 创建考试界面

1. Views：/main/testModule/taskbuild
   1. Component：/page/testModule/taskbuild/index.vue
   2. Api：
      1. /api/task.js
      2. /api/testpaper.js
      3. /api/class.js

### 考试列表界面（教师）

1. Views：/main/testModule/taskList
   1. Component：/page/testModule/taskList/index.vue
   2. Api：/api/task.js

### 管理员管理页面（超级管理员）

1. Views：/main/userModule/schoolmanage
   1. Component：/page/userModule/schoolmanage/index.vue
   2. Api：
      1. /api/teacher
      2. /api/school

### 教师管理页面（管理员）

1. Views：/main/userModule/teacher
   1. Component：/page/userModule/teacher/index.vue
   2. Api：
      1. /api/teacher
      2. /api/school

### 学生管理页面（教师）

1. Views：/main/userModule/student
   1. Component：/page/userModule/student/index.vue
   2. Api：
      1. /api/studentManage
      2. /api/class

### 学校管理页面（超级管理员）

1. Views：/main/userModule/school
   1. Component：/page/userModule/school/index.vue
   2. Api：/api/school

### 首页

1. Views：/main/home
   1. Component：/page/systemModule/home/index.vue
   2. Api：/api/school

### 考试内容界面

1. Views：/testDetail
   1. Component：/page/testModule/testDetail/index.vue.
   2. Api：/api/task.js

### 题目详情界面

1. Views：/testDetail/programDetail
   1. Component：
      1. /page/experimentModule/programDetail/index.vue
      2. /components/programDetail.vue
      3. /components/codeMirror.vue
      4. /components/codeResult.vue
   2. Api：
      1. /api/question.js
      2. /api/solutions.js

# 后端架构及开发事项

### 后端基础技术介绍

#### Java语言

Java是一门广泛应用的编程语言，其特点涵盖了简单易学、面向对象、平台无关性、高性能、自动内存管理、安全性、多线程支持、大型标准库、开源生态系统、异常处理、动态扩展性、注释支持、高级特性、面向网络编程、多用途、团队支持、丰富的工具链、标准化、适用范围广以及未来发展等方面。

##### 平台无关性

Java语言的平台无关性是其最著名的特点之一。这得益于Java虚拟机（JVM）的存在，JVM允许Java程序在任何安装了JVM的平台上运行，而不需要重新编译代码。这种跨平台的能力使得Java成为了一种“一次编写，到处运行”的解决方案。这对于开发者来说意味着更高的灵活性和可移植性。开发者只需关注编写一次代码，无需为不同平台单独优化和调试，从而大大降低了开发和维护成本。对于企业级应用和大型项目而言，这是一种巨大的优势，尤其是在跨多个操作系统和设备的场景下。

##### 大型标准库

Java拥有一个庞大而强大的标准库，包含了各种类和方法，涵盖了从基本数据类型到高级功能的各个方面。这些标准库为开发者提供了丰富的工具和功能，使得开发过程更加高效。

标准库中包含了众多常用的API，如I/O处理、字符串操作、集合框架、日期时间处理、网络通信等，这些API的存在大大减少了开发者编写重复代码的工作量。此外，标准库的使用也有助于提高代码的可读性和可维护性，因为使用标准库的API通常比自行实现更为简洁和稳定。

##### 开源生态系统

Java拥有一个庞大的开源生态系统，有众多优秀的第三方库和框架可供选择。这些开源工具提供了各种功能和解决方案，极大地丰富了Java开发者的选择。不论是针对Web开发、数据库操作、图形处理、安全性等领域，都可以找到开源工具来满足需求。

开源生态系统的优势在于共享和协作。开发者可以借助他人的开源工具来提高开发效率，同时也可以贡献自己的代码，为社区作出贡献。这种开放式的合作方式加速了技术的发展和创新，让Java生态系统不断蓬勃发展。

##### 丰富的工具链

Java提供了丰富的开发工具，这些工具为开发者提供了全面的支持，使得开发过程更加高效和便捷。

首先，Java开发者通常使用JDK（Java Development Kit）来进行开发。JDK包含了编译器、调试器、文档生成工具等。编译器（javac）将Java源代码编译成字节码，而调试器（jdb）允许开发者在运行时检查程序的状态，这些工具对于调试和代码分析非常有用。

其次，Java开发者还可以使用各种构建工具，如Apache Maven和Gradle。这些构建工具允许开发者管理项目依赖、自动化构建过程、运行测试等，大大提高了项目的管理效率和稳定性。

除了以上工具，Java还有诸如性能分析器（Java VisualVM）、集成开发环境（IDE，如Eclipse和IntelliJ IDEA）等工具，它们使得开发者能够更好地理解和管理自己的代码。

#### MySQL

数据库（database）就是一个存储数据库的仓库，为了方便数据的存储和管理，它将数据按照特定的规律存储在磁盘上。通过数据库管理系统，可以有效的组织和管理存储在数据库中的数据。MySQL数据库就是这样一个关系型数据库管理系统（RDBMS）,它可以称得上是目前运行速度最快的SQL数据库管理系统。

##### 选择MySQL的优势

1.开源免费：MySQL是一款开源数据库管理系统，可免费使用和分发，这使其成为许多开发者和企业的首选数据库。

2.跨平台支持：MySQL支持多种操作系统，包括Windows、Linux、macOS等，可在不同平台上运行。

3.高性能：MySQL被广泛认可为高性能数据库，其优化的查询执行引擎和索引技术使得在大规模数据处理时表现出色。

4.扩展性：MySQL能够支持大量的并发连接和高吞吐量，可以通过水平和垂直扩展来应对不断增长的数据和用户负载。

5.标准SQL语言支持：MySQL支持SQL标准，使得开发者能够使用标准化的查询语言进行数据操作，方便迁移和兼容其他数据库系统。

6.存储引擎：MySQL支持多种存储引擎，如InnoDB、MyISAM、MEMORY等，每种引擎都有不同的优势，可根据需求选择适合的引擎。

7\. ACID事务支持：MySQL支持ACID（原子性、一致性、隔离性、持久性）事务，确保数据的完整性和一致性。

8.复制和高可用性：MySQL支持数据复制和主从复制技术，可以创建多个副本用于负载均衡和高可用性需求。

9.备份和恢复：MySQL提供了备份和恢复功能，可以定期备份数据库，以防止数据丢失，并在需要时恢复数据库到先前状态。

10.数据安全和数据类型支持：MySQL提供了访问控制和权限管理功能，可以限制用户对数据库的访问权限，增强数据安全性。同时，MySQL支持多种数据类型，包括整数、浮点数、日期、文本等，满足不同数据存储需求。

11.全文搜索：MySQL提供了全文搜索功能，使得在文本字段中进行复杂的文本搜索变得更加高效。

13.触发器和存储过程：MySQL支持触发器和存储过程，可以在特定的数据库事件发生时自动执行操作，增强了数据库的灵活性和自动化能力。

14.优化器和分区表：MySQL的查询优化器可以分析查询语句，选择最佳执行计划，提高查询性能。MySQL支持分区表，将大型表拆分成小的逻辑分区，提高查询效率和管理便捷性。

15.性能监控和调优：MySQL提供了性能监控和调优工具，帮助开发者定位性能瓶颈并进行优化。

16.多语言支持和并发控制：MySQL支持多种语言和字符集，适合多语言环境下的应用需求，还使用锁机制来处理并发访问，确保数据的一致性和完整性。

17.社区支持：MySQL有庞大的用户社区和开发者社区，提供丰富的文档、教程和解决方案，对于问题和需求都能够得到快速的支持。

#### Tomcat

Tomcat是一个免费的，开放源代码的Web应用服务器，是Apache软件基金会项目中的一个核心项目，由Apache ，Sun和一些公司以及个人共同开发而成，深受Java爱好者的喜爱，是一款比较流行的web应用服务器。

##### 轻量级与跨平台支持

Tomcat是一个轻量级的Web服务器和Servlet容器，具有较小的内存占用和启动时间。这使得Tomcat适合部署在资源有限的环境中，如虚拟服务器、云计算环境等。

除此之外，Tomcat的跨平台支持是其另一个显著的特点。Tomcat可以在多种操作系统上运行，包括Windows、Linux、macOS等，具有良好的跨平台兼容性。这使得开发者可以在不同的操作系统上进行开发和部署，而无需担心平台差异性。

##### 高性能和并发支持

Tomcat作为Web服务器和Servlet容器，被广泛认可为高性能的解决方案。它的性能优化和请求处理机制使得在大规模数据处理时表现出色。Tomcat使用线程池管理请求，可以同时处理多个并发连接，满足高流量的Web应用需求。

在并发支持方面，Tomcat能够处理大量的并发请求，保持高响应性能。其可靠的请求处理机制和线程管理确保了每个请求得到及时处理，避免了请求阻塞和性能下降。

##### 安全性

对于Web服务器和Servlet容器来说，安全性尤为重要。Tomcat提供了多层次的安全机制，以确保Web应用和数据的安全性。它支持SSL/TLS协议，保证传输数据的加密，防止敏感信息被窃取。

此外，Tomcat可以配置访问控制和认证机制，以确保只有授权用户可以访问特定资源。这些安全特性使得Tomcat成为保护敏感数据和防范恶意攻击的有效工具。

##### 可扩展性和集群支持

Tomcat的可扩展性使得开发者可以根据需求添加自定义组件和功能。它支持插件和扩展，允许用户根据实际情况添加自己的实现或第三方扩展。这使得Tomcat可以满足不同应用场景下的特定需求，实现灵活的定制和扩展。

此外，Tomcat还支持构建集群，可以通过多个服务器节点实现负载均衡和故障恢复。通过Session复制技术，Tomcat可以将用户Session数据复制到多个Tomcat节点上，从而增加应用的高可用性和负载均衡能力。

##### 管理界面和热部署

Tomcat提供了可视化的管理界面，方便管理员监控和管理Tomcat服务器。通过管理界面，管理员可以进行Web应用的部署、启动和停止等操作，实现对服务器的远程管理。

另外，Tomcat支持热部署，这意味着在不重启整个服务器的情况下重新部署和更新Web应用。这为开发者带来了便利，可以在开发阶段快速调试和更新应用，提高了开发和部署效率。

#### Docker

Docker是一个开源的容器化平台，它可以帮助开发者将应用程序及其依赖项打包成一个独立的容器，以便在不同的环中进行部署和运行。使用Docker，开发者可以将应用程序与其所需的库、依赖和配置文件一起打包到一个轻量级、可移植的容器中，无论是在开发环境、测试环境还是生产环境中，都可以确保应用程序在不同的主机上运行一致性和可靠性。

##### 为什么使用Docker

与虚拟机相似，Docker使用的是镜像文件（ISO），不管你想安装什么软件，什么环境，只需要下载这个软件的镜像，通过Docker运行即可，这样就避免了测试环境与生产环境，版本等因素造成的问题。

在环境配置的时候，还可以使用创建虚拟机的方式来实现环境同步。如果服务器用的是centos的系统，可以在开发的时候就用centos开发。这确实解决了一些问题，但是还是需要去独立配置每一台机器。而且，虚拟机本身对多个程序之间的环境不兼容的时候无能为力，可能需要借助第三方的一些管理工具，而docker却可以轻松在一台机器上运行多个容器。

容器不是一台机器，Docker利用的是linux的资源分离机制，例如cgroups，以及linux核心命名空间（namespaces），来建立独立的容器（containers） 。容器看上去是一台机器，实际上是一个进程

相比于虚拟机，容器的优势主要有：

\--资源占用少

\--启动速度快

\--本身体积小

另外，基于docker容器的隔离性，一台服务器上可以同时跑多个程序而做到管理方便，很符合微服务架构的需要

#### Springboot

Springboot是一个开发基于Spring框架的应用的快速开发框架，它也是SpringCloud构建微服务分布式系统的基础设施。其主要特色包括构建独立的Spring应用、嵌入式的Web容器、固化的starter依赖、自动装配Spring模块或第三方库、产品就绪特性（日志、指标、健康检查、外部化配置等）、避免或简化XML配置等特性。

##### 可独立运行的Spring应用

相对于普通的Spring应用，使用SpringBoot构建的Spring应用可以直接打包为一个独立可执行的jar或war包，使用java -jar命令即可运行，不需要管理依赖的第三方库，也不需要依赖外部容器来启动应用。之前使用Spring开发的Java Web应用，一般都会在第三方的Web容器中启动，比如Tomcat等，而使用SpringBoot开发的Java Web应用，虽然基于Spring，但它提供了内嵌的Web容器（基于Servlet或Reactive的Web容器，如Tomcat、Jetty、Undertow、NettyWebServer），通过SpringBoot插件，把所有依赖的第三方库、Web容器和应用本身一起重新打包（repackage）为一个Fat Jar或Fat War，然后直接使用java -jar命令运行即可。

##### 嵌入式Web容器

SpringBoot内置了多种嵌入式Web容器，包括Tomcat、Jetty、Undertow、NettyWebServer等，用于运行基于Servlet或Reactive的Web应用，无需再打包部署WAR文件，即不需要依赖外部的Web容器部署。

##### 固化的Starter依赖

SpringBoot提供了一系列的starter依赖，用于快速的引入应用依赖的Spring模块及相关的第三方库。不同版本的SpringBoot，与其依赖的Spring模块及其相关的第三方库的版本关系是固定的，一一对应的。开发人员不需要关注依赖的Spring模块和第三方库的版本，只需要关心SpringBoot的版本即可，SpringBoot会正确引入依赖的Spring模块和第三方库的版本。固化依赖，可以降低SpringBoot应用管理依赖的成本。

##### 自动装配Spring模块或第三方库

SpringBoot启动应用时，会推断应用类型，并检测引入的Spring模块或第三方库，当条件满足时自动加载Spring模块或第三方库的组件到容器中，以提供给应用使用。

##### 产品就绪特性

SpringBoot提供了应用部署产品环境运行所必须的日志、指标、健康检查、外部化配置等特性，为部署后的运维提供工具支持，支撑应用尽可能快的部署到产品环境。

### 后端模块开发流程

# 数据库设计

## 表结构分析：
一、用户编程题ac的记录  

      : 名称：t_ac_record  
      : 字段：  
          1.user_id: 用户id
          2.problem_id：编程题id  

二、班级表  

      : 名称:t_class  
      : 字段:  
         1.id:班级id
         2.school_id:高校id
         3.class_name:班级名称
         4.created_time:创建时间
         5.updated_time:更新时间  

三、考试管理  

      : 名称：t_exam
      ：字段：  
         1.id：测验id
         2.title：测验标题
         3.description：测验描述
         4.start_time：考试起始时间
         5.end_time：考试结束时间
         6.paper_id：试卷ID
         7.creator_id：创建人ID
         8.created_time：创建时间
         8.updated_time：更新时间 

四、考试答卷关系表  

      : 名称：t_exam_answer_sheet
      : 字段：  
         1.id：表数据编号
         2.user_id：用户Id
         3.exam_id：实验Id
         4.class_id：班级Id
         5.answer_content：答卷内容
         6.created_time：创建时间
         7.updated_time：更新时间 

五、学校考试表  
    
      : 名称：t_exam_school_class
      : 字段:  
         1.id:数据编号
         2.classId：班级id
         3.school_id：学校id
         4.exam_id：考试id
         5.updated_time：更新时间
         6.created_time：创建时间  
         
六、考试得分表  
    
      : 名称：t_exam_score
      ：字段：
         1.id:数据编号
         2.answer_sheet_id：答卷id
         3.exam_id：考试id
         4.class_id：班级id
         5.user_id：学生id
         6.single_choice_score：单选题得分
         7.true_false_question_score：判断题得分
         8.program_question_score：编程题得分  

七、实验表  
  
     : 名称：t_experiment
     ：字段：
        1.id：数据编号
        2.name：实验名称
        3.creator_id：创建人的id
        4.note：实验备注  

八、实验问题表  
  
    : 名称：t_experiment_problem
    ：字段：  
        1.id：数据编号
        2.experiment_id：实验id
        3.problem_id：问题id
        4.category：问题种类
        5.updated_school_ids：更新问题的学校id

九、实验结果表  
  
    : 名称：t_experiment_result
    ：字段：
      1.id：数据编号
      2.user_id：学生学号
      3.user_name：用户名
      4.school_id：学校id
      5.class_id：班级id
      6.experiment_id：实验id
      7.experiment_score：实验成绩表
      8.experiment_problem_result_json：实验问题结果数据  

十、学校实验关系  
  
    : 名称：t_experiment_school_class
    ：字段：
      1.id：数据编号
      2.weight：实验排序的依据
      3.school_id：学校id
      4.class_id：班级id
      5.experiment_id：实验id
      6.status：实验的状态
      7.start_time：实验开始时间
      8.end_time：实验结束时间  

十一、章节表  
  
    : 名称：t_index
    ：字段：
      1.id：数据编号
      2.name：章节名
      3.order：章节号
      4.book_id：章节编号
      5.created_time：创建时间
      6.updated_time：修改时间  

十二、章节问题表
  
    : 名称：t_index_problem
    ：字段：  
      1.id：数据编号
      2.problem_id：问题id
      3.index_id：章节id

十三、输入输出示例表  
  
    : 名称：t_io_sample
    ：字段：   
      1.id：数据编号
      2.problem_id：题目id
      3.input_sample：输入示例
      4.output_sample：输出示例
      
十四、实验题目表  
  
    : 名称：t_problem
    ：字段；
      1.id：数据编号
      2.user_id：上传该题目的用户id
      3.title：题目名称
      4.description：题目描述
      5.input_description：输入描述
      6.output_description：输出描述
      7.time_limit：时间限制，单位为ms，默认1000ms
      8.memory_limit：内存限制，单位为MB，默认为20MB
      9.level：难度等级，目前只有五个等级，0为简单，1为中等，2为困难，3为难以克服的，4为地狱
      10.hint：答题提示
      11.visible：该题目是否可见，0为不可见，1为可见
      12.test_case_id：测试数据的id，也就是测试数据的文件夹id
      13.answer：正确答案

十五、实验标签表
  
    : 名称：t_problem_tag
    ：字段：
      1.id：数据编号
      2.problem_id：题目id
      3.tag_id：标签id
      4.type：标签类型

十六、输入输出测试用例存储表
  
    : 名称：t_program_answer
    ：字段：
      1.id：数据编号
      2.test_case_id：测试数据的id，也就是测试数据的文件夹id
      3.answer_content：答案内容
      4.problem_id：题目id
      5.input_content：输入文件的内容

十七、代码评判结果表
  
    : 名称：t_result
    ：字段：
     1.id：数据编号
     2.cpu_time：运行时间
     3.memory：占用内存大小
     4.compiler_result：编译结果
     5.process_result：执行结果，具体看代码注释
     6.error_data：执行出错的时候的描述信息
     7.test_case_index：测试用例的章节号
     8.user_answer：用户答案
     9.true_answer：正确答案

十八、用户角色表
  
    : 名称：t_role
    ：字段：
      1.id：角色唯一ID
      2.name：角色名
      3.description：角色描述

十九、高校表
  
    : 名称：t_school
    ；字段；
      1.id：学校id
      2.school_name：学校名称
      3.school_english_name：学校英文名称
      4.email：学校邮箱
      5.school_background：学校背景图

二十、学生成绩表
  
    : 名称：t_score
    ：字段：
      1.id：成绩id
      2.class_id：班级id
      3.student_id：学生id
      4.exam_id：测验id
      5.grade：分数

二十一、题目表
  
    : 名称：t_sel_problem
    ：字段：
      1.id：数据编号
      2.type： 0代表选择题，1代表判断题，2代表程序题
      3.title：题目
      4.answer：题目列表
      5.correct：答案
      6.level：题目难度：0代表低级  1代表中级  2代表高级 
      7.index_id：章节id【不使用】
      8.user_id：用户id

二十二、答题表
  
    : 名称：t_solution
    ：字段：
      1.id：数据编号
      2.problem_id：解决的问题id
      3.user_id：用户id
      4.source_code：提交上来的源代码
      5.language：代码语言
      6.mode：tinyint占用一个字节，0为ACM模式，也就是自己处理输入输出，1为模版模式，类似于Leetcode，默认为ACM模式
      7.result_id：处理结果id，如果有处理结果，会更新该值，如果还没有，默认为-1

二十三、答题结果表
  
    : 名称：t_solution_result_join_test
    ：字段：
      1.id：数据编号
      2.user_id：用户id
      3.mode：模块选择（实验0/考试1/华山论剑2）
      4.test_id：以上模块对应的id，如实验id，考试id
      5.problem_id：该模块中的题目id
      6.solution_id：提交方案id
      7.result_id：结果id

二十四、题目标签表
  
    : 名称：t_tag
    ：字段：
      1、id：标签主键
      2.name：标签名

二十五、测试数据评判结果表
  
    : 名称：t_test_case_result
    ：字段：
      1.id：数据编号
      2.solution_id：题目id
      3.cpu_time：执行代码耗费的CPU时间，默认为s
      4.real_time：执行代码实际用时，单位为s
      5.memory：执行代码耗费的内存，单位为B
      6.result：执行结果，具体看代码注释
      7.error：错误编码，具体看代码注释
      8.exit_code：代码返回时
      9.signal：程序如果出错，返回的信号
      10.output_md5：测试数据输出结果md5值
      11.output：测试数据输出
      12.test_case：使用的测试数据文件编号

二十六、试卷表
  
    : 名称：t_test_paper
    ：字段：
      1.id：数据编号
      2.title：试卷标题
      3.suggested_time：建议花费时间
      4.description：试卷描述
      5.score：试卷分数
      6.creator_id：创建人id

二十七、试卷试题表
  
    : 名称：t_test_paper_problem
    ：字段：
      1.id：数据编号
      2.paper_id： 试卷id
      3.problem_id：问题id
      4.type：问题类型
      5.score：题目分数

二十八、用户账号密码表
  
    : 名称：t_user
    ：字段：
      1.id：数据编号
      2.username：用户名，也是用户的唯一标识，默认为用户的邮箱
      3.password：用户密码，使用MD5加密
      4.status：用户状态，0为不可用，1为可用
      5.created_time：用户创建时间
      6.updated_time：更新时间

二十九、用户班级表
  
    : 名称：t_user_class
    ：字段：
      1.id：数据编号
      2.user_id：用户ID
      3.class_id：班级ID

三十、用户信息表
  
    : 名称：t_user_info
    ：字段：
      1.id：用户信息表唯一ID
      2.user_id：t_user表的id，用户唯一标识
      3.nickname：用户昵称，网站昵称
      4.phone：手机号
      5.gender：性别，0为未知，1为男，2为女
      6.created_time：创建时间	
      7.updated_time：用户更新时间
      8.school_id：用户所属学校id
      9.user_level：用户等级
      
三十一、用户角色表
  
    : 名称：t_user_role
    ：字段：
      1. id：用户角色表唯一ID
      2. user_id：用户id
      3. role_id：角色ID
      4. created_time：创建时间
      5. updated_time：更新时间







        







