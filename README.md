# 华南农业大学传智工作室    智能化程序设计实验管理系统

## 诚招开发者加入项目团队
联系邮箱：liximing_cn AT 163.com，请在邮件中说明个人所在 学校专业，年级，姓名，所熟悉的技术等个人简介信息。
### 介绍
华南农业大学传智工作室 的 【智能化程序设计实验管理系统】  包含了基本的程序设计实验管理功能，并包含OJ功能。

目标是实现C，java,python等语言的实验课程管理，可以按周次布置实验给学生，学生提交的代码可以在线评判，不需要教师手动修改。可以实现在线考试，系统直接评判，不需要教师自己批改学生的代码。

## 软件架构
【智能化程序设计实验管理系统】共有三个仓库，前台、后台和测试用例。 

## 代码解读 （持续更新中。。）


## 安装教程

见setup目录下最新文档，以及B站安装视频。

###  【（20230805）【开源项目】智能化程序设计实验管理系统  单独云服务器安装演示视频】 https://www.bilibili.com/video/BV14m4y1W78N/?share_source=copy_web&vd_source=9a7768a01ea4de47cab388a63203d454

###  【（20230530）智能化程序设计实验管理系统 本地虚拟机部署文档】 https://www.bilibili.com/video/BV1so4y137Gn/?share_source=copy_web&vd_source=9a7768a01ea4de47cab388a63203d454

## 使用说明

共三类用户。学生，教师和管理员。

##  【（20231121）【开源项目】智能化程序设计实验管理系统 教师如何设置考试以及学生如何参加考试】】 https://www.bilibili.com/video/BV1Ju4y1w7xB/?share_source=copy_web&vd_source=9a7768a01ea4de47cab388a63203d454

## 【（20231026） 【开源项目】智能化程序设计实验管理系统 如何添加教师以及教师如何管理学生】 https://www.bilibili.com/video/BV1WC4y137Ne/?share_source=copy_web&vd_source=9a7768a01ea4de47cab388a63203d454

## 【（20231023） 【开源项目】智能化程序设计实验管理系统   如何制作题目样例】 https://www.bilibili.com/video/BV1xM411R7hG/?share_source=copy_web&vd_source=9a7768a01ea4de47cab388a63203d454

## 【（20231023） 【开源项目】智能化程序设计实验管理系统  教师端如何新建上传编程题目】 https://www.bilibili.com/video/BV1oy4y1P7mv/?share_source=copy_web&vd_source=9a7768a01ea4de47cab388a63203d454

## 【（20231023） 【开源项目】智能化程序设计实验管理系统    如何新增学校管理员】 https://www.bilibili.com/video/BV1Ne411d711/?share_source=copy_web&vd_source=9a7768a01ea4de47cab388a63203d454

## 【（20230804）【开源项目】智能化程序设计实验管理系统   教师端操作演示视频】 https://www.bilibili.com/video/BV168411d7b2/?share_source=copy_web&vd_source=9a7768a01ea4de47cab388a63203d454

## 【（20230804）【开源项目】智能化程序设计实验管理系统   教师管理员端操作演示视频】 https://www.bilibili.com/video/BV1w94y1C7Lq/?share_source=copy_web&vd_source=9a7768a01ea4de47cab388a63203d454

##  学生端操作演示视频 【（20230722）智能化程序设计实验管理系统    学生端操作演示视频】 https://www.bilibili.com/video/BV1pk4y1G7uY/?share_source=copy_web&vd_source=9a7768a01ea4de47cab388a63203d454

#### 2022年3月录制的 智能化程序设计实验管理系统操作视频（即将更新）

##### 【【智能化程序设计实验管理系统】第一讲 系统概述及基本使用方法】

 https://www.bilibili.com/video/BV1mr4y1i7Cq/?share_source=copy_web

##### 【【智能化程序设计实验管理系统】第二讲 系统判断题目的原理和学生使用系统基本步骤】 

https://www.bilibili.com/video/BV1U34y1x7K9/?share_source=copy_web

##### 【【智能化程序设计实验管理系统】第三讲 华山论剑和查看自己提交的代码功能】 

https://www.bilibili.com/video/BV1ga41147ku/?share_source=copy_web

##### 【【智能化程序设计实验管理系统】第四讲 实验系统演示选择程序设计实例】

 https://www.bilibili.com/video/BV1mu411v7xh/?share_source=copy_web

##### 【【智能化程序设计实验管理系统】第五讲 学生和老师查看作答情况及每次提交的代码情况】

 https://www.bilibili.com/video/BV1Hq4y1a7gx/?share_source=copy_web

#### 参与贡献

 


#### 特技

 
