package com.masonluo.mlonlinejudge.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

/**
 * FastJson 工具类（阿里巴巴）
 *
 * @author 罗有冠
 * @date 2022/1/14 9:20
 */
public class FastJsonUtil {

    private static SerializeConfig config;

    static {
        config = new SerializeConfig();
        config.put(Date.class, new SimpleDateFormatSerializer("yyyy-MM-dd HH:mm:ss"));
    }

    /***
     * 解析为字符串
     *
     * @param object 对象
     * @return 返回JSONString
     */
    public static String getJSONString(Object object) {
        try {
            return JSON.toJSONString(object);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /***
     * 解析为字符串
     *
     * @param jsonString json字符串
     * @param key 需要的JSON对象中的键名称
     * @return 返回JSON对象中指定键的值
     */
    public static String getJSONString(String jsonString, String key) {
        try {
            if (jsonString != null && jsonString.length() > 0) {
                JSONObject jsonObject = JSONObject.parseObject(jsonString);
                return jsonObject.getString(key);
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /***
     * 解析为布尔
     *
     * @param jsonString json字符串
     * @param key 需要的JSON对象中的键名称
     * @return 返回JSON对象中指定键的值
     */
    public static Boolean getBoolean(String jsonString, String key) {
        try {
            if (jsonString != null && jsonString.length() > 0) {
                JSONObject jsonObject = JSONObject.parseObject(jsonString);
                return jsonObject.getBoolean(key);
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /***
     * 解析为String
     *     JSON对象 A:(B:C) 类型
     * @param jsonString json字符串
     * @param key 需要的JSON对象中的键名称
     * @return 返回JSON对象中指定键的值
     */
    public static String getJSONString(String jsonString, String key, String skey) {
        try {
            if (jsonString != null && jsonString.length() > 0) {
                JSONObject jsonObject = JSONObject.parseObject(jsonString);
                JSONObject jsonObject1 = jsonObject.getJSONObject(key);
                String jsonObject2 = jsonObject1.getString(skey);
                return jsonObject2;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /***
     * 解析为Int
     *     JSON对象 A:(B:C) 类型
     * @param jsonString json字符串
     * @param key 需要的JSON对象中的键名称
     * @return 返回JSON对象中指定键的值
     */
    public static int getInt(String jsonString, String key, String skey) {
        try {
            if (jsonString != null && jsonString.length() > 0) {
                JSONObject jsonObject = JSONObject.parseObject(jsonString);
                JSONObject jsonObject1 = jsonObject.getJSONObject(key);
                int jsonObject2 = jsonObject1.getInteger(skey);
                return jsonObject2;
            } else {
                return -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    /***
     * 解析为数字
     *
     * @param jsonString json字符串
     * @param key 需要的JSON对象中的键名称
     * @return 返回JSON对象中指定键的值
     */
    public static Integer getInteger(String jsonString, String key) {
        try {
            if (jsonString != null && jsonString.length() > 0) {
                JSONObject jsonObject = JSONObject.parseObject(jsonString);
                return jsonObject.getInteger(key);
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /***
     * 解析为长位十进制数
     *
     * @param jsonString json字符串
     * @param key 需要的JSON对象中的键名称
     * @return 返回JSON对象中指定键的值
     */
    public static BigDecimal getBigDecimal(String jsonString, String key) {
        try {
            if (jsonString != null && jsonString.length() > 0) {
                JSONObject jsonObject = JSONObject.parseObject(jsonString);
                return jsonObject.getBigDecimal(key);
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /***
     * 解析为双精度
     *
     * @param jsonString json字符串
     * @param key 需要的JSON对象中的键名称
     * @return 返回JSON对象中指定键的值
     */
    public static Double getDouble(String jsonString, String key) {
        try {
            if (jsonString != null && jsonString.length() > 0) {
                JSONObject jsonObject = JSONObject.parseObject(jsonString);
                return jsonObject.getDouble(key);
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /***
     * 解析为浮点数
     *
     * @param jsonString json字符串
     * @param key 需要的JSON对象中的键名称
     * @return 返回JSON对象中指定键的值
     */
    public static Float getFloat(String jsonString, String key) {
        try {
            if (jsonString != null && jsonString.length() > 0) {
                JSONObject jsonObject = JSONObject.parseObject(jsonString);
                return jsonObject.getFloat(key);
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /***
     * 解析为对象
     *
     * @param jsonString json字符串
     * @param key 需要的JSON对象中的键名称
     * @param t 泛型
     * @param <T> 泛型
     * @return 返回JSON对象中指定键的对象
     */
    public static <T> T getJSONObject(String jsonString, String key, Class<T> t) {
        if (jsonString != null) {
            try {
                JSONObject jsonObj = JSONObject.parseObject(jsonString);
                return JSONObject.toJavaObject(jsonObj.getJSONObject(key), t);
            } catch (Exception e) {
                return null;
            }
        } else {
            return null;
        }
    }

    /***
     * 解析为列表
     *
     * @param jsonString json字符串
     * @param key 需要的JSON对象中的键名称
     * @param t 泛型
     * @param <T>   泛型
     * @return 返回JSON对象中指定键的List集合
     */
    public static <T> ArrayList<T> getList(String jsonString, String key, Class<T> t) {
        ArrayList<T> list = new ArrayList<T>();
        if (jsonString != null && jsonString.length() > 0) {
            try {
                JSONObject jsonObj = JSONObject.parseObject(jsonString);
                JSONArray inforArray = jsonObj.getJSONArray(key);
                for (int index = 0; index < inforArray.size(); index++) {
                    list.add(JSONObject.toJavaObject(
                            inforArray.getJSONObject(index), t));
                }
            } catch (Exception e) {
            }
        }
        return list;
    }

    /***
     * 将传过来的JSON字符串直接解析为对象
     * @return 返回的bean
     */
    public static Object getJSONObject(String text) {
        return JSON.parse(text);
    }


    /***
     * 将传过来的JSON字符串直接解析为对象
     * @param <T> 泛型
     * @param jsonString json字符串
     * @return 返回的bean
     */
    public static <T> T getJSONObject(String jsonString, Class<T> t) {
        try {
            if (jsonString != null && jsonString.length() > 0) {
                return JSON.parseObject(jsonString, t);
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /***
     * 将传过来的JSON字符串直接解析为list
     * @param jsonString json字符串
     * @param t 泛型
     * @param <T> 泛型
     * @return 返回的泛型数组
     */
    public static <T> ArrayList<T> getList(String jsonString, Class<T> t) {
        ArrayList<T> list = null;
        try {
            list = new ArrayList<>();
            if (jsonString != null && jsonString.length() > 0) {
                list.addAll(JSON.parseArray(jsonString, t));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    /***
     * 将List直接转换为json
     * @param listBean 泛型数组
     * @return json字符串
     */
    public static <T> String ArrayToJson(ArrayList<T> listBean) {
        String jsonString = JSON.toJSONString(listBean);
        return jsonString;
    }

    /***
     * 将类转为json
     *
     * @param <T> 泛型
     * @return json字符串
     */
    public static <T> String BeanToJson(Object obj) {
        String jsonsString = JSON.toJSONString(obj);
        return jsonsString;
    }
}
