package com.masonluo.mlonlinejudge.exceptions;

/**
 * @author masonluo
 * @date 2020/12/29 3:47 下午
 */
public class UnknownException extends BaseException{
    public UnknownException() {
    }

    public UnknownException(String msg) {
        super(msg);
    }

    public UnknownException(String msg, Throwable throwable) {
        super(msg, throwable);
    }
}
