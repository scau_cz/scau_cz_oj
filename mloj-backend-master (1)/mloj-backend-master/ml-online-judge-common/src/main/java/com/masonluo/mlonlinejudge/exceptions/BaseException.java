package com.masonluo.mlonlinejudge.exceptions;

/**
 * @author masonluo
 * @date 2020/12/29 3:27 下午
 */
public abstract class BaseException extends RuntimeException {
    public BaseException() {
        super();
    }

    public BaseException(String msg) {
        super(msg);
    }

    public BaseException(String msg, Throwable throwable) {
        super(msg, throwable);
    }
}
