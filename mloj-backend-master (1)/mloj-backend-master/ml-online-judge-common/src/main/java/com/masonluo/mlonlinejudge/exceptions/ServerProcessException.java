package com.masonluo.mlonlinejudge.exceptions;

/**
 * 定义一个一场，服务器处理的时候出现的异常，并非用户数据处理时的异常
 *
 * @author masonluo
 * @date 2020/12/29 3:28 下午
 */
public class ServerProcessException extends BaseException {
    public ServerProcessException() {
    }

    public ServerProcessException(String msg) {
        super(msg);
    }

    public ServerProcessException(String msg, Throwable throwable) {
        super(msg, throwable);
    }
}
