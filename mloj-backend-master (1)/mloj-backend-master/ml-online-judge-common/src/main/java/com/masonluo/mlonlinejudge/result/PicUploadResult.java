package com.masonluo.mlonlinejudge.result;

import lombok.Data;

/**
 * 该类用于返回给前端的数据结构定义.
 */
@Data
public class PicUploadResult {
    // 文件唯一标识
    private String uid;

    // 文件名
    private String name;

    // 状态
    private String status;

}
