package com.masonluo.mlonlinejudge.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author masonluo
 * @date 2021/1/17 3:57 下午
 */
public class DateTimeUtils {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static String dateToString(LocalDateTime localDateTime) {
        if (localDateTime == null){
            return null;
        }else {
            return DATE_TIME_FORMATTER.format(localDateTime);
        }

    }

    public static LocalDateTime toDateStr(String time){
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return  time!=null?LocalDateTime.parse(time, DATE_TIME_FORMATTER):null;
    }
}
