package com.masonluo.mlonlinejudge.constant;

/**
 * @author masonluo
 * @date 2021/1/20 2:52 下午
 */
public interface SystemConstant {
    String TOKEN_KEY = "ml-online-judge";

    String TOKEN_HEADER = "X-HTTP-TOKEN";
}
