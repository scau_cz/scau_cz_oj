package com.masonluo.mlonlinejudge.constant;

/**
 * @author masonluo
 * @date 2020/12/29 4:17 下午
 */
public interface RedisConstant {
    String SEPARATOR = ":";
}
