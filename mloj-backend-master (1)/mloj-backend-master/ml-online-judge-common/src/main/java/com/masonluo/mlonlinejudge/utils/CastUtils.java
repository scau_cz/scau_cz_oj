package com.masonluo.mlonlinejudge.utils;

/**
 * @author masonluo
 * @date 2021/2/5 11:26 下午
 */
public class CastUtils {
    public static Long caseToLong(Object obj) {
        if (obj instanceof Long) {
            return (Long) obj;
        }
        if (obj instanceof Integer) {
            return Long.valueOf(String.valueOf(obj));
        }
        throw new ClassCastException("Can't change " + obj + "to Long");
    }

    public static Integer caseToInteger(Object obj){
        if (obj instanceof Long) {
            return Integer.valueOf(String.valueOf(obj));
        }
        if (obj instanceof Integer) {
            return (Integer) obj;
        }
        throw new ClassCastException("Can't change " + obj + "to Long");
    }
}
