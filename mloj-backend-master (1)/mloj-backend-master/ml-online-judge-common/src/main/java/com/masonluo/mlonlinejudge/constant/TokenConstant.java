package com.masonluo.mlonlinejudge.constant;

import java.util.HashMap;

/**
 * @author masonluo
 * @date 2020/12/31 8:33 下午
 */
public interface TokenConstant {

    /**
     * 默认的用户Token过期时间, 单位为秒
     */
    Integer DEFAULT_NORMAL_TOKEN_EXPIRED_SECONDS = 2 * 60 * 60;

    /**
     * 默认的Refresh token过期时间，单位为秒
     */
    Integer DEFAULT_REFRESH_TOKEN_EXPIRED_SECONDS = 7 * 24 * 60 * 60;
}
