package com.masonluo.mlonlinejudge.exceptions;

/**
 * @author masonluo
 * @date 2021/1/25 5:52 下午
 */
public class DecompressionException extends BaseException{
    public DecompressionException() {
    }

    public DecompressionException(String msg) {
        super(msg);
    }

    public DecompressionException(String msg, Throwable throwable) {
        super(msg, throwable);
    }

}
