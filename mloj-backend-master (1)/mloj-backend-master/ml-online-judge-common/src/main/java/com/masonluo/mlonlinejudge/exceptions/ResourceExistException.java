package com.masonluo.mlonlinejudge.exceptions;

/**
 * @author masonluo
 * @date 2020/12/29 3:40 下午
 */
public class ResourceExistException extends BaseException {
    public ResourceExistException() {
    }

    public ResourceExistException(String msg) {
        super(msg);
    }

    public ResourceExistException(String msg, Throwable throwable) {
        super(msg, throwable);
    }
}
