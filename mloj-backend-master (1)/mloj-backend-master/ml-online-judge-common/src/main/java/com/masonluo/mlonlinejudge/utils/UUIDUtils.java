package com.masonluo.mlonlinejudge.utils;

import java.util.UUID;

/**
 * @author masonluo
 * @date 2021/1/25 3:43 下午
 */
public class UUIDUtils {
    public static String getUuid() {
        return UUID.randomUUID().toString();
    }
}
