package com.masonluo.mlonlinejudge.exceptions;

/**
 * @author masonluo
 * @date 2021/1/18 4:29 下午
 */
public class TokenVerifyException extends BaseException {
    public TokenVerifyException() {
    }

    public TokenVerifyException(String msg) {
        super(msg);
    }

    public TokenVerifyException(String msg, Throwable throwable) {
        super(msg, throwable);
    }
}
