package com.masonluo.mlonlinejudge.utils;

import java.util.Collections;
import java.util.List;

/**
 * @author masonluo
 * @date 2021/3/30 7:54 下午
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class CollectionUtils {
    public static boolean isEmpty(List list){
        return list == null ? true : false;
    }

    private static final List EMPTY_LIST = Collections.emptyList();

    public static <T> List<T> emptyOrOriginList(List<T> list) {
        return list == null ? EMPTY_LIST : list;
    }
}
