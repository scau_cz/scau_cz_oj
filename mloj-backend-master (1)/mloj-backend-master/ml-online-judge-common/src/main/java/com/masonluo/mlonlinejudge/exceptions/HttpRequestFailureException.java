package com.masonluo.mlonlinejudge.exceptions;

/**
 * @author masonluo
 * @date 2021/2/5 10:04 下午
 */
public class HttpRequestFailureException extends Exception{
    public HttpRequestFailureException() {
    }

    public HttpRequestFailureException(String msg) {
        super(msg);
    }

    public HttpRequestFailureException(String msg, Throwable throwable) {
        super(msg, throwable);
    }
}
