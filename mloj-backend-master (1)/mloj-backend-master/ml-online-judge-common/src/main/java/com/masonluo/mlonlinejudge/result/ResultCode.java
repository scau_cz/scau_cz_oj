package com.masonluo.mlonlinejudge.result;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.http.HttpStatus;
import org.springframework.util.ObjectUtils;

/**
 * 统一返回值的状态码， 分别有三个属性:
 * 1.
 * {@link HttpStatus}, Http返回状态码， 用来控制Http状态码
 * 2.
 * code 返回一个返回定义状态码, 格式如下: {3位数}{2位数}
 * 第一部分为Http状态码， 第二部分为系统的状态码，由开发人员自定义
 * 3.
 * message 为所要返回给前端的状态码描述信息
 *
 * @author masonluo
 * @date 2021/1/17 7:58 下午
 */
public enum ResultCode {
    OK(HttpStatus.OK, 20000, "Success"),
    CREATED(HttpStatus.CREATED, 20100, "Created"),
    ACCEPTED(HttpStatus.ACCEPTED, 20200, "Accepted"),
    MULTIPLE_CHOICES(HttpStatus.MULTIPLE_CHOICES, 30000, "Multiple choices"),
    MOVED_PERMANENTLY(HttpStatus.MOVED_PERMANENTLY, 30100, "Move Permanently"),
    BAD_REQUEST(HttpStatus.BAD_REQUEST, 40000, "Bad request"),
    BAD_REQUEST_INVALID_PARAMETER(HttpStatus.BAD_REQUEST, 40001, "Invalid parameter"),
    BAD_REQUEST_INVALID_PARAMETER_IS_BLANK(HttpStatus.BAD_REQUEST, 40002, "Invalid parameter, the parameter should not be blank"),
    UNAUTHORIZED(HttpStatus.UNAUTHORIZED, 40100, "Unauthorized"),
    FORBIDDEN(HttpStatus.FORBIDDEN, 40300, "Forbidden"),
    NOT_FOUND(HttpStatus.NOT_FOUND, 40400, "Not found"),
    NOT_ACCEPTABLE(HttpStatus.NOT_ACCEPTABLE, 40600, "Not acceptable"),
    CONFLICT(HttpStatus.CONFLICT, 40900, "Conflict"),
    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, 50000, "Internal server error");


    private final HttpStatus httpStatus;

    private final Integer code;

    private final String message;

    ResultCode(HttpStatus httpStatus, Integer code, String message) {
        this.httpStatus = httpStatus;
        this.code = code;
        this.message = message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    @JsonValue
    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "ResultCode{" +
                "httpStatus=" + httpStatus +
                ", code=" + code +
                ", message='" + message + '\'' +
                '}';
    }

    @JsonCreator
    public static ResultCode getByCode(Integer code) {
        for (ResultCode resultCode : ResultCode.values()) {
            if (ObjectUtils.nullSafeEquals(code, resultCode.getCode())) {
                return resultCode;
            }
        }
        return null;
    }
}
