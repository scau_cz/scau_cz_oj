package com.masonluo.mlonlinejudge.utils;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author masonluo
 * @date 2020/12/29 4:27 下午
 */
public class StringUtils {
    private StringUtils() {

    }

    public static boolean isBlank(String arg) {
        return arg == null || arg.length() == 0;
    }

    /**
     * 将多个字符串进行拼接
     */
    public static String join(String separator, String... args) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < args.length; i++) {
            sb.append(args[i]);
            if (i != args.length - 1) {
                sb.append(separator);
            }
        }
        return sb.toString();
    }

    public static byte[] getBytes(String str) {
        return getBytes(str, StandardCharsets.UTF_8);
    }

    public static byte[] getBytes(String str, Charset charsets) {
        return str.getBytes(charsets);
    }

    public static void main(String[] args) {
        System.out.println(join("", "Hello", "World", "Hi"));
    }

}
