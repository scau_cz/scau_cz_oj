package com.masonluo.mlonlinejudge.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;

/**
 * 字符串加密工具类
 *
 * @author masonluo
 * @date 2020/12/28 1:56 下午
 */
public class CodecUtils {

    private static final Logger log = LoggerFactory.getLogger(CodecUtils.class);

    private static final Base64 base64 = new Base64();

    private CodecUtils() {

    }

    public static String base64Encode(String origin) {
        return base64.encodeAsString(origin.getBytes(StandardCharsets.UTF_8));
    }

    public static String base64Decode(String str) {
        return new String(base64.decode(str), StandardCharsets.UTF_8);
    }

    public static String md5DigestAsHex(String origin) {
        return DigestUtils.md5Hex(origin);
    }
}
