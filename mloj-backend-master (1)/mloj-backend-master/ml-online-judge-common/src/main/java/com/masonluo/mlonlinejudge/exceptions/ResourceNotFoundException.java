package com.masonluo.mlonlinejudge.exceptions;

/**
 * @author masonluo
 * @date 2021/2/6 3:54 下午
 */
public class ResourceNotFoundException extends BaseException{
    public ResourceNotFoundException() {
    }

    public ResourceNotFoundException(String msg) {
        super(msg);
    }

    public ResourceNotFoundException(String msg, Throwable throwable) {
        super(msg, throwable);
    }
}
