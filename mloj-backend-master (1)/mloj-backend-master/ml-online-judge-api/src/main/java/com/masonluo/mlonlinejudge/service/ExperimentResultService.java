package com.masonluo.mlonlinejudge.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.masonluo.mlonlinejudge.entity.ExperimentProblemResult;
import com.masonluo.mlonlinejudge.entity.ExperimentScoreResult;
import com.masonluo.mlonlinejudge.model.dto.ExperimentScoreResultDto;
import com.masonluo.mlonlinejudge.model.vo.ExperimentScoreResultVo;

import java.util.List;

/**
 * <p>
 * 实验结果表 服务类
 * </p>
 *
 * @author yangqi
 * @since 2021-11-05
 */
public interface ExperimentResultService extends IService<ExperimentScoreResult> {

    //List<ExperimentresultScoreDto>  findByExperimentAndClass(ExperimentResultParam experimentResultParam);

    //Integer getTotalStudent(ExperimentResultParam experimentResultParam);


    //List<ExperimentResult> findByUserId(Integer userId);

    List<ExperimentScoreResultDto> findScoreByUserId(Integer userId);

    List<ExperimentScoreResultVo> findScoreByTeacherIdAndClassId(Integer teacherId, Integer classId, Integer pageNum, Integer pageSize);

    List<ExperimentScoreResultVo> refreshScoreByTeacherIdAndClassId(Integer teacherId, Integer classId, Integer pageNum, Integer pageSize);


    List<ExperimentScoreResultVo> bulidScoreByTeacherIdAndClassId(Integer teacherId, Integer classId);

    boolean insertExperimentScoreResult(List<ExperimentScoreResult> experimentScoreResults, Integer schoolId, Integer classId);

    boolean updateExperimentScoreResult(List<ExperimentScoreResult> experimentScoreResults);

    List<ExperimentProblemResult> findExperimentProblemResult(Integer userId, Integer experimentId);
}
