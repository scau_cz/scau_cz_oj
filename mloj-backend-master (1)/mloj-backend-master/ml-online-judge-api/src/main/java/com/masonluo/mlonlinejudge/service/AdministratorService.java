//package com.masonluo.mlonlinejudge.service;
//
//import com.baomidou.mybatisplus.extension.service.IService;
//import com.masonluo.mlonlinejudge.entity.Administrator;
//import com.masonluo.mlonlinejudge.model.bo.AdministratorBo;
//import com.masonluo.mlonlinejudge.model.dto.AdministratorDto;
//import com.masonluo.mlonlinejudge.model.param.AdministratorParam;
//
///**
// * <p>
// * 用户账号密码表 服务类
// * </p>
// *
// * @author yangqi
// * @since 2021-09-10
// */
//public interface AdministratorService extends IService<Administrator> {
//    AdministratorBo createAdministrator(AdministratorDto administratorDto);
////    AdministratorBo createAdministrator(String username,String password);
//    int update(AdministratorParam administratorParam);
//
//
//}
