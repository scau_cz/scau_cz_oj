package com.masonluo.mlonlinejudge.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author masonluo
 * @date 2021/1/25 3:47 下午
 */
public interface FileUploadService {
    boolean upload(MultipartFile file, String dir) throws IOException;
}
