package com.masonluo.mlonlinejudge.service;

import com.masonluo.mlonlinejudge.model.bo.ProblemBo;
import com.masonluo.mlonlinejudge.model.bo.TagBo;
import com.masonluo.mlonlinejudge.model.dto.ProblemUploadDto;
import com.masonluo.mlonlinejudge.model.dto.TestCaseUploadDto;
import com.masonluo.mlonlinejudge.model.param.ProblemParam;
import com.masonluo.mlonlinejudge.model.vo.ProblemNameAndIdVo;
import com.masonluo.mlonlinejudge.model.vo.TagVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @author masonluo
 * @date 2021/1/22 4:53 下午
 */
public interface ProblemService {
    int countByVar(String tag, String title, Integer type);

    int countAll();

    int countByDifficulty(String difficulty);

    int countByIndexId(@Param("indexId") Integer indexId);

    int countByUserId(@Param("userId") Integer userId);

    boolean exist(Integer id);

    ProblemParam findById(Integer id, boolean withTag, boolean withIoSample);

    List<Integer> getAllProblemId(Integer pageSize, Integer pageNum);

    /**
     * 默认不获取标签和输入输出数据
     * @return
     */
    ProblemParam findById(Integer id);

    /**
     * 批量查找问题
     * @return
     */
    List<ProblemParam> listByIds(List<Integer> ids);

    List<ProblemBo> findByTagName(String tagName);

    List<TagBo> findTagByProblemId(Integer id);

    List<TagVo> findTagVoByProblemId(Integer id);

    ProblemParam uploadProblem(ProblemUploadDto problem);

    TestCaseUploadDto uploadTestCase(MultipartFile zipFile) throws IOException;

    List<ProblemNameAndIdVo> findProblemByIndexId(Integer indexId);

    List<ProblemParam> list(Integer pageNum, Integer pageSize);

    <T> List<T> searchAllProblems(String tag, String title, Integer type, Integer pageNum, Integer pageSize);

    List<ProblemParam> searchProblems(String tag, String title, Integer pageNum, Integer pageSize);

    List listAllProblem(Integer pageNum, Integer pageSize);

    List<ProblemParam> listByUserId(Integer userId, Integer pageNum, Integer pageSize);

    void delete(Integer problemId) throws Exception;

    ProblemParam updateProblemById(ProblemBo problemBo, Integer userId, Integer problemId);

    List<ProblemParam> searchByTagAndTitle(String tag, String title);

    List<ProblemParam> listAllProblemByDifficulty(String difficulty, Integer pageNum, Integer pageSize);

}
