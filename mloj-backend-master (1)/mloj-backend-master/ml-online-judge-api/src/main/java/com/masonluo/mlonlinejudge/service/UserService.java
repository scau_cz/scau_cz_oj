package com.masonluo.mlonlinejudge.service;

import com.masonluo.mlonlinejudge.model.bo.UserBo;
import com.masonluo.mlonlinejudge.model.dto.LoginInfoDto;
import com.masonluo.mlonlinejudge.model.bo.UserRankBo;
import com.masonluo.mlonlinejudge.model.dto.UserDto;
import com.masonluo.mlonlinejudge.model.dto.UserInfoDto;
import com.masonluo.mlonlinejudge.model.param.StudentQueryParam;
import com.masonluo.mlonlinejudge.model.param.UserInfoQueryParam;

import java.util.List;

/**
 * @author masonluo
 * @date 2020/12/28 2:36 下午
 */
public interface
UserService {
    /**
     * 用户注册
     *
     * @param username 用户名
     * @param password 密码
     */
    UserInfoDto register(String username, String password);

    /**
     * 用户登录功能的实现， 根据用户名、密码进行查询数据库，如果数据库
     * 没有这条记录的话，则证明用户名或者密码不正确
     */
    LoginInfoDto login(String username, String password,Integer schoolId);

    /**
     * 进行登出操作
     */
    void logout(String username);

    /**
     * 更新Token的过期时间，每次进行调用接口，都会进行Token过期时间的刷新（待定）
     */
    void renewalToken(String username, String token);

    boolean isLogin(String token);

    Integer findIdByUsername(String username);

    String findUsernameById(Integer userId);

    String findUserSchoolById(Integer userId);

    Integer countById(Integer id);

    String getUserRolesByUsername(String username);

    String getUserRolesByUserId(Integer userId);

    UserInfoDto getUserInfoByUserId(Integer userId);

    UserInfoDto getUserInfoByIdOrName(String username,Integer userId);

    int addUserList(List<UserDto> userDtoList, List<String> nameDuplicate);

    int deleteUserById(Integer userId);

    int updateUserInfo(UserBo userBo);

    List<UserInfoDto> queryPageUserInfoByRole(UserInfoQueryParam param);

    Integer getTotalByRole(Integer roleId);

    List<UserInfoDto>  queryPageUserInfoByNickOrName(UserInfoQueryParam param);

    Integer getTotalByNickOrNameKey(Integer schoolId,String username,String nickname,String role);

    boolean checkUserSchoolExist(List<UserDto> userDtoList);

    boolean checkUserClassExist(List<UserDto> userDtoList);

    List<String> checkUserNameDuplicate(List<UserDto> userDtoList);

    List<UserInfoDto> queryPageUserBoBySchoolId(Integer schoolId,String roleName,Integer pageNum,Integer pageSize);

    Integer getTotalBySchoolId(Integer schoolId,String roleName);

    void updateUserLevel(Integer userId);

    List<UserRankBo> queryPageRankBySchoolId(Integer schoolId, Integer pageNum, Integer pageSize);

    List<UserInfoDto> getInfoByClassIdAndUsernameOrNickname(StudentQueryParam param);

    Integer getTotalByClassIdAndUserNameOrNickName(Integer classId, String usernameKey,String nicknameKey);

}