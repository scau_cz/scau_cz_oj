package com.masonluo.mlonlinejudge.service;


import com.masonluo.mlonlinejudge.entity.School;
import com.masonluo.mlonlinejudge.model.bo.SchoolBo;
import com.masonluo.mlonlinejudge.model.param.SchoolQueryParam;
import com.masonluo.mlonlinejudge.model.vo.SchoolVo;

import java.util.List;

public interface SchoolService {

    Integer createSchool(SchoolVo school);

    //SchoolBo findById(Integer id);
    SchoolBo findSchoolById(Integer schoolId);

    SchoolBo findSchoolByName(String schoolName);

    List<SchoolBo> findSchoolListByName(SchoolQueryParam param);

    boolean exist(Integer id);

    List<SchoolBo> findList(Integer pageNum, Integer pageSize);

    void delete(Integer schoolId);

    SchoolBo update(SchoolBo schoolBo);

    int countAll();

    Integer getTotalByName(String schoolNameKey);
}
