package com.masonluo.mlonlinejudge.service;

import com.masonluo.mlonlinejudge.result.PicUploadResult;
import org.springframework.web.multipart.MultipartFile;

/**
 * 图片上传服务接口
 * @author WZT
 */
public interface PicUploadService {

    PicUploadResult uploadProblem(MultipartFile multipartFile);

    PicUploadResult uploadBackground(Integer schoolId,MultipartFile multipartFile);

    boolean deletePic(String key);
}
