package com.masonluo.mlonlinejudge.service;

import com.masonluo.mlonlinejudge.entity.Role;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/5/6 5:46 下午
 */
public interface RoleService {
    Role findByUserId(Integer userId);
    Role findById(Integer userId);
}
