package com.masonluo.mlonlinejudge.service;

import com.masonluo.mlonlinejudge.model.bo.BookBo;
import com.masonluo.mlonlinejudge.model.dto.BookCreateDto;
import com.masonluo.mlonlinejudge.model.param.EditBookParam;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/2/10 3:23 下午
 */
public interface BookService {
    BookBo createBook(BookCreateDto book);

    BookBo findById(Integer id);

    boolean exist(Integer id);

    List<BookBo> findList();

    void delete(Integer bookId);

    BookBo update(EditBookParam editBookParam);
}
