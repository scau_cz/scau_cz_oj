package com.masonluo.mlonlinejudge.service;

import com.masonluo.mlonlinejudge.model.bo.IndexBo;
import com.masonluo.mlonlinejudge.model.bo.SchoolBo;
import com.masonluo.mlonlinejudge.model.param.IndexParam;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/2/10 3:23 下午
 */
public interface IndexService {
    IndexBo createIndex(Integer bookId, String indexName, Integer order);

    List<IndexBo> listByBookId(Integer bookId, boolean order);

    List<IndexBo> listBySchoolId(Integer schoolId, boolean order);

    boolean exist(Integer id);

    List<IndexBo> listAll();

    void deleteById(Integer indexId);

    int update(IndexParam param);


}
