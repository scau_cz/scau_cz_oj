package com.masonluo.mlonlinejudge.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.masonluo.mlonlinejudge.entity.SelectProblem;
import com.masonluo.mlonlinejudge.model.dto.SelectProblemUploadDto;
import com.masonluo.mlonlinejudge.model.param.SelectProblemParam;
import com.masonluo.mlonlinejudge.model.vo.TagVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SelectProblemService {
    int countAll();

    int countByIndexId(Integer indexId);

    int countByUserId(Integer userId);

    Integer countByAll(String tag, String title, Integer type);


    /**
    * @Description: 根据id编辑一个判断题
    * @Param: id
    * @return:
    * @Author: LOTP
    * @Date: 2021/9/17
    */
    SelectProblemParam updateSelectProblemById(SelectProblemUploadDto selectProblemUploadDto, Integer userId);

    /**
     * 通过题目id获取题目详情
     * @param id
     * @return
     */
    SelectProblemParam getSelectProblemById(Integer id);

    /**
     * 通过章节id获取题目列表
     * @param indexId
     * @return
     */
    List<SelectProblemParam> getSelectProblemListByIndexId(Integer indexId,Integer pageNum, Integer pageSize);

    /**
     * 分页获取问题列表
     * @param pageNum
     * @param pageSize
     * @return
     */
    List<SelectProblemParam> getSelectProblemByPage(Integer pageNum, Integer pageSize);

    /**
     * 不分页获取问题列表
     * @return
     */
    List<SelectProblemParam> getSelectProblem();

    /**
     * 获取用户上传问题分页
     * @param pageNum
     * @param pageSize
     * @return
     */
    List<SelectProblemParam> getSelectProblemPageByUserId(Integer userId, Integer pageNum, Integer pageSize);

    /**
     * 上传一个问题
     * @param selectProblemUploadDto
     * @return
     */
    Integer add(SelectProblemUploadDto selectProblemUploadDto);

    /**
    * @Description: 根据id删除一个题目
    * @Param: problemId
    * @return: Boolean
    */
    boolean delete(Integer problemId);

    List<TagVo> findTagBySelectProblemId(Integer id);

    List<SelectProblemParam> sorter(IPage<SelectProblem> selectProblemIPage, Integer pageNum);

    /**
    * @Description: 分页查找题目
    */
    List<SelectProblemParam> searchProblems(String tag, String title, Integer type, Integer pageNum, Integer pageSize);

    List<SelectProblemParam> searchByTagAndTitle(String tag, String title);

    /**
    *根据编程题id修改编程题的testcaseid
    */
    boolean updateTestCaseIdById(Integer problemId,String testCaseId);

    /**
    *根据编程题id得到编程题的原testcaseid
    */
    String getTestCaseIdById(Integer problemId);
}
