package com.masonluo.mlonlinejudge.service;

import com.masonluo.mlonlinejudge.entity.Result;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.masonluo.mlonlinejudge.model.param.ResultFindParam;
import com.masonluo.mlonlinejudge.model.vo.ResultVo;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/3/12 11:27 下午
 */
public interface ResultService {
    /**
     * 根据Id进行查找评判结果
     * (直接返回实体类，偷个懒，加快一下开发进度)
     */
    Result findResultById(Integer id);

    Page<ResultVo> findResultByProblem(Integer userId,Integer problemId,Integer testId,Integer current,Integer limit);

    Page<ResultVo> findResultAll(ResultFindParam param);

    List<Result> listAll();

    String findResourceCodeByResultId(Integer resultId);
}
