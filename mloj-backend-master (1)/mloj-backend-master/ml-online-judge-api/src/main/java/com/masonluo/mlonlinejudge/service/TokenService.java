package com.masonluo.mlonlinejudge.service;

import com.masonluo.mlonlinejudge.enums.TokenType;
import com.masonluo.mlonlinejudge.security.Token;

/**
 * @author masonluo
 * @date 2020/12/28 2:34 下午
 */
public interface TokenService {

    String generateToken(String username, TokenType tokenType, long timestamp);

    /**
     * 根据userId和userName生成一个用户当前的Token
     */
    String generateToken(String username, TokenType tokenType);

    /**
     * 将bae64加密之后的token字符串还原成实体
     *
     * @throws com.masonluo.mlonlinejudge.exceptions.TokenVerifyException 如果base64串不符合格式，那么抛出异常
     */
    Token convert(String base64Token);
}
