package com.masonluo.mlonlinejudge.service;

import com.masonluo.mlonlinejudge.entity.TestCaseResult;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/2/7 3:11 下午
 */
public interface TestCaseResultService {
    int saveAll(List<TestCaseResult> testCaseResults);
}
