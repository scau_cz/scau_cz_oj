package com.masonluo.mlonlinejudge.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.masonluo.mlonlinejudge.entity.Score;
import com.masonluo.mlonlinejudge.model.param.ScoreParam;

/**
 * <p>
 * 学生成绩表 服务类
 * </p>
 *
 * @author yangqi
 * @since 2021-08-19
 */
public interface ScoreService extends IService<Score> {
    int update(ScoreParam scoreParam);

}
