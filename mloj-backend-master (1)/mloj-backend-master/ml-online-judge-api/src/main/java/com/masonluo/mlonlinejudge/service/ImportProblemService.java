package com.masonluo.mlonlinejudge.service;

import net.lingala.zip4j.exception.ZipException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 用于批量导入指定格式的文件，将编程题和测试样例文件读出并关联
 * @author mingkai
 */
public interface ImportProblemService {
     void importProblem(MultipartFile file) throws IOException, ZipException;



}
