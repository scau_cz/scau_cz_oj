package com.masonluo.mlonlinejudge.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.masonluo.mlonlinejudge.entity.Class;
import com.masonluo.mlonlinejudge.model.bo.ClassBo;
import com.masonluo.mlonlinejudge.model.bo.ClassFindByTeacherBo;
import com.masonluo.mlonlinejudge.model.bo.StudentBo;
import com.masonluo.mlonlinejudge.model.dto.ClassQueryBySchoolDto;
import com.masonluo.mlonlinejudge.model.param.ClassParam;
import com.masonluo.mlonlinejudge.model.param.EditClassParam;

import java.util.List;

/**
 * <p>
 * 班级表 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-08-18
 */
public interface ClassService extends IService<Class> {
    ClassBo createClass(ClassParam classParam);

    ClassBo update(EditClassParam editClassParam);

    List<ClassQueryBySchoolDto> getClassBySchool(Integer current, Integer limit, Integer id);

    Integer getTotalBySchoolId(Integer id);

    List<ClassFindByTeacherBo> getClassByTeacher(Integer id);

    Integer getClassIdByName(String className);

    List<StudentBo> getStudentByClassId(Integer classId, Integer teacherId, Integer current, Integer limit);

    List<Class> pageClass(Integer current, Integer limit);

    Integer getTotalClass();

    boolean classExist(String className, Integer schoolId);

    boolean checkUserRole(Integer userId);

    Integer getTotalStudentByClassId(Integer teacherId, Integer classId);

    Integer deleteUserClass(Integer classId);
}
