package com.masonluo.mlonlinejudge.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.masonluo.mlonlinejudge.entity.AnswerContent;
import com.masonluo.mlonlinejudge.model.bo.TaskBo;
import com.masonluo.mlonlinejudge.model.bo.TaskInfoBo;
import com.masonluo.mlonlinejudge.model.bo.TaskWithClassInfoBo;
import com.masonluo.mlonlinejudge.model.param.*;
import com.masonluo.mlonlinejudge.model.vo.TaskProblemSituationVo;
import com.masonluo.mlonlinejudge.result.Result;

import java.util.List;

public interface TaskService {

    TaskBo createTask(TaskCreateParam task);

    TaskBo findTaskById(Integer id);

    Page<TaskBo> findTaskByClassId(Integer pageNum, Integer pageSize,Integer classId);

    List<TaskBo> findList(Integer schoolId);

    void delete(Integer id);

    TaskBo update(EditTaskParam editTaskParam);

    void updateTime(TaskChangeTimeParam taskChangeTimeParam);

    TaskInfoBo editTask(ChangeTaskParam changeTaskParam);

    // TaskBo pushExam(TaskChangeStatusParam taskChangeStatusParam);

    void submit(SubmitParam submitParam);

    TaskInfoBo getTaskInfo(Integer taskId);

    Page<TaskWithClassInfoBo> List(Integer pageNum, Integer pageSize, Integer schoolId);

    boolean judgeTimeout(Integer examId);

    boolean judgeExist(SubmitParam submitParam);

    List<AnswerContent> queryAnswerContent(Integer userId, Integer examId);

    Result storeExamAnswerSheet(SubmitParam submitParam);

    Result getExamAnswerSheetCache(Integer userId, Integer examId);

    List<TaskProblemSituationVo> queryExamProblemResponseSituation(Integer userId, Integer examId);

}
