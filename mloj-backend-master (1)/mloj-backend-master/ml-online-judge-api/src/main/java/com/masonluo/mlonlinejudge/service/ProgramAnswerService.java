package com.masonluo.mlonlinejudge.service;

import com.masonluo.mlonlinejudge.entity.ProgramAnswerEntity;
import com.masonluo.mlonlinejudge.model.Pair;

import java.util.List;

/**
 * @author LZC
 * @create 2021-12-21 21:29
 */
public interface ProgramAnswerService {
    int insertAnswer(String testCaseId, List<Pair<String, String>> answers, List<Pair<String, String>> inputContents);

    int insertProgramAnswer(String testCaseId, List<Pair<String, String>> answers,
                            List<Pair<String, String>> inputContents, Integer problemId);

    int updateAnswer(String testCaseId, Integer problemId);

    List<ProgramAnswerEntity> findAnswer(String testCaseId);

    boolean deleteProgramAnswer(String testCaseId);

    ProgramAnswerEntity findAnswerByProblemId(Integer problemId);
}
