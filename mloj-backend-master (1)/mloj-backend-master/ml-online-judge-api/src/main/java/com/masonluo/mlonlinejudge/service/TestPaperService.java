package com.masonluo.mlonlinejudge.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.masonluo.mlonlinejudge.entity.TestPaper;
import com.masonluo.mlonlinejudge.model.bo.TestPaperBo;
import com.masonluo.mlonlinejudge.model.bo.TestPaperDetailBo;
import com.masonluo.mlonlinejudge.model.dto.TestPaperDto;
import com.masonluo.mlonlinejudge.model.dto.TestPaperSelProblem;
import com.masonluo.mlonlinejudge.model.param.UpdateTestPaperParam;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * <p>
 * 试卷表 服务类
 * </p>
 *
 * @author yangqi
 * @since 2021-10-28
 */
public interface TestPaperService extends IService<TestPaper> {

    Integer addTestPaper(TestPaperDto testPaperDto);
    Integer selectId(TestPaperDto testPaperDto);
    boolean addTestSelProblem(Integer paperId,List<TestPaperSelProblem> selProblemList, Integer type);

    List<TestPaperBo> pageTestPaper(Integer current,Integer limit);
    Integer getTotal();

    Integer deletePaperProblem(Integer id);

    List<TestPaperBo> findPaperByName(String name,Integer current,Integer limit);

    Integer getTotalByName(String name);

    TestPaperDetailBo getTestPaperDetail(Integer id,Integer taskId);

    Integer problemExist(Integer id,Integer type);

    Integer updateTestPaper(UpdateTestPaperParam updateTestPaperParam);


}
