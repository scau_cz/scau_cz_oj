package com.masonluo.mlonlinejudge.service;

import com.masonluo.mlonlinejudge.model.bo.IndexProblemBo;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/2/10 3:23 下午
 */
public interface IndexProblemService {

    /**
     * 将问题跟目录（索引）做一个关联
     */
    IndexProblemBo relatedProblem(Integer indexId, List<Integer> problemId);

    void deleteByIndexIdAndProblemId(Integer indexId, Integer problemId);
}
