package com.masonluo.mlonlinejudge.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.masonluo.mlonlinejudge.exceptions.HttpRequestFailureException;
import com.masonluo.mlonlinejudge.model.dto.JudgeParamDto;
import com.masonluo.mlonlinejudge.model.judge.JudgeResult;

/**
 * 评判Service， 主要是用来调用Judge Server的接口使用
 *
 * @author masonluo
 * @date 2021/2/5 9:13 下午
 */
public interface JudgeService {
    JudgeResult judge(JudgeParamDto judgeParamDto) throws HttpRequestFailureException, JsonProcessingException;
}
