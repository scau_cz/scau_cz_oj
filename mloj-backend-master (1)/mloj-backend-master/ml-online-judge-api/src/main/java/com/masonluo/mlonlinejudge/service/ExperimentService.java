package com.masonluo.mlonlinejudge.service;

import com.masonluo.mlonlinejudge.entity.Experiment;
import com.masonluo.mlonlinejudge.model.bo.ExperimentBo;
import com.masonluo.mlonlinejudge.model.dto.ExperimentUploadDto;
import com.masonluo.mlonlinejudge.model.param.ExperimentParam;
import com.masonluo.mlonlinejudge.model.param.ExperimentUploadParam;
import com.masonluo.mlonlinejudge.model.param.ProblemParam;
import com.masonluo.mlonlinejudge.model.vo.ExperimentIdAndNameVo;
import com.masonluo.mlonlinejudge.model.vo.ExperimentVo;
import org.apache.ibatis.annotations.Param;


import javax.validation.Valid;
import java.util.List;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2021-10-04 23:36
 **/

public interface ExperimentService {

    boolean exist(Integer id);

    ExperimentVo findById(Integer id);

    List<ExperimentIdAndNameVo> findExperimentIdAndNameBySchoolIdAndStatus(Integer schoolId, String status);

    ExperimentBo findByExperimentIdAndSchoolId(Integer experimentId, Integer schoolId);

    ExperimentBo findByExperimentIdAndUserId(Integer experimentId, Integer userId);

    //批量查找
    List<Experiment> listByIds(List<Integer> ids);

    //实验列表
    <T> List<T> list(Integer pageNum, Integer pageSize, Integer userId, String status);

    //管理员通过实验Id修改实验信息
    <T> T updateExperimentById(Integer userId, ExperimentParam experimentParam);

    //根据实验Id删除实验信息
    Boolean delete(Integer experimentId, Integer userId);

    //校管理员创建实验
    ExperimentBo addExperiment(Integer userId, ExperimentUploadDto experimentUploadDto);

    //创建实验
    ExperimentVo addExperiment(@Valid ExperimentUploadParam experimentUploadParam);

    //根据学校Id返回实验列表(分页)
    List<ExperimentBo> listBySchoolId(Integer schoolId,Integer userId,String status, Integer pageNum, Integer pageSize);

    //根据班级id获取班级参与的实验
    List<ExperimentBo> listByClassId(Integer classId, Integer schoolId, String status, Integer pageNum, Integer pageSize);

    //校管理员设置实验状态
    boolean setExperimentStatus(Integer experimentId, Integer schoolId, String status);

    //校管理员设置实验时间
    boolean setExperimentTime(Integer experimentId, Integer schoolId, String startTime, String endTime);

    //根据校管理获取其学校学校未关联的实验列表
    List<ExperimentVo> findNotRelatedSchool(Integer userId, Integer pageNum, Integer pageSize);

    int countNotRelatedSchool(Integer userId);

    //根据实验ID和学校ID以及题目是否选做必做查找题目
    List<ProblemParam>findProblemBySchoolId(Integer experimentId, Integer schoolId, String category);

    boolean doExperimentRelatedSchool(Integer experimentId, Integer schoolId);

    //根据实验ID与学校ID修改某题目的分类
    boolean updateProblemCategory(Integer experimentId, Integer schoolId, Integer problemId);

    int countAll();

    int countByAdmin();

    int countBySchoolId(Integer schoolId,String status);

    int countByUserId(Integer userId,String status);

    int countByClassIdAndSchoolId(Integer classId, Integer schoolId);

    String findNameByExperimentId(Integer id);

    boolean sortExperimentByIdsAndSchoolId(List<Integer> experimentIds, Integer schoolId);
}
