package com.masonluo.mlonlinejudge.service;

import com.masonluo.mlonlinejudge.model.dto.TestCaseUploadDto;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface TestcaseFileService {
    TestCaseUploadDto sendMultipartFile(MultipartFile zipFile) throws IOException;

    void downloadTestCase(String testCaseId, HttpServletRequest request, HttpServletResponse response) throws Exception;

    void deleteTestCase(String testCaseId) throws Exception;
}
