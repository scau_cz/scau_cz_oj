package com.masonluo.mlonlinejudge.service;


import org.apache.rocketmq.client.producer.SendResult;

/**
 * @author masonluo
 * @date 2021/2/5 2:55 下午
 */
public interface RocketMQService {
    SendResult syncSend(String topic, String tag, byte[] message);
}
