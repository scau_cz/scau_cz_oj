package com.masonluo.mlonlinejudge.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.masonluo.mlonlinejudge.entity.ExamScore;
import com.masonluo.mlonlinejudge.model.dto.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * <p>
 * 考试得分表 服务类
 * </p>
 *
 * @author yangqi
 * @since 2021-10-10
 */
public interface ExamScoreService extends IService<ExamScore> {

    List<ClassExamDto> findExamByClassId(Integer classId,Integer schoolId);

    List<ExamScoreDto> findStudentScore(Integer studentId);

    Integer countExamByTeacherId(Integer teacherId);

    List<ExamScoreDto> findAll(Integer current,Integer limit);
    List<ExamScoreDto> findByExamAndClass(Integer examId,Integer classId,Integer current,Integer limit);
//    List<ExamScoreDto> findByExamAndExperiment(Integer experimentId,Integer classId,Integer current,Integer limit);

    Integer getTotalStudentExam(Integer examId,Integer classId,Integer current,Integer limit);

    Integer getTotalAllStudentExam(Integer current,Integer limit);

    List<SchoolExamDto> findExamBySchoolId(Integer schoolId);

    ExamTitleDto findExamTitleAndClassNameByUserId(Integer userId);

    List<ExamScoreAndClassDto> findByTeacherId(Integer current,Integer limit,Integer teacherId,String examTitle,String className);
    //TODO
    void exportScore(HttpServletResponse response, Integer teacherId, String examTitle, String className);

    Integer countExam(Integer teacherId, String examTitle, String className);
}
