package com.masonluo.mlonlinejudge.service;

import com.masonluo.mlonlinejudge.model.bo.IoSampleBo;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/1/22 5:48 下午
 */
public interface IoSampleService {
    List<IoSampleBo> findByProblemId(Integer problemId);
}
