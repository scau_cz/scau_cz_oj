package com.masonluo.mlonlinejudge.service;


import com.masonluo.mlonlinejudge.entity.TimeSlot;

import java.util.List;

public interface AddFadeDataService {
    void addFadeData();

    void open(int num, double accuracy, List<TimeSlot> list);

    void close();
    double getAccuracy();
}
