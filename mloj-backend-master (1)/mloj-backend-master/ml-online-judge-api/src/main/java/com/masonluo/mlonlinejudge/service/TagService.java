package com.masonluo.mlonlinejudge.service;

import com.masonluo.mlonlinejudge.entity.Tag;
import com.masonluo.mlonlinejudge.model.bo.TagBo;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/1/22 4:12 下午
 */
public interface TagService {
    Tag addTag(String tagName);

    boolean deleteTag(String tagName);

    List<TagBo> findAll();
}
