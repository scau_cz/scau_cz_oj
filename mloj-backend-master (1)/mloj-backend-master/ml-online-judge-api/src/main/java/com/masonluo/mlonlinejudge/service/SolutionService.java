package com.masonluo.mlonlinejudge.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.masonluo.mlonlinejudge.entity.Result;
import com.masonluo.mlonlinejudge.entity.Solution;
import com.masonluo.mlonlinejudge.model.bo.SolutionBo;
import com.masonluo.mlonlinejudge.model.dto.SolutionDto;
import com.masonluo.mlonlinejudge.model.mq.JudgeMessage;
import com.masonluo.mlonlinejudge.model.vo.JudgeResultVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/2/6 1:32 下午
 */
public interface SolutionService {
    void judge(JudgeMessage param);

    void judgeFailAspect(Solution solution, Integer examId);

    void afterCompileFailAspect(Solution solution, Integer examId, String errorData);

    void afterJudgeSuccessAspect(Solution solution, Integer examId, Result result);

    SolutionBo save(SolutionDto solutionDto);

    /**
     * 提交问题，并且将问题放入队列中，等待评判
     */

    SolutionBo submit(SolutionDto solutionDto) throws JsonProcessingException;

    Solution findById(Integer id);

    int updateById(Solution solution);

    JudgeResultVo findJudgeResult(Integer solutionId);

    List<Solution> listByUserId(Integer userId);

    /**
     * 判断实验中的编程题题目是否ac
     *
     * @return
     */
    boolean judgeSuccessByProblemIDAndUserId(Integer experimentId, Integer problemId, Integer userId);

    /**
     * 判断实验中的编程题题目是否做过
     *
     * @return
     */
    boolean judgeDoneByProblemIDAndUserId(Integer experimentId, Integer problemId, Integer userId);

    /**
     * 列出实验中的所有ac的编程题题目id
     */
    //List<Integer>listPassProblem(List<Integer>problemIds, Integer userId);

    /**
     * 计算改用户在该实验中的所有ac的编程题总数
     */
    Integer countPassProblemByUserIdAndExperimentId(Integer userId, Integer experimentId);

    /**
     * 查找某用户做的某个实验中的某道题的最后一次提交的代码
     */
    String findRecentSourceCode(@Param("experimentId") Integer experimentId, @Param("problemId") Integer problemId, @Param("userId") Integer userId);

    public int judgeSubmitNextTime(Integer userId);
}