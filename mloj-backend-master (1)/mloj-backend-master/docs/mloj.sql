/*
 Navicat Premium Data Transfer

 Source Server         : localhost_mysql
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost
 Source Database       : mloj

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : utf-8

 Date: 04/04/2021 17:29:51 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `t_book`
-- ----------------------------
DROP TABLE IF EXISTS `t_book`;
CREATE TABLE `t_book` (
  `identification` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(36) NOT NULL,
  `author` varchar(36) NOT NULL,
  `publisher` varchar(255) NOT NULL COMMENT '出版商',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`identification`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `t_index`
-- ----------------------------
DROP TABLE IF EXISTS `t_index`;
CREATE TABLE `t_index` (
  `identification` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(36) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0' COMMENT '排序方式，值越小越靠前',
  `book_id` int(11) NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`identification`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `t_index_problem`
-- ----------------------------
DROP TABLE IF EXISTS `t_index_problem`;
CREATE TABLE `t_index_problem` (
  `identification` int(11) NOT NULL AUTO_INCREMENT,
  `problem_id` int(11) NOT NULL,
  `index_id` int(11) NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`identification`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `t_io_sample`
-- ----------------------------
DROP TABLE IF EXISTS `t_io_sample`;
CREATE TABLE `t_io_sample` (
  `identification` int(11) NOT NULL AUTO_INCREMENT,
  `problem_id` int(11) NOT NULL,
  `input_sample` varchar(256) NOT NULL COMMENT '输入示例',
  `output_sample` varchar(256) NOT NULL COMMENT '输出示例',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`identification`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `t_problem`
-- ----------------------------
DROP TABLE IF EXISTS `t_problem`;
CREATE TABLE `t_problem` (
  `identification` int(11) NOT NULL AUTO_INCREMENT COMMENT '题目ID',
  `user_id` int(11) NOT NULL COMMENT '上传该题目的用户id',
  `title` varchar(36) NOT NULL COMMENT '题目名称',
  `description` text NOT NULL COMMENT '题目描述',
  `input_description` text NOT NULL COMMENT '输入描述',
  `output_description` text NOT NULL COMMENT '输出描述',
  `time_limit` int(11) NOT NULL DEFAULT '1000' COMMENT '时间限制，单位为ms，默认1000ms',
  `memory_limit` int(11) NOT NULL DEFAULT '20' COMMENT '内存限制，单位为MB，默认为20MB',
  `level` tinyint(4) NOT NULL COMMENT '难度等级，目前只有三个等级，0为简单，1为中等，2为困难',
  `hint` text COMMENT '答题提示',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT '该题目是否可见，0为不可见，1为可见',
  `test_case_id` char(64) NOT NULL DEFAULT '' COMMENT '测试数据的id，也就是测试数据的文件夹id',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`identification`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `t_problem_tag`
-- ----------------------------
DROP TABLE IF EXISTS `t_problem_tag`;
CREATE TABLE `t_problem_tag` (
  `identification` int(11) NOT NULL,
  `problem_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`identification`),
  UNIQUE KEY `uq_problem_id_tag_id` (`problem_id`,`tag_id`) USING BTREE,
  KEY `idx_tag_id` (`tag_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `t_result`
-- ----------------------------
DROP TABLE IF EXISTS `t_result`;
CREATE TABLE `t_result` (
  `identification` int(11) NOT NULL AUTO_INCREMENT,
  `cpu_time` int(11) NOT NULL,
  `memory` int(11) NOT NULL,
  `compiler_result` tinyint(1) NOT NULL,
  `process_result` int(11) DEFAULT NULL COMMENT '执行结果，具体看代码注释',
  `error_data` varchar(256) DEFAULT NULL COMMENT '执行出错的时候的描述信息',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`identification`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `t_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `identification` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色唯一ID',
  `name` varchar(45) NOT NULL COMMENT '角色名',
  `description` varchar(200) NOT NULL DEFAULT '' COMMENT '角色描述',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`identification`),
  UNIQUE KEY `uq_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色表';

-- ----------------------------
--  Table structure for `t_solution`
-- ----------------------------
DROP TABLE IF EXISTS `t_solution`;
CREATE TABLE `t_solution` (
  `identification` int(11) NOT NULL AUTO_INCREMENT,
  `problem_id` int(11) NOT NULL COMMENT '解决的问题id',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `source_code` text NOT NULL COMMENT '提交上来的源代码',
  `language` tinyint(4) NOT NULL,
  `mode` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'tinyint占用一个字节，0为ACM模式，也就是自己处理输入输出，1为模版模式，类似于Leetcode，默认为ACM模式',
  `result_id` int(11) NOT NULL DEFAULT '-1' COMMENT '处理结果id，如果有处理结果，会更新该值，如果还没有，默认为-1',
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`identification`,`language`),
  KEY `idx_problem_id_user_id` (`user_id`,`problem_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `t_tag`
-- ----------------------------
DROP TABLE IF EXISTS `t_tag`;
CREATE TABLE `t_tag` (
  `identification` int(11) NOT NULL AUTO_INCREMENT COMMENT '标签主键',
  `name` varchar(36) NOT NULL COMMENT '标签名',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`identification`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `t_test_case_result`
-- ----------------------------
DROP TABLE IF EXISTS `t_test_case_result`;
CREATE TABLE `t_test_case_result` (
  `identification` int(11) NOT NULL AUTO_INCREMENT,
  `solution_id` int(11) NOT NULL,
  `cpu_time` int(11) NOT NULL DEFAULT '0' COMMENT '执行代码耗费的CPU时间，默认为s',
  `real_time` int(11) NOT NULL DEFAULT '0' COMMENT '执行代码实际用时，单位为s',
  `memory` int(11) NOT NULL DEFAULT '0' COMMENT '执行代码耗费的内存，单位为B',
  `result` int(11) NOT NULL COMMENT '执行结果，具体看代码注释',
  `error` int(11) NOT NULL COMMENT '错误编码，具体看代码注释',
  `exit_code` int(11) NOT NULL DEFAULT '0' COMMENT '代码返回时',
  `signal` int(11) NOT NULL COMMENT '程序如果出错，返回的信号',
  `output_md5` varchar(64) DEFAULT NULL COMMENT '测试数据输出结果md5值',
  `output` text NOT NULL COMMENT '测试数据输出',
  `test_case` int(11) NOT NULL DEFAULT '0' COMMENT '使用的测试数据文件编号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`identification`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `t_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `identification` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(36) NOT NULL COMMENT '用户名，也是用户的唯一标识，默认为用户的邮箱',
  `password` varchar(255) NOT NULL COMMENT '用户密码，使用MD5加密',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '用户状态，0为不可用，1为可用',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '用户创建时间',
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`identification`),
  UNIQUE KEY `uq_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='用户账号密码表';

-- ----------------------------
--  Table structure for `t_user_info`
-- ----------------------------
DROP TABLE IF EXISTS `t_user_info`;
CREATE TABLE `t_user_info` (
  `identification` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户信息表唯一ID',
  `user_id` int(11) NOT NULL COMMENT 't_user表的id，用户唯一标识',
  `nickname` varchar(45) DEFAULT NULL COMMENT '用户昵称，网站昵称',
  `phone` char(11) DEFAULT NULL COMMENT '手机号',
  `gender` tinyint(2) NOT NULL COMMENT '性别，0为未知，1为男，2为女',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间	',
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '用户更新时间',
  PRIMARY KEY (`identification`),
  UNIQUE KEY `uq_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- ----------------------------
--  Table structure for `t_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role` (
  `identification` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户角色表唯一ID',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`identification`),
  UNIQUE KEY `uq_user_id_role_id` (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色表';

SET FOREIGN_KEY_CHECKS = 1;
