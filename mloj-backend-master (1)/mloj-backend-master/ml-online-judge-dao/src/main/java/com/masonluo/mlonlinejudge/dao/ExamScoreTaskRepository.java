package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.ExamScore;

public interface ExamScoreTaskRepository extends BaseMapper<ExamScore> {

    int insertIntoExamScore(ExamScore examScore);

    void updateExamScore(ExamScore examScore);

}
