package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.IoSample;
import com.masonluo.mlonlinejudge.entity.User;
import com.masonluo.mlonlinejudge.model.bo.IoSampleBo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author masonluo
 * @date 2020/12/27 2:13 下午
 */
@Repository
public interface IoSampleRepository extends BaseMapper<IoSample> {
    List<IoSample> findByProblemId(@Param("problemId") Integer problemId);

    Integer insertAll(@Param("ioSamples") List<IoSample> ioSamples);

    Integer deleteByProblemId(@Param("problemId") Integer problemId);


    //试卷使用的方法：根据编程题的id查询编程题的示例
    List<IoSampleBo> findTestPaperIoSampleByProblemId(@Param("id") Integer id);

    //试卷模块使用的方法：判断是否存在编程题对应的实例
    Integer countByProblemId(@Param("id")Integer id);


}
