package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.Solution;
import com.masonluo.mlonlinejudge.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author masonluo
 * @date 2020/12/27 2:13 下午
 */
@Repository
public interface SolutionRepository extends BaseMapper<Solution> {

    @Override
    int updateById(@Param("solution") Solution solution);

    List<Solution> listAll();

    List<Solution> listByUserId(@Param("userId") Integer userId);

    List<Solution> queryByUserIdList(@Param("userIdList")List<Integer> userIdList,@Param("studentName")String studentName);

    Integer judgeSuccessByProblemIDAndUserId(@Param("experimentId") Integer experimentId, @Param("problemId") Integer problemId, @Param("userId") Integer userId);

    Integer judgeDoneByProblemIDAndUserId(@Param("experimentId") Integer experimentId, @Param("problemId") Integer problemId, @Param("userId") Integer userId);

    //List<Integer>listPassProblem(@Param("problemIds")List<Integer>problemIds, @Param("userId") Integer userId);

    Integer countPassProblemByUserIdAndExperimentId(@Param("userId") Integer userId, @Param("experimentId") Integer experimentId);

    String findRecentSourceCode(@Param("experimentId") Integer experimentId, @Param("problemId") Integer problemId, @Param("userId") Integer userId);

    String findSourceCodeByResultId(@Param("resultId") Integer resultId);
}
