package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.UserInfo;
import com.masonluo.mlonlinejudge.model.bo.UserInfoBo;
import com.masonluo.mlonlinejudge.model.bo.UserRankBo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author masonluo
 * @date 2020/12/27 2:13 下午
 */
@Repository
public interface UserInfoRepository extends BaseMapper<UserInfo> {

    @Select("select * from t_user_info where user_id=#{userId}")
    UserInfo getUserInfoByUserId(@Param("userId") Integer userId);

//    LoginInfoBo getLoginInfoBoByUserId(@Param("userId")Integer userId);

    int insertStudentInfoList(@Param("userInfoList") List<UserInfo> userInfoList);

    int insertTeacherOrSchoolAdminInfo(@Param("userInfoList") List<UserInfo> userInfoList);

    int insertSuperAdminInfo(@Param("userInfoList") List<UserInfo> userInfoList);

    int updateByUserId(UserInfo userInfo);

    List<UserInfo> getBatchByPrimaryKey(List<Integer> userIdList);

    List<UserInfoBo> queryPageUserInfoByRole(@Param("roleId") Integer roleId, @Param("pageNum") Integer pageNum,@Param("pageSize") Integer pageSize);

    List<UserInfoBo> queryPageUserInfoByUserIdList(@Param("userIdList") List<Integer> userIdList,@Param("pageNum")Integer pageNum,@Param("pageSize") Integer pageSize);

    List<UserInfoBo> queryUserBoBySchool(@Param("schoolId")Integer schoolId,@Param("roleId")Integer roleId,@Param("pageNum") Integer pageNum,@Param("pageSize") Integer pageSize);

    List<Integer> queryUserIdListBySchool(@Param("schoolId")Integer schoolId);

    Integer getTotalBySchoolId(@Param("schoolId")Integer schoolId,@Param("roleId")Integer roleId);

    void updateUserLevel(@Param("userId")Integer userId);

    List<UserRankBo> queryPageRankBySchoolId(@Param("schoolId") Integer schoolId, @Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);

//    List<UserInfoBo> getUserInfoByClassIdAndName(@Param("classId") Integer classId, @Param("usernameKey") String usernameKey, @Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);

    Integer getTotalByClassIdAndUserOrNick(@Param("classId") Integer classId, @Param("usernameKey") String usernameKey,@Param("nicknameKey") String nicknameKey);
}

