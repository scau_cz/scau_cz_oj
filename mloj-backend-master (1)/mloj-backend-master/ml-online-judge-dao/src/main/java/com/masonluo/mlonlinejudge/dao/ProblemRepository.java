package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.masonluo.mlonlinejudge.entity.Experiment;
import com.masonluo.mlonlinejudge.entity.Problem;
import com.masonluo.mlonlinejudge.model.bo.ProblemBo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author masonluo
 * @date 2020/12/27 2:13 下午
 */
@Repository
public interface ProblemRepository extends BaseMapper<Problem> {

    int countById(@Param("id") Integer id);

    List<Problem> listByIds(@Param("ids") List<Integer> ids);

    List<Problem> listAll();

    List<Integer> getAllProblemId(@Param("pageSize")Integer pageSize, @Param("pageNum")Integer pageNum);

    List<Problem> listByUserId(@Param("userId") Integer userId);

    IPage<Problem> searchByTag(IPage<Problem> page, @Param("tag") String tag);

    IPage<Problem> searchByTitle(IPage<Problem> page, @Param("title") String title);

    IPage<Problem> searchProblem(IPage<Problem> page, @Param("tag") String tag, @Param("title") String title);

    IPage<Problem> searchProblemByLevel(IPage<Problem> page,@Param("level")Integer level);

    List<Problem> searchByTagAndTitle(@Param("tag") String tag, @Param("title") String title);

    Integer countAll();

    Integer countByIndexId(@Param("indexId") Integer indexId);

    Integer countByUserId(@Param("userId") Integer userId);

    Integer countByTag(@Param("tag") String tag);

    Integer countByTitle(@Param("title") String title);

    Integer countByTagAndTitle(@Param("tag") String tag, @Param("title") String title);

    Integer countByLevel(@Param("level")Integer level);
    //ProblemBo updateProblemById(ProblemBo problemBo);
}
