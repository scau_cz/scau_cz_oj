//package com.masonluo.mlonlinejudge.dao;
//
//import com.baomidou.mybatisplus.core.mapper.BaseMapper;
//import com.masonluo.mlonlinejudge.entity.Administrator;
//import com.masonluo.mlonlinejudge.model.param.AdministratorParam;
//import org.apache.ibatis.annotations.Param;
//import org.springframework.stereotype.Repository;
//
///**
// * <p>
// * 用户账号密码表 Mapper 接口
// * </p>
// *
// * @author yangqi
// * @since 2021-09-10
// */
//@Repository
//public interface AdministratorRepository extends BaseMapper<Administrator> {
//
//    int update(@Param("administratorParam")AdministratorParam administratorParam);
//
//}
