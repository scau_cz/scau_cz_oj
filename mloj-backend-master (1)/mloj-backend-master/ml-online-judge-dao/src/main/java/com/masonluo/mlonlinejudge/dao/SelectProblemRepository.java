package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.masonluo.mlonlinejudge.entity.Problem;
import com.masonluo.mlonlinejudge.entity.SelectProblem;
import com.masonluo.mlonlinejudge.model.param.SelectProblemParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SelectProblemRepository extends BaseMapper<SelectProblem> {
    /**
    * @Description: 根据indexId/userId查询题目数量
    * @Param:indexId/userId
    * @return:integer
    */
    Integer countByIndexId(@Param("indexId") Integer indexId);

    Integer countByUserId(@Param("userId") Integer userId);

    Integer countAll();

    Integer countByTag( @Param("tag") String tag);

    Integer countByTitle(@Param("title") String title);

    Integer countByAll(@Param("tag") String tag, @Param("title") String title, @Param("type") Integer type);

    List<SelectProblem> getSelectProblem();

//    List<SelectProblem> searchByTag(@Param("tag") String tag);

    List<SelectProblem> searchByTitle(@Param("title") String title);

//    List<SelectProblem> searchByType(@Param("type") Integer type);

    List<SelectProblem> searchByTagAndTitle(@Param("tag") String tag, @Param("title") String title);

    IPage<SelectProblem> searchProblem(IPage<SelectProblem> page, @Param("tag") String tag, @Param("title") String title, @Param("type") Integer type);

    int updateTestCaseIdById(@Param("problemId")Integer problemId,@Param("testCaseId") String testCaseId);

    String getTestCaseIdById(@Param("problemId") Integer problemId);

    String getProblemAnswer(Integer problemId);
}