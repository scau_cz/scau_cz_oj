package com.masonluo.mlonlinejudge.typehandler;

import com.masonluo.mlonlinejudge.enums.ExecError;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

/**
 * @author masonluo
 * @date 2020/12/27 5:46 下午
 */
@MappedJdbcTypes(JdbcType.INTEGER)
@MappedTypes(ExecError.class)
public class ExecErrorEnumTypeHandler extends BaseEnumTypeHandler<ExecError> {
    @Override
    protected Class<ExecError> getEnumClass() {
        return ExecError.class;
    }
}
