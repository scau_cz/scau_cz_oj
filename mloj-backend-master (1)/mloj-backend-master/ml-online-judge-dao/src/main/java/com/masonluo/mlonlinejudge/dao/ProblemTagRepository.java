package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.Problem;
import com.masonluo.mlonlinejudge.entity.ProblemTag;
import com.masonluo.mlonlinejudge.entity.Tag;
import com.masonluo.mlonlinejudge.entity.User;
import com.masonluo.mlonlinejudge.model.bo.TestPaperTagBo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author masonluo
 * @date 2020/12/27 2:13 下午
 */
@Repository
public interface ProblemTagRepository extends BaseMapper<ProblemTag> {
    List<Problem> findProblemByTagName(@Param("tagName") String tagName);

    List<Tag> findTagByProblemId(@Param("problemId") Integer problemId);

    List<Tag> findTagBySelectProblemId(@Param("problemId") Integer problemId);

    int insertAll(@Param("problemTags") List<ProblemTag> problemTags);

    int countById(@Param("id") Integer id);

    int deleteByProblemId(@Param("problemId") Integer problemId, @Param("type") Integer type);


    //testpaper
    List<TestPaperTagBo> findTestPaperTagByProblemId(@Param("id")Integer id);//试卷接口新加的，根据问题id得到tag的id和name
    Integer countByProblemId(@Param("id")Integer id);
}
