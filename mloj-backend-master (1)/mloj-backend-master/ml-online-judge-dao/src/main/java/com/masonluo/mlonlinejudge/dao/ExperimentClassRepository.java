package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.masonluo.mlonlinejudge.entity.Experiment;
import com.masonluo.mlonlinejudge.entity.ExperimentClass;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;
@Repository
public interface ExperimentClassRepository extends BaseMapper<ExperimentClass> {

    IPage<Experiment> findExperimentByClassId(IPage<Experiment> page,@Param("map") Map map);
}
