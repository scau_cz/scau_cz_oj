package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.Book;
import com.masonluo.mlonlinejudge.model.param.EditBookParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/2/10 3:19 下午
 */
@Repository
public interface BookRepository extends BaseMapper<Book> {
    int countById(@Param("id") Integer id);

    List<Book> selectAll();

    int update(@Param("book") EditBookParam editBookParam);
}
