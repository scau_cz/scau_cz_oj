package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.masonluo.mlonlinejudge.entity.Experiment;
import com.masonluo.mlonlinejudge.entity.ExperimentProblem;
import com.masonluo.mlonlinejudge.entity.ExperimentSchool;
import com.masonluo.mlonlinejudge.entity.Tag;
import com.masonluo.mlonlinejudge.model.bo.ExperimentBo;
import com.masonluo.mlonlinejudge.model.dto.ExperimentScoreResultDto;
import com.masonluo.mlonlinejudge.model.vo.ExperimentIdAndNameVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Repository
public interface ExperimentSchoolRepository extends BaseMapper<ExperimentSchool> {

    List<ExperimentBo> findExperimentBySchoolIdAndStatus(@Param("schoolId") Integer schoolId, @Param("status")String status, @Param("pageNum")Integer pageNum, @Param("pageSize")Integer pageSize );

    List<ExperimentIdAndNameVo> findExperimentIdAndNameBySchoolIdAndStatus(@Param("schoolId") Integer schoolId, @Param("status")String status);

    IPage<Experiment> findExperimentBySchoolId(IPage<Experiment> page,@Param("schoolId") Integer schoolId);

    Integer findIdBySchoolIdAndExperimentId(@Param("schoolId") Integer schoolId,@Param("experimentId")Integer experimentId);

    List<Integer> findSchoolIdsByExperimentId(@Param("experimentId")Integer experimentId);

    List<String> findSchoolNamesByExperimentId(@Param("experimentId")Integer experimentId);

    List<ExperimentScoreResultDto> listExperimentsByUserId(@Param("userId")Integer userId);

    List<ExperimentSchool> selectByExperimentIdsAndSchoolId(@Param("schoolId") Integer schoolId,@Param("experimentIds")List<Integer> experimentIds, @Param("withWeight")boolean withWeight);

    ExperimentSchool selectByExperimentIdAndSchoolId(@Param("schoolId") Integer schoolId,@Param("experimentId")Integer experimentId);

    Integer selectWeightByExperimentIdAndSchoolId(@Param("schoolId") Integer schoolId,@Param("experimentId")Integer experimentId);
}
