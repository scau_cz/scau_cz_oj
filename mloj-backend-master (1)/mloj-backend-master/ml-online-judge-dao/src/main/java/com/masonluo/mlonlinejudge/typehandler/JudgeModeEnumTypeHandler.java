package com.masonluo.mlonlinejudge.typehandler;

import com.masonluo.mlonlinejudge.enums.ExecError;
import com.masonluo.mlonlinejudge.enums.JudgeMode;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

/**
 * @author masonluo
 * @date 2020/12/27 8:44 下午
 */
@MappedJdbcTypes(JdbcType.INTEGER)
@MappedTypes(JudgeMode.class)
public class JudgeModeEnumTypeHandler extends BaseEnumTypeHandler<JudgeMode> {
    @Override
    protected Class<JudgeMode> getEnumClass() {
        return JudgeMode.class;
    }
}
