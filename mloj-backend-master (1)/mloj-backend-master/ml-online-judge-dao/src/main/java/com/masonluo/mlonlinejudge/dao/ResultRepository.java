package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.Result;
import com.masonluo.mlonlinejudge.entity.SolutionResultJoinTestWithTime;
import com.masonluo.mlonlinejudge.entity.UserInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @author masonluo
 * @date 2020/12/27 2:13 下午
 */
@Repository
public interface ResultRepository extends BaseMapper<Result> {

    List<Result> listAll();

    UserInfo queryUserInfoByResultId(@Param("resultId") Integer resultId);

    List<Integer> queryResultIdListByProblemId(@Param("problemId")Integer problemId,@Param("userId")Integer userId,@Param("testId")Integer testId);

    String queryProblemTitleByResultId(@Param("resultId") Integer resultId);

    List<Integer> queryResultListByUserId(@Param("userId") Integer userId, @Param("startTime") Date startTime,@Param("endTime") Date endTime);

    List<Integer> queryResultListBySchoolIdAndNickName(@Param("schoolId")Integer schoolId,@Param("nickname")String nickname, @Param("startTime") Date startTime,@Param("endTime") Date endTime);

    void insertBatch(List<SolutionResultJoinTestWithTime> list);
}
