package com.masonluo.mlonlinejudge.typehandler;

import com.masonluo.mlonlinejudge.enums.Language;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

/**
 * @author masonluo
 * @date 2021/2/6 2:17 下午
 */
@MappedJdbcTypes(JdbcType.TINYINT)
@MappedTypes(Language.class)
public class LanguageEnumTypeHandler extends BaseEnumTypeHandler<Language> {
    @Override
    protected Class<Language> getEnumClass() {
        return Language.class;
    }
}
