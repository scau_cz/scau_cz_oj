package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.ExamAnswerSheet;
import com.masonluo.mlonlinejudge.model.param.SubmitParam;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ExamAnswerSheetRepository extends BaseMapper<ExamAnswerSheet> {

    int insertIntoExamAnswerSheet(ExamAnswerSheet examAnswerSheet);

    Integer judgeExist(SubmitParam submitParam);

    List<ExamAnswerSheet> queryExamAnswerSheet(@Param("userId")Integer userId, @Param("examId")Integer examId);
}
