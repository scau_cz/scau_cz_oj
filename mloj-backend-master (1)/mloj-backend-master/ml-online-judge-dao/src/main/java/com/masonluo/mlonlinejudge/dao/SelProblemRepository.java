package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.SelProblem;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangqi
 * @since 2021-11-10
 */
@Repository
public interface SelProblemRepository extends BaseMapper<SelProblem> {
    Integer selectByIdType(@Param("id") Integer id,@Param("type") Integer type);

//    List<SelProblemDetailDto> getSelProblem(@Param("id") Integer id,@Param("type")Integer type);
    Integer problemExist(@Param("id")Integer id,@Param("type")Integer type);
}
