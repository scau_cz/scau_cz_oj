package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.*;
import com.masonluo.mlonlinejudge.model.bo.SelectProblemBo;
import com.masonluo.mlonlinejudge.model.dto.ExperimentScoreResultDto;
import com.masonluo.mlonlinejudge.model.param.ProblemParam;
import com.masonluo.mlonlinejudge.model.vo.ProblemNameAndIdVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ExperimentProblemRepository extends BaseMapper<ExperimentProblem> {

    int countById(@Param("id") Integer id);

    int insertAll(@Param("experimentProblems") List<ExperimentProblem> experimentProblems);

    Integer findIdByExperimentIdAndProblemId(@Param("experimentId")Integer experimentId, @Param("problemId")Integer problemId);

    int deleteExperimentProblemByExperimentId(@Param("experimentId")Integer experimentId);

    List<Integer> findProblemIdsByExperimentId(@Param("experimentId") Integer experimentId);

    Integer countByExperimentId(@Param("experimentId") Integer experimentId);

    List<Integer> listProblemIdsByExperimentId(@Param("experimentId") Integer experimentId, @Param("category")String category);

    List<ProblemParam> findProblemByExperimentId(@Param("experimentId") Integer experimentId, @Param("category")String category);

    List<SelectProblemBo> findSelectProblemByExperimentId(@Param("experimentId") Integer experimentId);

    List<ExperimentProblemResult> findProblemNameAndIdByExperimentId(@Param("experimentId") Integer experimentId);

    List<Problem> findNotUpdatedProblems(@Param("experimentId") Integer experimentId, @Param("schoolId")Integer schoolId, @Param("category")String category);

    List<Problem> findUpdatedProblems(@Param("experimentId") Integer experimentId, @Param("schoolId")Integer schoolId, @Param("category")String category);





}
