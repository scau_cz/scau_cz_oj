package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.ExamSchoolClass;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExamSchoolClassRepository extends BaseMapper<ExamSchoolClass> {

    int create(ExamSchoolClass examSchoolClass);

    List<ExamSchoolClass> selectListBySchoolId(@Param("schoolId") Integer schoolId);
    List<ExamSchoolClass> selectListByClassId(@Param("classId") Integer classIdS);
    List<ExamSchoolClass> selectListByExamId(@Param("examId") Integer examId);
    int deleteByExamId(@Param("examId")Integer examId);
}
