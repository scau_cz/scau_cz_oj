package com.masonluo.mlonlinejudge.dao;


import com.masonluo.mlonlinejudge.entity.UserClass;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserClassRepository {
    int deleteByPrimaryKey(Integer id);

    int insert(UserClass record);

    int insertBatch(@Param("userClassList") List<UserClass> userClassList);

    int insertSelective(UserClass record);

    UserClass selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserClass record);

    int updateByPrimaryKey(UserClass record);

    int updateByUserId(UserClass userClass);

    int updateUserClass(@Param("classId") Integer classId,@Param("teacherId") Integer teacherId,@Param("newTeacherId") Integer newTeacherId);

    @Select("select user_id from t_user_class where class_id = #{classId}")
    List<Integer> listUserIdsByClassId(@Param("classId")Integer classId);

    @Select("select class_id from t_user_class where user_id = #{userId}")
    Integer findClassIdByUserId(@Param("userId")Integer userId);

   /* @Select("select distinct user_id from t_experiment_result  where class_id = #{classId} and user_id not in(select id from t_user)")
    List<Integer> findDeletedUserIds(@Param("classId")Integer classId);*/
}