package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.ProgramAnswerEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 编程题答案表 Mapper 接口
 * </p>
 *
 * @author lzc
 * @since 2021-12-20
 */
@Repository
public interface ProgramAnswerRepository extends BaseMapper<ProgramAnswerEntity> {
    List<ProgramAnswerEntity> findAnswer(String testCaseId);
    int insertIntoProgramAnswer(ProgramAnswerEntity programAnswerEntity);

    int insertProgramAnswer(ProgramAnswerEntity programAnswerEntity);

    int deleteProgramAnswer(@Param("testCaseId")String testCaseId);
    int updateProgramAnswer(String testCaseId,int problemId);
    int updateInputContent(String testCaseId,int problemId,String inputContent);
    ProgramAnswerEntity findAnswerByProblemId(Integer problemId);
}
