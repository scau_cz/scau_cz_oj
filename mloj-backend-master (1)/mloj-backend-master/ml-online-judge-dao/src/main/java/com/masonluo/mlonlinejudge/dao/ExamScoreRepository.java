package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.ExamScore;
import com.masonluo.mlonlinejudge.model.dto.ClassExamDto;
import com.masonluo.mlonlinejudge.model.dto.ExamScoreAndClassDto;
import com.masonluo.mlonlinejudge.model.dto.ExamScoreDto;
import com.masonluo.mlonlinejudge.model.dto.SchoolExamDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 考试得分表 Mapper 接口
 * </p>
 *
 * @author yangqi
 * @since 2021-10-10
 */
@Repository
public interface ExamScoreRepository extends BaseMapper<ExamScore> {
    List<ExamScoreDto> findStudentScore(@Param("studentId") Integer studentId);
    List<ExamScoreDto> findAll(@Param("current") Integer current,@Param("limit") Integer limit);
    List<ExamScoreDto> findByExamAndClass(@Param("examId") Integer examId,@Param("classId") Integer classId,
                                          @Param("current") Integer current,@Param("limit") Integer limit);
//    List<ExamScoreDto> findByExamAndExperiment(@Param("experimentId") Integer experimentId,@Param("classId") Integer classId,
//                                               @Param("current") Integer current,@Param("limit") Integer limit);


    Integer getTotalAllStudentExam(@Param("current") Integer current,@Param("limit") Integer limit);
    List<ClassExamDto> findExamByClassId(@Param("classId") Integer classId,@Param("schoolId")Integer schoolId);

    List<ClassExamDto> findExamBySchoolId(@Param("schoolId")Integer schoolId);

    Integer getTotalStudentExam(@Param("examId") Integer examId,@Param("classId") Integer classId,
                                @Param("current") Integer current,@Param("limit") Integer limit);
    List<SchoolExamDto> findExamInExamIds(@Param("examIds")Integer[] examIds);

    List<ClassExamDto> findExamTitleByClassIds(@Param("classIds")Integer[] classIds);

    List<ExamScoreAndClassDto> findExamScoreAndClass(@Param("current") Integer current,@Param("limit") Integer limit,
                                                     @Param("classIdList")List<Integer> classIdList,@Param("className")String className,
                                                     @Param("examTitle")String examTitle);

    Integer countExamByClassIds(@Param("classIds")Integer[] classIds);
    //TODO
    List<ExamScoreAndClassDto> findExamScoreWithoutLimit(@Param("examTitle") String examTitle, @Param("className") String className, @Param("classIdList") List<Integer> classIdList);

}
