package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.Task;
import com.masonluo.mlonlinejudge.model.bo.TaskBo;
import com.masonluo.mlonlinejudge.model.bo.TaskWithClassInfoBo;
import com.masonluo.mlonlinejudge.model.dto.TaskProblemSituationDto;
import com.masonluo.mlonlinejudge.model.param.EditTaskParam;
import com.masonluo.mlonlinejudge.model.param.SubmitParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends BaseMapper<Task> {

    //int countById(@Param("id") Integer taskId);

    TaskBo findTaskById(@Param("taskId") Integer taskId);

    List<TaskWithClassInfoBo> findTaskWithClassInfoBoById(@Param("taskId") Integer taskId);

    //List<Task> selectList();

    int create(@Param("task") TaskBo task);

    int updateTask(@Param("task") EditTaskParam task);

    Integer judgeExist(SubmitParam submitParam);

    List<TaskProblemSituationDto> queryTaskProblemSituation(@Param("userId")Integer userId,@Param("examId")Integer examId);
}
