package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.Index;
import com.masonluo.mlonlinejudge.model.bo.SchoolBo;
import com.masonluo.mlonlinejudge.model.param.IndexParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/2/10 3:19 下午
 */
@Repository
public interface IndexRepository extends BaseMapper<Index> {
    int countById(Integer id);

    List<Index> listByBookId(@Param("bookId") Integer bookId);

    List<Index> listBySchoolId(@Param("schoolId") Integer schoolId);

    int deleteById(@Param("id") Integer id);

    List<Index> listAll();


    @Override
    int insert(@Param("index") Index index);

    int update(@Param("index") IndexParam indexParam);


}
