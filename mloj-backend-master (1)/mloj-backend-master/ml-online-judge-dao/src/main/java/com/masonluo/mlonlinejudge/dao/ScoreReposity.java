package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.Score;
import com.masonluo.mlonlinejudge.model.param.ScoreParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 学生成绩表 Mapper 接口
 * </p>
 *
 * @author yangqi
 * @since 2021-08-19
 */
@Repository
public interface ScoreReposity extends BaseMapper<Score> {

    int update(@Param("scoreParam") ScoreParam scoreParam);
}
