package com.masonluo.mlonlinejudge.dao;

import com.masonluo.mlonlinejudge.entity.AcRecord;
import org.apache.ibatis.annotations.Param;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper(componentModel = "spring")
public interface AcRecordRepository {

    List<Integer> findByUserIdAndProblemId(@Param("userId") Integer userId);

    void insertIntoAcRecord(@Param("userId") Integer userId,@Param("problemId") Integer problemId);
}
