package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.School;
import com.masonluo.mlonlinejudge.model.bo.SchoolBo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchoolRepository extends BaseMapper<School> {
    int countById(@Param("id") Integer id);

    //int update(@Param("school") SchoolBo schoolBo);

    SchoolBo findSchoolById(@Param("schoolId") Integer schoolId);

    SchoolBo findSchoolByName(@Param("schoolName") String schoolName);

    Integer countAll();

    List<Integer> selectByPrimaryKey(@Param("schoolIdList") List<Integer> schoolIdList);

    Integer uploadSchoolBackground(@Param("schoolId") Integer schoolId,@Param("schoolBackground") String schoolBackground);

    @Override
    int insert(School school);

//    List<Integer> queryschoolIdListByName(@Param("schoolId") Integer schoolId,@Param("schoolNameKey") String schoolNameKey);

    List<SchoolBo> findSchoolListByName(@Param("schoolNameKey") String schoolNameKey,@Param("pageNum") Integer pageNum,@Param("pageSize") Integer pageSize);

    Integer getTotalByNameKey( @Param("schoolNameKey") String schoolNameKey);
}
