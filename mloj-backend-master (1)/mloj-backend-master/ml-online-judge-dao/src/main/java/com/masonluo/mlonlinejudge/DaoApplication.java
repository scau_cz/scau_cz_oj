package com.masonluo.mlonlinejudge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author masonluo
 * @date 2020/12/28 5:07 下午
 */
public class DaoApplication {
    public static void main(String[] args) {
        SpringApplication.run(DaoApplication.class);
    }
}
