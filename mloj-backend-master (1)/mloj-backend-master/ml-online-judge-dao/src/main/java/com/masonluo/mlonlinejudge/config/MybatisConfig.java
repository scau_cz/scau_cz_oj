package com.masonluo.mlonlinejudge.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author masonluo
 * @date 2020/12/28 4:56 下午
 */
@Configuration
@MapperScan(basePackages = {"com.masonluo.mlonlinejudge.dao"})
public class MybatisConfig {

}
