package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.ExperimentScore;
import com.masonluo.mlonlinejudge.entity.ExperimentScoreResult;
import com.masonluo.mlonlinejudge.model.dto.ExperimentScoreResultDto;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 实验结果表 Repository接口
 * </p>
 *
 * @author LOTP
 * @since 2022-3-05
 */
@Repository
public interface ExperimentResultRepository extends BaseMapper<ExperimentScoreResult> {


    Integer findByExperimentAndUserId(@Param("userId")Integer userId, @Param("experimentId")Integer experimentId);

    List<ExperimentScoreResult> findScoreByUserId(@Param("userId") Integer userId);

    List<ExperimentScoreResult> findScoreByUserIds(@Param("userIds") List<Integer> userIds);

    List<ExperimentScoreResultDto>findExperimentByClassIdAndSchoolId(@Param("classId") Integer classId,@Param("schoolId")Integer schoolId);

    List<ExperimentScore>findScoreByClassIdAndSchoolId(@Param("classId") Integer classId, @Param("schoolId")Integer schoolId);

    boolean insertExperimentScoreResult(@Param("experimentScoreResults")List<ExperimentScoreResult> experimentScoreResults,@Param("schoolId") Integer school,@Param("classId") Integer classId);

    boolean updateExperimentScoreResult(@Param("experimentScoreResults")List<ExperimentScoreResult> experimentScoreResults);

    @Select("select count(distinct user_id) from t_experiment_result where school_id = #{schoolId} and class_id = #{classId}")
    Integer countBySchoolIdAndClassId(@Param("schoolId")Integer schoolId, @Param("classId")Integer classId);

    ExperimentScoreResult findScoreByUserIdAndExperimentId(@Param("userId") Integer userId,@Param("experimentId") Integer experimentId);

    @Select("select experiment_id from t_experiment_result  where user_id = #{userId} and experiment_id not in(select id from t_experiment)")
    List<Integer> findDeletedExperimentIds(@Param("userId") Integer userId);

    boolean deleteByExperimentIds(@Param("experimentIds") List<Integer> experimentIds);

    boolean deleteByUserIds(@Param("userIds") List<Integer> userIds);

    @Delete("delete from t_experiment_result where class_id = #{classId} ")
    boolean deleteByClassId(@Param("classId") Integer classId);

    @Update("UPDATE t_experiment_result SET experiment_score = #{experimentScore} WHERE experiment_id = #{id};")
    boolean updateExperimentById(@Param("id")Integer experimentId, @Param("experimentScore") String experimentScore);

    @Update("UPDATE t_experiment_result SET user_name = #{nickName} WHERE user_id = #{userId};")
    boolean updateUserNameByUserId(@Param("userId") Integer userId, @Param("nickName")String nickName);
}
