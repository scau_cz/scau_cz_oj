package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.Role;
import com.masonluo.mlonlinejudge.entity.User;
import com.masonluo.mlonlinejudge.entity.UserRole;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author masonluo
 * @date 2020/12/27 2:13 下午
 */
@Repository
public interface UserRoleRepository extends BaseMapper<UserRole> {

    @Select("select role_id from t_user_role where user_id=#{userId}")
    Integer getRoleIdByUserId(@Param("userId") Integer userId);

    int insertUserRole(UserRole userRole);

    @Select("select user_id from t_user_info where role_id=#{roleId}")
    List<Integer> getUserIdByRole(@Param(("roleId")) Integer roleId);

    Integer getTotalByRole(@Param("roleId") Integer roleId);

    Integer insertBatch(@Param("userRoleList") List<UserRole> userRoleList);

}
