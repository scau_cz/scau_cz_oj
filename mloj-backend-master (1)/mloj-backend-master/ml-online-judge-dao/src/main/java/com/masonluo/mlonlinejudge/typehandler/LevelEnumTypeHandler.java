package com.masonluo.mlonlinejudge.typehandler;

import com.masonluo.mlonlinejudge.enums.Level;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

/**
 * @author masonluo
 * @date 2020/12/27 8:45 下午
 */
@MappedJdbcTypes(JdbcType.INTEGER)
@MappedTypes(Level.class)
public class LevelEnumTypeHandler extends BaseEnumTypeHandler<Level> {
    @Override
    protected Class<Level> getEnumClass() {
        return Level.class;
    }
}
