package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.SolutionResultJoinTest;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author 罗有冠
 * @date 2022/3/14 22:31
 */
@Repository
public interface SolutionResultJoinTestRepository extends BaseMapper<SolutionResultJoinTest> {
    void insertResultJoinTest(@Param("userId")Integer userId, @Param("mode")Integer mode,@Param("examAndExperimentId") Integer examAndExperimentId,@Param("problemId") Integer problemId,@Param("solutionId") Integer solutionId,@Param("resultId") Integer resultId);
}
