package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.TestCaseResult;
import com.masonluo.mlonlinejudge.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author masonluo
 * @date 2020/12/27 2:13 下午
 */
@Repository
public interface TestCaseResultRepository extends BaseMapper<TestCaseResult> {
    int insertAll(@Param("results") List<TestCaseResult> results);
}
