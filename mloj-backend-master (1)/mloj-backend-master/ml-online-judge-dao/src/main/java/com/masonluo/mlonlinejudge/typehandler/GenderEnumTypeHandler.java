package com.masonluo.mlonlinejudge.typehandler;

import com.masonluo.mlonlinejudge.enums.Gender;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

/**
 * @author masonluo
 * @date 2020/12/27 5:49 下午
 */
@MappedJdbcTypes(JdbcType.INTEGER)
@MappedTypes(Gender.class)
public class GenderEnumTypeHandler extends BaseEnumTypeHandler<Gender> {
    @Override
    protected Class<Gender> getEnumClass() {
        return Gender.class;
    }
}
