package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.TestPaper;
import com.masonluo.mlonlinejudge.model.bo.ProblemBo;
import com.masonluo.mlonlinejudge.model.bo.TestPaperBo;
import com.masonluo.mlonlinejudge.model.dto.*;
import com.masonluo.mlonlinejudge.model.param.UpdateTestPaperParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * <p>
 * 试卷表 Repository 接口
 * </p>
 *
 * @author yangqi
 * @since 2021-10-28
 */
@Repository
public interface TestPaperRepository extends BaseMapper<TestPaper> {
    Integer addTestPaper(@Param("testpaper")TestPaperDto testPaperDto);
    Integer selectId(@Param("testpaper") TestPaperDto testPaperDto);
    Integer addTestSelProblem(@Param("paperId") Integer paperId,
                              @Param("SelProblem")TestPaperSelProblem testPaperSelProblem,
                              @Param("type")Integer type);

    List<TestPaperBo> pageTestPaper(@Param("current")Integer current,@Param("limit")Integer limit);

    Integer getTotal();

    Integer deletePaperProblem(@Param("id") Integer id);

    Integer paperExist(@Param("id") Integer id);

    Integer paperProblemExist(@Param("id") Integer id);

    List<TestPaperBo> findPaperByName(@Param("name") String name,@Param("current") Integer current,@Param("limit") Integer limit);

    Integer getTotalByName(@Param("name")String name);

    Integer[] findExamId(@Param("id")Integer id);

    TestPaperBo findPaperDetail(@Param("id")Integer id);

    List<SelProblemDetailDto> getSelProblemDetail(@Param("id")Integer id,@Param("type")Integer type);

    List<ProblemDetailDto> getProgrammingQuestionsDetail(@Param("id")Integer id);

    Integer updateTitle(@Param("testPaper") UpdateTestPaperParam updateTestPaperParam);

    Integer updateScore(@Param("testPaper")UpdateTestPaperParam updateTestPaperParam);

//    Integer updateTime(@Param("testPaper")UpdateTestPaperParam updateTestPaperParam);

    Integer updateProblem(@Param("paperId") Integer paperId,@Param("problem")UpdateTPaperProblemDto updateTPaperProblemDto);

    Integer updateDescription(@Param("testPaper")UpdateTestPaperParam updateTestPaperParam);

    List<Integer> queryProgrammingQuestionsByExamId(@Param("examId")Integer examId);
}
