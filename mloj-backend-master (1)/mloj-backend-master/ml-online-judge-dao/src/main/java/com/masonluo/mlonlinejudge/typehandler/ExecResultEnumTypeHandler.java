package com.masonluo.mlonlinejudge.typehandler;

import com.masonluo.mlonlinejudge.enums.ExecError;
import com.masonluo.mlonlinejudge.enums.ExecResult;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

/**
 * @author masonluo
 * @date 2020/12/27 5:46 下午
 */
@MappedJdbcTypes(JdbcType.INTEGER)
@MappedTypes(ExecResult.class)
public class ExecResultEnumTypeHandler extends BaseEnumTypeHandler<ExecResult> {
    @Override
    protected Class<ExecResult> getEnumClass() {
        return ExecResult.class;
    }
}
