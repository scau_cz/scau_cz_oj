package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author masonluo
 * @date 2020/12/27 2:13 下午
 */
@Repository
public interface UserRepository extends BaseMapper<User> {
     int countByUsername(String username);

     int countById(Integer id);

     Integer findIdByUsername(@Param("username") String username);

     @Select("select username from t_user where id=#{userId}")
     String findUsernameById(@Param("userId") Integer userId);

     @Select("SELECT school_name FROM t_user_info left join t_school s on school_id = s.id where user_id=#{userId}")
     String findUserSchoolById(@Param("userId")Integer userId);

     List<User> selectByUsernameAndPassword(@Param("username") String username,
                                      @Param("password") String password);

    @Select("select * from t_user where username=#{username}")
     User getUserByUsername(@Param("username") String username);

    @Select("select * from t_user where id=#{userId} and status=1")
     User getUserByPrimaryKey(@Param("userId") Integer userId);

    @Select("select * from t_user where id=#{userId} or username=#{name}")
    User getUserByIdOrName(@Param("userId") Integer userId,@Param("name") String name);

     int insertUserList(@Param("userList")List<User> userList);

     @Override
     int insert(User user);

     int deleteById(@Param("id") Integer id);

    List<Integer> queryUserIdListByNickOrName(@Param("schoolId")Integer schoolId,@Param("usernameKey")String usernameKey,@Param("nicknameKey")String nicknameKey, @Param("roleId")Integer roleId);

    Integer getTotalByNickOrNameKey (@Param("schoolId")Integer schoolId,@Param("usernameKey")String usernameKey,@Param("nicknameKey")String nicknameKey, @Param("roleId")Integer roleId);

    int updateByPrimaryKey(User user);

    List<String> selectByUsername(@Param("usernameList") List<String> username);

    List<Integer> getUserIdListByUsernameOrnickName(@Param("classId") Integer classId, @Param("usernameKey")String usernameKey, @Param("nicknameKey")String nicknameKey);
}
