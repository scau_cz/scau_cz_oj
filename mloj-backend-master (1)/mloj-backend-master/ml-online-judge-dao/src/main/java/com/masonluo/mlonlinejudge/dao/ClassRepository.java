package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.Class;
import com.masonluo.mlonlinejudge.model.bo.ClassFindByTeacherBo;
import com.masonluo.mlonlinejudge.model.bo.ClassQueryBySchoolBo;
import com.masonluo.mlonlinejudge.model.bo.StudentBo;
import com.masonluo.mlonlinejudge.model.param.EditClassParam;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 班级表 Mapper 接口
 * </p>
 *
 * @author yangqi
 * @since 2021-08-18
 */
@Repository
public interface ClassRepository extends BaseMapper<Class> {
    Integer addClass(Class newClass);

    Integer updateClassName(@Param("class") EditClassParam editClassParam);

    @Select("select * from t_class where id=#{classId}")
    Class selectByPrimaryKey(@Param("classId") Integer classId);

    List<Integer> selectByPrimaryKeyList(@Param("classIdList") List<Integer> classId);

    List<Integer> queryClassIdListBySchoolId(@Param("schoolId") Integer id);

    List<ClassQueryBySchoolBo> queryClassListByClassIdList(@Param("classIdList") List<Integer> classIdList,
                                                           @Param("current") Integer current, @Param("limit") Integer limit);

    Integer getTotalBySchoolId(@Param("schoolId") Integer id);

    List<Integer> findClassIdListByTeacherId(@Param("teacherId") Integer id);

    List<ClassFindByTeacherBo> findClassIdListByClassId(@Param("classIdList") List<Integer> classIdList);

    Integer getClassIdByName(@Param("className") String className);

    List<StudentBo> getStudentByClassId(@Param("classId") Integer classId, @Param("teacherId") Integer teacherId,
                                        @Param("current") Integer current, @Param("limit") Integer limit);

    Integer insertUserClass(@Param("teacherId") Integer teacherId, @Param("classId") Integer classId);

    Integer selectIdByOther(@Param("className") String className, @Param("schoolId") Integer schoolId);

    Integer deleteUserClass(@Param("classId") Integer classId);

    Class selectByUserId(@Param("userId") Integer userId);

    List<Class> selectClassListByUserId(@Param("userId") Integer userId);

    Integer deleteOneUserClass(@Param("classId") Integer classId, @Param("teacherId") Integer teacherId);

    List<Class> pageClass(@Param("current") Integer current, @Param("limit") Integer limit);

    Integer getTotalClass();

    Integer classExist(@Param("className") String className, @Param("schoolId") Integer schoolId);

    Integer checkUserRole(@Param("userId") Integer userId);

    Integer getTotalStudentByClassId(@Param("teacherId") Integer teacherId, @Param("classId") Integer classId);

    List<Integer> getTotalClassIdList();

    Integer ClassHasExam(Integer classId);

    Integer ClassHasExperiment(Integer classId);
}
