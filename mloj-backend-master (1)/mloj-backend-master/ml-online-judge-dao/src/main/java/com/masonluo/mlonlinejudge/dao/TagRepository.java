package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.Tag;
import com.masonluo.mlonlinejudge.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author masonluo
 * @date 2020/12/27 2:13 下午
 */
@Repository
public interface TagRepository extends BaseMapper<Tag> {

    Tag selectByName(@Param("tagName") String tagName);

    Integer countByName(@Param("tagName") String tagName);

    Integer deleteByTagName(@Param("tagName") String tagName);

    List<Tag> selectAll();
}
