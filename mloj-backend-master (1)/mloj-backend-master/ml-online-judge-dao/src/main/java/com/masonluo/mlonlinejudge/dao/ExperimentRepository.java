package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.Experiment;
import com.masonluo.mlonlinejudge.model.bo.ExperimentBo;
import com.masonluo.mlonlinejudge.model.vo.ExperimentVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2021-10-04 23:36
 **/
@Repository
public interface ExperimentRepository extends BaseMapper<Experiment> {
    int countAll();

    int countByAdmin();

    int countBySchoolId(@Param("schoolId")Integer schoolId,@Param("status")String status);

    List<Experiment>  listAll(@Param("current")Integer pageNum,@Param("limit")Integer pageSize);

    List<Experiment>  listByIds(@Param("ids") List<Integer> ids);

    int countById(@Param("id") Integer id);

    int countByClassIdAndSchoolId(@Param("classId")Integer classId, @Param("schoolId")Integer schoolId);

    ExperimentBo findByExperimentIdAndSchoolId(@Param("id")Integer experimentId, @Param("schoolId")Integer schoolId);

    List<Experiment> findNotRelatedSchool(@Param("schoolId")Integer schoolId, @Param("pageNum")Integer pageNum, @Param("pageSize") Integer pageSize);

    int countNotRelatedSchool(@Param("schoolId")Integer schoolId);

    String findNameByExperimentId(@Param("id")Integer id);
}
