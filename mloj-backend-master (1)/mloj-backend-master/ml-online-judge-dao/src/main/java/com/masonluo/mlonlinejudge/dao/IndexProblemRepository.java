package com.masonluo.mlonlinejudge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.masonluo.mlonlinejudge.entity.IndexProblem;
import com.masonluo.mlonlinejudge.model.vo.ProblemNameAndIdVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/2/10 3:20 下午
 */
@Repository
public interface IndexProblemRepository extends BaseMapper<IndexProblem> {
    int insertAll(@Param("indexProblems") List<IndexProblem> indexProblems);

    /**
     * 偷个懒，直接返回Vo对象，以后请仅仅返回DO
     */
    List<ProblemNameAndIdVo> findProblemByIndexId(@Param("indexId") Integer indexId);

    int deleteByIndexIdAndProblemId(@Param("indexId") Integer indexId,
                                    @Param("problemId") Integer problemId);
}
