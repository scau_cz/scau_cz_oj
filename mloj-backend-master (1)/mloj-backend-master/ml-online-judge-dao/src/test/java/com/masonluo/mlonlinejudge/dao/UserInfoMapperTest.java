package com.masonluo.mlonlinejudge.dao;

import com.masonluo.mlonlinejudge.entity.UserInfo;
import com.masonluo.mlonlinejudge.enums.Gender;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;

/**
 * @author masonluo
 * @date 2020/12/27 5:50 下午
 */
@SpringBootTest
class UserInfoMapperTest {

    private UserInfoRepository userInfoMapper;

    @Test
    public void testUserInfoInsert() {
        UserInfo userInfo = new UserInfo();
        userInfo.setGender(Gender.FEMALE);
        userInfo.setUserId(2);
        userInfo.setNickname("hello");
        userInfoMapper.insert(userInfo);
    }

    @Test
    public void testUserInfoSelect() {
        UserInfo userInfo = userInfoMapper.selectById(1);
        System.out.println(userInfo);
    }

    @Test
    public void testSystemVariable() {
        Map<String, String> map = System.getenv();
        System.out.println(map.get("MLOJ_HOME"));
    }
}