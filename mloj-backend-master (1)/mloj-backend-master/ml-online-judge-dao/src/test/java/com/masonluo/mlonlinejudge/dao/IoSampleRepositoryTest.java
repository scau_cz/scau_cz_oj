package com.masonluo.mlonlinejudge.dao;

import com.masonluo.mlonlinejudge.entity.IoSample;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author masonluo
 * @date 2021/1/26 3:50 下午
 */
@SpringBootTest
class IoSampleRepositoryTest {

    @Autowired
    private IoSampleRepository ioSampleRepository;

    @Test
    public void testInsertAll() {
        List<IoSample> ioSamples = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            IoSample sample = new IoSample();
            sample.setProblemId(i);
            sample.setInputSample("input" + i);
            sample.setOutputSample("output" + i);
            ioSamples.add(sample);
        }
        ioSampleRepository.insertAll(ioSamples);
    }
}