package com.masonluo.mlonlinejudge.dao;

import com.masonluo.mlonlinejudge.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author masonluo
 * @date 2020/12/27 2:15 下午
 */
@SpringBootTest
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testInsert() {
        User user = new User();
        user.setUsername("hello3");
        user.setPassword("password");
        userRepository.insert(user);
        System.out.println(user);
    }
}