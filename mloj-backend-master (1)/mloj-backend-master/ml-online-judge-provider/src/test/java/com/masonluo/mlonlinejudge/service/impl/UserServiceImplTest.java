package com.masonluo.mlonlinejudge.service.impl;

import com.masonluo.mlonlinejudge.enums.Gender;
import com.masonluo.mlonlinejudge.model.dto.UserDto;
import com.masonluo.mlonlinejudge.model.dto.UserInfoDto;
import com.masonluo.mlonlinejudge.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author masonluo
 * @date 2020/12/29 3:51 下午
 */
@SpringBootTest
class UserServiceImplTest {
    @Resource
    public UserService userService;

    @Test
    public void testRegister() {
        UserInfoDto userInfoDto = userService.register("Masonluo", "junquan..0918..");
        System.out.println(userInfoDto);
    }

    @Test
    public void testLogin() {
//        LoginDto loginDto = userService.login("Masonluo", "junquan..0918..");
//        System.out.println(loginDto);
    }

    @Test
    void addUserList() {
        UserDto userDto1 =new UserDto();
        userDto1.setUsername("zhangsan");
        userDto1.setPassword("123456");
        userDto1.setNickname("zhangsanNickname");
        userDto1.setPhone("123456");
        userDto1.setGender(Gender.MALE);
        userDto1.setRole("TEACHER");


        UserDto userDto2 =new UserDto();
        userDto2.setUsername("zhangsan");
        userDto2.setPassword("123456");
        userDto2.setNickname("zhangsanNickname");
        userDto2.setPhone("123456");
        userDto2.setGender(Gender.MALE);
        userDto2.setRole("TEACHER");
        List<UserDto> userDtoList =new ArrayList<>();
        userDtoList.add(userDto1);
        userDtoList.add(userDto2);
        List<String> nameDuplicate = userService.checkUserNameDuplicate(userDtoList);
        userService.addUserList(userDtoList, nameDuplicate);
    }
}