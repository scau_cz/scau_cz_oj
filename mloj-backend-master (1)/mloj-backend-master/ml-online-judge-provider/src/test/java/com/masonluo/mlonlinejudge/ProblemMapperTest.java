package com.masonluo.mlonlinejudge;

import com.masonluo.mlonlinejudge.entity.Problem;
import com.masonluo.mlonlinejudge.enums.Level;
import com.masonluo.mlonlinejudge.mapper.ProblemMapper;
import com.masonluo.mlonlinejudge.model.bo.ProblemBo;
import com.masonluo.mlonlinejudge.model.dto.ProblemUploadDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author masonluo
 * @date 2021/1/25 3:19 下午
 */
@SpringBootTest
class ProblemMapperTest {

    @Autowired
    private ProblemMapper problemMapper;
//
//    @Test
//    public void problemUploadDtoToProblem() {
//        ProblemUploadDto dto = new ProblemUploadDto();
//        dto.setDescription("description");
//        dto.setHint("hint");
//        dto.setInputDescription("input_description");
//        dto.setLevel(0);
//        Problem problem = problemMapper.problemUploadDtoToProblem(dto);
//        System.out.println(problem);
//    }


    @Test
    public void problemFromBO(){
        ProblemBo problemBO=new ProblemBo();
        problemBO.setTitle("test");
        problemBO.setLevel(0);
        Problem problem=problemMapper.from(problemBO);
        System.out.println(problem);
        System.out.println(problem.getLevel() instanceof Level);
    }
}