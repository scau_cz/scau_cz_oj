package com.masonluo.mlonlinejudge.service.impl;

import com.masonluo.mlonlinejudge.entity.TestCaseResult;
import com.masonluo.mlonlinejudge.enums.ExecError;
import com.masonluo.mlonlinejudge.enums.ExecResult;
import com.masonluo.mlonlinejudge.service.TestCaseResultService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

/**
 * @author masonluo
 * @date 2021/2/7 10:30 下午
 */
@SpringBootTest
class TestCaseResultServiceImplTest {

    @Autowired
    private TestCaseResultService testCaseResultService;

    @Test
    public void testInsertAll() {
        TestCaseResult testCaseResult = new TestCaseResult();
        testCaseResult.setTestCase(1);
        testCaseResult.setSolutionId(1);
        testCaseResult.setCpuTime(1000);
        testCaseResult.setMemory(1000);
        testCaseResult.setRealTime(2);
        testCaseResult.setError(ExecError.SUCCESS);
        testCaseResult.setResult(ExecResult.SUCCESS);
        testCaseResult.setOutput("1");
        testCaseResult.setOutputMd5("1");
        testCaseResult.setRealTime(1);
        testCaseResult.setExitCode(0);
        testCaseResult.setSignal(10);
        testCaseResultService.saveAll(Arrays.asList(testCaseResult));
    }
}