package com.masonluo.mlonlinejudge.config;

import com.masonluo.mlonlinejudge.model.cache.TokenCacheMap;
import com.masonluo.mlonlinejudge.model.cache.TokenCache;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Map;

/**
 * @author masonluo
 * @date 2021/1/20 5:08 下午
 */
@SpringBootTest
class RedisConfigTest {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void genericJackson2SerializerTest() {
        TokenCacheMap map = new TokenCacheMap();
        TokenCache tokenCache = new TokenCache("12", 1L, 10L, "hello");
        map.setToken(tokenCache);
        map.setUsername("hello");
        redisTemplate.opsForHash().putAll("Hello", map);
        Map res = redisTemplate.opsForHash().entries("Hello");
        TokenCacheMap normalTokenCacheMap = new TokenCacheMap(res);
        System.out.println(normalTokenCacheMap.getToken());
    }
}