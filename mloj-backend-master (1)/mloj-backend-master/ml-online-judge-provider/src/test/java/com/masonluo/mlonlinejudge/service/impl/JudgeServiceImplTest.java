package com.masonluo.mlonlinejudge.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.masonluo.mlonlinejudge.exceptions.HttpRequestFailureException;
import com.masonluo.mlonlinejudge.model.compile.factory.JavaLanguageConfigFactory;
import com.masonluo.mlonlinejudge.model.dto.JudgeParamDto;
import com.masonluo.mlonlinejudge.service.JudgeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author masonluo
 * @date 2021/2/5 10:11 下午
 */
@SpringBootTest
class JudgeServiceImplTest {
    @Autowired
    JudgeService judgeService;

    @Test
    public void testJudgeService() throws HttpRequestFailureException, JsonProcessingException {
        JudgeParamDto dto = new JudgeParamDto();
        dto.setSrc("import java.util.Scanner;public class Main { public static void main(String[] args) { int a, b; Scanner input = new Scanner(System.in); a = input.nextInt();b = input.nextInt();System.out.print(a + b + 1);}}");
        dto.setLanguageConfig(new JavaLanguageConfigFactory().getLanguageConfig());
        dto.setMaxCpuTime(5000);
        dto.setMaxMemory(524328969);
        dto.setTestCaseId("2");
        dto.setOutput(true);
        judgeService.judge(dto);
    }

}