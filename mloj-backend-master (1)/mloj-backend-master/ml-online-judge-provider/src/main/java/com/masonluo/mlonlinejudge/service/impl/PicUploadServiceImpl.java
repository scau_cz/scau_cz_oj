package com.masonluo.mlonlinejudge.service.impl;


import com.masonluo.mlonlinejudge.config.TC_COS_Config;
import com.masonluo.mlonlinejudge.dao.SchoolRepository;
import com.masonluo.mlonlinejudge.result.PicUploadResult;
import com.masonluo.mlonlinejudge.result.Result;
import com.masonluo.mlonlinejudge.service.PicUploadService;
import com.masonluo.mlonlinejudge.utils.RandomUtils;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.exception.CosClientException;
import com.qcloud.cos.exception.CosServiceException;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;

/**
 * 图片上传服务
 * 上传至腾讯云COS(对象存储中心)
 * @author WZT
 */
@Service
public class PicUploadServiceImpl implements PicUploadService {
    @Autowired
    private COSClient cosClient;

    @Autowired
    private TC_COS_Config tc_cos_config;

    @Autowired
    private SchoolRepository schoolRepository;

    // 允许上传的格式
    private static final String[] IMAGE_TYPE = new String[]{".bmp", ".jpg", ".jpeg", ".gif", ".png"};

    public PicUploadResult uploadProblem(MultipartFile uploadFile){
        // 校验图片格式
        boolean isLegal = false;
        for(String type : IMAGE_TYPE){
            if(StringUtils.endsWithIgnoreCase(uploadFile.getOriginalFilename(),type)){
                isLegal = true;
                break;
            }
        }
        // 封装Result对象，并且将文件的byte数组放置到result对象中
        PicUploadResult fileUploadResult = new PicUploadResult();
        Result fileResult = new Result();
        if(!isLegal){
            fileUploadResult.setStatus("error:illegal file");
            return fileUploadResult;
        }
        // 文件新路径
        String fileName = uploadFile.getOriginalFilename();
        String filePath = "/proimages/" + getFilePath(fileName);
        //上传文件
        try {
            // 指定要上传到的存储桶
            String bucketName = tc_cos_config.getBucketName();
            // 指定要上传到 COS 上对象键
            String key = filePath;
            //这里的key是查找文件的依据，妥善保管
            cosClient.putObject(bucketName,key,new ByteArrayInputStream(uploadFile.getBytes()),null);
            //设置输出信息
            fileUploadResult.setStatus("done");
            fileUploadResult.setName(this.tc_cos_config.getPath()+filePath);
            fileUploadResult.setUid(String.valueOf(System.currentTimeMillis()));
            return fileUploadResult;
        }
        catch (Exception e){
            e.printStackTrace();
            fileUploadResult.setStatus("error: uploadFail");
            return fileUploadResult;
        }

    }

    @Override
    public PicUploadResult uploadBackground(Integer schoolId, MultipartFile multipartFile) {
        // 校验图片格式
        boolean isLegal = false;
        for(String type : IMAGE_TYPE){
            if(StringUtils.endsWithIgnoreCase(multipartFile.getOriginalFilename(),type)){
                isLegal = true;
                break;
            }
        }
        // 封装Result对象，并且将文件的byte数组放置到result对象中
        PicUploadResult fileUploadResult = new PicUploadResult();
        Result fileResult = new Result();
        if(!isLegal){
            fileUploadResult.setStatus("error:illegal file");
            return fileUploadResult;
        }
        // 文件新路径
        String fileName = multipartFile.getOriginalFilename();
        String filePath = "/backgroundimages/" + getFilePath(fileName);
        //上传文件
        try {
            // 指定要上传到的存储桶
            String bucketName = tc_cos_config.getBucketName();
            // 指定要上传到 COS 上对象键
            String key = filePath;
            //这里的key是查找文件的依据，妥善保管
            cosClient.putObject(bucketName,key,new ByteArrayInputStream(multipartFile.getBytes()),null);
            //设置输出信息
            fileUploadResult.setStatus("done");
            fileUploadResult.setName(this.tc_cos_config.getPath()+filePath);
            fileUploadResult.setUid(String.valueOf(System.currentTimeMillis()));
            schoolRepository.uploadSchoolBackground(schoolId, fileUploadResult.getName());
            return fileUploadResult;
        }
        catch (Exception e){
            e.printStackTrace();
            fileUploadResult.setStatus("error: uploadFail");
            return fileUploadResult;
        }
    }

    /**
     * 删除某个图片
     * @param key
     */
    public boolean deletePic(String key){
        try {
            // 指定对象所在的存储桶
            String bucketName = this.tc_cos_config.getBucketName();
            cosClient.deleteObject(bucketName, key);
            return true;
        } catch (CosServiceException serverException) {
            serverException.printStackTrace();
            return false;
        } catch (CosClientException clientException) {
            clientException.printStackTrace();
            return false;
        }
    }

    /**
     * 生成文件路径
     * @param sourceFileName
     * @return
     */
    private static String getFilePath(String sourceFileName) {
        DateTime dateTime = new DateTime();
        return dateTime.toString("yyyy") + "/" + dateTime.toString("MM")
                + "/" + dateTime.toString("dd")
                + "/" + System.currentTimeMillis()
                + RandomUtils.randomInt(100, 9999) + "."
                + StringUtils.substringAfterLast(sourceFileName, ".");

    }
}
