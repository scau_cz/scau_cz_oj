package com.masonluo.mlonlinejudge.service.impl;

import com.masonluo.mlonlinejudge.enums.TokenType;
import com.masonluo.mlonlinejudge.security.Token;
import com.masonluo.mlonlinejudge.service.TokenService;
import com.masonluo.mlonlinejudge.utils.TokenUtils;
import org.springframework.stereotype.Service;

;

/**
 * @author masonluo
 * @date 2020/12/28 4:16 下午
 */
@Service
public class TokenServiceImpl implements TokenService {

    @Override
    public String generateToken(String username, TokenType tokenType, long timestamp) {
        return TokenUtils.generateToken(username, tokenType, timestamp);
    }

    @Override
    public String generateToken(String username, TokenType tokenType) {
        return generateToken(username, tokenType, System.currentTimeMillis());
    }

    @Override
    public Token convert(String base64Token) {
        return TokenUtils.convert(base64Token);
    }
}
