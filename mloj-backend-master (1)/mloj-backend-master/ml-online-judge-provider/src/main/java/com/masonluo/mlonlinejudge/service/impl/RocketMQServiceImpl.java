package com.masonluo.mlonlinejudge.service.impl;
import com.masonluo.mlonlinejudge.exceptions.ServerProcessException;
import com.masonluo.mlonlinejudge.service.RocketMQService;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.springframework.stereotype.Service;

/**
 * @author masonluo
 * @date 2021/2/5 3:06 下午
 */
@Service
public class RocketMQServiceImpl implements RocketMQService {

    private final DefaultMQProducer producer;

    public RocketMQServiceImpl(DefaultMQProducer producer) {
        this.producer = producer;
    }

    @Override
    public SendResult syncSend(String topic, String tag, byte[] message) {
        try {
            Message msg = new Message(topic, tag, message);
            return producer.send(msg);
        } catch (MQClientException | RemotingException | MQBrokerException | InterruptedException e) {
            e.printStackTrace();
            throw new ServerProcessException("Can't send message to rocket mq", e);
        }
    }
}
