package com.masonluo.mlonlinejudge.service.impl;

import com.masonluo.mlonlinejudge.dao.TestCaseResultRepository;
import com.masonluo.mlonlinejudge.entity.TestCaseResult;
import com.masonluo.mlonlinejudge.service.TestCaseResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/2/7 3:11 下午
 */
@Service
public class TestCaseResultServiceImpl implements TestCaseResultService {
    @Autowired
    private TestCaseResultRepository testCaseResultRepository;

    @Override
    public int saveAll(List<TestCaseResult> testCaseResults) {
        return testCaseResultRepository.insertAll(testCaseResults);
    }
}
