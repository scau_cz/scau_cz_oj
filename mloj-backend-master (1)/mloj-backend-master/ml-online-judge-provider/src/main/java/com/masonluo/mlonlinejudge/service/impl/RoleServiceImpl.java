package com.masonluo.mlonlinejudge.service.impl;

import com.masonluo.mlonlinejudge.dao.RoleRepository;
import com.masonluo.mlonlinejudge.entity.Role;
import com.masonluo.mlonlinejudge.exceptions.ResourceNotFoundException;
import com.masonluo.mlonlinejudge.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author masonluo
 * @date 2021/5/6 5:51 下午
 */
@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role findByUserId(Integer userId) {
        Role role = roleRepository.findByUserId(userId);
        if (Objects.isNull(role)) {
            return null;
        }
        return role;
    }
    @Override
    public Role findById(Integer userId){
        Role role = roleRepository.selectById(userId);
        if(role==null){
            throw new ResourceNotFoundException("Fail to find the role,please confirm the role exists");
        }
        return role;
    }
}
