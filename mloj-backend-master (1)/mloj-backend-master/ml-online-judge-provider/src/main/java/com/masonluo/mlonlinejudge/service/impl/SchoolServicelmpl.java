package com.masonluo.mlonlinejudge.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.masonluo.mlonlinejudge.dao.SchoolRepository;
import com.masonluo.mlonlinejudge.entity.School;
import com.masonluo.mlonlinejudge.exceptions.ResourceNotFoundException;
import com.masonluo.mlonlinejudge.mapper.SchoolMapper;
import com.masonluo.mlonlinejudge.model.bo.SchoolBo;
import com.masonluo.mlonlinejudge.model.bo.UserInfoBo;
import com.masonluo.mlonlinejudge.model.dto.UserInfoDto;
import com.masonluo.mlonlinejudge.model.param.SchoolQueryParam;
import com.masonluo.mlonlinejudge.model.vo.SchoolVo;
import com.masonluo.mlonlinejudge.service.SchoolService;
import com.masonluo.mlonlinejudge.utils.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2021-08-13 12:42
 **/
@Service
public class SchoolServicelmpl implements SchoolService {

    @Autowired
    private SchoolMapper schoolMapper;
    @Autowired
    private SchoolRepository schoolRepository;


    @Override
    public Integer createSchool(SchoolVo schoolVo) {
        if (!checkEmail(schoolVo.getEmail())){
            throw new ResourceNotFoundException("Fail to Create the School entity, please confirm the email correct");
        }
        SchoolBo bo = schoolRepository.findSchoolByName(schoolVo.getSchoolName());
        if (bo != null){
            throw new ResourceNotFoundException(String.format("The school [%s] has already existed.", schoolVo.getSchoolName()));
        }
        School school = schoolMapper.voToDo(schoolVo);
        schoolRepository.insert(school);
        return school.getId();
    }

    @Override
    public SchoolBo findSchoolById(Integer id) {
        SchoolBo schoolBo = schoolRepository.findSchoolById(id);
        if (schoolBo == null){
            throw new ResourceNotFoundException(String.format("The school [%d] does not exist.", id));
        }
        //return schoolMapper.doToBo(school);
        return schoolBo;
    }

    @Override
    public SchoolBo findSchoolByName(String schoolName) {
        SchoolBo schoolBo = schoolRepository.findSchoolByName(schoolName);
        if (schoolBo == null){
            throw new ResourceNotFoundException(String.format("The school [%s] does not exist.", schoolName));
        }
        return schoolBo;
    }

    @Override
    public List<SchoolBo> findSchoolListByName(SchoolQueryParam param) {
        System.out.println(param.getSchoolNameKey());
        List<SchoolBo> schoolBoList = schoolRepository.findSchoolListByName(param.getSchoolNameKey(), param.getPageNum(), param.getPageSize());
        System.out.println(schoolBoList);
        return schoolBoList;
    }

    @Override
    public boolean exist(Integer id) {
        return schoolRepository.countById(id) > 0;
    }

    @Override
    public List<SchoolBo> findList(Integer pageNum, Integer pageSize) {
        /*List<School> schools = schoolRepository.findList();
        List<SchoolBo> schoolBos = schoolMapper.doToBo(schools);*/
        IPage<School> page = new Page<>(pageNum, pageSize);
        IPage<School>schoolIPage = schoolRepository.selectPage(page, null);
        if(schoolIPage.getRecords().size() != 0 ) {
            List<SchoolBo> schools = new ArrayList<>();
            if (!CollectionUtils.isEmpty(schoolIPage.getRecords())) {
                schools = (List<SchoolBo>) page.getRecords().stream().map(school -> {
                    return schoolMapper.doToBo(school);
                }).collect(Collectors.toList());
            }
            return schools;
        }else {
            throw new ResourceNotFoundException(String.format("The pageNum [%d] does not exist.", pageNum));
        }
    }

    @Override
    public void delete(Integer schoolId) {
        schoolRepository.deleteById(schoolId);
    }

    @Override
    public SchoolBo update(SchoolBo schoolBo) {
        if (!checkEmail(schoolBo.getEmail())){
            throw new ResourceNotFoundException("Fail to update the School entity, please confirm the email correct");
        }
        School school = schoolMapper.from(schoolBo);
        if (schoolRepository.updateById(school) > 0){
            return schoolRepository.findSchoolById(schoolBo.getId());
        }
        throw new ResourceNotFoundException("Fail to update the School entity, please confirm the school exist");
    }


    @Override
    public int countAll() {
        return schoolRepository.countAll();
    }

    @Override
    public Integer getTotalByName(String schoolNameKey) {
        return schoolRepository.getTotalByNameKey(schoolNameKey);
    }

    public Boolean checkEmail(String email){
        String regex="^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
        boolean match=email.matches(regex);
        if(match) {
            return true;
        }
        else {
            return false;
        }
    }
}


