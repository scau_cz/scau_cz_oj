package com.masonluo.mlonlinejudge.service.impl;

import com.masonluo.mlonlinejudge.service.FileUploadService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @author masonluo
 * @date 2021/1/25 3:48 下午
 */
@Service
public class FileUploadServiceImpl implements FileUploadService {

    @Override
    public boolean upload(MultipartFile file, String dir) throws IOException {
        File uploadDir = new File(dir);
        if (!uploadDir.exists()) {
            uploadDir.mkdirs();
        }
        String rootPath = uploadDir.getPath();
        File res = new File(rootPath + "/" + file.getOriginalFilename());
        if (!res.exists()) {
            file.transferTo(res);
            return true;
        }
        return false;
    }
}
