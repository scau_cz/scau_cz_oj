package com.masonluo.mlonlinejudge.service.impl;

import com.masonluo.mlonlinejudge.dao.ProgramAnswerRepository;
import com.masonluo.mlonlinejudge.entity.ProgramAnswerContent;
import com.masonluo.mlonlinejudge.entity.ProgramAnswerEntity;
import com.masonluo.mlonlinejudge.model.Pair;

import com.masonluo.mlonlinejudge.service.ProgramAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author LZC
 * @create 2021-12-21 21:39
 * 保存测试用例的答案
 */
@Service
public class ProgramAnswerServiceImpl implements ProgramAnswerService {
    @Autowired
    ProgramAnswerRepository programAnswerRepository;
    public ProgramAnswerServiceImpl(ProgramAnswerRepository programAnswerRepository){
        this.programAnswerRepository = programAnswerRepository;
    }
    @Override
    public int insertAnswer(String testCaseId, List<Pair<String, String>> answers,List<Pair<String,String>> inputContents) {
        ProgramAnswerEntity programAnswerEntity = new ProgramAnswerEntity();
        programAnswerEntity.setTest_case_id(testCaseId);
        programAnswerEntity.setAnswer_content(getJsonString(answers));
        programAnswerEntity.setInput_content(getJsonString(inputContents));
        return  programAnswerRepository.insertIntoProgramAnswer(programAnswerEntity);
    }

    @Override
    public int insertProgramAnswer(String testCaseId, List<Pair<String, String>> answers,
                                   List<Pair<String,String>> inputContents,Integer problemId){
        ProgramAnswerEntity programAnswerEntity = new ProgramAnswerEntity();
        programAnswerEntity.setTest_case_id(testCaseId);
        programAnswerEntity.setAnswer_content(getJsonString(answers));
        programAnswerEntity.setInput_content(getJsonString(inputContents));
        programAnswerEntity.setProblem_id(problemId);
        return  programAnswerRepository.insertProgramAnswer(programAnswerEntity);
    }
    //把输入、输出文件的内容转为json
    private String getJsonString(List<Pair<String, String>> answers) {
        if(answers==null||answers.size()==0)return "";
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[");
        for(Pair<String,String> pair: answers){
            if(pair.getSecond()==null||"".equals(pair.getSecond()))//输入文件内容为空，直接返回
                return "";
            ProgramAnswerContent content = new ProgramAnswerContent();
            content.setId(Integer.parseInt(pair.getFirst()));
            content.setAnswer(pair.getSecond());
            stringBuffer.append(content.toString());
            stringBuffer.append(",");
        }
        stringBuffer = stringBuffer.deleteCharAt(stringBuffer.length()-1);
        stringBuffer.append("]");
        return stringBuffer.toString();
    }

    @Override
    public int updateAnswer(String testCaseId, Integer problemId) {
        return programAnswerRepository.updateProgramAnswer(testCaseId,problemId);
    }

    @Override
    public List<ProgramAnswerEntity> findAnswer(String testCaseId) {
        List<ProgramAnswerEntity> list = programAnswerRepository.findAnswer(testCaseId);
        return list;
    }

    @Override
    public ProgramAnswerEntity findAnswerByProblemId(Integer problemId) {
        ProgramAnswerEntity entity = programAnswerRepository.findAnswerByProblemId(problemId);
        return entity;
    }


    @Override
    public boolean deleteProgramAnswer(String testCaseId){
        return programAnswerRepository.deleteProgramAnswer(testCaseId)>0;
    }
}
