package com.masonluo.mlonlinejudge.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.masonluo.mlonlinejudge.dao.ScoreReposity;
import com.masonluo.mlonlinejudge.entity.Score;
import com.masonluo.mlonlinejudge.model.param.ScoreParam;
import com.masonluo.mlonlinejudge.service.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 学生成绩表 服务实现类
 * </p>
 *
 * @author yangqi
 * @since 2021-08-19
 */
@Service
public class ScoreServiceImpl extends ServiceImpl<ScoreReposity, Score> implements ScoreService {
    @Autowired
    private ScoreReposity scoreReposity;
    @Override
    public int update(ScoreParam scoreParam){
        return scoreReposity.update(scoreParam);
    }

}
