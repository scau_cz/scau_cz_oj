package com.masonluo.mlonlinejudge.config;

import com.masonluo.mlonlinejudge.utils.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author masonluo
 * @date 2021/1/23 12:23 下午
 */
@Configuration
public class SystemVariableConfig {
    @Bean("mlojHome")
    public String getHome() {
        String mlHome = System.getenv("MLOJ_HOME");
        if (StringUtils.isBlank(mlHome)) {
//            throw new IllegalStateException("MLOJ_HOME does not found, please set the system environment");
        mlHome="D:\\java";
        }

        return mlHome;
    }

    @Bean("testCaseDir")
    public String getTestCaseDir() {
        String dataHome = System.getenv("MLOJ_DATA_HOME");
        if (StringUtils.isBlank(dataHome)) {
//            throw new IllegalStateException("MLOJ_HOME does not found, please set the system environment");
            dataHome="D:\\java";
        }
        return dataHome + "/test_case";
    }

    @Bean("tmpDir")
    public String getTmpDir() {
        String mlHome = getHome();
        return mlHome + "/tmp";
    }
}
