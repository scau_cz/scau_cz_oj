package com.masonluo.mlonlinejudge.aspect;

import com.masonluo.mlonlinejudge.entity.Result;
import com.masonluo.mlonlinejudge.entity.Solution;
import com.masonluo.mlonlinejudge.enums.ExecResult;
import com.masonluo.mlonlinejudge.model.dto.JudgeResultDto;
import com.masonluo.mlonlinejudge.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * @author 罗有冠
 * @date 2022/1/17 0:21
 */
@Aspect
@Component
@Slf4j
public class RedisAspect {

    @Autowired
    private RedisUtil redisUtil;

    @After("execution(* com.masonluo.mlonlinejudge.service.impl.SolutionServiceImpl.judgeFailAspect(..)) && args(solution,examId)")
    public void judgeFailAspect(Solution solution, Integer examId) {
        log.info("评判失败,切点方法执行后");
        Integer userId = solution.getUserId();
        JudgeResultDto judgeResultDto = new JudgeResultDto(examId, solution.getProblemId(), false, null, ExecResult.SYSTEM_ERROR);
        redisUtil.lSet(userId.toString(), judgeResultDto, 14400);
    }

    @After("execution(* com.masonluo.mlonlinejudge.service.impl.SolutionServiceImpl.afterCompileFailAspect(..)) && args(solution,examId,errorData)")
    public void afterCompileFail(Solution solution, Integer examId, String errorData) {
        log.info("编译失败,切点方法执行后");
        Integer userId = solution.getUserId();
        JudgeResultDto judgeResultDto = new JudgeResultDto(examId, solution.getProblemId(), false, errorData, null);
        redisUtil.lSet(userId.toString(), judgeResultDto, 14400);
    }


    @After("execution(* com.masonluo.mlonlinejudge.service.impl.SolutionServiceImpl.afterJudgeSuccessAspect(..)) && args(solution,examId,result)")
    public void afterJudgeSuccessAspect(Solution solution, Integer examId, Result result) {
        log.info("评判ac,切点方法执行后");
        Integer userId = solution.getUserId();
        JudgeResultDto judgeResultDto = new JudgeResultDto(examId, solution.getProblemId(), true, null, result.getProcessResult());
        redisUtil.lSet(userId.toString(), judgeResultDto, 14400);
    }

}
