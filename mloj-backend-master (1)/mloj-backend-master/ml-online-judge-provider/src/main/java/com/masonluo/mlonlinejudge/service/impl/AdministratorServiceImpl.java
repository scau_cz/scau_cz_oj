//package com.masonluo.mlonlinejudge.service.impl;
//
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.masonluo.mlonlinejudge.entity.Administrator;
//import com.masonluo.mlonlinejudge.dao.AdministratorRepository;
//import com.masonluo.mlonlinejudge.mapper.AdministratorMapper;
//import com.masonluo.mlonlinejudge.model.bo.AdministratorBo;
//import com.masonluo.mlonlinejudge.model.dto.AdministratorDto;
//import com.masonluo.mlonlinejudge.model.param.AdministratorParam;
//import com.masonluo.mlonlinejudge.service.AdministratorService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
///**
// * <p>
// * 用户账号密码表 服务实现类
// * </p>
// *
// * @author yangqi
// * @since 2021-09-10
// */
//@Service
//public class AdministratorServiceImpl extends ServiceImpl<AdministratorRepository, Administrator> implements AdministratorService{
//    @Autowired
//    private AdministratorService administratorService;
//
//    @Autowired
//    private AdministratorRepository administratorRepository;
//
//    @Autowired
//    private AdministratorMapper administratorMapper;
//
//    @Override
//    public AdministratorBo createAdministrator(AdministratorDto administratorDto) {
//        Administrator administrator = administratorMapper.createDtoToDo(administratorDto);
//        administratorRepository.insert(administrator);
//        return administratorMapper.doToBo(administrator);
////        administratorRepository.update(administrator,null);
////        Administrator administrator= new Administrator();
////        administrator.setUsername(username);
////        administrator.setPassword(password);
////        administratorRepository.insert(administrator);
////        return administratorMapper.doToBo(administrator);
////        return null;
//    }
//
//    @Override
//    public int update(AdministratorParam administratorParam) {
//        return administratorRepository.update(administratorParam);
//    }
//
//
//
//}
