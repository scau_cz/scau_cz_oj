package com.masonluo.mlonlinejudge.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.masonluo.mlonlinejudge.dao.ClassRepository;
import com.masonluo.mlonlinejudge.dao.ExamScoreRepository;
import com.masonluo.mlonlinejudge.dao.UserInfoRepository;
import com.masonluo.mlonlinejudge.entity.ExamScore;
import com.masonluo.mlonlinejudge.entity.Role;
import com.masonluo.mlonlinejudge.model.bo.ClassFindByTeacherBo;
import com.masonluo.mlonlinejudge.model.dto.*;
import com.masonluo.mlonlinejudge.service.ClassService;
import com.masonluo.mlonlinejudge.service.ExamScoreService;
import com.masonluo.mlonlinejudge.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 考试得分表 服务实现类
 * </p>
 *
 * @author yangqi
 * @since 2021-10-10
 */
@Service
public class ExamScoreServiceImpl extends ServiceImpl<ExamScoreRepository, ExamScore> implements ExamScoreService {
    @Autowired
    private ExamScoreRepository examScoreRepository;
    @Autowired
    private ClassService classService;
    @Autowired
    private ClassRepository classRepository;
    @Autowired
    private RoleService roleService;
    @Autowired
    private UserInfoRepository userInfoRepository;
    @Override
    public List<ExamScoreDto> findStudentScore(Integer studentId){
        List<ExamScoreDto> examScoreDtos = examScoreRepository.findStudentScore(studentId);
        for(ExamScoreDto examScoreDto : examScoreDtos){
            examScoreDto.setTotalScore(examScoreDto.getSingleChoiceScore()+examScoreDto.getTrueFalseQuestionScore()+examScoreDto.getProgramQuestionScore());
        }
        return examScoreDtos;
    }

    @Override
    public Integer countExamByTeacherId(Integer teacherId) {
        Role role = roleService.findByUserId(teacherId);
        List<Integer> classIdList;
        if (role.getName().equals("superAdmin")){
            classIdList = classRepository.getTotalClassIdList();
        }else if (role.getName().equals("schoolAdmin")){
            classIdList = classRepository.queryClassIdListBySchoolId(userInfoRepository.getUserInfoByUserId(teacherId).getSchoolId());
        }else if (role.getName().equals("teacher")){
            classIdList = classRepository.findClassIdListByTeacherId(teacherId);
        }else {
            classIdList = new ArrayList<>(0);
        }
        //List<ClassFindByTeacherBo> getClassByTeacher = classService.getClassByTeacher(teacherId);
        Integer[] classIds = new Integer[classIdList.size()];
        int i =0;
        for(Integer classid : classIdList){
            classIds[i] = classid;
            i++;
        }
        if (classIds.length==0) return 0;
        return examScoreRepository.countExamByClassIds(classIds);
    }

    @Override
    public List<ExamScoreDto> findAll(Integer current,Integer limit){
        List<ExamScoreDto> examScoreDtos = examScoreRepository.findAll(current,limit);
        for(ExamScoreDto examScoreDto : examScoreDtos){
            examScoreDto.setTotalScore(examScoreDto.getSingleChoiceScore()+examScoreDto.getTrueFalseQuestionScore()+examScoreDto.getProgramQuestionScore());
        }
        return examScoreDtos;
    }
    @Override
    public List<ExamScoreDto> findByExamAndClass(Integer examId,Integer classId,Integer current,Integer limit){
        List<ExamScoreDto> examScoreDtos = examScoreRepository.findByExamAndClass(examId,classId,current,limit);
        for(ExamScoreDto examScoreDto : examScoreDtos){
            examScoreDto.setTotalScore(examScoreDto.getSingleChoiceScore()+examScoreDto.getTrueFalseQuestionScore()+examScoreDto.getProgramQuestionScore());
        }
        return examScoreDtos;
    }

    @Override
    public List<ClassExamDto> findExamByClassId(Integer classId,Integer schoolId){
        return examScoreRepository.findExamByClassId(classId,schoolId);
    }

    @Override
    public Integer getTotalStudentExam(Integer examId,Integer classId,Integer current,Integer limit){
        return examScoreRepository.getTotalStudentExam(examId,classId,current,limit);
    }

    @Override
    public List<SchoolExamDto> findExamBySchoolId(Integer schoolId) {
        List<ClassExamDto> classExamDtos =  examScoreRepository.findExamBySchoolId(schoolId);
        Integer[] examIds = new Integer[classExamDtos.size()];
        int i = 0;
        for(ClassExamDto classExamDto : classExamDtos){
            examIds[i] = classExamDto.getExamId();
            i++;
        }
        return examScoreRepository.findExamInExamIds(examIds);
    }

    @Override
    public ExamTitleDto findExamTitleAndClassNameByUserId(Integer userId) {
        ExamTitleDto examTitleDto = new ExamTitleDto();
        List<String> titleList = new ArrayList<>();
        List<String> classNameList = new ArrayList<>();
        List<ClassFindByTeacherBo> getClassByTeacher = classService.getClassByTeacher(userId);
        Integer[] classIds = new Integer[getClassByTeacher.size()];
        int i =0;
        for(ClassFindByTeacherBo classFindByTeacherBo : getClassByTeacher){
            classNameList.add(classFindByTeacherBo.getClassName());
            classIds[i] = classFindByTeacherBo.getId();
            i++;
        }
        for (ClassExamDto classExamDto : examScoreRepository.findExamTitleByClassIds(classIds)){
            titleList.add(classExamDto.getExamName());
        }
        examTitleDto.setTitleList(titleList);
        examTitleDto.setClassNameList(classNameList);
        return examTitleDto;
    }

    @Override
    public List<ExamScoreAndClassDto> findByTeacherId(Integer current, Integer limit, Integer teacherId,String examTitle, String className) {
        Role role = roleService.findByUserId(teacherId);
        List<Integer> classIdList;
        if (role.getName().equals("superAdmin")){
            classIdList = classRepository.getTotalClassIdList();
        }else if (role.getName().equals("schoolAdmin")){
            classIdList = classRepository.queryClassIdListBySchoolId(userInfoRepository.getUserInfoByUserId(teacherId).getSchoolId());
        }else if (role.getName().equals("teacher")){
            classIdList = classRepository.findClassIdListByTeacherId(teacherId);
        }else {
            classIdList = new ArrayList<>(0);
        }

        if (classIdList.isEmpty()) return new ArrayList<ExamScoreAndClassDto>();
        List<ExamScoreAndClassDto> examScoreAndClassDtoList = examScoreRepository.findExamScoreAndClass(current,limit,classIdList,className,examTitle);
        for(ExamScoreAndClassDto examScoreAndClassDto : examScoreAndClassDtoList){
            if(examScoreAndClassDto.getUserName()!=null) {
                examScoreAndClassDto.setTotalScore(examScoreAndClassDto.getProgramQuestionScore() + examScoreAndClassDto.getTrueFalseQuestionScore() + examScoreAndClassDto.getSingleChoiceScore());
            }
        }
//        for(int j=0;j<examScoreAndClassDtoList.size();j++){
//            if(examScoreAndClassDtoList.get(j).getUserName()==null) examScoreAndClassDtoList.remove(j--);
//        }
        return examScoreAndClassDtoList;
    }
    //TODO
    @Override
    public void exportScore(HttpServletResponse response, Integer teacherId, String examTitle, String className) {
        Role role = roleService.findByUserId(teacherId);
        List<Integer> classIdList;
        if (role.getName().equals("superAdmin")){
            classIdList = classRepository.getTotalClassIdList();
        }else if (role.getName().equals("schoolAdmin")){
            classIdList = classRepository.queryClassIdListBySchoolId(userInfoRepository.getUserInfoByUserId(teacherId).getSchoolId());
        }else if (role.getName().equals("teacher")){
            classIdList = classRepository.findClassIdListByTeacherId(teacherId);
        }else {
            classIdList = new ArrayList<>(0);
        }
        List<ExamScoreAndClassDto> resultList = examScoreRepository.
                findExamScoreWithoutLimit(examTitle, className,classIdList);
        for(ExamScoreAndClassDto examScoreAndClassDto : resultList){
            if(examScoreAndClassDto.getUserName()!=null) {
                examScoreAndClassDto.setTotalScore(examScoreAndClassDto.getProgramQuestionScore() + examScoreAndClassDto.getTrueFalseQuestionScore() + examScoreAndClassDto.getSingleChoiceScore());
            }
        }
        try {
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + "导出成绩表" + ".xlsx");
            ServletOutputStream outputStream=response.getOutputStream();
            EasyExcel.write(outputStream,ExamScoreAndClassDto.class).sheet().doWrite(()->{
                return resultList;
            });
            outputStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Integer countExam(Integer teacherId, String examTitle, String className) {
        Role role = roleService.findByUserId(teacherId);
        List<Integer> classIdList;
        if (role.getName().equals("superAdmin")){
            classIdList = classRepository.getTotalClassIdList();
        }else if (role.getName().equals("schoolAdmin")){
            classIdList = classRepository.queryClassIdListBySchoolId(userInfoRepository.getUserInfoByUserId(teacherId).getSchoolId());
        }else if (role.getName().equals("teacher")){
            classIdList = classRepository.findClassIdListByTeacherId(teacherId);
        }else {
            classIdList = new ArrayList<>(0);
        }
        long count = examScoreRepository.findExamScoreWithoutLimit(examTitle, className, classIdList).stream().count();
        return Math.toIntExact(count);
    }

    @Override
    public Integer getTotalAllStudentExam(Integer current, Integer limit) {
        return examScoreRepository.getTotalAllStudentExam(current, limit);
    }
}
