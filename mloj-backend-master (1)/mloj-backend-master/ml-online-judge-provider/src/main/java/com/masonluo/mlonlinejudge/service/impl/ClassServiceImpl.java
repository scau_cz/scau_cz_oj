package com.masonluo.mlonlinejudge.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.masonluo.mlonlinejudge.dao.ClassRepository;
import com.masonluo.mlonlinejudge.dao.UserClassRepository;
import com.masonluo.mlonlinejudge.entity.Class;
import com.masonluo.mlonlinejudge.exceptions.ResourceNotFoundException;
import com.masonluo.mlonlinejudge.mapper.ClassMapper;
import com.masonluo.mlonlinejudge.model.bo.ClassBo;
import com.masonluo.mlonlinejudge.model.bo.ClassFindByTeacherBo;
import com.masonluo.mlonlinejudge.model.bo.ClassQueryBySchoolBo;
import com.masonluo.mlonlinejudge.model.bo.StudentBo;
import com.masonluo.mlonlinejudge.model.dto.ClassQueryBySchoolDto;
import com.masonluo.mlonlinejudge.model.param.ClassParam;
import com.masonluo.mlonlinejudge.model.param.EditClassParam;
import com.masonluo.mlonlinejudge.service.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 班级表 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-08-18
 */
@Service
public class ClassServiceImpl extends ServiceImpl<ClassRepository, Class> implements ClassService {
    @Autowired
    private ClassRepository classRepository;
    @Autowired
    private ClassMapper classMapper;

    @Autowired
    private UserClassRepository userClassRepository;

    @Override
    public ClassBo createClass(ClassParam classParam) {
        Integer schoolId = classParam.getSchoolId();
        Integer teacherId = classParam.getTeacherId();
        String className = classParam.getGrade() + classParam.getMajor() + classParam.getClazz() + "班";
        Integer grade = classParam.getGrade();
        String major = classParam.getMajor();
        Integer clazz = classParam.getClazz();

        // 构造新的班级并插入到数据库中
        Class newClass = new Class();
        newClass.setSchoolId(schoolId);
        newClass.setClassName(className);
        newClass.setGrade(grade);
        newClass.setMajor(major);
        newClass.setClazz(clazz);
        classRepository.addClass(newClass);

        ClassBo classBo = new ClassBo();
        classBo.setClassName(className);
        classBo.setSchoolId(schoolId);
        classBo.setTeacherId(teacherId);
        classBo.setGrade(grade);
        classBo.setMajor(major);
        classBo.setClazz(clazz);

        Integer classId = classRepository.selectIdByOther(className, schoolId);
        classBo.setId(classId);
        // 往（教师-班级）表中插入新的数据
        classRepository.insertUserClass(teacherId, classId);

        return classBo;
    }

    @Override
    public ClassBo update(EditClassParam editClassParam) {
        boolean classIdUpdate = !Objects.isNull(editClassParam.getClassName());
        boolean teacherIdUpdate = !Objects.isNull(editClassParam.getTeacherId());

        int updateSuccess = 0;

        if (classIdUpdate) {
            updateSuccess = updateClassName(editClassParam);
        }
        if (teacherIdUpdate) {
            Integer classId = editClassParam.getId();
            Integer newteacherId = editClassParam.getTeacherId();
            Integer teacherId = editClassParam.getOldTeacherId();
            updateSuccess = updateClassTeacher(classId, teacherId, newteacherId);
        }

        if (updateSuccess > 0) {
            return new ClassBo(editClassParam.getId(),
                    editClassParam.getClassName(),
                    editClassParam.getSchoolId(),
                    editClassParam.getTeacherId());
        }
        throw new ResourceNotFoundException("班级更新失败！");
    }

    private int updateClassName(EditClassParam editClassParam) {
        return classRepository.updateClassName(editClassParam);
    }

    private int updateClassTeacher(Integer classId, Integer teacherId, Integer newTeacherId) {
        return userClassRepository.updateUserClass(classId, teacherId, newTeacherId);
    }

    @Override
    public List<ClassQueryBySchoolDto> getClassBySchool(Integer current, Integer limit, Integer id) {

        List<Integer> classIdList = classRepository.queryClassIdListBySchoolId(id);
        List<ClassQueryBySchoolBo> classQueryBySchoolBoList = classRepository.queryClassListByClassIdList(classIdList, current, limit);
        List<ClassQueryBySchoolDto> classQueryBySchoolDtoList = classMapper.to(classQueryBySchoolBoList);

        return classQueryBySchoolDtoList;
    }

    @Override
    public Integer getTotalBySchoolId(Integer id) {
        return classRepository.getTotalBySchoolId(id);
    }

    @Override
    public List<ClassFindByTeacherBo> getClassByTeacher(Integer id) {
        List<Integer> classIdList = classRepository.findClassIdListByTeacherId(id);
        List<ClassFindByTeacherBo> classFindByTeacherIdBoList = classRepository.findClassIdListByClassId(classIdList);
        //        List<ClassFindByTeacherIdDto> classFindByTeacherIdDtoList=classMapper.(classFindByTeacherIdBoList);
        return classFindByTeacherIdBoList;
    }

    @Override
    public Integer getClassIdByName(String className) {
        Integer classId = classRepository.getClassIdByName(className);
        return classId;
    }

    @Override
    public List<StudentBo> getStudentByClassId(Integer classId, Integer teacherId, Integer current, Integer limit) {
        List<StudentBo> studentBoList = classRepository.getStudentByClassId(classId, teacherId, current, limit);
        return studentBoList;
    }

    @Override
    public List<Class> pageClass(Integer current, Integer limit) {
        return classRepository.pageClass(current, limit);
    }

    @Override
    public Integer getTotalClass() {
        return classRepository.getTotalClass();
    }

    @Override
    public boolean classExist(String className, Integer schoolId) {
        return classRepository.classExist(className, schoolId) > 0;
    }

    @Override
    public boolean checkUserRole(Integer userId) {
        return classRepository.checkUserRole(userId) > 0;
    }

    @Override
    public Integer getTotalStudentByClassId(Integer teacherId, Integer classId) {
        return classRepository.getTotalStudentByClassId(teacherId, classId);
    }

    @Override
    public Integer deleteUserClass(Integer classId) {
        if (classRepository.ClassHasExam(classId) == 1 || classRepository.ClassHasExperiment(classId) == 1) {
            throw new IllegalArgumentException("该班级存在实验或考试数据，无法删除");
        }
        return classRepository.deleteUserClass(classId);
    }
}
