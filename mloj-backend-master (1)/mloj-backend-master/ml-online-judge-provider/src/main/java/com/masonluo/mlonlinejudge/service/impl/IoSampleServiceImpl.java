package com.masonluo.mlonlinejudge.service.impl;

import com.masonluo.mlonlinejudge.dao.IoSampleRepository;
import com.masonluo.mlonlinejudge.entity.IoSample;
import com.masonluo.mlonlinejudge.mapper.IoSampleMapper;
import com.masonluo.mlonlinejudge.model.bo.IoSampleBo;
import com.masonluo.mlonlinejudge.service.IoSampleService;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * @author masonluo
 * @date 2021/1/22 5:49 下午
 */
@Service
public class IoSampleServiceImpl implements IoSampleService {

    private final IoSampleRepository ioSampleRepository;

    private final IoSampleMapper ioSampleMapper;

    public IoSampleServiceImpl(IoSampleRepository ioSampleRepository, IoSampleMapper ioSampleMapper) {
        this.ioSampleRepository = ioSampleRepository;
        this.ioSampleMapper = ioSampleMapper;
    }

    @Override
    public List<IoSampleBo> findByProblemId(Integer problemId) {
        List<IoSample> res = ioSampleRepository.findByProblemId(problemId);
        if (res == null) {
            return Collections.emptyList();
        }
        return ioSampleMapper.to(res);
    }
}
