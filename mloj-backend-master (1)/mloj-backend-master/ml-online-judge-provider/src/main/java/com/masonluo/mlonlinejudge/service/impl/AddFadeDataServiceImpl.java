package com.masonluo.mlonlinejudge.service.impl;

import com.masonluo.mlonlinejudge.dao.ResultRepository;

import com.masonluo.mlonlinejudge.entity.SolutionResultJoinTestWithTime;
import com.masonluo.mlonlinejudge.entity.TimeSlot;
import com.masonluo.mlonlinejudge.service.AddFadeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class AddFadeDataServiceImpl implements AddFadeDataService {
    @Autowired
    private ResultRepository resultRepository;
    private int num=-1;
    private boolean isOpen=false;
    private double accuracy=-1;
    private List<TimeSlot> timeList;
    private int temp;
    private double tempAccuracy;
    private int index=0;
    @Override
    public double getAccuracy() {
        if(accuracy==-1){
            return tempAccuracy;
        }
        return accuracy;
    }
    @Override
    @Scheduled(cron = "0 0/30 * * * ? ")
    public void addFadeData() {
        if(!timeList.isEmpty()) {
            TimeSlot timeSlot = timeList.get(index);
            //可能传了空值过来，做个判断，不为空就继续往下走，为空就当空list处理一次性把num全部插入
            if(timeSlot.getStart()!=null&&timeSlot.getEnd()!=null) {
                if (LocalTime.now().isAfter(timeSlot.getStart()) && isOpen&&index<timeList.size()) {
                    //如果以现在时间超过取出来的end，再更新index再去取一次timeslot，会让数据产生更均匀，但毕竟是Random出来的插入量，本身就有一次插入很多的可能导致其他插入量少，暂时懒得改了
                    index++;
                    int n;
                    //到了最后一个时间段，直接把剩余的temp全部插入，并重置temp
                    if (index == timeList.size()) {
                        n = temp;
                        temp = num;
                    }
                    //还没到最后，随机生成一个范围内的插入，并减少下一次插入的上限
                    else {
                        n = new Random().nextInt(temp + 1);
                        temp -= n;
                    }
                    insert(n);
                }
            }
            //为空值，一次性全部插入后清空list集合,并把temp清0
            else {
                insert(temp);
                temp=0;
                timeList.clear();
            }
        }else{
            insert(temp);
            temp=0;
        }
    }

    private void insert(int a) {
        List<SolutionResultJoinTestWithTime> list = new ArrayList<>();
        for (int i = 0; i < a; i++) {
            SolutionResultJoinTestWithTime s = new SolutionResultJoinTestWithTime();
            s.setUserId(-1);
            s.setProblemId(-1);
            s.setMode(-1);
            s.setSolutionId(-1);
            s.setTestId(-1);
            s.setResultId(-1);
            String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            LocalDateTime l = LocalDateTime.parse(time, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            s.setCreateTime(l);
            s.setUpdateTime(l);
            list.add(s);
        }
        if (!list.isEmpty()) {
            resultRepository.insertBatch(list);
        }
    }

    @Scheduled(cron = "0 0 0 1/1 * ? ")
    public void setAccuracy(){
        if(accuracy==-1){
            Random random = new Random();
            tempAccuracy = Double.valueOf(String.format("%.2f", random.nextDouble()));
        }
        //每天把index和temp重新赋值
        index=0;
        temp=num;
    }
    @Override
    public void open(int num, double accuracy, List<TimeSlot> list) {
        this.num=num;
        this.temp=num;
        isOpen=true;
        this.timeList=list;
        this.accuracy=accuracy;
        this.index=0;
    }

    @Override
    public void close() {
        isOpen=false;
    }
}
