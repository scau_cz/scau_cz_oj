package com.masonluo.mlonlinejudge.acl.core;

/**
 * @author masonluo
 * @date 2021/5/5 10:57 上午
 */
public interface NameGenerator {
    String getName(Class<?> clazz);
}
