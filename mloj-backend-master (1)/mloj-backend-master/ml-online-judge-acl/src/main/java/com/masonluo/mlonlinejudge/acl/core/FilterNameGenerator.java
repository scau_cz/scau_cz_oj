package com.masonluo.mlonlinejudge.acl.core;

import com.masonluo.mlonlinejudge.acl.annotations.Filter;
import com.masonluo.mlonlinejudge.utils.StringUtils;

/**
 * @author masonluo
 * @date 2021/5/5 10:58 上午
 */
public class FilterNameGenerator implements NameGenerator {

    @Override
    public String getName(Class<?> clazz) {
        if (!clazz.isAnnotationPresent(Filter.class)) {
            throw new IllegalArgumentException("The name generator should only use in filter class");
        }
        Filter annotation = clazz.getAnnotation(Filter.class);
        String name = annotation.name();
        if (StringUtils.isBlank(name)) {
            name = buildNameForClass(clazz);
        }
        return name;
    }

    private String buildNameForClass(Class<?> clazz) {
        String simpleClassName = clazz.getSimpleName();
        return firstCharacterLowerCase(simpleClassName);
    }

    private String firstCharacterLowerCase(String simpleClassName) {
        char first = simpleClassName.charAt(0);
        first = (char) (first + 32);
        StringBuilder sb = new StringBuilder();
        sb.append(first);
        if (simpleClassName.length() > 1) {
            sb.append(simpleClassName.substring(1));
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        FilterNameGenerator generator = new FilterNameGenerator();
        String name = generator.getName(FilterNameGenerator.class);
        System.out.println(name);
    }
}
