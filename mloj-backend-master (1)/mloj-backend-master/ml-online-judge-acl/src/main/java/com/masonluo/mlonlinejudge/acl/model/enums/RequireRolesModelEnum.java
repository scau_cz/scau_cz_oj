package com.masonluo.mlonlinejudge.acl.model.enums;

import com.masonluo.mlonlinejudge.acl.annotations.RequireRoles;

/**
 * 匹配模式，当某个方法被{@link RequireRoles}注解的时候，
 * 用这个来标识匹配模式，目前仅有Contain和Match All两种模式
 *
 * @author masonluo
 * @date 2021/4/16 8:46 下午
 */
public enum RequireRolesModelEnum {
    /**
     * Contain模式，用户仅需包含设定好的某种角色
     */
    CONTAIN,

    /**
     * Match all模式，用户需要拥有所有角色
     */
    MATCH_ALL;
}
