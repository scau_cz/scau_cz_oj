package com.masonluo.mlonlinejudge.acl.annotations;

import java.lang.annotation.*;

/**
 * 注解到方法上，表示该方法需要登录验证
 * 注解到类上，表示该类的所有方法需要登录验证
 *
 * @author masonluo
 * @date 2021/4/16 8:40 下午
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface RequireAuthentication {
}
