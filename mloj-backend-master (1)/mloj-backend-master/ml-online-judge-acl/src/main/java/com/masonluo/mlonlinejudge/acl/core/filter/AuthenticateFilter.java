package com.masonluo.mlonlinejudge.acl.core.filter;

import com.masonluo.mlonlinejudge.acl.core.Identification;

/**
 * 该过滤器用以对用户是否已经登录进行判断
 *
 * @author masonluo
 * @date 2021/4/17 2:38 下午
 */
public interface AuthenticateFilter {

    /**
     * 鉴权，判断用户是否已经登录，默认返回true，证明不需要判断是否已登录
     */
    default FilterResult authenticate(Identification<String> token) {
        return FilterResult.SUCCESS;
    }
}
