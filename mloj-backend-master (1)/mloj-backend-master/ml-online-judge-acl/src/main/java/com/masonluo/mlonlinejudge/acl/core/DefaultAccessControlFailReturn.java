package com.masonluo.mlonlinejudge.acl.core;

import com.masonluo.mlonlinejudge.acl.core.filter.FilterResult;
import com.masonluo.mlonlinejudge.result.Result;
import com.masonluo.mlonlinejudge.result.ResultCode;

/**
 * 默认授权异常返回类
 *
 * @author masonluo
 * @date 2021/5/6 12:55 上午
 */
public class DefaultAccessControlFailReturn implements AccessControlFailReturn {

    @Override
    public Object failReturn(FilterResult failResult) {
        return new Result<>(ResultCode.UNAUTHORIZED, failResult.getErrorMsg(), failResult.getExtraMessage());
    }
}
