package com.masonluo.mlonlinejudge.acl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * @author masonluo
 * @date 2021/4/17 3:31 下午
 */
// @SpringBootApplication
public class AclApplication {
    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(AclApplication.class, args);
        System.out.println(context);
    }
}
