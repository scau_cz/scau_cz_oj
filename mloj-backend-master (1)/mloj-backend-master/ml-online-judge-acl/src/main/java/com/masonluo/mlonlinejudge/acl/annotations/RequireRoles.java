package com.masonluo.mlonlinejudge.acl.annotations;

import com.masonluo.mlonlinejudge.acl.model.enums.RequireRolesModelEnum;

import java.lang.annotation.*;

/**
 * 注解到方法上，表示该方法需要有values里面含有的角色
 * 注解到类上，表示该类的所有方法需要登录验证
 *
 * @author masonluo
 * @date 2021/4/16 8:44 下午
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@RequireAuthentication
public @interface RequireRoles {
    /**
     * 该方法所需要的角色
     */
    String[] values() default {};

    /**
     * 模式，详见{@link RequireRolesModelEnum}, 默认为Match all模式
     */
    RequireRolesModelEnum model() default RequireRolesModelEnum.MATCH_ALL;
}
