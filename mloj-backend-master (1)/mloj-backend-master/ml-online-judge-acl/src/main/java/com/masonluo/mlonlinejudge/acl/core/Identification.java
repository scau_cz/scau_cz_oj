package com.masonluo.mlonlinejudge.acl.core;

/**
 * Id持有类，通过这个来获取用户的唯一标识Id
 *
 * @author masonluo
 * @date 2021/4/17 2:35 下午
 */
public interface Identification<T> {
    T getIdentification();
}
