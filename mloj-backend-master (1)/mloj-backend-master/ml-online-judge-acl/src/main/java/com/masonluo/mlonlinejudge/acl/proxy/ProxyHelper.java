package com.masonluo.mlonlinejudge.acl.proxy;

import net.sf.cglib.proxy.Enhancer;

/**
 * @author masonluo
 * @date 2021/5/6 2:07 下午
 */
public class ProxyHelper {
    public static Object proxy(ProxyBean proxyBean) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(proxyBean.getOrigin().getClass());
        enhancer.setCallback(new AclMethodInterceptor(proxyBean));
        return enhancer.create();
    }
}
