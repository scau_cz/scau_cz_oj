package com.masonluo.mlonlinejudge.acl.core;

/**
 * 该接口可以获取用户在鉴权所需要的token数据和用户唯一标识id
 *
 * @author masonluo
 * @date 2021/5/6 5:15 下午
 */
public interface AuthenticationIdentificationAchiever {
    Identification<String> obtainToken();
}
