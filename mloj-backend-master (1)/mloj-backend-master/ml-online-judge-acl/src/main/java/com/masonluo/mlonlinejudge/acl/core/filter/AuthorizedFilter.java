package com.masonluo.mlonlinejudge.acl.core.filter;

import com.masonluo.mlonlinejudge.acl.core.Identification;
import com.masonluo.mlonlinejudge.acl.core.RoleAccessor;

import java.util.List;

/**
 * 继承自{@link AuthenticateFilter}, 增强了功能，增加了判断是否拥有该角色的鉴权过滤
 *
 * @author masonluo
 * @date 2021/5/5 7:52 下午
 */
public interface AuthorizedFilter extends AuthenticateFilter {

    default FilterResult authorized(Identification<String> token, List<RoleAccessor> needRoles) {
        return FilterResult.SUCCESS;
    }
}
