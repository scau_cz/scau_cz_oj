package com.masonluo.mlonlinejudge.acl.core;

import com.masonluo.mlonlinejudge.acl.core.filter.FilterResult;

/**
 * 失败之后的返回结果
 * <p>
 * 由于采用的是统一返回，所以应该返回一个{@link com.masonluo.mlonlinejudge.result.Result}类，以控制Http返回的状态码
 * <p>
 * 全局只能有一个，如果没有将这个类的实现类加载入Spring容器中的话，会采用默认的返回{@link DefaultAccessControlFailReturn}
 *
 * @author masonluo
 * @date 2021/5/5 9:27 下午
 */
public interface AccessControlFailReturn {
    Object failReturn(FilterResult failResult);
}
