package com.masonluo.mlonlinejudge.acl.core;

import com.masonluo.mlonlinejudge.acl.model.enums.RequireRolesModelEnum;

import java.util.HashSet;
import java.util.Set;

/**
 * @author masonluo
 * @date 2021/4/17 2:49 下午
 */
public class RoleAccessor {

    private Set<String> roles = new HashSet<>(16);

    private RequireRolesModelEnum model;

    public RoleAccessor() {

    }

    public Set<String> getRoles() {
        return roles;
    }

    public RequireRolesModelEnum getModel() {
        return model;
    }

    public void setModel(RequireRolesModelEnum model) {
        this.model = model;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public boolean contain(String role) {
        return roles.contains(role);
    }
}
