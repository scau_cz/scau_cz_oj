package com.masonluo.mlonlinejudge.acl.core.filter;

/**
 * 过滤结果，可以通过这个Result进行回传一些需要的参数
 *
 * @author masonluo
 * @date 2021/5/5 9:31 下午
 */
public class FilterResult {

    public static final FilterResult SUCCESS = new FilterResult(true);

    private final boolean success;

    private String errorMsg;

    private ExtraMessage extraMessage;

    private FilterResult(boolean success) {
        this.success = success;
    }

    private FilterResult(boolean success, String errorMsg) {
        this(success, errorMsg, null);
    }

    public FilterResult(boolean success, String errorMsg, ExtraMessage extraMessage) {
        this.success = success;
        this.errorMsg = errorMsg;
        this.extraMessage = extraMessage;
    }


    public boolean isSuccess() {
        return success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ExtraMessage getExtraMessage() {
        return extraMessage;
    }

    public void setExtraMessage(ExtraMessage extraMessage) {
        this.extraMessage = extraMessage;
    }

    public static FilterResult error(String msg) {
        return new FilterResult(false, msg);
    }

    public static FilterResult error(String msg, AuthenticateFilter filter, Object[] args) {
        return new FilterResult(false, msg, new ExtraMessage(filter, args));
    }

    public static class ExtraMessage {
        private AuthenticateFilter filter;

        private Object[] args;

        public ExtraMessage() {
        }

        public ExtraMessage(AuthenticateFilter filter, Object[] args) {
            this.filter = filter;
            this.args = args;
        }

        public AuthenticateFilter getFilter() {
            return filter;
        }

        public void setFilter(AuthenticateFilter filter) {
            this.filter = filter;
        }

        public Object[] getArgs() {
            return args;
        }

        public void setArgs(Object[] args) {
            this.args = args;
        }
    }

    @Override
    public String toString() {
        return "FilterResult{" +
                "success=" + success +
                ", errorMsg='" + errorMsg + '\'' +
                '}';
    }
}
