package com.masonluo.mlonlinejudge.acl.core;

import java.util.Objects;

/**
 * 整形id身份表示标识
 *
 * @author masonluo
 * @date 2021/4/17 2:37 下午
 */
public class IntegerIdentification implements Identification<Integer> {
    private Integer id;

    public IntegerIdentification() {
    }

    public IntegerIdentification(Integer id) {
        this.id = id;
    }

    @Override
    public Integer getIdentification() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntegerIdentification that = (IntegerIdentification) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "IntegerIdHolder{" +
                "id=" + id +
                '}';
    }
}
