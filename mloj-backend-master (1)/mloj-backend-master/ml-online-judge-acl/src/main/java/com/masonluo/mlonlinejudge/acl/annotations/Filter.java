package com.masonluo.mlonlinejudge.acl.annotations;

import com.masonluo.mlonlinejudge.acl.core.filter.AuthenticateFilter;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 实现了{@link AuthenticateFilter}之后，
 * 将这个注解标注到类上，让类可以自动被扫描到，并且加载入容器中
 *
 * @author masonluo
 * @date 2021/4/25 11:43 上午
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Filter {
    String name() default "";
}
