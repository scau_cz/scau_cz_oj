package com.masonluo.mlonlinejudge.acl.core;

import com.masonluo.mlonlinejudge.constant.SystemConstant;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author masonluo
 * @date 2021/5/6 5:18 下午
 */
public class DefaultAuthenticationMetadataAchiever implements AuthenticationIdentificationAchiever {

    @Override
    public Identification<String> obtainToken() {
        HttpServletRequest request = obtainHttpServletRequest();
        String token = request.getHeader(SystemConstant.TOKEN_HEADER);
        return () -> token;
    }

    public HttpServletRequest obtainHttpServletRequest() {
        return ((ServletRequestAttributes) (RequestContextHolder.getRequestAttributes())).getRequest();
    }
}
