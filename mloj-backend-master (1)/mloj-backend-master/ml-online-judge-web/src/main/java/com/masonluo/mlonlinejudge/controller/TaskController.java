package com.masonluo.mlonlinejudge.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.masonluo.mlonlinejudge.entity.AnswerContent;
import com.masonluo.mlonlinejudge.mapper.TaskMapper;
import com.masonluo.mlonlinejudge.model.bo.TaskBo;
import com.masonluo.mlonlinejudge.model.bo.TaskInfoBo;
import com.masonluo.mlonlinejudge.model.bo.TaskWithClassInfoBo;
import com.masonluo.mlonlinejudge.model.param.*;
import com.masonluo.mlonlinejudge.model.vo.TaskProblemSituationVo;
import com.masonluo.mlonlinejudge.result.Result;
import com.masonluo.mlonlinejudge.result.ResultCode;
import com.masonluo.mlonlinejudge.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(description = "任务管理")
@RestController
@RequestMapping("/task")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskMapper taskMapper;

    @ApiOperation(value = "创建任务")
    @PostMapping("/createTask")
    public Result<TaskBo> createTask(@RequestBody @Valid TaskCreateParam taskParam){

        //System.out.println(taskParam.toString());
        TaskBo taskBo = taskService.createTask( taskParam );
        return Result.created(taskBo);

    }

    @ApiOperation(value = "修改考试时间")
    @PatchMapping("/changeTime")
    public Result updateTime(@RequestBody @Valid TaskChangeTimeParam taskChangeTimeParam){
        taskService.updateTime(taskChangeTimeParam);
        return Result.ok();
    }

    @ApiOperation(value = "任务列表")
    @GetMapping("/taskList")
    public Result<Page<TaskWithClassInfoBo>> listTask(
            @RequestParam("pageNum") Integer pageNum,
            @RequestParam("pageSize") Integer pageSize,
            @RequestParam("schoolId") Integer schoolId){
        Page<TaskWithClassInfoBo> tasks = taskService.List(pageNum, pageSize, schoolId);
        return Result.ok(tasks);
    }


    @ApiOperation(value = "根据班级id获取任务")
    @GetMapping("/classTask/{classId}")
    public Result<Page<TaskBo>> classTask(
            @RequestParam("pageNum") Integer pageNum,
            @RequestParam("pageSize") Integer pageSize,
            @RequestParam("classId") Integer classId){
        Page<TaskBo> tasks = taskService.findTaskByClassId(pageNum, pageSize, classId);
        return Result.ok(tasks);
    }

    @ApiOperation(value = "删除任务")
    @DeleteMapping("/deleteTask/{taskId}")
    public Result deleteById(@PathVariable("taskId") Integer taskId){
        Result result = new Result();
        result.setCode(ResultCode.OK);
        try {
            taskService.delete(taskId);
        }catch (IllegalArgumentException e){
            result.setCode(ResultCode.CONFLICT);
            result.setMessage("考试进行中,无法删除");
        }
        return result;
    }

    @ApiOperation(value = "提交试卷")
    @PostMapping("/submit")
    public Result submit(@RequestBody SubmitParam submitParam){
        if(taskService.judgeExist(submitParam)){
            Result result = new Result();
            result.setCode(ResultCode.FORBIDDEN);
            result.setMessage("答卷已提交");
            return result;
        }
        taskService.submit(submitParam);
        return Result.ok();
    }

    @ApiOperation(value = "缓存答卷")
    @PostMapping("/storeExamAnswerSheet")
    public Result storeExamAnswerSheet(@RequestBody SubmitParam submitParam){
        return taskService.storeExamAnswerSheet(submitParam);
    }

    @ApiOperation(value = "获取缓存答卷")
    @PostMapping("/getExamAnswerSheetCache/{userId}/{examId}")
    public Result getExamAnswerSheetCache(@PathVariable("userId") Integer userId,@PathVariable("examId") Integer examId){
        return taskService.getExamAnswerSheetCache(userId,examId);
    }

    @ApiOperation(value = "考试信息")
    @GetMapping("/getTaskInfo/{taskId}")
    public Result<TaskInfoBo> getTaskInfo(@PathVariable("taskId") Integer taskId){
        TaskInfoBo taskInfoBo = taskService.getTaskInfo(taskId);
        return  Result.ok(taskInfoBo);
    }

    @ApiOperation(value = "编辑任务")
    @PatchMapping("/editTask")
    public Result<TaskInfoBo> editTask(@RequestBody @Valid ChangeTaskParam changeTaskParam){
        TaskInfoBo taskInfoBo = taskService.editTask(changeTaskParam);
        return Result.ok(taskInfoBo);
    }

    @ApiOperation(value = "获取用户答卷")
    @GetMapping("/getAnswerContent")
    public Result<List<AnswerContent>> getAnswerContent(@RequestParam("userId")Integer userId,
                                                        @RequestParam("examId")Integer examId){
        List<AnswerContent> answerContents = taskService.queryAnswerContent(userId, examId);
        return Result.ok(answerContents);
    }


    @ApiOperation(value = "查看考试编程题作答情况")
    @GetMapping("/getExamProblemResponseSituation")
    public Result<List<TaskProblemSituationVo>> getExamProblemResponseSituation(@RequestParam("userId")Integer userId,
                                                  @RequestParam("examId")Integer examId){
        List<TaskProblemSituationVo> taskProblemSituationVoList = taskService.queryExamProblemResponseSituation(userId, examId);
        return Result.ok(taskProblemSituationVoList);
    }
}
