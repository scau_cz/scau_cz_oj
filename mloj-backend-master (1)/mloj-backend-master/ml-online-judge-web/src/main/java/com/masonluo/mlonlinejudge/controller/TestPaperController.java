package com.masonluo.mlonlinejudge.controller;


import com.masonluo.mlonlinejudge.dao.TestPaperRepository;
import com.masonluo.mlonlinejudge.model.bo.TestPaperBo;
import com.masonluo.mlonlinejudge.model.bo.TestPaperDetailBo;
import com.masonluo.mlonlinejudge.model.dto.AddTestPaperDto;
import com.masonluo.mlonlinejudge.model.dto.TestPaperDto;
import com.masonluo.mlonlinejudge.model.dto.TestPaperSelProblem;
import com.masonluo.mlonlinejudge.model.param.FindPaperParam;
import com.masonluo.mlonlinejudge.model.param.UpdateTestPaperParam;
import com.masonluo.mlonlinejudge.result.Result;
import com.masonluo.mlonlinejudge.result.ResultCode;
import com.masonluo.mlonlinejudge.service.TestPaperService;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 试卷表 前端控制器
 * </p>
 *
 * @author yangqi
 * @since 2021-10-28
 */
@RestController
@RequestMapping("/testpaper")
public class TestPaperController {
    @Autowired
    private TestPaperService testPaperService;

    @Autowired
    private TestPaperRepository testPaperRepository;

    @ApiOperation(value = "修改试卷")
    @PostMapping("updateTestPaper")
    public Result updateTestPaper(@RequestBody UpdateTestPaperParam updateTestPaperParam){
        Integer updateTestPaper = testPaperService.updateTestPaper(updateTestPaperParam);
        if(updateTestPaper<=0){
            throw new IllegalArgumentException("试卷更新失败");
        }
        return Result.ok("更新试卷成功");
    }

    @ApiOperation(value = "根据试卷id和考试id获取试卷详情")
    @GetMapping("showPaperDetail")
    public Result<TestPaperDetailBo> showTestPaperDetail(@RequestParam("testPaperId")Integer id,
                                                         @RequestParam(value = "examId",required = false)Integer examId){
        TestPaperDetailBo testPaperDetailBo = testPaperService.getTestPaperDetail(id,examId);
        return Result.ok(testPaperDetailBo);
    }

    @ApiOperation(value = "试卷列表分页显示")
    @PostMapping("pageTestPaper")
    public Result<List<TestPaperBo>> pageTestPaper(@RequestBody FindPaperParam findPaperParam){
        String name= findPaperParam.getName();
        Integer current = findPaperParam.getCurrent();
        Integer limit = findPaperParam.getLimit();
        Integer total=0;
        List<TestPaperBo> testPaperBoList;
        if(name!=null){
            testPaperBoList = testPaperService.findPaperByName(name,current,limit);
            total = testPaperService.getTotalByName(name);
        }
        else {
            testPaperBoList = testPaperService.pageTestPaper(current,limit);
            total = testPaperService.getTotal();
        }
        return new Result(ResultCode.OK,"试卷列表分页显示成功",testPaperBoList,current,limit,total);
    }


    @ApiOperation(value = "添加试卷")
    @PostMapping("addTestPaper")
    public Result addTestPaper(@RequestBody AddTestPaperDto addTestPaperDto){
        if(Objects.isNull(addTestPaperDto.getTitle())){
            throw new IllegalArgumentException("试卷名不可为空");
        }
        if(Objects.isNull(addTestPaperDto.getChoicesProblem())){
            throw new IllegalArgumentException("选择题不可为空");
        }
        if(Objects.isNull(addTestPaperDto.getProgrammingQuestions())){
            throw new IllegalArgumentException("编程题不可为空");
        }
        if(Objects.isNull(addTestPaperDto.getTrueOrFalse())){
            throw new IllegalArgumentException("判断题不可为空");
        }

        TestPaperDto testPaperDto = new TestPaperDto(addTestPaperDto.getTitle(),addTestPaperDto.getDesc(),
                addTestPaperDto.getScore(),addTestPaperDto.getCreatorId());
        Integer addTestPaper = testPaperService.addTestPaper(testPaperDto);//把试卷基本信息存入t_test_paper
        Integer paperId = testPaperService.selectId(testPaperDto);//试卷id

        List<TestPaperSelProblem> choicesProblemList = addTestPaperDto.getChoicesProblem();
        boolean addChoicesProblem= testPaperService.addTestSelProblem(paperId,choicesProblemList,0);

        List<TestPaperSelProblem> trueOrFalseList = addTestPaperDto.getTrueOrFalse();
        boolean addTureOrFalse = testPaperService.addTestSelProblem(paperId,trueOrFalseList,1);

        List<TestPaperSelProblem> programmingList=addTestPaperDto.getProgrammingQuestions();
        boolean addProgramming = testPaperService.addTestSelProblem(paperId,programmingList,2);

        if(addTestPaper<=0){
            throw new IllegalArgumentException("添加试卷基本信息失败");
        }
        if(!addChoicesProblem){
            throw new IllegalArgumentException("添加选择题失败");
        }
        if(!addTureOrFalse){
            throw new IllegalArgumentException("添加判断题失败");
        }
        if(!addProgramming){
            throw new IllegalArgumentException("添加程序题失败");
        }

        return Result.ok("添加试卷成功");


    }



    @ApiOperation(value = "删除试卷")
    @DeleteMapping("deleteTestPaper/{id}")
    public Result deleteTestPaper(@PathVariable Integer id){
        if(testPaperRepository.paperExist(id)<=0){
            throw new IllegalArgumentException("试卷不存在");
        }
        if(!(testPaperRepository.paperProblemExist(id)<=0)){
            //throw new IllegalArgumentException("试卷与题目之间不存在关联");
            if(testPaperService.deletePaperProblem(id)<=0){
                throw new IllegalArgumentException("删除试卷与题目关联失败");
            }
        }
        if(!testPaperService.removeById(id)){
            throw new IllegalArgumentException("删除试卷失败");
        }

        return Result.ok("试卷删除成功");
    }


}

