package com.masonluo.mlonlinejudge.controller;

import com.masonluo.mlonlinejudge.mapper.SchoolMapper;
import com.masonluo.mlonlinejudge.model.bo.SchoolBo;
import com.masonluo.mlonlinejudge.model.param.SchoolQueryParam;
import com.masonluo.mlonlinejudge.model.vo.SchoolVo;
import com.masonluo.mlonlinejudge.result.Result;
import com.masonluo.mlonlinejudge.result.ResultCode;
import com.masonluo.mlonlinejudge.service.SchoolService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2021-08-13 13:39
 **/
@Api(description = "高校管理")
@RestController
@RequestMapping("/schools")
public class SchoolController {
    @Autowired
    private SchoolService schoolService;

    @Autowired
    private SchoolMapper schoolMapper;

    @ApiOperation(value = "所有高校列表")
    @GetMapping
    public Result listSchool(@RequestParam("pageNum") Integer pageNum,
                                  @RequestParam("pageSize") Integer pageSize) {
        List<SchoolBo> schools = schoolService.findList(pageNum,pageSize);
        return new Result(ResultCode.OK,"学校信息查找成功",schools,pageNum,pageSize,schoolService.countAll());
    }

    @ApiOperation(value = "根据id查高校")
    @GetMapping("/findBySchoolId/{schoolId}")
    public Result<SchoolBo> listBySchoolId(@PathVariable("schoolId") Integer SchoolId) {
        if(Objects.isNull(SchoolId)){
            throw new IllegalArgumentException("学校ID不可为空");
        }
        return Result.ok(schoolService.findSchoolById(SchoolId));
    }

    @ApiOperation(value = "根据名字查高校")
    @PostMapping("/findBySchoolName")
    public Result<SchoolBo> listBySchoolName(@RequestBody SchoolQueryParam param) {
        if (Objects.isNull(param.getPageNum()) || param.getPageNum() == 0) {
            param.setPageNum(1);
        }
        if (Objects.isNull(param.getPageSize()) || param.getPageSize() == 0) {
            param.setPageSize(10);
        }
        List<SchoolBo> schoolList = schoolService.findSchoolListByName(param);
        Integer total = schoolService.getTotalByName(param.getSchoolNameKey());
        return new Result(ResultCode.OK, "学校信息查找成功", schoolList, param.getPageNum(), param.getPageSize(),total);
    }

    @ApiOperation(value = "添加高校")
    @PostMapping
    public Result<Integer> add(@RequestBody @Valid SchoolVo schoolVo) {
        if(Objects.isNull(schoolVo)){
            throw new IllegalArgumentException("输入不可为空");
        }
        return Result.created(schoolService.createSchool(schoolVo));
    }

    @ApiOperation(value = "根据id修改高校")
    @PatchMapping
    public Result<SchoolBo> update(@RequestBody @Valid SchoolBo schoolBo) {
        if(Objects.isNull(schoolBo)){
            throw new IllegalArgumentException("输入不可为空");
        }
        SchoolBo bo = schoolService.update(schoolBo);
        return Result.ok(bo);
    }

    @ApiOperation(value = "根据id删除高校")
    @SuppressWarnings("rawtypes")
    @DeleteMapping("/{school_id}")
    public Result delete(@PathVariable("school_id") Integer schoolId) {
        if(Objects.isNull(schoolId)){
            throw new IllegalArgumentException("学校ID不可为空");
        }
        schoolService.delete(schoolId);
        return Result.ok();
    }

}


