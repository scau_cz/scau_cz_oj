package com.masonluo.mlonlinejudge.controller;

import com.masonluo.mlonlinejudge.entity.Tag;
import com.masonluo.mlonlinejudge.model.bo.TagBo;
import com.masonluo.mlonlinejudge.model.vo.TagVo;
import com.masonluo.mlonlinejudge.result.Result;
import com.masonluo.mlonlinejudge.service.TagService;



import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/tag")
public class TagController {

    @Autowired
    private TagService tagService;

    @ApiOperation(value = "标签添加", notes = "新建一个名为tagName的Tag")
    @ApiImplicitParam(name = "tagName", value = "标签名称")

    @PostMapping("/{tagName}")
    public Result<TagVo> addTag(@PathVariable("tagName") String tagName) {
        Tag tag = tagService.addTag(tagName);
        TagVo vo = new TagVo();
        vo.setId(tag.getId());
        vo.setName(tag.getName());
        return Result.created(vo);
    }

    @ApiOperation(value = "标签删除", notes = "删除名为tagName的Tag")
    @PostMapping("/deleteTag/{tagName}")
    public boolean deleteTag(@PathVariable("tagName") String tagName) {
        boolean tagFlag = tagService.deleteTag(tagName);
        return tagFlag;
    }

    @ApiOperation(value = "标签显示", notes = "不输入字段，列出全部标签")
    @PostMapping("/listAllTag")
    public List<TagBo> listAllTag() {
        List<TagBo> tagList = tagService.findAll();
        return tagList;
    }
}
