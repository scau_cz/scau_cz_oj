package com.masonluo.mlonlinejudge;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author masonluo
 * @date 2020/12/24 7:44 PM
 */
@SpringBootApplication
//@ComponentScan(basePackages = {"com.masonluo"})
@MapperScan(basePackages = {"com.masonluo.mlonlinejudge.dao"})
@EnableAspectJAutoProxy(exposeProxy = true)
@EnableScheduling // 开启定时任务
public class WebApplication {
    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(WebApplication.class);
    }
}
