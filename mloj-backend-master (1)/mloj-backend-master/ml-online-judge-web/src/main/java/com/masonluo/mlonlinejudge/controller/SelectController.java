package com.masonluo.mlonlinejudge.controller;

import com.masonluo.mlonlinejudge.model.bo.SelectProblemBo;
import com.masonluo.mlonlinejudge.model.dto.SelectProblemUploadDto;
import com.masonluo.mlonlinejudge.model.dto.TestCaseUploadDto;
import com.masonluo.mlonlinejudge.model.param.SelectProblemParam;
import com.masonluo.mlonlinejudge.model.vo.TestCaseUploadVo;
import com.masonluo.mlonlinejudge.result.Result;
import com.masonluo.mlonlinejudge.result.ResultCode;
import com.masonluo.mlonlinejudge.service.ProblemService;
import com.masonluo.mlonlinejudge.service.SelectProblemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.*;

/**
 * @author LOTP
 * @date 2021/8/17 5:33 下午
 */
@Api(description = "选择题模块")
@RestController
@RequestMapping("/select")
public class SelectController {

    @Autowired
    private ProblemService problemService;

    @Autowired
    private SelectProblemService selectProblemService;

    @ApiOperation(value = "根据id修改题目")
    @PatchMapping("/updateSelectProblemById")
    public Result<SelectProblemParam> updateSelectProblemById(@RequestBody @Valid SelectProblemUploadDto selectProblemUploadDto, @RequestParam Integer userId) {
        if(Objects.isNull(selectProblemUploadDto)){
            throw new IllegalArgumentException("输入不可为空");
        }
        return Result.ok(selectProblemService.updateSelectProblemById(selectProblemUploadDto,userId));
    }

    @ApiOperation(value = "根据id查找题目")
    @GetMapping("/getSelectProblemById")
    public Result<SelectProblemParam> getSelectProblemById(@RequestParam("id") Integer id) {
        if(Objects.isNull(id)){
            throw new IllegalArgumentException("问题ID不可为空");
        }
        return Result.ok(selectProblemService.getSelectProblemById(id));
    }

    @ApiOperation(value = "根据id删除题目")
    @DeleteMapping("/{problemId}")
    public Result<Boolean> deleteSelectProblemById(@PathVariable("problemId") Integer id) {
        if(Objects.isNull(id)){
            throw new IllegalArgumentException("问题ID不可为空");
        }
        return Result.ok(selectProblemService.delete(id));
    }

    @ApiOperation(value = "所有题目")
    @GetMapping("/getSelectProblemByPage")
    public Result getSelectProblemByPage(@RequestParam("pageNum") Integer pageNum,
                                              @RequestParam("pageSize") Integer pageSize) {
        List<SelectProblemParam> selectProblems = selectProblemService.getSelectProblemByPage(pageNum, pageSize);
        return new Result(ResultCode.OK,"题目查找成功",selectProblems,pageNum,pageSize,selectProblemService.countAll());
    }

    @ApiOperation(value = "根据章节id查找该章节下的所有题目")
    @GetMapping("/getSelectProblemListByIndexId")
    public Result getSelectProblemListByIndexId(@RequestParam("indexId") Integer indexId,
                                                                       @RequestParam("pageNum") Integer pageNum,
                                                                       @RequestParam("pageSize") Integer pageSize) {
        List<SelectProblemParam> selectProblems = selectProblemService.getSelectProblemListByIndexId(indexId, pageNum, pageSize);
        return new Result(ResultCode.OK,"题目查找成功",selectProblems,pageNum,pageSize,selectProblemService.countByIndexId(indexId));

    }

    @ApiOperation(value = "所有该用户上传的题目")
    @GetMapping("/getSelectProblemPageByUserId")
    public Result getSelectProblemPageByUserId(@RequestParam("userId") Integer userId,
                                                                    @RequestParam("pageNum") Integer pageNum,
                                                                    @RequestParam("pageSize") Integer pageSize) {
        if(Objects.isNull(userId)){
            throw new IllegalArgumentException("用户ID不可为空");
        }
        List<SelectProblemParam> selectProblems = selectProblemService.getSelectProblemPageByUserId(userId, pageNum, pageSize);
        return new Result(ResultCode.OK,"题目查找成功",selectProblems,pageNum,pageSize,selectProblemService.countByUserId(userId));
    }

    @ApiOperation(value = "上传一个题目")
    @PostMapping("/add")
    public Result<Integer> add(@RequestBody @Valid SelectProblemUploadDto selectProblemUploadDto) {
        if(Objects.isNull(selectProblemUploadDto)){
            throw new IllegalArgumentException("输入不可为空");
        }
        return Result.ok(selectProblemService.add(selectProblemUploadDto));
    }

   /* @ApiOperation(value = "测试数据上传，返回测试数据id")
    @PostMapping("/testCase")
    public Result<TestCaseUploadVo> uploadTestCase(@RequestParam("testCaseFile") MultipartFile testCaseFile) {
        TestCaseUploadDto dto = null;
        try {
            dto = problemService.uploadTestCase(testCaseFile);
        } catch (IOException e) {
            throw new IllegalArgumentException("Please uplaod a regular zip file", e);
        }
        Result<TestCaseUploadVo> res = new Result<>(ResultCode.CREATED, "uplaod success", new TestCaseUploadVo(dto.getTestCaseId()));
        return res;
    }*/
}
