package com.masonluo.mlonlinejudge.controller;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@RestController

public class TestController {
    @Resource
    private DefaultMQProducer defaultMQProducer;

    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    @RequestMapping("/test")
    @ResponseBody
    public String ComponentTest() throws MQBrokerException, RemotingException, InterruptedException, MQClientException {
        StringBuilder builder = new StringBuilder();
        //mq test
        SendResult judge = defaultMQProducer.send(new Message("Judge","x2", "{\"test\":\"success\"}".getBytes()),10000);
        System.out.println("mq test:"+judge);
        defaultMQProducer.shutdown();
        builder.append("mq test success.\n");
        //redis test
        redisTemplate.boundValueOps("test").set("test999",1, TimeUnit.DAYS);
        Object test = redisTemplate.boundValueOps("test").get();
        System.out.println("redis key test:"+test);
        builder.append("redis success.\n");
        return builder.toString();
    }
}
