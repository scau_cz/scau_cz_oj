package com.masonluo.mlonlinejudge.controller;


import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.masonluo.mlonlinejudge.entity.ExamScore;
import com.masonluo.mlonlinejudge.model.dto.*;
import com.masonluo.mlonlinejudge.model.param.ExperimentResultParam;
import com.masonluo.mlonlinejudge.result.Result;
import com.masonluo.mlonlinejudge.result.ResultCode;
import com.masonluo.mlonlinejudge.service.ClassService;
import com.masonluo.mlonlinejudge.service.ExamScoreService;
import com.masonluo.mlonlinejudge.service.ExperimentResultService;
import io.swagger.annotations.ApiOperation;
import org.aspectj.weaver.ast.HasAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 考试得分表 前端控制器
 * </p>
 *
 * @author yangqi
 * @since 2021-10-10
 */
@RestController
@RequestMapping("/examscore")
public class ExamScoreController {
    @Autowired
    private ExamScoreService examScoreService;

    @ApiOperation("根据学生id获取学生考试成绩")
    @GetMapping("findByStudentId/{studentId}")
    public Result<List<ExamScoreDto>> findByStudentId(@PathVariable Integer studentId){
        List<ExamScoreDto> score = examScoreService.findStudentScore(studentId);
//        return Result.ok(score);
        return new Result(ResultCode.OK,"信息查找成功",score);
    }

    @ApiOperation("根据班级id和学校id获取班级参与的所有考试")
    @GetMapping("findExamByClassId/{classId}/{schoolId}")
    public Result<List<ClassExamDto>> findExamByClassId(@PathVariable Integer classId,@PathVariable Integer schoolId){
        List<ClassExamDto> classExamDtoList = examScoreService.findExamByClassId(classId,schoolId);
        return new Result(ResultCode.OK,"信息查找成功",classExamDtoList);
    }


    @ApiOperation("学生成绩分页")
    @GetMapping("pageExamScore")
    public Result<List<ExamScoreAndClassDto>> pageExamScore(@RequestParam("current") Integer current,
                                                    @RequestParam("limit") Integer limit,
                                                    @RequestParam("teacherId")Integer teacherId,
                                                    @RequestParam(value = "examTitle",required = false)String examTitle,
                                                    @RequestParam(value = "className",required = false)String className){
        if(Objects.isNull(current)){
            current=1;
        }
        if(Objects.isNull(limit)){
            limit=10;
        }
        List<ExamScoreAndClassDto> examScoreDtos = examScoreService.findByTeacherId(current,limit,teacherId,examTitle,className);
        //TODO total_bug 不能直接CountByTeacherId
        /*Integer total = examScoreService.countExamByTeacherId(teacherId);*/
        Integer total=examScoreService.countExam(teacherId,examTitle,className);
        return new Result(ResultCode.OK,examScoreDtos,total);
    }
    @ApiOperation("根据考试id和班级id返回学生考试成绩")
    @GetMapping("findByExamAndClass/{examId}/{classId}/{current}/{limit}")
    public Result<List<ExamScoreDto>> findByExamAndClass(@PathVariable Integer examId,
                                                         @PathVariable Integer classId,
                                                         @PathVariable Integer current,
                                                         @PathVariable Integer limit){
        List<ExamScoreDto> examScoreDtoList =examScoreService.findByExamAndClass(examId,classId,current,limit);
        Integer total = examScoreService.getTotalStudentExam(examId,classId,current,limit);
        return new Result(ResultCode.OK,"信息查找成功",examScoreDtoList,current,limit,total);
    }



    @ApiOperation("根据学校id获取所有考试")
    @GetMapping("findExamByClassId/{schoolId}")
    public Result<List<SchoolExamDto>> findExamBySchoolId(@PathVariable Integer schoolId){
        List<SchoolExamDto> schoolExamDtoList = examScoreService.findExamBySchoolId(schoolId);
        return new Result(ResultCode.OK,"信息查找成功",schoolExamDtoList);
    }

    @ApiOperation("根据教师id获取考试标题和班级名称")
    @GetMapping("findExamTitleAndClassByUserId/{userId}")
    public Result<ExamTitleDto> findExamTitleAndClassByUserId(@PathVariable Integer userId){
        ExamTitleDto examTitleDto = examScoreService.findExamTitleAndClassNameByUserId(userId);
        return new Result(ResultCode.OK,"信息查找成功",examTitleDto);
    }
    @ApiOperation("成绩导出")
    @GetMapping("/exportScore")
    public void exportScore(HttpServletResponse response,@RequestParam("teacherId")Integer teacherId,
                            @RequestParam(value = "examTitle",required = false)String examTitle,
                            @RequestParam(value = "className",required = false)String className){
        examScoreService.exportScore(response,teacherId,examTitle,className);
    }
}

