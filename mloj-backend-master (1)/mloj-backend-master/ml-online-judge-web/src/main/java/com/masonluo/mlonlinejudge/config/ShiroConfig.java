package com.masonluo.mlonlinejudge.config;


import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {

    /**
     * 创建过滤器
     *
     * @param defaultWebSecurityManager
     * @return
     */
    @Bean(name = "shiroFilter")
    public ShiroFilterFactoryBean getShiroFilterFactoryBean(@Qualifier("securityManager") DefaultWebSecurityManager defaultWebSecurityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        // 设置安全管理器
        shiroFilterFactoryBean.setSecurityManager(defaultWebSecurityManager);
        //进行顺序拦截器配置
        Map<String, String> filterMap = new LinkedHashMap<>();
        //开放的资源要放在前面
        //登录url放行
//        filterMap.put("/guide/users/login", "anon");
//        filterMap.put("/guide/users/register", "anon");
//        filterMap.put("/datahub/swagger-ui/**", "anon");
//        filterMap.put("/guide/collection/**", "authc");
//        filterMap.put("/guide/comment/**", "authc");
//        filterMap.put("/guide/form/**", "authc");
//        filterMap.put("/guide/users/**", "authc");
        //设置默认登录的url
        shiroFilterFactoryBean.setLoginUrl("/guide/users/login");
        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterMap);
        return shiroFilterFactoryBean;
    }


    /**
     * 创建安全管理器
     *
     * @param realm
     * @return
     */
    @Bean(name = "securityManager")
    public DefaultWebSecurityManager getDefaultWebSecurityManager(@Qualifier("realm") Realm realm) {
        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
        defaultWebSecurityManager.setRealm(realm);
        return defaultWebSecurityManager;
    }

    /**
     * 创建自定义Realm
     *
     * @return
     */
    @Bean(name = "realm")
    public Realm getRealm() {
        UserRealm userRealm = new UserRealm();
//        // 创建hash凭证器
//        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
//        // 设置MD5算法
//        hashedCredentialsMatcher.setHashAlgorithmName("md5");
//        // 设置散列次数
//        hashedCredentialsMatcher.setHashIterations(1024);
//        userRealm.setCredentialsMatcher(hashedCredentialsMatcher);
        return userRealm;
    }

    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(@Qualifier("securityManager") SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }


}

