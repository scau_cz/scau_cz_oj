package com.masonluo.mlonlinejudge.controller;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.masonluo.mlonlinejudge.entity.TimeSlot;
import com.masonluo.mlonlinejudge.result.Result;
import com.masonluo.mlonlinejudge.service.AddFadeDataService;
import com.masonluo.mlonlinejudge.utils.FastJsonUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/test")
public class AddFadeDataController {
    @Resource
    private AddFadeDataService addFadeDataService;

    @PostMapping("/addFadeData")
    public Result addFadeData(@RequestParam(value = "num")int num,
                              @RequestParam("isOpen") boolean isOpen,
                              @RequestParam(value = "accuracy")double accuracy,
                              @RequestBody JSONArray list){
        List<TimeSlot> timeSlots = JSONUtil.toList(list,TimeSlot.class);
        if(isOpen) {
            addFadeDataService.open(num,accuracy,timeSlots);
        }else {
            addFadeDataService.close();
        }
        return Result.ok();
    }
    @PostMapping("/test")
    public Result test(@RequestParam("num")int num,
                       @RequestParam("isOpen") boolean isOpen,
                       @RequestParam("accuracy")double accuracy,
                       @RequestBody JSONArray list){
        List<TimeSlot> timeSlots = JSONUtil.toList(list,TimeSlot.class);
        if(isOpen) {
            addFadeDataService.open(num,accuracy,timeSlots);
        }else {
            addFadeDataService.close();
        }
        addFadeDataService.addFadeData();
        return Result.ok();
    }

    @GetMapping("/getAccuracy")
    public Result getAccuracy(){
        return Result.ok(addFadeDataService.getAccuracy());
    }
}
