package com.masonluo.mlonlinejudge.aspect;

import com.masonluo.mlonlinejudge.exceptions.BaseException;
import com.masonluo.mlonlinejudge.exceptions.ResourceExistException;
import com.masonluo.mlonlinejudge.exceptions.ResourceNotFoundException;
import com.masonluo.mlonlinejudge.result.Result;
import com.masonluo.mlonlinejudge.result.ResultCode;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.security.auth.login.AccountException;

/**
 * 全部异常处理， 在这里将一些controller抛出的异常进行处理，返回规定格式的返回值
 *
 * @author masonluo
 * @date 2021/1/18 4:16 下午
 */
@SuppressWarnings("rawtypes")
@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(BaseException.class)
    public Result handleBaseException(BaseException e) {
        return new Result(ResultCode.INTERNAL_SERVER_ERROR, e.getMessage(), e.getStackTrace());
    }

    @ExceptionHandler(ResourceExistException.class)
    public Result handleResourceExistException(ResourceExistException e) {
        return new Result(ResultCode.BAD_REQUEST, e.getMessage());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public Result handleIllegalArgumentException(IllegalArgumentException e) {
        return new Result(ResultCode.BAD_REQUEST_INVALID_PARAMETER, e.getMessage());
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public Result handleResourceNotFoundException(ResourceNotFoundException e) {
        return new Result(ResultCode.NOT_FOUND, e.getMessage());
    }

    @ExceptionHandler(AccountException.class)
    public Result handleAccountException(AccountException e){
        return new Result(ResultCode.UNAUTHORIZED,e.getMessage());
    }

}
