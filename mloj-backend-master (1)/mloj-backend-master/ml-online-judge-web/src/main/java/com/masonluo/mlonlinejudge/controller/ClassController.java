package com.masonluo.mlonlinejudge.controller;


import com.masonluo.mlonlinejudge.dao.ClassRepository;
import com.masonluo.mlonlinejudge.entity.Class;
import com.masonluo.mlonlinejudge.model.bo.ClassBo;
import com.masonluo.mlonlinejudge.model.bo.ClassFindByTeacherBo;
import com.masonluo.mlonlinejudge.model.bo.ClassQueryBySchoolBo;
import com.masonluo.mlonlinejudge.model.bo.StudentBo;
import com.masonluo.mlonlinejudge.model.dto.ClassQueryBySchoolDto;
import com.masonluo.mlonlinejudge.model.dto.UserInfoDto;
import com.masonluo.mlonlinejudge.model.param.ClassParam;
import com.masonluo.mlonlinejudge.model.param.EditClassParam;
import com.masonluo.mlonlinejudge.model.param.StudentQueryParam;
import com.masonluo.mlonlinejudge.result.Result;
import com.masonluo.mlonlinejudge.result.ResultCode;
import com.masonluo.mlonlinejudge.service.ClassService;
import com.masonluo.mlonlinejudge.service.RoleService;
import com.masonluo.mlonlinejudge.service.SchoolService;
import com.masonluo.mlonlinejudge.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 班级表 前端控制器
 * </p>
 *
 * @author yangqi
 * @since 2021-08-18
 */
@RestController
@RequestMapping("/class")
public class ClassController {
    @Autowired
    private ClassService classService;

    @Autowired
    private ClassRepository classRepository;

    @Autowired
    private RoleService roleService;

    @Autowired
    private SchoolService schoolService;

    @Autowired
    private UserService userService;

    @ApiOperation(value = "所有班级列表")
    @GetMapping("findAll")
    public Result<List<Class>> findAll() {
        // 调用service的方法实现查询所有的操作
        List<Class> lists = classService.list(null);
        return Result.ok(lists);
    }

    @ApiOperation(value = "根据教师id查看其管理的班级")
    @GetMapping("findByTeacherId/{teacherId}")
    public Result<List<Class>> findByTeacherId(@PathVariable Integer teacherId) {
        if (Objects.isNull(teacherId)) {
            throw new IllegalArgumentException("id不可为空");
        }
        List<ClassFindByTeacherBo> classFindByTeacherIdBoList = classService.getClassByTeacher(teacherId);
        return new Result(ResultCode.OK, "信息查找成功", classFindByTeacherIdBoList);
    }

    @ApiOperation(value = "根据班级名称获取班级id")
    @GetMapping("getClassIdByClassName/{className}")
    public Result<Integer> getClassIdByName(@PathVariable String className) {
        if (Objects.isNull(className)) {
            throw new IllegalArgumentException("班级名称不可为空");
        }
        Integer classId = classService.getClassIdByName(className);
        return Result.ok(classId);
    }

    @ApiOperation(value = "根据班级id分页显示学生信息")
    @GetMapping("getStudentByClassId/current/limit/{classId}/{teacherId}/{current}/{limit}")
    public Result<List<StudentBo>> getStudentByClassId(@PathVariable("current") Integer current,
                                                       @PathVariable("limit") Integer limit,
                                                       @PathVariable("classId") Integer classId,
                                                       @PathVariable("teacherId") Integer teacherId) {
        if (Objects.isNull(classId) || Objects.isNull(teacherId) || Objects.isNull(current) || Objects.isNull(limit)) {
            throw new IllegalArgumentException("信息不可为空");
        }
        List<StudentBo> studentBoList = classService.getStudentByClassId(classId, teacherId, current, limit);
        Integer total = classService.getTotalStudentByClassId(teacherId, classId);
        return new Result(ResultCode.OK, "信息返回成功", studentBoList, current, limit, total);

    }

    @ApiOperation(value = "分页查询班级")
    @GetMapping("pageClass/{current}/{limit}")// current:当前页 Limit：每页数据条数
    public Result<List<Class>> pageListClass(@PathVariable Integer current,
                                             @PathVariable Integer limit) {
        List<Class> classList = classService.pageClass(current, limit);
        Integer total = classService.getTotalClass();
        return new Result(ResultCode.OK, "班级信息查找成功", classList, current, limit, total);
    }


    @ApiOperation(value = "根据学校id分页查询班级")
    @PostMapping("pageClassBySchool/{current}/{limit}/{id}")
    public Result<List<ClassQueryBySchoolBo>> pageClassBySchool(@RequestParam("current") Integer current,
                                                                @RequestParam("limit") Integer limit,
                                                                @RequestParam("id") Integer id) {
        if (Objects.isNull(id)) {
            throw new IllegalArgumentException("id不可为空");
        }
        List<ClassQueryBySchoolDto> classQueryBySchoolDtoList = classService.getClassBySchool(current, limit, id);
        Integer total = classService.getTotalBySchoolId(id);

        return new Result(ResultCode.OK, "信息查找成功", classQueryBySchoolDtoList, current, limit, total);
    }

    @ApiOperation(value = "添加班级")
    @PostMapping("addClass")
    public Result<ClassBo> addClass(@RequestBody ClassParam classParam) {
        Integer countTeacher = userService.countById(classParam.getTeacherId());// 获取该id的老师数量
        boolean school = schoolService.exist(classParam.getSchoolId());
        boolean classExist = classService.classExist(classParam.getClassName(), classParam.getSchoolId());
        if (!classService.checkUserRole(classParam.getTeacherId())) {
            throw new IllegalArgumentException("非教师不能创建班级");
        }
        if (countTeacher <= 0) {
            throw new IllegalArgumentException("教师id不存在！");
        }
        if (!school) {
            throw new IllegalArgumentException("学校id不存在！");
        }
        if (classExist) {
            throw new IllegalArgumentException("同个学校内不能添加相同名字的班级！");
        }

        ClassBo classBo = classService.createClass(classParam);
        return Result.created(classBo);
    }

    @ApiOperation(value = "修改班级")
    @PostMapping("updateClass")
    public Result<ClassBo> updateClass(@RequestBody EditClassParam editClassParam) {

        ClassBo classBo = classService.update(editClassParam);
        return Result.ok(classBo);
    }

    @ApiOperation(value = "按班级id删除班级")
    @DeleteMapping("deleteClass/{id}")
    public Result removeClass(@PathVariable Integer id) {

        if (Objects.isNull(id)) {
            throw new IllegalArgumentException("班级id不可为空");
        }
        classService.deleteUserClass(id);
        return Result.ok(classService.removeById(id));
        //        return classService.removeById(id);
    }

    @PostMapping("/getInfoByClassIdAndUsernameOrNickname")
    @ApiOperation("根据班级ID和用户名或昵称查找用户信息")
    public Result<UserInfoDto> getInfoByClassIdAndUsernameOrNickname(@RequestBody StudentQueryParam param) {
        if (Objects.isNull(param.getPageNum()) || param.getPageNum() == 0) {
            param.setPageNum(1);
        }
        if (Objects.isNull(param.getPageSize()) || param.getPageSize() == 0) {
            param.setPageSize(10);
        }
        List<UserInfoDto> userInfoDtoList = userService.getInfoByClassIdAndUsernameOrNickname(param);
        Integer total = userService.getTotalByClassIdAndUserNameOrNickName(param.getClassId(), param.getUsernameKey(), param.getNicknameKey());
        return new Result(ResultCode.OK, "查找成功", userInfoDtoList, param.getPageNum(), param.getPageSize(), total);
    }
}

