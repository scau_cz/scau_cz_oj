//package com.masonluo.mlonlinejudge.controller;
//
//
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.baomidou.mybatisplus.extension.api.R;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.masonluo.mlonlinejudge.entity.Administrator;
//import com.masonluo.mlonlinejudge.mapper.AdministratorMapper;
//import com.masonluo.mlonlinejudge.model.bo.AdministratorBo;
//import com.masonluo.mlonlinejudge.model.dto.AdministratorDto;
//import com.masonluo.mlonlinejudge.model.param.AdministratorParam;
//import com.masonluo.mlonlinejudge.model.vo.AdministratorVo;
//import com.masonluo.mlonlinejudge.result.Result;
//import com.masonluo.mlonlinejudge.service.AdministratorService;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.util.StringUtils;
//import org.springframework.web.bind.annotation.*;
//
//import javax.validation.Valid;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * <p>
// * 用户账号密码表 前端控制器
// * </p>
// *
// * @author yangqi
// * @since 2021-09-10
// */
//@RestController
//@RequestMapping("/Administrator")
//public class AdministratorController {
//    @Autowired
//    private AdministratorService administratorService;
//
//    @Autowired
//    private AdministratorMapper administratorMapper;
//
//    @ApiOperation(value = "添加校管理员")
//    @PostMapping("/addAdministrator")
//    public Result<AdministratorBo> add(@RequestBody @Valid AdministratorDto administratorDto){
//        AdministratorBo bo = administratorService.createAdministrator(administratorDto);
//        return Result.created(bo);
//    }
////    public Result<AdministratorBo> add(@RequestParam("username") String username,
////                                       @RequestParam("password") String password){
////
////        AdministratorBo bo = administratorService.createAdministrator(username,password);
////        return Result.created(bo);
////    }
//
//    @ApiOperation(value = "修改校管理员")
//    @PostMapping("update")
//    public Result updateAdministrator(@RequestBody @Valid AdministratorParam administratorParam){
//        administratorService.update(administratorParam);
//        return Result.ok();
//    }
//
//    @ApiOperation(value = "逻辑删除管理员")
//    @DeleteMapping("delete/{id}")
//    public Result remove(@ApiParam(name = "id", value = "管理员ID", required = true)
//                             @PathVariable Integer id){
//        boolean flag = administratorService.removeById(id);
//        if(flag){
//            return Result.ok();
//        }else{
//            return Result.error();
//        }
//    }
//
//    @ApiOperation("分页查询管理员列表")
//    @GetMapping("findList/{current}/{limit}")
//    public Result pageListAdministrator(@PathVariable long current,
//                                        @PathVariable long limit){
//        Page<Administrator> pageAdministrator = new Page<>(current,limit);
//        administratorService.page(pageAdministrator);
//        List<Administrator> records = pageAdministrator.getRecords();
//        return Result.ok(records);
//
//    }
//
//    @ApiOperation("根据id查找管理员")
//    @GetMapping("findById/{id}")
//    public Result findAdministratorById(@PathVariable("id") Integer id){
//        Administrator administrator = administratorService.getById(id);
//        return Result.ok(administrator);
//
//    }
//
//    @ApiOperation("根据username查找管理员")
//    @GetMapping("findByName/{username}")
//    public Result<List<Administrator>> findAdmninistratorByName(@PathVariable("username") String username){
//        Map<String,Object> map = new HashMap<>();
//        map.put("username",username);
//        List<Administrator> list = administratorService.listByMap(map);
//        return Result.ok(list);
//    }
//
//
//
//}
//
