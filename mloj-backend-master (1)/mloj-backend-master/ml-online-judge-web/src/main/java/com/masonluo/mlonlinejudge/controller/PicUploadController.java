package com.masonluo.mlonlinejudge.controller;

import com.masonluo.mlonlinejudge.result.PicUploadResult;
import com.masonluo.mlonlinejudge.result.Result;
import com.masonluo.mlonlinejudge.result.ResultCode;
import com.masonluo.mlonlinejudge.service.PicUploadService;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/imgs")
public class PicUploadController {

    @Autowired
    private PicUploadService picUploadService;

    @PostMapping("/upload/problem")
    @ResponseBody
    @ApiOperation(value = "上传题目图片",notes = "POST")
    public Result upload(@RequestParam("file") MultipartFile multipartFile){
        PicUploadResult uploadProblem = picUploadService.uploadProblem(multipartFile);
        return new Result(ResultCode.OK,"上传成功",uploadProblem);
    }

    @PostMapping("/upload/background")
    @ResponseBody
    @ApiOperation(value = "上传学校背景图片",notes = "POST")
    public Result upload(@RequestParam("school_id") Integer schoolId, @RequestParam("file") MultipartFile multipartFile){
        PicUploadResult uploadBackground = picUploadService.uploadBackground(schoolId,multipartFile);
        return new Result(ResultCode.OK,"上传成功",uploadBackground);
    }

}
