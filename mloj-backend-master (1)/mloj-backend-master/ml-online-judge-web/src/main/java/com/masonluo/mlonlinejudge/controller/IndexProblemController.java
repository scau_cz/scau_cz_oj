package com.masonluo.mlonlinejudge.controller;

import com.masonluo.mlonlinejudge.mapper.IndexProblemMapper;
import com.masonluo.mlonlinejudge.model.bo.IndexProblemBo;
import com.masonluo.mlonlinejudge.model.param.IndexProblemRelatedParam;
import com.masonluo.mlonlinejudge.model.vo.IndexProblemVo;
import com.masonluo.mlonlinejudge.result.Result;
import com.masonluo.mlonlinejudge.service.IndexProblemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Scanner;

/**
 * @author masonluo
 * @date 2021/2/11 12:43 下午
 */
@RestController
@RequestMapping("/books/index")
public class IndexProblemController {

    @Autowired
    private IndexProblemService indexProblemService;

    @Autowired
    private IndexProblemMapper indexProblemMapper;

    @PostMapping("/{indexId}/problems")
    public Result<IndexProblemVo> related(@PathVariable("indexId") Integer indexId,
                                          @RequestBody IndexProblemRelatedParam param) {
        if (ObjectUtils.isEmpty(param.getProblemId())) {
            throw new IllegalArgumentException("The problem id collection should not be empty");
        }
        IndexProblemBo bo = indexProblemService.relatedProblem(indexId, param.getProblemId());
        return Result.created(indexProblemMapper.boToVo(bo));
    }

    @SuppressWarnings("rawtypes")
    @DeleteMapping("/{indexId}/problems/{problemId}")
    public Result delete(@PathVariable("indexId") Integer indexId,
                         @PathVariable("problemId") Integer problemId) {
        indexProblemService.deleteByIndexIdAndProblemId(indexId, problemId);
        return Result.ok();
    }
}
