package com.masonluo.mlonlinejudge.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.masonluo.mlonlinejudge.constant.WarningMessageConstant;
import com.masonluo.mlonlinejudge.entity.ProgramAnswerContent;
import com.masonluo.mlonlinejudge.entity.ProgramAnswerEntity;
import com.masonluo.mlonlinejudge.enums.ExecResult;
import com.masonluo.mlonlinejudge.model.param.ResultFindParam;
import com.masonluo.mlonlinejudge.model.vo.ResultVo;
import com.masonluo.mlonlinejudge.result.Result;
import com.masonluo.mlonlinejudge.service.ProgramAnswerService;
import com.masonluo.mlonlinejudge.service.ResultService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author masonluo
 * @data 2021/3/12 11:21 下午
 */
@Controller
@RequestMapping("/results")
public class ResultController {

    @Autowired
    ResultService resultService;
    @Autowired
    ProgramAnswerService programAnswerService;

    @ResponseBody
    @ApiOperation(value = "结果查询", notes = "根据resultId查询结果")
    @ApiImplicitParam(name = "resultId", value = "结果Id")
    @GetMapping("/{resultId}")
    private Result<com.masonluo.mlonlinejudge.entity.Result> findResult(@PathVariable("resultId") Integer resultId) {
        return Result.ok(resultService.findResultById(resultId));
    }
    @ResponseBody
    @ApiOperation(value = "评判结果查询", notes = "根据resultId和问题id查询结果")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "resultId", value = "结果Id"),
        @ApiImplicitParam(name = "problemId",value = "题目Id")
    })
    @GetMapping("/{resultId}/{problemId}")
    private Result<ResultVo> findResult(@PathVariable("resultId") Integer resultId,@PathVariable("problemId") Integer problemId) {
        ResultVo resultVo = new ResultVo();
        com.masonluo.mlonlinejudge.entity.Result res = resultService.findResultById(resultId);
        resultVo.setCompilerResult(res.isCompilerResult());
        resultVo.setProcessResult(res.getProcessResult());
        resultVo.setCpuTime(res.getCpuTime());
        resultVo.setErrorData(res.getErrorData());
        if(res.getMemory()!=null){
            int a = res.getMemory();
            resultVo.setMemory(a/1024);
        }

        resultVo.setTestCaseIndex(res.getTestCaseIndex());
        resultVo.setUserAnswer(res.getUserAnswer());
        if(res.isCompilerResult()&&res.getProcessResult().compareTo(ExecResult.SUCCESS)!=0){//编译通过且做错题才需要查看
            ProgramAnswerEntity programAnswerEntity = programAnswerService.findAnswerByProblemId(problemId);
            if(programAnswerEntity!=null){
                int tid = res.getTestCaseIndex();//第几个测试用例
                String s = programAnswerEntity.getAnswer_content();//获取正确的答案
                String inputContent=programAnswerEntity.getInput_content();//获取输入文件的内容
                if(inputContent!=null&&!"".equals(inputContent)){//输入文件可能为空的
                    List<ProgramAnswerContent> inContents = JSON.parseArray(inputContent, ProgramAnswerContent.class);
                    resultVo.setInputContent(getContentByIndex(tid,inContents));
                }
                List<ProgramAnswerContent> contents = JSON.parseArray(s, ProgramAnswerContent.class);
                resultVo.setTrueAnswer(getContentByIndex(tid,contents));
            }
        }
        return Result.ok(resultVo);
    }
    //根据测试用例的序号查找输入输出文件的内容
    private String getContentByIndex(int tid,List<ProgramAnswerContent> contents){
        for(ProgramAnswerContent pc:contents){//查找第几个测试用例的正确答案
            if(pc.getId()==tid){
                return pc.getAnswer();
            }
        }
        return "";
    }

    @ResponseBody
    @ApiOperation(value = "查看历史提交", notes = "分页查询Result")
    @PostMapping ("/findResult")
    public Result<Page<com.masonluo.mlonlinejudge.model.vo.ResultVo>> findResultAll(@RequestBody ResultFindParam resultFindParam) throws Exception{
        return Result.ok(resultService.findResultAll(resultFindParam));
    }

    @ResponseBody
    @ApiOperation(value = "分页查询实验题目提交记录", notes = "分页查询Result")
    @GetMapping ("/findResultByProblem")
    public Result<Page<com.masonluo.mlonlinejudge.model.vo.ResultVo>> findResultByProblem(@RequestParam("userId") Integer userId,
                                                                                    @RequestParam(value = "problemId") Integer problemId,
                                                                                    @RequestParam(value = "testId") Integer testId,
                                                                                    @RequestParam("current") Integer current,
                                                                                    @RequestParam("limit") Integer limit) throws Exception{
        return Result.ok(resultService.findResultByProblem(userId,problemId,testId,current,limit));
    }


    @ResponseBody
    @ApiOperation(value = "查看源代码")
    @GetMapping("/getSourceCode/{resultId}")
    public Result<java.lang.String> getSourceCodeByResultId(@PathVariable("resultId") Integer resultId){
        return Result.ok(resultService.findResourceCodeByResultId(resultId));
    }

}
