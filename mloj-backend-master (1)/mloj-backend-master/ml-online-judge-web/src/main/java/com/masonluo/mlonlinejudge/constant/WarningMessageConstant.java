package com.masonluo.mlonlinejudge.constant;

public class WarningMessageConstant {
    /**
     * 用户提交 JAVA 代码到评判系统后，评判系统给出的警告信息
     */
    public static String JAVA_WARNING_MESSAGE = "WARNING: A command line option has enabled the Security Manager\nWARNING: The Security Manager is deprecated and will be removed in a future release\n";
}
