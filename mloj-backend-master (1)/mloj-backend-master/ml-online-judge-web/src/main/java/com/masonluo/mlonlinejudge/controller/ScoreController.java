package com.masonluo.mlonlinejudge.controller;



import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.masonluo.mlonlinejudge.entity.Role;
import com.masonluo.mlonlinejudge.entity.Score;
import com.masonluo.mlonlinejudge.model.param.ScoreParam;
import com.masonluo.mlonlinejudge.result.Result;
import com.masonluo.mlonlinejudge.service.ScoreService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 学生成绩表 前端控制器
 * </p>
 *
 * @author yangqi
 * @since 2021-08-19
 */
@RestController
@RequestMapping("/score")
public class ScoreController {
    @Autowired
    private ScoreService scoreService;

    @ApiOperation(value = "录入学生成绩")
    @PostMapping("addScore")
    public Result<List<Score>> addScore(@RequestBody List<Score> scores) {
        boolean save = scoreService.saveBatch(scores);
        if(save) {
            return Result.created(scores);
        } else {
            return Result.error();
        }
    }

    @ApiOperation(value = "修改学生成绩")
    @PostMapping("updateScore")
    public Result updateScore(@RequestBody @Valid ScoreParam scoreParam){
        scoreService.update(scoreParam);
        return Result.ok();
    }

    @ApiOperation(value = "按成绩id删除学生成绩")
    @DeleteMapping("deleteScore/{id}")
    public boolean removeScore(@PathVariable Integer id){
        Map<String,Object> map = new HashMap<>();
        map.put("exam_id",id);

        boolean flag = scoreService.removeByMap(map);

        return flag;
    }


    //current 当前页
    //limit 每页记录数
    @ApiOperation(value = "分页查询学生成绩")
    @GetMapping("pageSorce/{current}/{limit}")
    public Result<List<Score>> pageListSorce(@PathVariable long current,
                             @PathVariable long limit) {

        Page<Score> pageSorce = new Page<>(current,limit);
        scoreService.page(pageSorce,null);

        long total = pageSorce.getTotal();//总记录数
        List<Score> records = pageSorce.getRecords(); //数据list集合

        return Result.ok(records);
    }


    @ApiOperation(value = "根据班级id获取学生成绩")
    @GetMapping("findByClassId/{classId}")
    public Result<List<Score>> findByClass(@PathVariable Integer classId){
        Map<String,Object> map = new HashMap<>();
        map.put("class_id",classId);
        List<Score> scores = scoreService.listByMap(map);
        return Result.ok(scores);
    }

    @ApiOperation(value = "根据学生id获取学生成绩")
    @GetMapping("findByStudentId/{studentId}")
    public Result<List<Score>> findByStudent(@PathVariable Integer studentId){
        Map<String,Object> map = new HashMap<>();
        map.put("student_id",studentId);
        List<Score> scores = scoreService.listByMap(map);
        return Result.ok(scores);
    }









}

