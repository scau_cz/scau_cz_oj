package com.masonluo.mlonlinejudge.dao;

import com.alibaba.fastjson.JSON;
import com.masonluo.mlonlinejudge.entity.Problem;
import com.masonluo.mlonlinejudge.entity.ProgramAnswerContent;
import com.masonluo.mlonlinejudge.entity.ProgramAnswerEntity;
import com.masonluo.mlonlinejudge.model.Pair;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LZC
 * @create 2021-12-21 22:55
 */
@SpringBootTest
public class ProgramAnswerServiceImplTest {
    @Autowired
    ProgramAnswerRepository programAnswerRepository;
    @Autowired
    ProblemRepository problemRepository;
    @Test
    public void test(){
        List<Pair<Integer,String>> list = new ArrayList<>();
        for(int i=1;i<=1;i++){
            Pair<Integer,String> pair = new Pair<>();
            pair.setFirst(i);
            pair.setSecond("ABC\n" +
                    "A B CD\n" +
                    "ABC DD\n" +
                    "1A2A3C");
            list.add(pair);
        }
        ProgramAnswerEntity programAnswerEntity = new ProgramAnswerEntity();
        programAnswerEntity.setTest_case_id("a7e74321-e339-4812-aecd-2aaa5ec319e0");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[");
        for(Pair<Integer,String> pair:list){
            ProgramAnswerContent content = new ProgramAnswerContent();
            content.setId(pair.getFirst());
            content.setAnswer(pair.getSecond());
            stringBuffer.append(content.toString());
            stringBuffer.append(",");
        }
        stringBuffer = stringBuffer.deleteCharAt(stringBuffer.length()-1);
        stringBuffer.append("]");
        programAnswerEntity.setAnswer_content(stringBuffer.toString());
        programAnswerRepository.insertIntoProgramAnswer(programAnswerEntity);
    }
    @Test
    public void testQuery(){
//        List<ProgramAnswerEntity> list = programAnswerRepository.findAnswer("testCaseId");
//        for(ProgramAnswerEntity entity:list){
////            String s = "[{\"id\":1,\"answer\":\"1\"},{\"id\":2,\"answer\":\"2\"},{\"id\":3,\"answer\":\"3\"},{\"id\":4,\"answer\":\"4\"},{\"id\":5,\"answer\":\"5\"}]";
//            String s = entity.getAnswer_content();
//            List<ProgramAnswerContent> contents = JSON.parseArray(s,ProgramAnswerContent.class);
//
//            for(ProgramAnswerContent pc :contents){
//                System.out.println(pc.getAnswer());
//            }
//            System.out.println(entity.toString());
//        }
    }
    @Test
    public void testUpdate(){
//        List<Problem> datas = problemRepository.listAll();
//        for(Problem problem:datas){
//            if(problem.getTestCaseId()!=null)
//                programAnswerRepository.updateProgramAnswer(problem.getTestCaseId(),problem.getId());
//        }
//        for(int i=0;i<10;i++){
//            int row = programAnswerRepository.updateProgramAnswer("3279a192-3c16-4750-8f66-bb7e864d608b",150);
//            System.out.println("row=="+row);
//        }userRoleRepository.getRoleIdByUserId(userId);

    }
}
