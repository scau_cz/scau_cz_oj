package com.masonluo.mlonlinejudge.dao;

import com.masonluo.mlonlinejudge.entity.ExperimentScore;
import com.masonluo.mlonlinejudge.mapper.ExperimentMapper;
import com.masonluo.mlonlinejudge.mapper.ExperimentResultMapper;
import com.masonluo.mlonlinejudge.model.bo.SchoolBo;
import com.masonluo.mlonlinejudge.model.bo.StudentBo;
import com.masonluo.mlonlinejudge.model.dto.ExperimentScoreResultDto;
import com.masonluo.mlonlinejudge.model.vo.ExperimentScoreResultVo;
import com.masonluo.mlonlinejudge.service.ClassService;
import com.masonluo.mlonlinejudge.service.ExperimentResultService;
import com.masonluo.mlonlinejudge.service.SchoolService;
import com.masonluo.mlonlinejudge.service.SolutionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author masonluo
 * @date 2021/3/27 11:22 下午
 */
@SpringBootTest
public class ProblemRepositoryTest {
    @Autowired
    private ProblemRepository repository;
    @Autowired
    private ExperimentResultRepository experimentResultRepository;
    @Autowired
    private ExperimentResultService experimentResultService;
    @Autowired
    private ExperimentSchoolRepository experimentSchoolRepository;
    @Autowired
    private ExperimentProblemRepository experimentProblemRepository;
    @Autowired
    private SolutionService solutionService;
    @Autowired
    private ClassService classService;
    @Autowired
    private ExperimentResultMapper experimentResultMapper;
    @Autowired
    private SchoolService schoolService;
    @Test
    public void test(){
        repository.listByIds(Arrays.asList(5,6,7));
    }

    @Test
    public void test2() {
        List<SchoolBo> list = schoolService.findList(1, 1);
    }

    @Test
    public void test1(){
        //List<ExperimentScoreResultDto> experimentScoreResultDtos = experimentResultService.findByClassId(3, 80, 1, 5000);


        /*List<ExperimentScoreResultDto> experimentScoreResultDtos1 = experimentSchoolRepository.listExperimentsByUserId(1);
        List<ExperimentScore> scoreByExperimentIds = experimentProblemRepository.findScoreByExperimentIds(experimentScoreResultDtos1);
        System.out.println("-----------------------------------");
        if (experimentScoreResultDtos1.size()==scoreByExperimentIds.size()){
            for (int i = 0; i<experimentScoreResultDtos1.size();i++) {
                //System.out.println(experimentScoreResultDtos1.get(i).getExperimentId()+"\t"+scoreByExperimentIds.get(i).getId());
                scoreByExperimentIds.get(i).setPassNum(solutionService.countPassProblemByUserIdAndExperimentId(1,experimentScoreResultDtos1.get(i).getExperimentId()));
                System.out.println(Collections.singletonList(scoreByExperimentIds.get(i)));
            }
        }*/

//        System.out.println("-----------------------------------");
        /*List<Integer> problemIds = experimentProblemRepository.listProblemIdsByExperimentId(108, null);
        List<Integer> passProblems = solutionService.listPassProblem(problemIds,1);
        System.out.println(problemIds);
        System.out.println(passProblems);
        Integer passedNum = solutionService.countPassProblemByUserIdAndExperimentId(1, 108);
        System.out.println(passedNum);*/
        /*List<StudentBo> studentBos = classService.getStudentByClassId(80, 3, 1, 5000);
        List<Integer> studentIds = new ArrayList<>();
        studentBos.forEach(studentBo -> {
            studentIds.add(studentBo.getUserId());
        });
        //System.out.println(studentIds);
        System.out.println("-------------------------------------------------");
        List<ExperimentScoreResultDto> experimentScoreResultDtos = experimentResultService.refeshExperimentScoreResult(studentIds);
        experimentResultMapper.dtoToDo(experimentScoreResultDtos).forEach(experimentScoreResultDto -> {
            System.out.println(experimentScoreResultDto);
            //System.out.println(experimentScoreResultDto.getExperimentScore());
        });
        experimentResultMapper.dtoToDo(experimentScoreResultDtos).forEach(experimentScoreResultDto -> {
            //System.out.println(experimentScoreResultDto);
            System.out.println(experimentScoreResultDto.getExperimentScore());
        });
        System.out.println("-----------------------------------");*/
//        List<Integer> userIds = new ArrayList<>();
//        userIds.add(671);
//        userIds.add(672);
        //List<ExperimentScoreResultDto> scoreByUserIds = experimentResultService.findScoreByUserIds(userIds);
//        List<ExperimentScoreResultVo> scoreByTeacherIdAndClassIds = experimentResultService.findScoreByTeacherIdAndClassId(3, 80, 1, 2);
//        scoreByTeacherIdAndClassIds.forEach(scoreByUserId->{
//            System.out.println(scoreByUserId);
//        });
        /*List<ExperimentScoreResultDto> experimentScoreResultDtos = experimentResultService.refeshExperimentScoreResult(userIds);
        experimentScoreResultDtos.forEach(scoreByUserId->{
            System.out.println(scoreByUserId);
        });*/
    }

}