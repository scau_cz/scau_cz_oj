/**
 * Bo (Business Object)，主要作用于Service层参数的返回。
 * 如果Controller层所需要的展示的参数和Bo对象一样的话，可以不用进行Bo -> Vo的转换操作，
 * 不必太过于遵循转换模型,直接在Controller层返回Bo即可
 *
 * @author masonluo
 * @date 2021/2/10 3:51 下午
 */
package com.masonluo.mlonlinejudge.model.bo;

