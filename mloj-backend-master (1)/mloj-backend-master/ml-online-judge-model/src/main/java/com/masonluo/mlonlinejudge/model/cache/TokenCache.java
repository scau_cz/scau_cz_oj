package com.masonluo.mlonlinejudge.model.cache;

/**
 * 用来缓存在Redis里面的数据结构，保存了Token的过期时间等信息
 *
 * @author masonluo
 * @date 2021/1/20 2:56 下午
 */
public class TokenCache {
    private String token;

    private long createdTimestamp;

    private long expiredSeconds;

    private String username;

    public TokenCache() {
    }

    public TokenCache(String token, long createdTimestamp, long expiredSeconds, String username) {
        this.token = token;
        this.createdTimestamp = createdTimestamp;
        this.expiredSeconds = expiredSeconds;
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(long createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public long getExpiredSeconds() {
        return expiredSeconds;
    }

    public void setExpiredSeconds(long expiredSeconds) {
        this.expiredSeconds = expiredSeconds;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "TokenCache{" +
                "token='" + token + '\'' +
                ", createdTimestamp=" + createdTimestamp +
                ", expiredSeconds=" + expiredSeconds +
                ", username='" + username + '\'' +
                '}';
    }
}
