package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

@TableName("t_exam_school_class")
public class ExamSchoolClass extends BaseEntity{

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    @TableField(value = "classId")
    private Integer classId;
    private Integer schoolId;
    private Integer examId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public Integer getExamId() {
        return examId;
    }

    public void setExamId(Integer examId) {
        this.examId = examId;
    }

    @Override
    public String toString(){
        return "ExamSchoolClass{" +
                "id=" + id +
                "classId=" + classId +
                "schoolId=" + schoolId +
                "examId=" + examId +
                "}";
    }
}
