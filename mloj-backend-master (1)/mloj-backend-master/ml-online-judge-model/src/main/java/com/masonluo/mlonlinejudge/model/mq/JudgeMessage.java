package com.masonluo.mlonlinejudge.model.mq;

import java.util.Date;

/**
 * 用来在MQ之间进行消息的传递, 保存着要评判的数据的id
 *
 * @author masonluo
 * @date 2021/2/6 3:01 下午
 */
public class JudgeMessage {
    private Integer solutionId;

    private Integer problemId;

    private Integer userId;

    private Date createTime;

    private Integer examAndExperimentId;

    public Integer getExamAndExperimentId() {
        return examAndExperimentId;
    }

    public void setExamAndExperimentId(Integer examAndExperimentId) {
        this.examAndExperimentId = examAndExperimentId;
    }

    public Integer getSolutionId() {
        return solutionId;
    }

    public void setSolutionId(Integer solutionId) {
        this.solutionId = solutionId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "JudgeParam{" +
                "solutionId=" + solutionId +
                ", problemId=" + problemId +
                ", userId=" + userId +
                ", createTime=" + createTime +
                '}';
    }
}
