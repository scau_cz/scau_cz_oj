package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 问题标签
 *
 * @author masonluo
 * @date 2020/12/26 4:15 下午
 */
@TableName("t_tag")
public class Tag extends BaseEntity {

    private String name;


    public Tag() {
    }

    public Tag(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", createdTime=" + createdTime +
                ", updatedTime=" + updatedTime +
                ", name='" + name + '\'' +
                '}';
    }
}
