package com.masonluo.mlonlinejudge.model.dto;

public class ExamScoreDto {

    private Integer userId;

    private Integer examId;

    private String examName;

    private Integer classId;

    private Integer singleChoiceScore;

    private Integer trueFalseQuestionScore;

    private Integer programQuestionScore;

    private Integer totalScore;

    @Override
    public String toString() {
        return "ExamScoreDto{" +
                "userId=" + userId +
                ", examId=" + examId +
                ", examName='" + examName + '\'' +
                ", classId=" + classId +
                ", singleChoiceScore=" + singleChoiceScore +
                ", trueFalseQuestionScore=" + trueFalseQuestionScore +
                ", programQuestionScore=" + programQuestionScore +
                ", totalScore=" + totalScore +
                '}';
    }

    public Integer getExamId() {
        return examId;
    }

    public void setExamId(Integer examId) {
        this.examId = examId;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getSingleChoiceScore() {
        return singleChoiceScore;
    }

    public void setSingleChoiceScore(Integer singleChoiceScore) {
        this.singleChoiceScore = singleChoiceScore;
    }

    public Integer getTrueFalseQuestionScore() {
        return trueFalseQuestionScore;
    }

    public void setTrueFalseQuestionScore(Integer trueFalseQuestionScore) {
        this.trueFalseQuestionScore = trueFalseQuestionScore;
    }

    public Integer getProgramQuestionScore() {
        return programQuestionScore;
    }

    public void setProgramQuestionScore(Integer programQuestionScore) {
        this.programQuestionScore = programQuestionScore;
    }

    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
