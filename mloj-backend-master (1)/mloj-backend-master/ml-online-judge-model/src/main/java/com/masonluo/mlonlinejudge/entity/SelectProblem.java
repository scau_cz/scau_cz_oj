package com.masonluo.mlonlinejudge.entity;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.masonluo.mlonlinejudge.enums.Level;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;


@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@TableName("t_sel_problem")
public class SelectProblem extends BaseEntity{
    //private Integer id;

    /**
     *  0代表选择题，1代表判断题，2代表程序题
     */
    private Integer type;

    /**
     * 题目
     */
    private String title;

    /**
     * 题目列表
     */
    private String answer;

    /**
     * 答案
     */
    private String correct;

    /**
     * 题目难度：0代表低级  1代表中级  2代表高级 
     */
    private Level level;


    /**
     * 创建时间
     */
    //@TableField(fill = FieldFill.INSERT)
    private LocalDateTime createdTime;

    /**
     * 修改时间
     */
    //@TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updatedTime;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 章节id
     */
    private Integer indexId;


    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getIndexId() {
        return indexId;
    }

    public void setIndexId(Integer indexId) {
        this.indexId = indexId;
    }



    @Override
    public String toString() {
        return "SelectProblem{" +
                "type=" + type +
                ", title='" + title + '\'' +
                ", answer='" + answer + '\'' +
                ", correct='" + correct + '\'' +
                ", level='" + level + '\'' +
                ", createdTime=" + createdTime +
                ", updatedTime=" + updatedTime +
                ", userId=" + userId +
                ", indexId=" + indexId +
                '}';
    }
}