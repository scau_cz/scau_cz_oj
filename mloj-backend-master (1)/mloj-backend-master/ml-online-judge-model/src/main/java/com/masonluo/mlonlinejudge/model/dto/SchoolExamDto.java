package com.masonluo.mlonlinejudge.model.dto;

public class SchoolExamDto {
    private Integer examId;

    private String examName;

    public Integer getExamId() {
        return examId;
    }

    public void setExamId(Integer examId) {
        this.examId = examId;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    @Override
    public String toString() {
        return "SchoolExamDto{" +
                "examId=" + examId +
                ", examName='" + examName + '\'' +
                '}';
    }
}
