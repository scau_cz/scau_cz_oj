package com.masonluo.mlonlinejudge.mapper;

import com.masonluo.mlonlinejudge.entity.Problem;
import com.masonluo.mlonlinejudge.entity.Tag;
import com.masonluo.mlonlinejudge.model.bo.ProblemBo;
import com.masonluo.mlonlinejudge.model.bo.TagBo;
import com.masonluo.mlonlinejudge.model.param.TagParam;
import com.masonluo.mlonlinejudge.model.vo.TagVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;
import java.util.List;

/**
 * @author masonluo
 * @date 2021/1/22 4:17 下午
 */
@Mapper(componentModel = "spring")
public interface TagMapper extends BasicObjectMapper<Tag, TagBo> {

    TagVo doToVo(Tag tag);


    List<TagVo> doToVo(Collection<Tag> tags);


    Tag from(TagVo tagVo);


    TagBo boToParam(TagParam tagParam);

    //List<Tag> from(Collection<TagVo> tagVos);
}
