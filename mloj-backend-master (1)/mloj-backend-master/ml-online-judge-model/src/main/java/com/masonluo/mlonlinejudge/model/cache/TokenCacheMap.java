package com.masonluo.mlonlinejudge.model.cache;

import com.masonluo.mlonlinejudge.enums.TokenType;

import java.util.HashMap;
import java.util.Map;

/**
 * 用来缓存Token信息的结构， 保存着Token本身信息、刷新这个Token的Token信息以及这个Token的用户名
 *
 * @author masonluo
 * @date 2021/1/20 3:06 下午
 */
public class TokenCacheMap extends HashMap<Object, Object> {

    public static final String KEY_USERNAME = "username";

    public static final String KEY_TOKEN = "token";

    private String username;

    private TokenCache token;

    private TokenType tokenType;

    public TokenCacheMap() {
    }

    public TokenCacheMap(Map<?, ?> m) {
        super(m);
    }

    public String getUsername() {
        return (String) get(KEY_USERNAME);
    }

    public void setUsername(String username) {
        this.username = username;
        put(KEY_USERNAME, username);
    }

    public TokenCache getToken() {
        return (TokenCache) get(KEY_TOKEN);
    }

    public void setToken(TokenCache token) {
        this.token = token;
        put(KEY_TOKEN, token);
    }

    public TokenType getTokenType() {
        return tokenType;
    }

    public void setTokenType(TokenType tokenType) {
        this.tokenType = tokenType;
    }
}
