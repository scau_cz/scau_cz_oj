package com.masonluo.mlonlinejudge.model.bo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author masonluo
 * @date 2021/2/10 3:51 下午
 */
public class IndexProblemBo {
    private Integer indexId;

    private List<Integer> problemId = new ArrayList<>();

    public Integer getIndexId() {
        return indexId;
    }

    public void setIndexId(Integer indexId) {
        this.indexId = indexId;
    }

    public List<Integer> getProblemId() {
        return problemId;
    }

    public void setProblemId(List<Integer> problemId) {
        this.problemId = problemId;
    }
}
