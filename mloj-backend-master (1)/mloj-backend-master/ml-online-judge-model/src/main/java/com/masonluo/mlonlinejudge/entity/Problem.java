package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.masonluo.mlonlinejudge.enums.Level;

/**
 * @author masonluo
 * @date 2020/12/26 3:59 下午
 */
@TableName("t_problem")
public class Problem extends BaseEntity {
    /**
     * 该问题上传者的用户id
     */
    private Integer userId;

    private String title;
    /**
     * 题目描述
     */
    private String description;
    /**
     * 输入描述
     */
    private String inputDescription;
    /**
     * 输出描述
     */
    private String outputDescription;
    /**
     * 时间限制，默认为s
     */
    private Integer timeLimit;
    /**
     * 内存限制，默认为B
     */
    private Integer memoryLimit;
    /**
     * 难度等级
     */
    private Level level;
    /**
     * 提示
     */
    private String hint;
    /**
     * 是否可见
     */
    private boolean visible;
    /**
     * 测试数据id
     */
    private String testCaseId;
    /**
     * 参考答案
     */
    private String answer;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInputDescription() {
        return inputDescription;
    }

    public void setInputDescription(String inputDescription) {
        this.inputDescription = inputDescription;
    }

    public String getOutputDescription() {
        return outputDescription;
    }

    public void setOutputDescription(String outputDescription) {
        this.outputDescription = outputDescription;
    }

    public Integer getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(Integer timeLimit) {
        this.timeLimit = timeLimit;
    }

    public Integer getMemoryLimit() {
        return memoryLimit;
    }

    public void setMemoryLimit(Integer memoryLimit) {
        this.memoryLimit = memoryLimit;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getTestCaseId() {
        return testCaseId;
    }

    public void setTestCaseId(String testCaseId) {
        this.testCaseId = testCaseId;
    }

    @Override
    public String toString() {
        return "Problem{" +
                "id=" + id +
                ", createdTime=" + createdTime +
                ", updatedTime=" + updatedTime +
                ", userId=" + userId +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", inputDescription='" + inputDescription + '\'' +
                ", outputDescription='" + outputDescription + '\'' +
                ", timeLimit=" + timeLimit +
                ", memoryLimit=" + memoryLimit +
                ", level=" + level +
                ", hint='" + hint + '\'' +
                ", visible=" + visible +
                ", testCaseId='" + testCaseId + '\'' +
                '}';
    }
}
