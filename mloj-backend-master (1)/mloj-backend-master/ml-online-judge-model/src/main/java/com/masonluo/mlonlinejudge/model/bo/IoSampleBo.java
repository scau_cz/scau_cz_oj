package com.masonluo.mlonlinejudge.model.bo;

/**
 * @author masonluo
 * @date 2021/1/22 5:22 下午
 */
public class IoSampleBo {
    private String inputSample;

    private String outputSample;

    public IoSampleBo() {
    }

    public IoSampleBo(String inputSample, String outputSample) {
        this.inputSample = inputSample;
        this.outputSample = outputSample;
    }

    public String getInputSample() {
        return inputSample;
    }

    public void setInputSample(String inputSample) {
        this.inputSample = inputSample;
    }

    public String getOutputSample() {
        return outputSample;
    }

    public void setOutputSample(String outputSample) {
        this.outputSample = outputSample;
    }
}
