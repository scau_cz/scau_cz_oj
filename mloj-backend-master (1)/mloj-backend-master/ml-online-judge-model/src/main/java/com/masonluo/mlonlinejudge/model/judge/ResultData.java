package com.masonluo.mlonlinejudge.model.judge;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.masonluo.mlonlinejudge.enums.ExecError;
import com.masonluo.mlonlinejudge.enums.ExecResult;

/**
 * 如果编译成功， Judge Server服务器返回的Json字符串包含一个data
 * <p>
 * 该类就是data所包含的数据
 *
 * @author masonluo
 * @date 2021/2/5 9:26 下午
 */
public class ResultData {

    @JsonProperty("cpu_time")
    private Integer cpuTime;

    @JsonProperty("real_time")
    private Integer realTime;

    private Integer memory;

    private Integer signal;

    @JsonProperty("exit_code")
    private Integer exitCode;

    private ExecError error;

    private ExecResult result;

    @JsonProperty("test_case")
    private String testCase;

    @JsonProperty("output_md5")
    private String outputMd5;

    private String output;

    private String trueAnswer;

    public Integer getCpuTime() {
        return cpuTime;
    }

    public void setCpuTime(Integer cpuTime) {
        this.cpuTime = cpuTime;
    }

    public Integer getRealTime() {
        return realTime;
    }

    public void setRealTime(Integer realTime) {
        this.realTime = realTime;
    }

    public Integer getMemory() {
        return memory;
    }

    public void setMemory(Integer memory) {
        this.memory = memory;
    }

    public Integer getSignal() {
        return signal;
    }

    public void setSignal(Integer signal) {
        this.signal = signal;
    }

    public Integer getExitCode() {
        return exitCode;
    }

    public void setExitCode(Integer exitCode) {
        this.exitCode = exitCode;
    }

    public ExecError getError() {
        return error;
    }

    public void setError(ExecError error) {
        this.error = error;
    }

    public ExecResult getResult() {
        return result;
    }

    public void setResult(ExecResult result) {
        this.result = result;
    }

    public String getTestCase() {
        return testCase;
    }

    public void setTestCase(String testCase) {
        this.testCase = testCase;
    }

    public String getOutputMd5() {
        return outputMd5;
    }

    public void setOutputMd5(String outputMd5) {
        this.outputMd5 = outputMd5;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public void setTrueAnswer(String trueAnswer) {
        this.trueAnswer = trueAnswer;
    }

    public String getTrueAnswer() {
        return trueAnswer;
    }
}
