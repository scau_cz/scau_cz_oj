package com.masonluo.mlonlinejudge.model.vo;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2021-08-13 12:56
 **/
public class SchoolVo {

    private String schoolName;

    private String email;

    /*private String create_time;

    private String update_time;*/

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

   /*public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }*/
}


