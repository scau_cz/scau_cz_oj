package com.masonluo.mlonlinejudge.model.vo;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author masonluo
 * @date 2021/2/9 11:08 下午
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JudgeResultVo {
    private Integer solutionId;

    private Integer problemId;

    /**
     * 是否已经评判完成
     */
    private boolean done;

    private Integer resultId;

    public Integer getSolutionId() {
        return solutionId;
    }

    public void setSolutionId(Integer solutionId) {
        this.solutionId = solutionId;
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public Integer getResultId() {
        return resultId;
    }

    public void setResultId(Integer resultId) {
        this.resultId = resultId;
    }
}
