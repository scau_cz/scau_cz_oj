package com.masonluo.mlonlinejudge.model.bo;

import com.masonluo.mlonlinejudge.model.dto.SelProblemDetailDto;
import com.masonluo.mlonlinejudge.model.dto.TestPaperSelProblemDetailDto;

import java.util.List;

public class TestPaperDetailBo {
    private Integer examId[];

    private String title;

    private String desc;

    private Integer score;

    private Integer creatorId;

    private List<TestPaperSelProblemDetailDto> choicesProblem;

    private List<TestPaperSelProblemDetailDto> trueOrFalse;

    private List<TestPaperProblemBo> programmingQuestions;

    public Integer[] getExamId() {
        return examId;
    }

    public void setExamId(Integer[] examId) {
        this.examId = examId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public List<TestPaperSelProblemDetailDto> getChoicesProblem() {
        return choicesProblem;
    }

    public void setChoicesProblem(List<TestPaperSelProblemDetailDto> choicesProblem) {
        this.choicesProblem = choicesProblem;
    }

    public List<TestPaperSelProblemDetailDto> getTrueOrFalse() {
        return trueOrFalse;
    }

    public void setTrueOrFalse(List<TestPaperSelProblemDetailDto> trueOrFalse) {
        this.trueOrFalse = trueOrFalse;
    }

    public List<TestPaperProblemBo> getProgrammingQuestions() {
        return programmingQuestions;
    }

    public void setProgrammingQuestions(List<TestPaperProblemBo> programmingQuestions) {
        this.programmingQuestions = programmingQuestions;
    }

    public Integer getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Integer creatorId) {
        this.creatorId = creatorId;
    }

    @Override
    public String toString() {
        return "TestPaperDetailBo{" +
                "examId=" + examId +
                ", title='" + title + '\'' +
                ", desc='" + desc + '\'' +
                ", score=" + score +
                ", creatorId=" + creatorId +
                ", choicesProblem=" + choicesProblem +
                ", trueOrFalse=" + trueOrFalse +
                ", programmingQuestions=" + programmingQuestions +
                '}';
    }
}
