package com.masonluo.mlonlinejudge.model.param;

import javax.validation.constraints.NotNull;

public class ExamSchoolClassParam {

    @NotNull
    private Integer classId;
    @NotNull
    private Integer schoolId;
    @NotNull
    private Integer examId;

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public Integer getExamId() {
        return examId;
    }

    public void setExamId(Integer examId) {
        this.examId = examId;
    }
}
