package com.masonluo.mlonlinejudge.entity;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2022-02-25 19:55
 **/
@JsonInclude(JsonInclude.Include.NON_NULL)
@TableName("t_experiment_result")
@ApiModel(value="TExperimentResult对象", description="实验结果表")
public class ExperimentScoreResult extends BaseEntity{

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "用户学号")
    private String userNum;

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "学校id")
    private Integer schoolId;

    @ApiModelProperty(value = "班级id")
    private Integer classId;

    @ApiModelProperty(value = "实验id")
    private Integer experimentId;

    @ApiModelProperty(value = "实验成绩")
    private String experimentScore;

    @ApiModelProperty(value = "实验各个题目的情况")
    @TableField(value = "experiment_problem_result_json")
    private String experimentProblemResult;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserNum() {
        return userNum;
    }

    public void setUserNum(String userNum) {
        this.userNum = userNum;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getExperimentId() {
        return experimentId;
    }

    public void setExperimentId(Integer experimentId) {
        this.experimentId = experimentId;
    }

    public String getExperimentScore() {
        return experimentScore;
    }

    public void setExperimentScore(String experimentScore) {
        this.experimentScore = experimentScore;
    }

    public String getExperimentProblemResult() {
        return experimentProblemResult;
    }

    public void setExperimentProblemResult(String experimentProblemResult) {
        this.experimentProblemResult = experimentProblemResult;
    }


    @Override
    public String toString() {
        return "ExperimentScoreResult{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", schoolId=" + schoolId +
                ", classId=" + classId +
                ", experimentId=" + experimentId +
                ", experimentScore='" + experimentScore + '\'' +
                '}';
    }
}


