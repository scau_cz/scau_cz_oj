package com.masonluo.mlonlinejudge.model.dto;

import com.masonluo.mlonlinejudge.enums.TokenType;

/**
 * @author masonluo
 * @date 2021/1/17 3:29 下午
 */
public class TokenDto {
    private String token;

    private TokenType tokenType;

    private long createTimeStamp;

    private long expiredSeconds;

    public TokenDto() {
    }

    public TokenDto(String token, TokenType tokenType, long createTimeStamp, long expiredSeconds) {
        this.token = token;
        this.tokenType = tokenType;
        this.createTimeStamp = createTimeStamp;
        this.expiredSeconds = expiredSeconds;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public TokenType getTokenType() {
        return tokenType;
    }

    public void setTokenType(TokenType tokenType) {
        this.tokenType = tokenType;
    }

    public long getCreateTimeStamp() {
        return createTimeStamp;
    }

    public void setCreateTimeStamp(long createTimeStamp) {
        this.createTimeStamp = createTimeStamp;
    }

    public long getExpiredSeconds() {
        return expiredSeconds;
    }

    public void setExpiredSeconds(long expiredSeconds) {
        this.expiredSeconds = expiredSeconds;
    }

    @Override
    public String toString() {
        return "TokenDto{" +
                "token='" + token + '\'' +
                ", tokenType=" + tokenType +
                ", createTimeStamp=" + createTimeStamp +
                ", expiredSeconds=" + expiredSeconds +
                '}';
    }
}
