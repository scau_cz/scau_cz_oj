package com.masonluo.mlonlinejudge.model.dto;

import com.masonluo.mlonlinejudge.model.bo.IoSampleBo;
import com.masonluo.mlonlinejudge.model.bo.TagBo;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/1/25 3:11 下午
 */
public class ProblemUploadDto {
    private Integer userId;

    private String title;

    private String description;

    private String inputDescription;

    private String outputDescription;

    private Integer timeLimit;

    private Integer memoryLimit;

    private Integer level;

    private String hint;

    private boolean visible;

    private List<TagBo> tags;

    private List<IoSampleBo> ioSamples;

    private String testCaseId;

    private String answer;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInputDescription() {
        return inputDescription;
    }

    public void setInputDescription(String inputDescription) {
        this.inputDescription = inputDescription;
    }

    public String getOutputDescription() {
        return outputDescription;
    }

    public void setOutputDescription(String outputDescription) {
        this.outputDescription = outputDescription;
    }

    public Integer getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(Integer timeLimit) {
        this.timeLimit = timeLimit;
    }

    public Integer getMemoryLimit() {
        return memoryLimit;
    }

    public void setMemoryLimit(Integer memoryLimit) {
        this.memoryLimit = memoryLimit;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public List<TagBo> getTags() {
        return tags;
    }

    public void setTags(List<TagBo> tags) {
        this.tags = tags;
    }

    public List<IoSampleBo> getIoSamples() {
        return ioSamples;
    }

    public void setIoSamples(List<IoSampleBo> ioSamples) {
        this.ioSamples = ioSamples;
    }

    public String getTestCaseId() {
        return testCaseId;
    }

    public void setTestCaseId(String testCaseId) {
        this.testCaseId = testCaseId;
    }
}
