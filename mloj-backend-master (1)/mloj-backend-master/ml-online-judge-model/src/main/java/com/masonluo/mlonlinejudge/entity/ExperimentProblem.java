package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2021-10-05 00:28
 **/
@TableName("t_experiment_problem")
public class ExperimentProblem extends BaseEntity{

    private Integer experimentId;

    private Integer problemId;

    private String category;

    private String updatedSchoolIds;

    public Integer getExperimentId() {
        return experimentId;
    }

    public void setExperimentId(Integer experimentId) {
        this.experimentId = experimentId;
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUpdatedSchoolIds() {
        return updatedSchoolIds;
    }

    public void setUpdatedSchoolIds(String updatedSchoolIds) {
        this.updatedSchoolIds = updatedSchoolIds;
    }

    @Override
    public String toString() {
        return "ExperimentProblem{" +
                "id=" + id +
                ", experimentId=" + experimentId +
                ", problemId=" + problemId +
                ", createdTime=" + createdTime +
                ", updatedTime=" + updatedTime +
                '}';
    }
}


