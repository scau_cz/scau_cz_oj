package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 保存用户的角色信息
 *
 * @author masonluo
 * @date 2020/12/26 3:46 下午
 */
@TableName("t_user_role")
public class UserRole extends BaseEntity {
    private Integer userId;

    private Integer roleId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @Override
    public String toString() {
        return "UserRole{" +
                "id=" + id +
                ", createdTime=" + createdTime +
                ", updatedTime=" + updatedTime +
                ", userId=" + userId +
                ", roleId=" + roleId +
                '}';
    }
}
