package com.masonluo.mlonlinejudge.model.judge;

/**
 * @author masonluo
 * @date 2021/2/5 9:24 下午
 */
public class JudgeResult {
    private String err;

    /**
     * data有两种形式：
     * 1. 编译失败，返回一个编译失败的字符串
     * 2. 编译成功，进行运行，会返回一个{@link ResultData}类型的数据
     */
    private Object data;
    private String testCaseId;

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getTestCaseId() {
        return testCaseId;
    }

    public void setTestCaseId(String testCaseId) {
        this.testCaseId = testCaseId;
    }
}
