package com.masonluo.mlonlinejudge.model.param;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author masonluo
 * @date 2021/2/11 12:55 下午
 */
public class UserParam {

    private String username;

    private String password;

    private Integer schoolId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }
}
