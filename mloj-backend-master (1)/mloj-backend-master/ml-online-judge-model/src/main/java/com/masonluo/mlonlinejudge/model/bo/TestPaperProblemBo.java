package com.masonluo.mlonlinejudge.model.bo;

import java.util.ArrayList;
import java.util.List;

/**
*
*@author: yangqi
*@date: 2021-11-11
*/
public class TestPaperProblemBo {
    private Integer id;

    private Integer userId;

    private String title;

    private String description;

    private String inputDescription;

    private String outputDescription;

    private Integer timeLimit;

    private Integer memoryLimit;

    private String level;

    private String hint;

    private boolean visible;

    private List<TestPaperTagBo> tags = new ArrayList<>();

    private List<IoSampleBo> IoSamples = new ArrayList<>();

    private String testCaseId;

    private Integer score;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInputDescription() {
        return inputDescription;
    }

    public void setInputDescription(String inputDescription) {
        this.inputDescription = inputDescription;
    }

    public String getOutputDescription() {
        return outputDescription;
    }

    public void setOutputDescription(String outputDescription) {
        this.outputDescription = outputDescription;
    }

    public Integer getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(Integer timeLimit) {
        this.timeLimit = timeLimit;
    }

    public Integer getMemoryLimit() {
        return memoryLimit;
    }

    public void setMemoryLimit(Integer memoryLimit) {
        this.memoryLimit = memoryLimit;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public List<TestPaperTagBo> getTags() {
        return tags;
    }

    public void setTags(List<TestPaperTagBo> tags) {
        this.tags = tags;
    }

    public List<IoSampleBo> getIoSamples() {
        return IoSamples;
    }

    public void setIoSamples(List<IoSampleBo> ioSamples) {
        IoSamples = ioSamples;
    }

    public String getTestCaseId() {
        return testCaseId;
    }

    public void setTestCaseId(String testCaseId) {
        this.testCaseId = testCaseId;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "TestPaperProblemBo{" +
                "id=" + id +
                ", userId=" + userId +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", inputDescription='" + inputDescription + '\'' +
                ", outputDescription='" + outputDescription + '\'' +
                ", timeLimit=" + timeLimit +
                ", memoryLimit=" + memoryLimit +
                ", level='" + level + '\'' +
                ", hint='" + hint + '\'' +
                ", visible=" + visible +
                ", tags=" + tags +
                ", IoSamples=" + IoSamples +
                ", testCaseId='" + testCaseId + '\'' +
                ", score=" + score +
                '}';
    }
}
