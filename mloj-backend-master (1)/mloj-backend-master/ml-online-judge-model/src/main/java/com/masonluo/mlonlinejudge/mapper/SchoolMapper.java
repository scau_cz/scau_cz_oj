package com.masonluo.mlonlinejudge.mapper;


import com.masonluo.mlonlinejudge.entity.School;
import com.masonluo.mlonlinejudge.model.bo.SchoolBo;
import com.masonluo.mlonlinejudge.model.dto.SchoolCreateDto;
import com.masonluo.mlonlinejudge.model.param.SchoolParam;
import com.masonluo.mlonlinejudge.model.vo.SchoolVo;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SchoolMapper {

    School from(SchoolBo schoolBo);

    SchoolBo doToBo(School school);

    SchoolVo boToVo(SchoolBo bo);

    SchoolVo doToVo(School school);

    School voToDo(SchoolVo schoolVo);

    List<SchoolVo> boToVo(List<SchoolBo> Schools);

    List<SchoolBo> doToBo(List<School> selectAll);
}
