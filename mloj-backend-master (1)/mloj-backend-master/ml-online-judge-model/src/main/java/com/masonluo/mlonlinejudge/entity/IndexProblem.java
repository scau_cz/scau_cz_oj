package com.masonluo.mlonlinejudge.entity;

/**
 * 记录了每一个章节下面，有什么编程题目的类
 *
 * @author masonluo
 * @date 2021/2/10 3:17 下午
 */
public class IndexProblem extends BaseEntity {
    private Integer problemId;

    private Integer indexId;

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public Integer getIndexId() {
        return indexId;
    }

    public void setIndexId(Integer indexId) {
        this.indexId = indexId;
    }

    @Override
    public String toString() {
        return "IndexProblem{" +
                "id=" + id +
                ", createdTime=" + createdTime +
                ", updatedTime=" + updatedTime +
                ", problemId=" + problemId +
                ", indexId=" + indexId +
                '}';
    }
}
