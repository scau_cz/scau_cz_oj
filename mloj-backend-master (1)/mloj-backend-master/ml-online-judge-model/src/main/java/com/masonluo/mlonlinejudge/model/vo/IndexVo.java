package com.masonluo.mlonlinejudge.model.vo;

/**
 * @author masonluo
 * @date 2021/2/11 12:36 下午
 */
public class IndexVo {
    private Integer id;

    private String name;

    private Integer order;

    private Integer bookId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }
}
