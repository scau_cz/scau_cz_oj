package com.masonluo.mlonlinejudge.model.vo;

import java.util.List;

/**
 * @Author: shenma
 * @Date: 2024/4/4  12:50
 */
public class StatsOfWeeksVo {
    private List<StatsOfWeekVo> list;

    public List<StatsOfWeekVo> getList() {
        return list;
    }

    public void setList(List<StatsOfWeekVo> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "StatsOfWeeksVo{" +
                "list=" + list +
                '}';
    }
}
