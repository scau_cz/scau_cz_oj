package com.masonluo.mlonlinejudge.mapper.decorator;

import com.masonluo.mlonlinejudge.enums.ExecError;
import com.masonluo.mlonlinejudge.enums.ExecResult;
import com.masonluo.mlonlinejudge.mapper.ResultDataMapper;
import com.masonluo.mlonlinejudge.model.judge.ResultData;
import com.masonluo.mlonlinejudge.utils.CastUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author masonluo
 * @date 2021/2/5 11:09 下午
 */
public abstract class ResultDataMapperDecorator implements ResultDataMapper {
    @Override
    public ResultData map(Map<String, Object> map) {
        ResultData resultData = new ResultData();
        resultData.setResult(ExecResult.getById((int) map.get("result")));
        resultData.setError(ExecError.getById((int) map.get("error")));
        resultData.setCpuTime(CastUtils.caseToInteger(map.get("cpu_time")));
        resultData.setMemory(CastUtils.caseToInteger(map.get("memory")));
        resultData.setRealTime(CastUtils.caseToInteger(map.get("real_time")));
        resultData.setSignal((Integer) map.get("signal"));
        resultData.setExitCode((Integer) map.get("exit_code"));
        resultData.setOutputMd5((String) map.get("output_md5"));
        resultData.setOutput((String) map.get("output"));
        resultData.setTestCase((String) map.get("test_case"));
        return resultData;
    }

    @Override
    public List<ResultData> map(List<Map<String, Object>> map) {
        List<ResultData> res = new ArrayList<>();
        for (Map<String, Object> member : map) {
            res.add(map(member));
        }
        return res;
    }
}
