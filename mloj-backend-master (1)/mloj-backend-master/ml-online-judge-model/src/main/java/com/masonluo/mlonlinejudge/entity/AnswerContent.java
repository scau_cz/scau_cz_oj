package com.masonluo.mlonlinejudge.entity;

public class AnswerContent {
    //题目在试卷上的id
    private Integer id;
    //题目id
    private Integer problemId;
    //题目类型
    private Integer examProblemType;
    //学生答案:选择题/判断题:存储学生答案；编程题：存储编程题id
    private String answer;
    //1:正确；0：错误
    private Integer result;
    //单纯一道题的分数
    private Integer score;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public Integer getExamProblemType() {
        return examProblemType;
    }

    public void setExamProblemType(Integer examProblemType) {
        this.examProblemType = examProblemType;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public AnswerContent(Integer id, Integer problemId, Integer examProblemType, String answer, Integer result, Integer score) {
        this.id = id;
        this.problemId = problemId;
        this.examProblemType = examProblemType;
        this.answer = answer;
        this.result = result;
        this.score = score;
    }

    public AnswerContent() {
    }
}
