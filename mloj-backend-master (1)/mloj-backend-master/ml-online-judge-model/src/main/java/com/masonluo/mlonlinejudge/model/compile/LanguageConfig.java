package com.masonluo.mlonlinejudge.model.compile;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author masonluo
 * @date 2021/2/5 2:33 下午
 */
public class LanguageConfig {
    private String name;

    private Compile compile;

    private Run run;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Compile getCompile() {
        return compile;
    }

    public void setCompile(Compile compile) {
        this.compile = compile;
    }

    public Run getRun() {
        return run;
    }

    public void setRun(Run run) {
        this.run = run;
    }

    @Override
    public String toString() {
        return "LanguageConfig{" +
                "name='" + name + '\'' +
                ", compile=" + compile +
                ", run=" + run +
                '}';
    }
}
