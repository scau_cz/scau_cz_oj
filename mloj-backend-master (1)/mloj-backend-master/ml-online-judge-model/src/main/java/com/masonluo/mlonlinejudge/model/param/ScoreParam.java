package com.masonluo.mlonlinejudge.model.param;

import javax.validation.constraints.NotNull;

public class ScoreParam {
    @NotNull(message = "成绩id不能为空")
    private Integer id;

    @NotNull(message = "成绩不能为空")
    private Integer grade;

    @NotNull(message = "测验id不能为空")
    private Integer examId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Integer getExamId() {
        return examId;
    }

    public void setExamId(Integer examId) {
        this.examId = examId;
    }

    @Override
    public String toString() {
        return "ScoreParam{" +
                "id=" + id +
                ", grade=" + grade +
                ", examId=" + examId +
                '}';
    }
}
