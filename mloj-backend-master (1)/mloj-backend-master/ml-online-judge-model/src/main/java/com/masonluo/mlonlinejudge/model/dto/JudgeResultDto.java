package com.masonluo.mlonlinejudge.model.dto;

import com.masonluo.mlonlinejudge.entity.Result;
import com.masonluo.mlonlinejudge.enums.ExecResult;

import java.util.Date;

/**
 * @author 罗有冠
 * @date 2022/1/27 17:47
 */

public class JudgeResultDto {

    private Integer examId;

    private Integer problemId;

    private boolean compilerResult;

    /**
     * 编译错误提示信息，只有当{@link Result##compilerResult}为false时，才会有值
     */
    private String errorData;
    /**
     * 运行结果，只有当{@link Result##compilerResult}为true时，才有效
     */
    private ExecResult processResult;

    public Integer getExamId() {
        return examId;
    }

    public void setExamId(Integer examId) {
        this.examId = examId;
    }

    public String getErrorData() {
        return errorData;
    }

    public void setErrorData(String errorData) {
        this.errorData = errorData;
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public boolean isCompilerResult() {
        return compilerResult;
    }

    public void setCompilerResult(boolean compilerResult) {
        this.compilerResult = compilerResult;
    }

    public ExecResult getProcessResult() {
        return processResult;
    }

    public void setProcessResult(ExecResult processResult) {
        this.processResult = processResult;
    }

    public JudgeResultDto(Integer examId, Integer problemId, boolean compilerResult, String errorData, ExecResult processResult) {
        this.examId = examId;
        this.problemId = problemId;
        this.compilerResult = compilerResult;
        this.errorData = errorData;
        this.processResult = processResult;
    }

    public JudgeResultDto() {
    }
}
