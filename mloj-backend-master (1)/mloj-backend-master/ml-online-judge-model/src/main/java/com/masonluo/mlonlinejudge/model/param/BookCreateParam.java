package com.masonluo.mlonlinejudge.model.param;

/**
 * @author masonluo
 * @date 2021/2/10 3:30 下午
 */
public class BookCreateParam {
    private String name;

    private String author;

    private String publisher;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
}
