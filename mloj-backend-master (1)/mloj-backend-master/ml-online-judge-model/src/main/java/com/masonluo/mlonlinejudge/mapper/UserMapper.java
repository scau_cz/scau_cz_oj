package com.masonluo.mlonlinejudge.mapper;

import com.masonluo.mlonlinejudge.entity.User;
import com.masonluo.mlonlinejudge.model.bo.UserBo;
import com.masonluo.mlonlinejudge.model.dto.LoginDto;
import com.masonluo.mlonlinejudge.model.dto.UserDto;
import com.masonluo.mlonlinejudge.model.vo.LoginVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author masonluo
 * @date 2021/2/11 1:16 下午
 */
@Mapper(componentModel = "spring")
public interface UserMapper {
    LoginVo loginDtoToLoginVo(LoginDto dto);

    UserBo userDtoToUserBo(UserDto userDto);

    @Mappings({
            @Mapping(source="userId",target = "id")
    })
    User userBoToUser(UserBo bo);
}
