package com.masonluo.mlonlinejudge.model.param;

import io.swagger.annotations.ApiModelProperty;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInfoQueryParam {

    @ApiModelProperty(name = "学校id")
    Integer schoolId;

    @ApiModelProperty(name = "用户名")
    String usernameKey;

    @ApiModelProperty(name = "用户昵称")
    String nicknameKey;

    @ApiModelProperty(name = "用户角色")
    String role;

    @ApiModelProperty(name = "页码")
    Integer pageNum;

    @ApiModelProperty(name = "页数")
    Integer pageSize;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }


    public String getUsernameKey() {
        return usernameKey;
    }

    public void setUsernameKey(String usernameKey) {
        this.usernameKey = usernameKey;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public String getNicknameKey() {
        return nicknameKey;
    }

    public void setNicknameKey(String nicknameKey) {
        this.nicknameKey = nicknameKey;
    }
}
