package com.masonluo.mlonlinejudge.mapper;

import com.masonluo.mlonlinejudge.entity.Solution;
import com.masonluo.mlonlinejudge.mapper.decorator.SolutionMapperDecorator;
import com.masonluo.mlonlinejudge.model.bo.SolutionBo;
import com.masonluo.mlonlinejudge.model.dto.SolutionDto;
import com.masonluo.mlonlinejudge.model.param.SolutionParam;
import com.masonluo.mlonlinejudge.model.vo.SolutionVo;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/2/6 1:37 下午
 */
@Mapper(componentModel = "spring")
@DecoratedWith(SolutionMapperDecorator.class)
public interface SolutionMapper {
    @Mapping(source = "src", target = "sourceCode")
    Solution dtoToDo(SolutionDto dto);

    @Mapping(source = "sourceCode", target = "src")
    SolutionDto doTODto(Solution solutionDo);

    @Mapping(target = "language", expression = "java(com.masonluo.mlonlinejudge.enums.Language.getById(solutionParam.getLanguage()))")
    SolutionDto paramToDto(SolutionParam solutionParam);

    SolutionVo boToVo(SolutionBo bo);

    List<SolutionVo> boToVo(List<SolutionBo> listByUserId);
}
