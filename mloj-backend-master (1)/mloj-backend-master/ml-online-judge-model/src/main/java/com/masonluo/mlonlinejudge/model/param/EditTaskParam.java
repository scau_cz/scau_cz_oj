package com.masonluo.mlonlinejudge.model.param;

import javax.validation.constraints.NotNull;

public class EditTaskParam extends TaskParam{

    @NotNull
    private Integer id;

    @NotNull
    private Integer creatorId;

    private Integer[] classIds;

    private Integer schoolId;

    public Integer getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Integer creatorId) {
        this.creatorId = creatorId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer[] getClassIds() {
        return classIds;
    }

    public void setClassIds(Integer[] classIds) {
        this.classIds = classIds;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }
}
