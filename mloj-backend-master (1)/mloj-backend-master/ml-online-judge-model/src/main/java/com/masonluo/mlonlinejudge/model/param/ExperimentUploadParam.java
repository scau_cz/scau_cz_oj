package com.masonluo.mlonlinejudge.model.param;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2021-10-23 00:37
 **/
public class ExperimentUploadParam {
    private  Integer id;

    private String name;

    private String note;

    private List<Integer> requiredQuestions = new ArrayList();

    private List<Integer> optionalQuestions = new ArrayList();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<Integer> getRequiredQuestions() {
        return requiredQuestions;
    }

    public void setRequiredQuestions(List<Integer> requiredQuestions) {
        this.requiredQuestions = requiredQuestions;
    }

    public List<Integer> getOptionalQuestions() {
        return optionalQuestions;
    }

    public void setOptionalQuestions(List<Integer> optionalQuestions) {
        this.optionalQuestions = optionalQuestions;
    }
}


