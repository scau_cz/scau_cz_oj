package com.masonluo.mlonlinejudge.model.bo;

/**
 * @author masonluo
 * @date 2021/1/22 4:13 下午
 */
public class TagBo {
    private Integer id;

    private String name;

    private Integer type;

    public TagBo() {
    }

    public TagBo(Integer id, String name,Integer type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
