package com.masonluo.mlonlinejudge.mapper;

import com.masonluo.mlonlinejudge.entity.Problem;
import com.masonluo.mlonlinejudge.entity.SelectProblem;
import com.masonluo.mlonlinejudge.model.bo.SelectProblemBo;
import com.masonluo.mlonlinejudge.model.dto.SelectProblemUploadDto;
import com.masonluo.mlonlinejudge.model.param.SelectProblemUploadParam;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;
import java.util.List;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2021-10-07 18:05
 **/
@Mapper(componentModel = "spring")
public interface SelectProblemMapper {
    @Mapping(source = "level.description", target = "level")
    @Mapping(target = "answer", expression = "java(Level.conversion(selectproblem.getAnswer()))")
    SelectProblemBo to(SelectProblem selectproblem);

    @Mapping(source = "level.description", target = "level")
    List<SelectProblemBo> to(Collection<SelectProblem> selectProblems);

    @Mapping(target = "level", expression = "java(Level.getById(selectProblemBo.getLevel()))")
    @Mapping(target = "answer", expression = "java(Level.toJsonStr(selectProblemBo.getAnswer()))")
    SelectProblem from(SelectProblemBo selectProblemBo);


    List<SelectProblem> from(Collection<SelectProblemBo> selectProblemBos);

    @Mapping(target = "level", expression = "java(Level.getById(selectProblemUploadDto.getLevel()))")
    @Mapping(target = "answer", expression = "java(Level.toJsonStr(selectProblemUploadDto.getAnswer()))")
    SelectProblem uploadDtoToDo(SelectProblemUploadDto selectProblemUploadDto);

    SelectProblemUploadDto uploadParamToUploadDto(SelectProblemUploadParam param);




}


