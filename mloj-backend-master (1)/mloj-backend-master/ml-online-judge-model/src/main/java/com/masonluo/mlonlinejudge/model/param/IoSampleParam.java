package com.masonluo.mlonlinejudge.model.param;

/**
 * @author masonluo
 * @date 2021/1/25 2:58 下午
 */
public class IoSampleParam {
    private String inputSample;

    private String outputSample;

    public String getInputSample() {
        return inputSample;
    }

    public void setInputSample(String inputSample) {
        this.inputSample = inputSample;
    }

    public String getOutputSample() {
        return outputSample;
    }

    public void setOutputSample(String outputSample) {
        this.outputSample = outputSample;
    }
}
