package com.masonluo.mlonlinejudge.model.compile.factory;

import com.masonluo.mlonlinejudge.model.compile.Compile;
import com.masonluo.mlonlinejudge.model.compile.LanguageConfig;
import com.masonluo.mlonlinejudge.model.compile.Run;

import java.util.Arrays;

/**
 * @author yanzuwu
 */
public class PythonLanguageConfigFactory implements LanguageConfigFactory{
    @Override
    public LanguageConfig getLanguageConfig() {
        LanguageConfig config = new LanguageConfig();
        // 设置编译选项
        Compile compile = new Compile();
        // TODO 支持的编程语言中添加Python
        compile.setSrcName("solution.py");
        compile.setExeName("solution.py");
        compile.setMaxCpuTime(3000);
        compile.setMaxRealTime(5000);
        compile.setMaxMemory(128 * 1024 * 1024);
        compile.setCompileCommand("/usr/bin/python3 -m py_compile {src_path}");

        // 设置run选项
        Run run = new Run();
        run.setCommand("/usr/bin/python3 -BS {exe_path}");
        run.setSeccompRule("general");
        run.setEnv(Arrays.asList(Run.DEFAULT_ENV));
        run.setMemoryLimitCheckOnly(1);

        config.setCompile(compile);
        config.setRun(run);
        config.setName("python");
        return config;
    }
}
