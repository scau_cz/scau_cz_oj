package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @author 罗有冠
 * @date 2022/3/14 22:33
 */
@TableName("t_solution_result_join_test")
public class SolutionResultJoinTest extends BaseEntity{
    private Integer userId;

    private Integer mode;

    private Integer testId;

    private Integer problemId;

    private Integer solutionId;

    private Integer resultId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getMode() {
        return mode;
    }

    public void setMode(Integer mode) {
        this.mode = mode;
    }

    public Integer getTestId() {
        return testId;
    }

    public void setTestId(Integer testId) {
        this.testId = testId;
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public Integer getSolutionId() {
        return solutionId;
    }

    public void setSolutionId(Integer solutionId) {
        this.solutionId = solutionId;
    }

    public Integer getResultId() {
        return resultId;
    }

    public void setResultId(Integer resultId) {
        this.resultId = resultId;
    }

    public SolutionResultJoinTest(Integer userId, Integer mode, Integer testId, Integer problemId, Integer solutionId, Integer resultId) {
        this.userId = userId;
        this.mode = mode;
        this.testId = testId;
        this.problemId = problemId;
        this.solutionId = solutionId;
        this.resultId = resultId;
    }

    public SolutionResultJoinTest() {
    }
}

