package com.masonluo.mlonlinejudge.model.vo;

/**
 * @author masonluo
 * @date 2021/3/14 8:27 下午
 */
public class ProblemNameAndIdVo {
    private Integer id;

    private String title;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
