package com.masonluo.mlonlinejudge.model.compile.factory;

import com.masonluo.mlonlinejudge.enums.Language;
import com.masonluo.mlonlinejudge.model.compile.LanguageConfig;

/**
 * @author masonluo
 * @date 2021/2/5 2:39 下午
 */
public interface LanguageConfigFactory {
    LanguageConfig getLanguageConfig();
}
