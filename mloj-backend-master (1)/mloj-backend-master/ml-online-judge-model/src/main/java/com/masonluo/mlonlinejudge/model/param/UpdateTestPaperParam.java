package com.masonluo.mlonlinejudge.model.param;

import com.masonluo.mlonlinejudge.model.dto.UpdateTPaperProblemDto;

import java.util.List;

public class UpdateTestPaperParam {
    private Integer id;

    private String title;

    private String description;

    private List<UpdateTPaperProblemDto> questionList;

    private Integer score;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<UpdateTPaperProblemDto> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(List<UpdateTPaperProblemDto> questionList) {
        this.questionList = questionList;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "UpdateTestPaperParam{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", questionList=" + questionList +
                ", score=" + score +
                '}';
    }
}
