package com.masonluo.mlonlinejudge.mapper;

import com.masonluo.mlonlinejudge.entity.IndexProblem;
import com.masonluo.mlonlinejudge.model.bo.IndexProblemBo;
import com.masonluo.mlonlinejudge.model.vo.IndexProblemVo;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/2/10 3:49 下午
 */
@Mapper(componentModel = "spring")
public interface IndexProblemMapper {
    IndexProblemVo boToVo(IndexProblemBo bo);
}
