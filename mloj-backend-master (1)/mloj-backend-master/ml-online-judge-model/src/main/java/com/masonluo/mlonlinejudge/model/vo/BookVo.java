package com.masonluo.mlonlinejudge.model.vo;

/**
 * @author masonluo
 * @date 2021/2/10 5:01 下午
 */
public class BookVo {
    private Integer id;

    private String name;

    private String author;

    private String publisher;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
}
