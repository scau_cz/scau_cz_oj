package com.masonluo.mlonlinejudge.model.param;

import javax.validation.constraints.NotNull;

public class TaskChangeStatusParam {

    @NotNull
    private Integer id;

    @NotNull
    private String Status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
