package com.masonluo.mlonlinejudge.model.param;

import javax.validation.constraints.NotBlank;

/**
 * @author masonluo
 * @date 2021/2/10 4:55 下午
 */
public class BookParam {

    @NotBlank
    private String name;

    @NotBlank
    private String author;

    @NotBlank
    private String publisher;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
}
