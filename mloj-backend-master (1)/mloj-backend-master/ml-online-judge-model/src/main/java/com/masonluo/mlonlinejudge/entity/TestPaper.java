package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 试卷表
 * </p>
 *
 * @author yangqi
 * @since 2021-10-28
 */

@TableName("t_test_paper")
@ApiModel(value="TestPaper对象", description="试卷表")
public class TestPaper extends BaseEntity{

    @ApiModelProperty(value = "试卷id")
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "试卷标题")
    private String title;

    @ApiModelProperty(value = "试卷描述")
    private String desc;

    @ApiModelProperty(value = "创建人id")
    private Integer creatorId;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Integer creatorId) {
        this.creatorId = creatorId;
    }

    @Override
    public String toString() {
        return "TestPaper{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", desc='" + desc + '\'' +
                ", creatorId=" + creatorId +
                '}';
    }
}
