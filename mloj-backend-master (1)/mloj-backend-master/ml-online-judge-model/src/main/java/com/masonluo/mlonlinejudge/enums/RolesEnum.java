package com.masonluo.mlonlinejudge.enums;

import java.util.Objects;

public enum RolesEnum {
    STUDENT(1,"学生"),
    TEACHER(2,"教师"),
    SCHOOLADMIN(3,"校管理员"),
    SUPERADMIN(4,"超级管理员");


    private Integer roleId;
    private String roleNameCh;

     RolesEnum(Integer roleId,String roleNameCh){
        this.roleId=roleId;
        this.roleNameCh=roleNameCh;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public String getRoleNameCh() {
        return roleNameCh;
    }

    public static int getRoleId(RolesEnum e){
         return e.roleId;
    }

    public static RolesEnum getByName(String roleName){
        for (RolesEnum e: values()) {
            if(Objects.equals(e.name(),roleName)){
                return e;
            }
        }
        return null;
    }

    public static boolean isTeacher(String role){return role.equalsIgnoreCase(TEACHER.name());}

    public static boolean isStudent(String role){return role.equalsIgnoreCase(STUDENT.name());}

    public static boolean isSchoolAdmin(String role){return role.equalsIgnoreCase(SCHOOLADMIN.name());}

    public static boolean isSuperAdmin(String role){return role.equalsIgnoreCase(SUPERADMIN.name());}

}
