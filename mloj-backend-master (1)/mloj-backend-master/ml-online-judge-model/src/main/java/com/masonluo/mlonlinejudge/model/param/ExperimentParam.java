package com.masonluo.mlonlinejudge.model.param;


import java.util.ArrayList;
import java.util.List;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2021-10-07 00:06
 **/

public class ExperimentParam extends EditExperimentParam{

    private String name;

    private String note;

    private String status;

    protected String startTime;

    protected String endTime;

    //private List<LocalDateTime> limitDateTime = new ArrayList();

    private List<Integer> requiredQuestions = new ArrayList();

    private List<Integer> optionalQuestions = new ArrayList();
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public List<Integer> getRequiredQuestions() {
        return requiredQuestions;
    }

    public void setRequiredQuestions(List<Integer> requiredQuestions) {
        this.requiredQuestions = requiredQuestions;
    }

    public List<Integer> getOptionalQuestions() {
        return optionalQuestions;
    }

    public void setOptionalQuestions(List<Integer> optionalQuestions) {
        this.optionalQuestions = optionalQuestions;
    }
}


