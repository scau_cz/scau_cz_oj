package com.masonluo.mlonlinejudge.mapper;

import com.masonluo.mlonlinejudge.entity.Task;
import com.masonluo.mlonlinejudge.model.bo.TaskBo;
import com.masonluo.mlonlinejudge.model.vo.TaskVo;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper(componentModel = "spring")
public interface TaskMapper {

    //Task createDtoToDo(TaskCreateDto createDto);

    TaskBo doToBo(TaskBo bo);

    TaskVo boToVo(TaskVo vo);

    //TaskCreateDto paramToCreateDto(TaskParam param);

    List<TaskVo> boToVo(List<TaskBo> Tasks);

    List<TaskBo> doToBo(List<Task> selectAll);

}
