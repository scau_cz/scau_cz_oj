package com.masonluo.mlonlinejudge.model.dto;

public class UpdateTPaperProblemDto {
    private Integer problemId;

    private Integer type;

    private Integer score;

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}
