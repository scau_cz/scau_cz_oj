package com.masonluo.mlonlinejudge.model.param;

import io.swagger.annotations.ApiModelProperty;

public class StudentQueryParam {
    @ApiModelProperty(name = "班级id")
    Integer classId;

    @ApiModelProperty(name = "学号（用户名）")
    String usernameKey;

    @ApiModelProperty(name = "姓名")
    String nicknameKey;

    @ApiModelProperty(name = "页码")
    Integer pageNum;

    @ApiModelProperty(name = "页数")
    Integer pageSize;

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getNicknameKey() {
        return nicknameKey;
    }

    public void setNicknameKey(String nicknameKey) {
        this.nicknameKey = nicknameKey;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getUsernameKey() {
        return usernameKey;
    }

    public void setUsernameKey(String usernameKey) {
        this.usernameKey = usernameKey;
    }
}
