package com.masonluo.mlonlinejudge.model.dto;

import com.masonluo.mlonlinejudge.model.param.ProblemParam;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2021-10-09 16:54
 **/
public class ExperimentUploadDto {
    private  Integer id;

    @NotBlank(message = "实验名称不能为空")
    private String name;

    @NotBlank(message = "实验状态不能为空")
    private String status;

    private String note;

    protected String startTime;

    protected String endTime;

    private Integer school;

    private List<Integer> requiredQuestions = new ArrayList();

    private List<Integer> optionalQuestions = new ArrayList();

    public List<Integer> getRequiredQuestions() {
        return requiredQuestions;
    }

    public void setRequiredQuestions(List<Integer> requiredQuestions) {
        this.requiredQuestions = requiredQuestions;
    }

    public List<Integer> getOptionalQuestions() {
        return optionalQuestions;
    }

    public void setOptionalQuestions(List<Integer> optionalQuestions) {
        this.optionalQuestions = optionalQuestions;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getSchool() {
        return school;
    }

    public void setSchool(Integer school) {
        this.school = school;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}


