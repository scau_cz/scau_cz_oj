package com.masonluo.mlonlinejudge.model.vo;

/**
 * @author masonluo
 * @date 2021/2/11 12:54 下午
 */
public class UserInfoVo {
    private String username;

    private String nickname;

    private String phone;

    private String gender;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
