package com.masonluo.mlonlinejudge.mapper.decorator;

import com.masonluo.mlonlinejudge.entity.User;
import com.masonluo.mlonlinejudge.entity.UserInfo;
import com.masonluo.mlonlinejudge.mapper.UserInfoMapper;
import com.masonluo.mlonlinejudge.model.bo.UserInfoBo;
import com.masonluo.mlonlinejudge.model.dto.UserInfoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author masonluo
 * @date 2020/12/28 3:24 下午
 */
public abstract class UserInfoMapperDecorator implements UserInfoMapper {

    @Autowired
    @Qualifier("delegate")
    private UserInfoMapper delegate;

    @Override
    public UserInfoDto userInfoToUserInfoDto(User user, UserInfo userInfo) {
        UserInfoDto userInfoDto = new UserInfoDto();
        userInfoDto.setUsername(user.getUsername());
        userInfoDto.setGender(userInfo.getGender().description);
        userInfoDto.setPhone(userInfo.getPhone());
        userInfoDto.setNickname(userInfo.getNickname());
        return userInfoDto;
    }


//    @Override
//    public List<UserInfoDto> UserInfoDtoListToUserInfoBoList(List<UserInfoBo> userInfoBoList) {
//        List<UserInfoDto> userInfoDtoList=userInfoBoList.stream().map(userInfoBo -> {
//            UserInfoDto userInfoDto=new UserInfoDto();
//            userInfoDto.setUserId(userInfoBo.getUserId());
//            userInfoDto.setUsername(userInfoBo.getUsername());
//            userInfoDto.setNickname(userInfoBo.getNickname());
//            userInfoDto.setGender(userInfoBo.getGender());
//            userInfoDto.setPhone(userInfoBo.getPhone());
//            userInfoDto.setSchoolId(userInfoBo.getSchoolId());
//            userInfoDto.setSchoolName(userInfoBo.getSchoolName());
//            userInfoDto.setClassList(userInfoBo.getClassList());
//            return userInfoDto;
//        }).collect(Collectors.toList());
//        return userInfoDtoList;
//    }
//
}
