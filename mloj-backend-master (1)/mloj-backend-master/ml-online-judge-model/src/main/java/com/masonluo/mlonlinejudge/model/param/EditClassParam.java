package com.masonluo.mlonlinejudge.model.param;

import javax.validation.constraints.NotNull;

public class EditClassParam extends ClassParam {
    @NotNull
    private Integer id;

    @NotNull
    private Integer OldTeacherId;

    public Integer getOldTeacherId() {
        return OldTeacherId;
    }

    public void setOldTeacherId(Integer oldTeacherId) {
        OldTeacherId = oldTeacherId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
