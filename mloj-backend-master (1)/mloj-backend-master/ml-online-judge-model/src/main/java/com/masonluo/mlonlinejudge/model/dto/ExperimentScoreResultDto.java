package com.masonluo.mlonlinejudge.model.dto;

import com.masonluo.mlonlinejudge.entity.Experiment;
import com.masonluo.mlonlinejudge.entity.ExperimentProblemResult;
import com.masonluo.mlonlinejudge.entity.ExperimentScore;
import com.masonluo.mlonlinejudge.entity.ExperimentScoreResult;

import java.util.List;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2022-02-25 20:38
 **/
public class ExperimentScoreResultDto {


    private Integer userId;

    private String userNum;

    private String userName;

    private Integer experimentId;

    private List<ExperimentScore> experimentScore;

    private List<ExperimentProblemResult> experimentProblemResult;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserNum() {
        return userNum;
    }

    public void setUserNum(String userNum) {
        this.userNum = userNum;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getExperimentId() {
        return experimentId;
    }

    public void setExperimentId(Integer experimentId) {
        this.experimentId = experimentId;
    }

    public List<ExperimentScore> getExperimentScore() {
        return experimentScore;
    }

    public void setExperimentScore(List<ExperimentScore> experimentScore) {
        this.experimentScore = experimentScore;
    }

    public List<ExperimentProblemResult> getExperimentProblemResult() {
        return experimentProblemResult;
    }

    public void setExperimentProblemResult(List<ExperimentProblemResult> experimentProblemResult) {
        this.experimentProblemResult = experimentProblemResult;
    }

    @Override
    public String toString() {
        return "ExperimentScoreDto{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", experimentScores=" + experimentScore +
                '}';
    }
}


