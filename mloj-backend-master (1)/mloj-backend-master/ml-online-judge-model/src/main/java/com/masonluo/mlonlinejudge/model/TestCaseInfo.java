package com.masonluo.mlonlinejudge.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

/**
 * @author masonluo
 * @date 2021/1/25 11:20 下午
 */
public class TestCaseInfo {

    private boolean spj = false;

    @JsonProperty("test_cases")
    private Map<Integer, TestCase> testCase = new HashMap<>();

    public boolean isSpj() {
        return spj;
    }

    public void setSpj(boolean spj) {
        this.spj = spj;
    }

    public Map<Integer, TestCase> getTestCase() {
        return testCase;
    }

    public void setTestCase(Map<Integer, TestCase> testCase) {
        this.testCase = testCase;
    }

    public static void main(String[] args) throws JsonProcessingException {
        TestCaseInfo info = new TestCaseInfo();
        info.setSpj(false);
        TestCase one = new TestCase();
        one.setInputName("1.in");
        one.setOutputName("1.out");
        one.setStrippedOutputMd5("edflajdjfalkjkdljafl");
        one.setInputSize(1);
        one.setOutputSize(2);


        TestCase two = new TestCase();
        two.setInputName("1.in");
        two.setOutputName("1.out");
        two.setStrippedOutputMd5("edflajdjfalkjkdljafl");
        two.setInputSize(1);
        two.setOutputSize(2);
        info.getTestCase().put(1, one);
        info.getTestCase().put(2, two);

        ObjectMapper mapper = new ObjectMapper();
        System.out.println(mapper.writeValueAsString(info));
    }
}
