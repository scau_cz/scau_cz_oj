package com.masonluo.mlonlinejudge.model.dto;

import java.util.List;

public class ExamTitleDto {

    private List<String> titleList;

    private List<String> classNameList;

    public List<String> getTitleList() {
        return titleList;
    }

    public void setTitleList(List<String> titleList) {
        this.titleList = titleList;
    }

    public List<String> getClassNameList() {
        return classNameList;
    }

    public void setClassNameList(List<String> classNameList) {
        this.classNameList = classNameList;
    }

    @Override
    public String toString() {
        return "ExamTitleDto{" +
                "titleList=" + titleList +
                ", classNameList=" + classNameList +
                '}';
    }
}
