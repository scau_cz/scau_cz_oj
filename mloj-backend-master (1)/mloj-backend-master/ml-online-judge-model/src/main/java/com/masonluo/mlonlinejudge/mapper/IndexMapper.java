package com.masonluo.mlonlinejudge.mapper;

import com.masonluo.mlonlinejudge.entity.Index;
import com.masonluo.mlonlinejudge.model.bo.IndexBo;
import com.masonluo.mlonlinejudge.model.vo.IndexVo;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/2/10 3:42 下午
 */
@Mapper(componentModel = "spring")
public interface IndexMapper {
    IndexBo doToBo(Index index);

    List<IndexBo> doToBo(List<Index> index);

    IndexVo boToVo(IndexBo bo);

    List<IndexVo> boToVo(List<IndexBo> bo);
}
