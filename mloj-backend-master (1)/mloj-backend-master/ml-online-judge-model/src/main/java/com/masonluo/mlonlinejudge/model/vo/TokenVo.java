package com.masonluo.mlonlinejudge.model.vo;

import com.masonluo.mlonlinejudge.enums.TokenType;

/**
 * @author masonluo
 * @date 2021/2/11 1:12 下午
 */
public class TokenVo {
    private String token;

    private TokenType tokenType;

    private long createTimeStamp;

    private long expiredSeconds;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public TokenType getTokenType() {
        return tokenType;
    }

    public void setTokenType(TokenType tokenType) {
        this.tokenType = tokenType;
    }

    public long getCreateTimeStamp() {
        return createTimeStamp;
    }

    public void setCreateTimeStamp(long createTimeStamp) {
        this.createTimeStamp = createTimeStamp;
    }

    public long getExpiredSeconds() {
        return expiredSeconds;
    }

    public void setExpiredSeconds(long expiredSeconds) {
        this.expiredSeconds = expiredSeconds;
    }
}
