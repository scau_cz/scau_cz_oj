package com.masonluo.mlonlinejudge.model.bo;

public class ExamSchoolClassBo {

    private Integer classId;
    private Integer schoolId;
    private Integer examId;

    public ExamSchoolClassBo(){};

    public ExamSchoolClassBo(Integer classId, Integer schoolId, Integer examId){

        this.classId=classId;
        this.schoolId=schoolId;
        this.examId=examId;

    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public Integer getExamId() {
        return examId;
    }

    public void setExamId(Integer examId) {
        this.examId = examId;
    }
}
