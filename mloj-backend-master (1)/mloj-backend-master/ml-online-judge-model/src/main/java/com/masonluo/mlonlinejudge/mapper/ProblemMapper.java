package com.masonluo.mlonlinejudge.mapper;

import com.masonluo.mlonlinejudge.entity.Problem;
import com.masonluo.mlonlinejudge.model.bo.ProblemBo;
import com.masonluo.mlonlinejudge.model.dto.ProblemUploadDto;
import com.masonluo.mlonlinejudge.model.param.ProblemParam;
import com.masonluo.mlonlinejudge.model.param.ProblemUploadParam;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

/**
 * @author masonluo
 * @date 2021/1/22 5:20 下午
 */
@Component
@Mapper(componentModel = "spring")
public interface ProblemMapper {

    @Mapping(source = "level.description", target = "level")
    ProblemParam doToParam(Problem problem);

    @Mapping(source = "level.description", target = "level")
    List<ProblemParam> doToParam(Collection<Problem> problems);

    @Mapping(source = "level.description", target = "level")
    ProblemBo to(Problem problem);

    @Mapping(source = "level.description", target = "level")
    List<ProblemBo> to(Collection<Problem> problems);

    @Mapping(target = "level", expression = "java(Level.getById(problemBo.getLevel()))")
    Problem from(ProblemBo problemBo);


    List<Problem> from(Collection<ProblemBo> problemBos);

    @Mapping(target = "level", expression = "java(Level.getById(problemUploadDto.getLevel()))")
    Problem problemUploadDtoToProblem(ProblemUploadDto problemUploadDto);

    ProblemUploadDto uploadParamToUploadDto(ProblemUploadParam param);
}
