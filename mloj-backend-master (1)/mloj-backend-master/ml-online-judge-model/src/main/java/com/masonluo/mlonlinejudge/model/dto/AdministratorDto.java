package com.masonluo.mlonlinejudge.model.dto;

public class AdministratorDto {
    private String username;

    private String password;

//    private Integer status;

    public AdministratorDto(String username, String password, Integer status) {
        this.username = username;
        this.password = password;
//        this.status = status;
    }
    public AdministratorDto(){

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

//    public Integer getStatus() {
//        return status;
//    }
//
//    public void setStatus(Integer status) {
//        this.status = status;
//    }
}
