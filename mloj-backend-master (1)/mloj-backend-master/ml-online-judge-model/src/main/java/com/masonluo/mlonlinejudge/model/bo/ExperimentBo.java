package com.masonluo.mlonlinejudge.model.bo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.masonluo.mlonlinejudge.entity.ExperimentProblem;
import com.masonluo.mlonlinejudge.entity.ExperimentProblemResult;
import com.masonluo.mlonlinejudge.entity.ExperimentScore;
import com.masonluo.mlonlinejudge.model.param.ExperimentParam;
import com.masonluo.mlonlinejudge.model.param.ProblemParam;
import com.masonluo.mlonlinejudge.utils.DateTimeUtils;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2021-10-05 00:34
 **/
public class ExperimentBo {

    private  Integer id;

    private Integer weight;

    private String name;

    private String status;

    protected String startTime;

    protected String endTime;

    private Integer schoolId;

    private String creator;

    private String creatorSchool;

    private String note;

    private List<String> limitDateTime = new ArrayList();

    private List<ProblemParam> requiredQuestions = new ArrayList();

    private List<ProblemParam> optionalQuestions = new ArrayList();

    public List<String> getLimitDateTime() {
        return limitDateTime;
    }

    public void setLimitDateTime(String startTime,String endTime) {
        this.limitDateTime.add(0, startTime);
        this.limitDateTime.add(1,endTime);
    }

    public List<ProblemParam> getRequiredQuestions() {
        return requiredQuestions;
    }

    public void setRequiredQuestions(List<ProblemParam> requiredQuestions) {
        this.requiredQuestions = requiredQuestions;
    }

    public List<ProblemParam> getOptionalQuestions() {
        return optionalQuestions;
    }

    public void setOptionalQuestions(List<ProblemParam> optionalQuestions) {
        this.optionalQuestions = optionalQuestions;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public String getCreatorSchool() {
        return creatorSchool;
    }

    public void setCreatorSchool(String creatorSchool) {
        this.creatorSchool = creatorSchool;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }


}


