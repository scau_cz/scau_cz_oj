/**
 * 用于在Redis做缓存的对象，全部归属于这个包内
 *
 * @author masonluo
 * @date 2021/3/30 7:38 下午
 */
package com.masonluo.mlonlinejudge.model.cache;