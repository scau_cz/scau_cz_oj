package com.masonluo.mlonlinejudge.mapper;

import com.masonluo.mlonlinejudge.entity.User;
import com.masonluo.mlonlinejudge.entity.UserClass;
import com.masonluo.mlonlinejudge.entity.UserInfo;
import com.masonluo.mlonlinejudge.mapper.decorator.UserInfoMapperDecorator;
import com.masonluo.mlonlinejudge.model.bo.UserBo;
import com.masonluo.mlonlinejudge.model.bo.UserInfoBo;
import com.masonluo.mlonlinejudge.model.dto.UserDto;
import com.masonluo.mlonlinejudge.model.dto.UserInfoDto;
import com.masonluo.mlonlinejudge.model.vo.UserInfoVo;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.Collection;
import java.util.List;

/**
 * @author masonluo
 * @date 2020/12/28 3:17 下午
 */
@Mapper(componentModel = "spring")
@DecoratedWith(UserInfoMapperDecorator.class)
public interface UserInfoMapper extends BasicObjectMapper<UserInfo, UserInfoDto> {

    UserInfoDto userInfoToUserInfoDto(User user, UserInfo userInfo);

    UserInfoVo dtoToVo(UserInfoDto userInfoDto);

    @Mappings({
            @Mapping(source = "userId", target = "userId")})
    UserInfo userBoToUserInfo(UserBo userBo);

//    List<UserInfoDto> UserInfoDtoListToUserInfoBoList(List<UserInfoBo> userInfoBoList);

    List<UserInfoDto> to(List<UserInfoBo> userInfoBoList);

    UserClass userBoToUserClass(UserBo userBo);

}
