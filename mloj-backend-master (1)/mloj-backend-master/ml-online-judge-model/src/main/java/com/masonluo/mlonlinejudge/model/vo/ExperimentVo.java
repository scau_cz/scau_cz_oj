package com.masonluo.mlonlinejudge.model.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.masonluo.mlonlinejudge.entity.ExperimentProblemResult;
import com.masonluo.mlonlinejudge.model.param.ProblemParam;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2021-10-22 22:28
 **/

public class ExperimentVo {
    private  Integer id;

    private String name;

    private String note;

    private String creator;

    private String creatorSchool;

    private List<ExperimentProblemResult> experimentProblemResults;

    private List<ProblemParam> requiredQuestions = new ArrayList();

    private List<ProblemParam> optionalQuestions = new ArrayList();

    @JsonInclude(JsonInclude.Include.NON_NULL)//若被注解的字段值为 null，则序列化时忽略该字段
    private List<Integer> schoolIds;

    //@JsonInclude(JsonInclude.Include.NON_NULL)
    private List<String> schoolNames;

    public List<Integer> getSchoolIds() {
        return schoolIds;
    }

    public void setSchoolIds(List<Integer> schoolIds) {
        this.schoolIds = schoolIds;
    }

    public List<ProblemParam> getRequiredQuestions() {
        return requiredQuestions;
    }

    public void setRequiredQuestions(List<ProblemParam> requiredQuestions) {
        this.requiredQuestions = requiredQuestions;
    }

    public List<ProblemParam> getOptionalQuestions() {
        return optionalQuestions;
    }

    public void setOptionalQuestions(List<ProblemParam> optionalQuestions) {
        this.optionalQuestions = optionalQuestions;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCreatorSchool() {
        return creatorSchool;
    }

    public void setCreatorSchool(String creatorSchool) {
        this.creatorSchool = creatorSchool;
    }

    public List<String> getSchoolNames() {
        return schoolNames;
    }

    public void setSchoolNames(List<String> schoolNames) {
        this.schoolNames = schoolNames;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<ExperimentProblemResult> getExperimentProblemResults() {
        return experimentProblemResults;
    }

    public void setExperimentProblemResults(List<ExperimentProblemResult> experimentProblemResults) {
        this.experimentProblemResults = experimentProblemResults;
    }
}


