package com.masonluo.mlonlinejudge.model.compile;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author masonluo
 * @date 2021/2/5 2:33 下午
 */
public class Compile {

    @JsonProperty("src_name")
    private String srcName;

    @JsonProperty("exe_name")
    private String exeName;

    @JsonProperty("max_cpu_time")
    private long maxCpuTime;

    @JsonProperty("max_real_time")
    private long maxRealTime;

    @JsonProperty("max_memory")
    private long maxMemory;

    @JsonProperty("compile_command")
    private String compileCommand;

    public String getSrcName() {
        return srcName;
    }

    public void setSrcName(String srcName) {
        this.srcName = srcName;
    }

    public String getExeName() {
        return exeName;
    }

    public void setExeName(String exeName) {
        this.exeName = exeName;
    }

    public long getMaxCpuTime() {
        return maxCpuTime;
    }

    public void setMaxCpuTime(long maxCpuTime) {
        this.maxCpuTime = maxCpuTime;
    }

    public long getMaxRealTime() {
        return maxRealTime;
    }

    public void setMaxRealTime(long maxRealTime) {
        this.maxRealTime = maxRealTime;
    }

    public long getMaxMemory() {
        return maxMemory;
    }

    public void setMaxMemory(long maxMemory) {
        this.maxMemory = maxMemory;
    }

    public String getCompileCommand() {
        return compileCommand;
    }

    public void setCompileCommand(String compileCommand) {
        this.compileCommand = compileCommand;
    }

    @Override
    public String toString() {
        return "Compile{" +
                "srcName='" + srcName + '\'' +
                ", exeName='" + exeName + '\'' +
                ", maxCpuTime=" + maxCpuTime +
                ", maxRealTime=" + maxRealTime +
                ", maxMemory=" + maxMemory +
                ", compileCommand='" + compileCommand + '\'' +
                '}';
    }
}
