package com.masonluo.mlonlinejudge.model.compile;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.List;

/**
 * @author masonluo
 * @date 2021/2/5 2:33 下午
 */
public class Run {

    public static final String[] DEFAULT_ENV = {"LANG=en_US.UTF-8", "LANGUAGE=en_US:en", "LC_ALL=en_US.UTF-8"};

    private String command;

    @JsonProperty("seccomp_rule")
    private String seccompRule;

    private List<String> env = Arrays.asList(DEFAULT_ENV);

    @JsonProperty("memory_limit_check_only")
    private Integer memoryLimitCheckOnly;

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getSeccompRule() {
        return seccompRule;
    }

    public void setSeccompRule(String seccompRule) {
        this.seccompRule = seccompRule;
    }

    public List<String> getEnv() {
        return env;
    }

    public void setEnv(List<String> env) {
        this.env = env;
    }

    public Integer getMemoryLimitCheckOnly() {
        return memoryLimitCheckOnly;
    }

    public void setMemoryLimitCheckOnly(Integer memoryLimitCheckOnly) {
        this.memoryLimitCheckOnly = memoryLimitCheckOnly;
    }

    @Override
    public String toString() {
        return "Run{" +
                "command='" + command + '\'' +
                ", seccompRule='" + seccompRule + '\'' +
                ", env=" + env +
                ", memoryLimitCheckOnly=" + memoryLimitCheckOnly +
                '}';
    }
}
