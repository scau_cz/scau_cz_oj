package com.masonluo.mlonlinejudge.model.bo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;

public class ExamScoreBo {
    private Integer id;

    private Integer answerSheetId;

    private Integer examId;

    private Integer classId;

    private Integer singleChoiceScore;

    private Integer trueFalseQuestionScore;

    private Integer programQuestionScore;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAnswerSheetId() {
        return answerSheetId;
    }

    public void setAnswerSheetId(Integer answerSheetId) {
        this.answerSheetId = answerSheetId;
    }

    public Integer getExamId() {
        return examId;
    }

    public void setExamId(Integer examId) {
        this.examId = examId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getSingleChoiceScore() {
        return singleChoiceScore;
    }

    public void setSingleChoiceScore(Integer singleChoiceScore) {
        this.singleChoiceScore = singleChoiceScore;
    }

    public Integer getTrueFalseQuestionScore() {
        return trueFalseQuestionScore;
    }

    public void setTrueFalseQuestionScore(Integer trueFalseQuestionScore) {
        this.trueFalseQuestionScore = trueFalseQuestionScore;
    }

    public Integer getProgramQuestionScore() {
        return programQuestionScore;
    }

    public void setProgramQuestionScore(Integer programQuestionScore) {
        this.programQuestionScore = programQuestionScore;
    }
}
