package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @author masonluo
 * @date 2020/12/26 4:16 下午
 */
@TableName("t_problem_tag")
public class ProblemTag extends BaseEntity {
    private Integer problemId;

    private Integer tagId;

    private Integer type;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public Integer getTagId() {
        return tagId;
    }

    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

    @Override
    public String toString() {
        return "ProblemTag{" +
                "id=" + id +
                ", createdTime=" + createdTime +
                ", updatedTime=" + updatedTime +
                ", problemId=" + problemId +
                ", tagId=" + tagId +
                '}';
    }
}
