package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * <p>
 * 题目实体
 * </p>
 *
 * @author yangqi
 * @since 2021-11-10
 */

@ApiModel(value="SelProblem对象", description="")
public class SelProblem implements Serializable {



      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = " 0代表选择题，1代表判断题，2代表程序题")
    private Integer type;

    @ApiModelProperty(value = "题目")
    private String title;

    @ApiModelProperty(value = "题目列表")
    private String answer;

    @ApiModelProperty(value = "答案")
    private String correct;

    @ApiModelProperty(value = "题目难度：0代表低级  1代表中级  2代表高级 ")
    private Integer level;

    @ApiModelProperty(value = "章节id【不使用】")
    private Long indexId;

    @ApiModelProperty(value = "用户id")
    private Long userId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Long getIndexId() {
        return indexId;
    }

    public void setIndexId(Long indexId) {
        this.indexId = indexId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
