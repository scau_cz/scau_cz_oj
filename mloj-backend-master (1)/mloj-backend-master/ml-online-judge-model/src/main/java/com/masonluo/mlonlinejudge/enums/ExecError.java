package com.masonluo.mlonlinejudge.enums;

import java.util.Objects;

/**
 * @author masonluo
 * @date 2020/12/26 4:23 下午
 */
public enum ExecError {
    SUCCESS("success", 0),
    INVALID_CONFIG("invalid config", -1),
    CLONE_FAILED("clone failed", -2),
    PTHREAD_FAILED("pthread failed", -3),
    WAIT_FAILED("wait failed", -4),
    ROOT_REQUIRED("root required", -5),
    LOAD_SECCOMP_FAILED("load seccomp failed", -6),
    SETRLIMIT_FAILED("setrlimit failed", -7),
    DUP2_FAILED("dup2 failed", -8),
    SETUID_FAILED("setuid failed", -9),
    EXECVE_FAILED("execve failed", -10),
    SPJ_ERROR("spj error", -11);

    ExecError(String description, int id) {
        this.id = id;
        this.description = description;
    }

    public final int id;

    public final String description;

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public static ExecError getById(int id) {
        for (ExecError execError : ExecError.values()) {
            if (Objects.equals(id, execError.id)) {
                return execError;
            }
        }
        return null;
    }
}
