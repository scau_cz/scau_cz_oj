package com.masonluo.mlonlinejudge.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 编程语言
 *
 * @author masonluo
 * @date 2021/2/5 2:23 下午
 */
public enum Language {
    C(0, "c"),
    CPP(1, "c++"),
    JAVA(2, "java"),
    PYTHON(3, "python");
    // TODO 支持的编程语言中添加Python
    // TODO 支持的编程语言中添加C++
    public int id;

    public String name;

    Language(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @JsonValue
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonCreator
    public static Language getById(int id) {
        for (Language language : Language.values()) {
            if (language.getId() == id) {
                return language;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "Language{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
