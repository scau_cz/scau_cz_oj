/**
 * 调用评判服务器返回的对象
 *
 * @author masonluo
 * @date 2021/3/30 7:43 下午
 */
package com.masonluo.mlonlinejudge.model.judge;