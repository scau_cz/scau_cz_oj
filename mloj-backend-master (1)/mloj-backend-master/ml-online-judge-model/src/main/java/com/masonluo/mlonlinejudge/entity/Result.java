package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.masonluo.mlonlinejudge.enums.ExecResult;

/**
 * 代码执行结果
 *
 * @author masonluo
 * @date 2020/12/26 4:16 下午
 */
@TableName("t_result")
public class Result extends BaseEntity {
    /**
     * 最大Cpu时间，因为有多组测试数据，当运行结果正确({@link ExecResult#SUCCESS})时不为空
     */
    private Integer cpuTime = 0;
    /**
     * 最大内存使用，因为有多组测试数据，当运行结果正确({@link ExecResult#SUCCESS})时不为空
     */
    private Integer memory = 0;
    /**
     * 编译结果， 是否编译通过
     * TODO 这个应该变为多种类型的结果
     */
    private boolean compilerResult;
    /**
     * 编译错误提示信息，只有当{@link Result##compilerResult}为false时，才会有值
     */
    private String errorData;
    /**
     * 运行结果，只有当{@link Result##compilerResult}为true时，才有效
     */
    private ExecResult processResult;
    /***
     * 测试用例的正确答案
     */
    private String trueAnswer;
    private String userAnswer;//用户输出的答案
    private Integer testCaseIndex;//第几个测试用例
    public Integer getCpuTime() {
        return cpuTime;
    }

    public void setCpuTime(Integer cpuTime) {
        this.cpuTime = cpuTime;
    }

    public Integer getMemory() {
        return memory;
    }

    public void setMemory(Integer memory) {
        this.memory = memory;
    }

    public boolean isCompilerResult() {
        return compilerResult;
    }

    public void setCompilerResult(boolean compilerResult) {
        this.compilerResult = compilerResult;
    }

    public String getErrorData() {
        return errorData;
    }

    public void setErrorData(String errorData) {
        this.errorData = errorData;
    }

    public ExecResult getProcessResult() {
        return processResult;
    }

    public void setProcessResult(ExecResult processResult) {
        this.processResult = processResult;
    }

    public void setTrueAnswer(String trueAnswer) {
        this.trueAnswer = trueAnswer;
    }

    public String getTrueAnswer() {
        return trueAnswer;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }


    public Integer getTestCaseIndex() {
        return testCaseIndex;
    }

    public void setTestCaseIndex(Integer testCaseIndex) {
        this.testCaseIndex = testCaseIndex;
    }

    @Override
    public String toString() {
        return "Result{" +
                "id=" + id +
                ", createdTime=" + createdTime +
                ", updatedTime=" + updatedTime +
                ", avgCpuTime=" + cpuTime +
                ", avgMemory=" + memory +
                ", compilerResult=" + compilerResult +
                ", errorData='" + errorData + '\'' +
                ", processResult=" + processResult +
                ", trueAnswer=" + trueAnswer +
                ", userAnswer=" + userAnswer +
                ", testCaseIndex=" + testCaseIndex +
                '}';
    }
}
