package com.masonluo.mlonlinejudge.enums;

import java.util.Objects;

/**
 * 评判模式，当前只有ACM模式，也就是需要用户自己处理输入输出
 *
 * @author masonluo
 * @date 2020/12/26 9:09 下午
 */
public enum JudgeMode {
    ACM("acm", 0);

    JudgeMode(String description, int id) {
        this.id = id;
        this.description = description;
    }

    public final int id;

    public final String description;

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public static JudgeMode getById(int id) {
        for (JudgeMode judgeMode : JudgeMode.values()) {
            if (Objects.equals(id, judgeMode.id)) {
                return judgeMode;
            }
        }
        return null;
    }
}
