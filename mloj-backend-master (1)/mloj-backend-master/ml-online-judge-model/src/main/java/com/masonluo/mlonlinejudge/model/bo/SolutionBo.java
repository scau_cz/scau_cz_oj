package com.masonluo.mlonlinejudge.model.bo;

/**
 * service层所输出的数据
 *
 * @author masonluo
 * @date 2021/2/6 2:40 下午
 */
public class SolutionBo {
    private Integer id;

    private Integer problemId;

    private Integer examAndExperimentId;

    private Integer userId;

    public Integer getExamAndExperimentId() {
        return examAndExperimentId;
    }

    public void setExamAndExperimentId(Integer examAndExperimentId) {
        this.examAndExperimentId = examAndExperimentId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
