package com.masonluo.mlonlinejudge.model.dto;
/**
*
*@author: yangqi
*@date: 2021-10-24
*/
public class ClassQueryBySchoolDto {
    private Integer id;//班级id

    private String className;

    private Integer schoolId;//学校id

    private String schoolName;

    private Integer teacherId;

    private String teacherName;

    @Override
    public String toString() {
        return "ClassQueryBySchoolDto{" +
                "id=" + id +
                ", className='" + className + '\'' +
                ", schoolId=" + schoolId +
                ", schoolName='" + schoolName + '\'' +
                ", teacherId=" + teacherId +
                ", teacherName='" + teacherName + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }
}
