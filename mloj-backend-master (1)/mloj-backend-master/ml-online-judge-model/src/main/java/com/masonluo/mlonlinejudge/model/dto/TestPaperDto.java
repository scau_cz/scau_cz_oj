package com.masonluo.mlonlinejudge.model.dto;

public class TestPaperDto {
    private String title;

    private String desc;

    private Integer score;

    private Integer creatorId;

    public TestPaperDto(String title, String desc, Integer score, Integer creatorId) {
        this.title=title;
        this.desc = desc;
        this.score=score;
        this.creatorId=creatorId;
    }

    @Override
    public String toString() {
        return "TestPaperDto{" +
                "title='" + title + '\'' +
                ", desc='" + desc + '\'' +
                ", score=" + score +
                ", creatorId=" + creatorId +
                '}';
    }
}
