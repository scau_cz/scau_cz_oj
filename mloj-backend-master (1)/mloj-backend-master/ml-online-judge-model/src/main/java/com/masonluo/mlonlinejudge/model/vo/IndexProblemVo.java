package com.masonluo.mlonlinejudge.model.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author masonluo
 * @date 2021/2/11 12:44 下午
 */
public class IndexProblemVo {
    private Integer indexId;

    private List<Integer> problemId = new ArrayList<>();

    public Integer getIndexId() {
        return indexId;
    }

    public void setIndexId(Integer indexId) {
        this.indexId = indexId;
    }

    public List<Integer> getProblemId() {
        return problemId;
    }

    public void setProblemId(List<Integer> problemId) {
        this.problemId = problemId;
    }
}
