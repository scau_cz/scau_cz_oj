package com.masonluo.mlonlinejudge.model.dto;


import java.util.List;

public class AddTestPaperDto {
    private String title;

    private String desc;

    private Integer score;

    private Integer creatorId;

    private List<TestPaperSelProblem> choicesProblem;

    private List<TestPaperSelProblem> trueOrFalse;

    private List<TestPaperSelProblem> programmingQuestions;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public List<TestPaperSelProblem> getChoicesProblem() {
        return choicesProblem;
    }

    public void setChoicesProblem(List<TestPaperSelProblem> choicesProblem) {
        this.choicesProblem = choicesProblem;
    }

    public List<TestPaperSelProblem> getTrueOrFalse() {
        return trueOrFalse;
    }

    public void setTrueOrFalse(List<TestPaperSelProblem> trueOrFalse) {
        this.trueOrFalse = trueOrFalse;
    }

    public List<TestPaperSelProblem> getProgrammingQuestions() {
        return programmingQuestions;
    }

    public void setProgrammingQuestions(List<TestPaperSelProblem> programmingQuestions) {
        this.programmingQuestions = programmingQuestions;
    }

    public Integer getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Integer creatorId) {
        this.creatorId = creatorId;
    }

    @Override
    public String toString() {
        return "AddTestPaperDto{" +
                "title='" + title + '\'' +
                ", desc='" + desc + '\'' +
                ", score=" + score +
                ", creatorId=" + creatorId +
                ", choicesProblem=" + choicesProblem +
                ", trueOrFalse=" + trueOrFalse +
                ", programmingQuestions=" + programmingQuestions +
                '}';
    }
}
