package com.masonluo.mlonlinejudge.model.dto;

import com.masonluo.mlonlinejudge.model.Pair;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/1/25 11:15 下午
 */
public class TestCaseUploadDto {
    private String testCaseId;

    private List<Pair<String, String>> files;

    private List<Pair<String, String>> answers;
    private List<Pair<String, String>> inputContents;

    public String getTestCaseId() {
        return testCaseId;
    }

    public void setTestCaseId(String testCaseId) {
        this.testCaseId = testCaseId;
    }

    public List<Pair<String, String>> getFiles() {
        return files;
    }

    public void setFiles(List<Pair<String, String>> files) {
        this.files = files;
    }

    public List<Pair<String, String>> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Pair<String, String>> answers) {
        this.answers = answers;
    }

    public void setInputContents(List<Pair<String, String>> inputContents) {
        this.inputContents = inputContents;
    }

    public List<Pair<String, String>> getInputContents() {
        return inputContents;
    }
}
