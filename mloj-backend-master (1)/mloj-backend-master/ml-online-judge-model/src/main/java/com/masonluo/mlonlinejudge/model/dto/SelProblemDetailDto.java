package com.masonluo.mlonlinejudge.model.dto;

public class SelProblemDetailDto {
    private Integer id;

    private Integer type;

    private String title;

    private String answer;

    private String correct;

    private Integer level;

    private Integer score;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }


    @Override
    public String toString() {
        return "SelProblemDetailDto{" +
                "id=" + id +
                ", type=" + type +
                ", title='" + title + '\'' +
                ", answer='" + answer + '\'' +
                ", correct='" + correct + '\'' +
                ", level=" + level +
                ", score=" + score +
                '}';
    }
}
