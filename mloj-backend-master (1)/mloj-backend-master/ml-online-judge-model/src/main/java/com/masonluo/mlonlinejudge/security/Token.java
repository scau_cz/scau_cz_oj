package com.masonluo.mlonlinejudge.security;

import com.masonluo.mlonlinejudge.enums.TokenType;

/**
 * 生成一个Token的所需要信息，前端用到的token为这个Token对象的Json格式进行Base64加密之后得到
 *
 * @author masonluo
 * @date 2021/1/17 3:17 下午
 */
public class Token {

    private String username;

    private final TokenType tokenType;

    /**
     * 创建该Token的时间戳
     * TODO 改为时间戳+自增数
     */
    private final long createTimeStamp;

    private String privateKey;

    public Token(TokenType tokenType, long createTimeStamp) {
        this.tokenType = tokenType;
        this.createTimeStamp = createTimeStamp;
    }

    public Token(String username, long createTimeStamp, TokenType tokenType, String privateKey) {
        this.createTimeStamp = createTimeStamp;
        this.tokenType = tokenType;
        this.username = username;
        this.privateKey = privateKey;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public TokenType getTokenType() {
        return tokenType;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public long getCreateTimeStamp() {
        return createTimeStamp;
    }
}
