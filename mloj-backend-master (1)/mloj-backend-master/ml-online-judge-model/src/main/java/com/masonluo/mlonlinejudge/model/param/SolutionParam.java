package com.masonluo.mlonlinejudge.model.param;

import java.util.Scanner;

/**
 * 接受前端传递的数据
 *
 * @author masonluo
 * @date 2021/2/5 2:22 下午
 */
public class SolutionParam {
    /**
     * 用户提交的源代码
     */

    private String src;
    /**
     * 用户id
     */

    private Integer userId;
    /**
     * 使用的代码语言，0表示C，1表示C++，2表示Java，3表示Python
     */
    // TODO 支持的编程语言中添加Python
    // TODO 支持的编程语言中添加C++
    private int language;

    /**
     * 解决的问题id
     */
    private Integer problemId;

    /**
     * 考试: id
     * 实验：-id
     * 非考试则为0
     */
    private Integer examAndExperimentId;

    public Integer getExamAndExperimentId() {
        return examAndExperimentId;
    }

    public void setExamAndExperimentId(Integer examAndExperimentId) {
        this.examAndExperimentId = examAndExperimentId;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public int getLanguage() {
        return language;
    }

    public void setLanguage(int language) {
        this.language = language;
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
