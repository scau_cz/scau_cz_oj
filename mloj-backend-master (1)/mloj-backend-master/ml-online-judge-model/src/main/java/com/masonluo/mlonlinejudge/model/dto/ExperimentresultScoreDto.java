package com.masonluo.mlonlinejudge.model.dto;

import com.masonluo.mlonlinejudge.entity.ExperimentProblemResult;

import java.util.List;

public class ExperimentresultScoreDto {
    private Integer experimentId;

    private Integer schoolId;

    private Integer classId;

    private Integer studentId;

    private String studentName;

    private List<ExperimentProblemResult> resultJson;

    private Integer passedNum;

    public Integer getExperimentId() {
        return experimentId;
    }

    public void setExperimentId(Integer experimentId) {
        this.experimentId = experimentId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public List<ExperimentProblemResult> getResultJson() {
        return resultJson;
    }

    public void setResultJson(List<ExperimentProblemResult> resultJson) {
        this.resultJson = resultJson;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public Integer getPassedNum() {
        return passedNum;
    }

    public void setPassedNum(Integer passedNum) {
        this.passedNum = passedNum;
    }


    @Override
    public String toString() {
        return "ExperimentresultScoreDto{" +
                "experimentId=" + experimentId +
                ", schoolId=" + schoolId +
                ", classId=" + classId +
                ", studentId=" + studentId +
                ", studentName='" + studentName + '\'' +
                ", resultJson='" + resultJson + '\'' +
                ", passedNum=" + passedNum +
                '}';
    }
}
