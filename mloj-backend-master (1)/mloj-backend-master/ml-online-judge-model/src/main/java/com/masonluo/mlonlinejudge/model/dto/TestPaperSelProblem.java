package com.masonluo.mlonlinejudge.model.dto;

public class TestPaperSelProblem {
    private Integer id;//题目Id

    private Integer score;//每小题成绩

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "TestPaperSelProblem{" +
                "id=" + id +
                ", score=" + score +
                '}';
    }
}
