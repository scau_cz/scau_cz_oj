package com.masonluo.mlonlinejudge.mapper;

import com.masonluo.mlonlinejudge.model.bo.ClassFindByTeacherBo;
import com.masonluo.mlonlinejudge.model.bo.ClassQueryBySchoolBo;
import com.masonluo.mlonlinejudge.model.dto.ClassFindByTeacherIdDto;
import com.masonluo.mlonlinejudge.model.dto.ClassQueryBySchoolDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ClassMapper{
    List<ClassQueryBySchoolDto> to(List<ClassQueryBySchoolBo> classQueryBySchoolBoList);
//    List<ClassFindByTeacherIdDto> to(List<ClassFindByTeacherBo> classFindByTeacherBoList);
}
