package com.masonluo.mlonlinejudge.model.bo;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2021-08-13 12:26
 **/
public class SchoolBo {
    private Integer id;

    private String schoolName;

    private String schoolEnglishName;

    private String email;

    private String schoolBackground;

    /*private String createTime;

    private String updateTime;*/

    public SchoolBo(){}



    public SchoolBo(Integer id, String schoolName, String schoolEnglishName) {
        this.id = id;
        this.schoolName = schoolName;
        this.schoolEnglishName = schoolEnglishName;
        /*this.updateTime =updateTime;
        this.createTime = createTime;*/
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolBackground() {
        return schoolBackground;
    }

    public void setSchoolBackground(String schoolBackground) {
        this.schoolBackground = schoolBackground;
    }

    public String getSchoolEnglishName() {
        return schoolEnglishName;
    }

    public void setSchoolEnglishName(String schoolEnglishName) {
        this.schoolEnglishName = schoolEnglishName;
    }

    /* public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }*/
}

