package com.masonluo.mlonlinejudge.model.vo;

public class AdministratorVo {
    private Integer id;
    private String username;

    public AdministratorVo(Integer id, String username) {
        this.id = id;
        this.username = username;
    }
    public AdministratorVo(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return username;
    }

    public void setName(String username) {
        this.username = username;
    }
}
