package com.masonluo.mlonlinejudge.entity;

/**
 * @author LZC
 * @create 2021-12-20 20:36
 */
public class ProgramAnswerContent {
    private Integer id;
    private String answer;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":" + id +
                ",\"answer\":"+"\""+answer+"\""+
                "}";
    }
}
