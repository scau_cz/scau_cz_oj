package com.masonluo.mlonlinejudge.model.dto;

import java.util.Arrays;

public class TestPaperSelProblemDetailDto {
    private Integer id;

    private Integer type;

    private String title;

    private TestPaperSelProblemAnswerDto[] answers;

    private String correct;

    private Integer level;

    private Integer score;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TestPaperSelProblemAnswerDto[] getAnswers() {
        return answers;
    }

    public void setAnswers(TestPaperSelProblemAnswerDto[] answers) {
        this.answers = answers;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "TestPaperSelProblemDetailDto{" +
                "id=" + id +
                ", type=" + type +
                ", title='" + title + '\'' +
                ", answers=" + Arrays.toString(answers) +
                ", correct='" + correct + '\'' +
                ", level=" + level +
                ", score=" + score +
                '}';
    }
}
