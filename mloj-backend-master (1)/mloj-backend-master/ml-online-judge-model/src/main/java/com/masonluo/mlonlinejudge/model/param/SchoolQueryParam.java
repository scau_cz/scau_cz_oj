package com.masonluo.mlonlinejudge.model.param;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;

public class SchoolQueryParam {

    @ApiModelProperty(name = "学校名称")
    private String schoolNameKey;

//    @ApiModelProperty(name = "学校id")
//    private Integer schoolId;

    @ApiModelProperty(name = "页码")
    Integer pageNum;

    @ApiModelProperty(name = "页数")
    Integer pageSize;

    public String getSchoolNameKey() {
        return schoolNameKey;
    }

    public void setSchoolNameKey(String schoolNameKey) {
        this.schoolNameKey = schoolNameKey;
    }

//    public Integer getSchoolId() {
//        return schoolId;
//    }
//
//    public void setSchoolId(Integer schoolId) {
//        this.schoolId = schoolId;
//    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
