package com.masonluo.mlonlinejudge.model.param;

import javax.validation.constraints.NotNull;

/**
 * @author masonluo
 * @date 2021/3/28 12:30 上午
 */
public class EditBookParam extends BookParam {

    @NotNull
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
