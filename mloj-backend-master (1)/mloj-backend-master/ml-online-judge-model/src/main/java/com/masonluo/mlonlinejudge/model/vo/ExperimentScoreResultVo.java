package com.masonluo.mlonlinejudge.model.vo;

import com.masonluo.mlonlinejudge.entity.ExperimentScore;

import java.util.List;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2022-03-01 20:58
 **/
public class ExperimentScoreResultVo {
    private Integer userId;

    private String userNum;

    private String userName;

    private List<ExperimentScore> experimentScore;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserNum() {
        return userNum;
    }

    public void setUserNum(String userNum) {
        this.userNum = userNum;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<ExperimentScore> getExperimentScore() {
        return experimentScore;
    }

    public void setExperimentScore(List<ExperimentScore> experimentScore) {
        this.experimentScore = experimentScore;
    }

    @Override
    public String toString() {
        return "ExperimentScoreDto{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", experimentScores=" + experimentScore +
                '}';
    }
}


