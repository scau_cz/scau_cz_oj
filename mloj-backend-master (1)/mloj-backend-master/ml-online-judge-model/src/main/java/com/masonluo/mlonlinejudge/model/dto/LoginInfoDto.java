package com.masonluo.mlonlinejudge.model.dto;

import com.masonluo.mlonlinejudge.entity.Class;

import java.util.List;

public class LoginInfoDto {

    private Integer userId;

    private String username;

    private String nickname;

    private Integer userLevel;

    private String roleName;

    private List<Class> classList;

    private Integer schoolId;

    private String schoolName;

    private String schoolBackground;



    public List<Class> getClassList() {
        return classList;
    }

    public void setClassList(List<Class> classList) {
        this.classList = classList;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }


    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }


    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public Integer getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(Integer userLevel) {
        this.userLevel = userLevel;
    }

    public String getSchoolBackground() {
        return schoolBackground;
    }

    public void setSchoolBackground(String schoolBackground) {
        this.schoolBackground = schoolBackground;
    }
}
