package com.masonluo.mlonlinejudge.entity;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;

import java.util.List;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2022-02-26 18:33
 **/
public class ExperimentProblemResult {
    private Integer problemId;

    private String problemTitle;

    private String status;

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public String getProblemTitle() {
        return problemTitle;
    }

    public void setProblemTitle(String problemTitle) {
        this.problemTitle = problemTitle;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static List<ExperimentProblemResult> toStrJson(String str){
        List<ExperimentProblemResult> experimentProblemResults = JSON.parseArray(str, ExperimentProblemResult.class);
        return experimentProblemResults;
    }
    public static String toJsonStr(List<ExperimentProblemResult> experimentProblemResults){
        return JSONUtil.toJsonStr(experimentProblemResults);
    }

    @Override
    public String toString() {
        return "ExperimentProblemResult{" +
                "problemId=" + problemId +
                ", problemTitle='" + problemTitle + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}


