package com.masonluo.mlonlinejudge.model.bo;

import com.masonluo.mlonlinejudge.enums.Gender;

public class UserBo {
    private Integer userId;

    /**
     * 用户的账号，默认为邮箱
     */

    private String username;

    private String password;

    /**
     * 用户状态，对应着数据库状态，0为false，1为true
     * <p>
     * 如果用户可用，则为true
     * 否则，为false
     */
    private boolean status;

    private String nickname;

    private String phone;

    private Gender gender;

    private String role;

    private Integer classId;

    private Integer schoolId;

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }


    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
