package com.masonluo.mlonlinejudge.model.compile.factory;

import com.masonluo.mlonlinejudge.model.compile.Compile;
import com.masonluo.mlonlinejudge.model.compile.LanguageConfig;
import com.masonluo.mlonlinejudge.model.compile.Run;

import java.util.Arrays;

/**
 * @author masonluo
 * @date 2021/2/5 2:40 下午
 */
public class JavaLanguageConfigFactory implements LanguageConfigFactory {
    @Override
    public LanguageConfig getLanguageConfig() {
        LanguageConfig config = new LanguageConfig();
        // 设置编译选项
        Compile compile = new Compile();
        compile.setSrcName("Main.java");
        compile.setExeName("Main");
        compile.setMaxCpuTime(3000);
        compile.setMaxRealTime(5000);
        compile.setMaxMemory(-1);
        compile.setCompileCommand("/usr/bin/javac {src_path} -d {exe_dir} -encoding UTF8");
        // 设置run选项
        Run run = new Run();
        run.setCommand("/usr/bin/java -cp {exe_dir} -XX:MaxRAM={max_memory}k -Djava.security.manager -Dfile.encoding=UTF-8 -Djava.security.policy==/etc/java_policy -Djava.awt.headless=true Main");
        run.setSeccompRule(null);
        run.setEnv(Arrays.asList(Run.DEFAULT_ENV));
        run.setMemoryLimitCheckOnly(1);

        config.setCompile(compile);
        config.setRun(run);
        config.setName("java");
        return config;
    }
}
