package com.masonluo.mlonlinejudge.model.vo;
/**
*
*@author: yangqi
*@date: 2021-09-28
*/
public class ClassVo {
    private String className;

    private Integer schoolId;

    private Integer teacherId;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }
}
