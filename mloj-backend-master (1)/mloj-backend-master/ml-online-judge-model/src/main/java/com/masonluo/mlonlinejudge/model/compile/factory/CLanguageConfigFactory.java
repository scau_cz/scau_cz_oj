package com.masonluo.mlonlinejudge.model.compile.factory;

import com.masonluo.mlonlinejudge.model.compile.Compile;
import com.masonluo.mlonlinejudge.model.compile.LanguageConfig;
import com.masonluo.mlonlinejudge.model.compile.Run;

import java.util.Arrays;

/**
 * @author LZC
 * @create 2022-05-16 16:28
 */
public class CLanguageConfigFactory implements LanguageConfigFactory{
    @Override
    public LanguageConfig getLanguageConfig() {
        LanguageConfig config = new LanguageConfig();
        // 设置编译选项
        Compile compile = new Compile();
        compile.setSrcName("main.c");
        compile.setExeName("main");
        compile.setMaxCpuTime(3000);
        compile.setMaxRealTime(5000);
        compile.setMaxMemory(128 * 1024 * 1024);
        compile.setCompileCommand("/usr/bin/gcc -DONLINE_JUDGE -O2 -w -fmax-errors=3 -std=c99 {src_path} -lm -o {exe_path}");
        // 设置run选项
        Run run = new Run();
        run.setCommand("{exe_path}");
        run.setSeccompRule("c_cpp");
        run.setEnv(Arrays.asList(Run.DEFAULT_ENV));
        run.setMemoryLimitCheckOnly(1);

        config.setCompile(compile);
        config.setRun(run);
        config.setName("c");
        return config;
    }
}
