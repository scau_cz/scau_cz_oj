package com.masonluo.mlonlinejudge.mapper;

import com.masonluo.mlonlinejudge.entity.Book;
import com.masonluo.mlonlinejudge.model.bo.BookBo;
import com.masonluo.mlonlinejudge.model.dto.BookCreateDto;
import com.masonluo.mlonlinejudge.model.param.BookParam;
import com.masonluo.mlonlinejudge.model.vo.BookVo;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author masonluo
 * @date 2021/2/10 3:32 下午
 */
@Mapper(componentModel = "spring")
public interface BookMapper {
    Book createDtoToDo(BookCreateDto createDto);

    BookBo doToBo(Book book);
    
    BookCreateDto paramToCreateDto(BookParam param);

    BookVo boToVo(BookBo bo);

    List<BookVo> boToVo(List<BookBo> books);

    List<BookBo> doToBo(List<Book> selectAll);
}
