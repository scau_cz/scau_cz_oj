package com.masonluo.mlonlinejudge.model.bo;

/**
 * @author masonluo
 * @date 2021/2/10 3:35 下午
 */
public class BookBo {
    private Integer id;

    private String name;

    private String author;

    private String publisher;

    public BookBo(Integer id, String name, String author, String publisher) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.publisher = publisher;
    }

    public BookBo() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
}
