/**
 * Vo（View Object），一般用以Controller的返回，如果Vo无特殊要求，可以在Controller里直接返回Bo或者Dto对象
 *
 * @author masonluo
 * @date 2021/3/30 7:46 下午
 */
package com.masonluo.mlonlinejudge.model.vo;