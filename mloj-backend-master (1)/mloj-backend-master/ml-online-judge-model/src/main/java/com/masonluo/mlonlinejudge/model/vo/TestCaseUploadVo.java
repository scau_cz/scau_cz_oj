package com.masonluo.mlonlinejudge.model.vo;

/**
 * @author masonluo
 * @date 2021/2/7 5:34 下午
 */
public class TestCaseUploadVo {
    private String testCaseId;

    public TestCaseUploadVo() {
    }

    public TestCaseUploadVo(String testCaseId) {
        this.testCaseId = testCaseId;
    }

    public String getTestCaseId() {
        return testCaseId;
    }

    public void setTestCaseId(String testCaseId) {
        this.testCaseId = testCaseId;
    }
}
