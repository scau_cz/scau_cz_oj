package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户账号密码表
 * </p>
 *
 * @author yangqi
 * @since 2021-09-10
 */

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_user")
public class Administrator extends BaseEntity {


    @ApiModelProperty(value = "用户名，也是用户的唯一标识，默认为用户的邮箱")
    private String username;

    @ApiModelProperty(value = "用户密码，使用MD5加密")
    private String password;

    @ApiModelProperty(value = "用户状态，1为不可用，0为可用")
    @TableLogic(value = "0", delval = "1")
    private Integer status;


    @Override
    public String toString() {
        return "Administrator{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", status=" + status +
                '}';
    }
}
