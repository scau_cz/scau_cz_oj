package com.masonluo.mlonlinejudge.model.param;

import javax.validation.constraints.NotNull;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2021-08-13 12:35
 **/
public class EditSchoolParam extends SchoolParam{
    @NotNull
    private Integer id;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}


