package com.masonluo.mlonlinejudge.entity;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;

import java.util.List;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2022-02-27 16:28
 **/
public class ExperimentScore {
    private Integer id;

    private String name;

    private Integer passNum;

    private Integer total;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPassNum() {
        return passNum;
    }

    public void setPassNum(Integer passNum) {
        this.passNum = passNum;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public static List<ExperimentScore> toStrJson(String str){
        List<ExperimentScore> experimentScores = JSON.parseArray(str, ExperimentScore.class);
        return experimentScores;
    }
    public static String toJsonStr(List<ExperimentScore> experimentScores){
        return JSONUtil.toJsonStr(experimentScores);
    }

    @Override
    public String toString() {
        return "ExperimentScore{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", passNum=" + passNum +
                ", total=" + total +
                '}';
    }
}


