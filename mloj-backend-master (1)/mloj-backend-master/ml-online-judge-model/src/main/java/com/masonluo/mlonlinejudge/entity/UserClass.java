package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 用户班级关联实体
 * @author yellowSea
 */
@TableName("t_user_class")
public class UserClass extends BaseEntity{

    Integer userId;

    Integer classId;


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }
}
