package com.masonluo.mlonlinejudge.model.vo;


/**
 * @Author: shenma
 * @Date: 2024/4/3  16:05
 */
public class StatsOfTableVo {
    private Integer  countOfToday;
    private Integer  countOfYesterday;
    private Integer  countOfWeek;
    private Integer  countOfMonth;
    private Integer  countOfYear;

    public Integer getCountOfToday() {
        return countOfToday;
    }

    public void setCountOfToday(Integer countOfToday) {
        this.countOfToday = countOfToday;
    }

    public Integer getCountOfYesterday() {
        return countOfYesterday;
    }

    public void setCountOfYesterday(Integer countOfYesterday) {
        this.countOfYesterday = countOfYesterday;
    }

    public Integer getCountOfWeek() {
        return countOfWeek;
    }

    public void setCountOfWeek(Integer countOfWeek) {
        this.countOfWeek = countOfWeek;
    }

    public Integer getCountOfMonth() {
        return countOfMonth;
    }

    public void setCountOfMonth(Integer countOfMonth) {
        this.countOfMonth = countOfMonth;
    }

    public Integer getCountOfYear() {
        return countOfYear;
    }

    public void setCountOfYear(Integer countOfYear) {
        this.countOfYear = countOfYear;
    }

    @Override
    public String toString() {
        return "StatsOfTableVo{" +
                "countOfToday=" + countOfToday +
                ", countOfYesterday=" + countOfYesterday +
                ", countOfWeek=" + countOfWeek +
                ", countOfMonth=" + countOfMonth +
                ", countOfYear=" + countOfYear +
                '}';
    }
}
