package com.masonluo.mlonlinejudge.model.param;

import javax.validation.constraints.NotNull;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2021-10-07 00:04
 **/
public class EditExperimentParam {
    @NotNull
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}


