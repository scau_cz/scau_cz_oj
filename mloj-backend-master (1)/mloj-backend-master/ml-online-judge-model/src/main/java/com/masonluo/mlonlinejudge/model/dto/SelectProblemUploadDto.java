package com.masonluo.mlonlinejudge.model.dto;

import com.masonluo.mlonlinejudge.entity.Answer;
import com.masonluo.mlonlinejudge.model.bo.TagBo;
import com.masonluo.mlonlinejudge.model.vo.TagVo;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2021-10-07 18:16
 **/
public class SelectProblemUploadDto {


    private Integer id;

    /**
     *  0代表选择题，1代表判断题，2代表程序题
     */
    private Integer type;

    /**
     * 题目
     */
    private String title;

    /**
     * 题目列表
     */
    private List<Answer> answer;

    /**
     * 答案
     */
    private String correct;

    /**
     * 题目难度：0代表低级  1代表中级  2代表高级
     */
    private Integer level;

    /**
     * 标识
     */
    private List<TagVo> tags;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 章节id
     */

    private Integer indexId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Answer> getAnswer() {
        return answer;
    }

    public void setAnswer(List<Answer> answer) {
        this.answer = answer;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public List<TagVo> getTags() {
        return tags;
    }

    public void setTags(List<TagVo> tags) {
        this.tags = tags;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getIndexId() {
        return indexId;
    }

    public void setIndexId(Integer indexId) {
        this.indexId = indexId;
    }

    @Override
    public String toString() {
        return "SelectProblemParam{" +
                "type=" + type +
                ", title='" + title + '\'' +
                ", answer=" + answer +
                ", correct='" + correct + '\'' +
                ", level='" + level + '\'' +
                ", tags='" + tags + '\'' +
                ", userId=" + userId +
                ", indexId=" + indexId +
                '}';
    }
}


