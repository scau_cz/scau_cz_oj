package com.masonluo.mlonlinejudge.model.dto;
/**
*
*@author: yangqi
*@date: 2021-10-28
*/
public class ClassFindByTeacherIdDto {
    private Integer id;//班级id

    private String className;

    private Integer schoolId;//学校id



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    @Override
    public String toString() {
        return "ClassFindByTeacherIdDto{" +
                "id=" + id +
                ", className='" + className + '\'' +
                ", schoolId=" + schoolId +
                '}';
    }
}
