package com.masonluo.mlonlinejudge.model.vo;


/**
 * @Author: shenma
 * @Date: 2024/4/4  1:03
 */
public class StatsOfWeekVo {

    private Integer count;
    private String date;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "StatsOfWeekVo{" +
                "count=" + count +
                ", date='" + date + '\'' +
                '}';
    }
}
