package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.masonluo.mlonlinejudge.enums.ExecError;
import com.masonluo.mlonlinejudge.enums.ExecResult;

/**
 * 测试数据运行结果
 *
 * @author masonluo
 * @date 2020/12/26 9:12 下午
 */
@TableName("t_test_case_result")
public class TestCaseResult extends BaseEntity {
    /**
     * 运行该测试数据的solution id
     */
    private Integer solutionId;
    /**
     * 使用的Cpu时间
     */
    private Integer cpuTime = 0;
    /**
     * 实际执行耗时（用户感知时间）
     */
    private Integer realTime = 0;
    /**
     * 耗费的内存用量
     */
    private Integer memory = 0;
    /**
     * 执行结果
     */
    private ExecResult result;
    /**
     * 执行时出现的错误（非编译期错误）
     */
    private ExecError error = ExecError.SUCCESS;
    /**
     * 代码退出时的code
     */
    private Integer exitCode = 0;
    /**
     * 代码异常终止时的信号
     */
    private Integer signal = 0;
    /**
     * 运行测试数据的输出结果的md5值
     */
    private String outputMd5 = "";
    /**
     * 运行测试数据的输出结果
     */
    private String output = "";
    /**
     * 使用的是哪一个测试数据
     */
    private Integer testCase = 0;

    public Integer getSolutionId() {
        return solutionId;
    }

    public void setSolutionId(Integer solutionId) {
        this.solutionId = solutionId;
    }

    public Integer getCpuTime() {
        return cpuTime;
    }

    public void setCpuTime(Integer cpuTime) {
        this.cpuTime = cpuTime;
    }

    public Integer getRealTime() {
        return realTime;
    }

    public void setRealTime(Integer realTime) {
        this.realTime = realTime;
    }

    public Integer getMemory() {
        return memory;
    }

    public void setMemory(Integer memory) {
        this.memory = memory;
    }

    public ExecResult getResult() {
        return result;
    }

    public void setResult(ExecResult result) {
        this.result = result;
    }

    public ExecError getError() {
        return error;
    }

    public void setError(ExecError error) {
        this.error = error;
    }

    public Integer getExitCode() {
        return exitCode;
    }

    public void setExitCode(Integer exitCode) {
        this.exitCode = exitCode;
    }

    public Integer getSignal() {
        return signal;
    }

    public void setSignal(Integer signal) {
        this.signal = signal;
    }

    public String getOutputMd5() {
        return outputMd5;
    }

    public void setOutputMd5(String outputMd5) {
        this.outputMd5 = outputMd5;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public Integer getTestCase() {
        return testCase;
    }

    public void setTestCase(Integer testCase) {
        this.testCase = testCase;
    }

    @Override
    public String toString() {
        return "TestCaseResult{" +
                "id=" + id +
                ", createdTime=" + createdTime +
                ", updatedTime=" + updatedTime +
                ", solutionId=" + solutionId +
                ", cpuTime=" + cpuTime +
                ", realTime=" + realTime +
                ", memory=" + memory +
                ", result=" + result +
                ", error=" + error +
                ", exitCode=" + exitCode +
                ", signal=" + signal +
                ", outputMd5='" + outputMd5 + '\'' +
                ", output='" + output + '\'' +
                ", testCase=" + testCase +
                '}';
    }
}
