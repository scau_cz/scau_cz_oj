package com.masonluo.mlonlinejudge.model.bo;

import java.util.Objects;

public class UserRankBo {
    private String userName;
    private String nickname;
    private Integer userLevel;
    private Integer ranking;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userId) {
        this.userName = userId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(Integer userLevel) {
        this.userLevel = userLevel;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    @Override
    public String toString() {
        return "UserRankBo{" +
                "userName=" + userName +
                ", nickname='" + nickname + '\'' +
                ", userLevel=" + userLevel +
                ", ranking=" + ranking +
                '}';
    }
}
