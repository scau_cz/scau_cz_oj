package com.masonluo.mlonlinejudge.model.bo;

/**
 * 作者：吴彦祖
 * 日期：2024年4月25日20:44:44
 */
public class ClassBo {
    private Integer id;

    private String className;

    private Integer schoolId;

    private Integer teacherId;

    private Integer grade;

    private String major;

    private Integer clazz;

    public ClassBo() {

    }

    public ClassBo(Integer id, String className, Integer schoolId, Integer teacherId) {
        this.id = id;
        this.className = className;
        this.schoolId = schoolId;
        this.teacherId = teacherId;
    }

    public ClassBo(String className, Integer schoolId, Integer teacherId, Integer grade, String major, Integer clazz) {
        this.className = className;
        this.schoolId = schoolId;
        this.teacherId = teacherId;
        this.grade = grade;
        this.major = major;
        this.clazz = clazz;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Integer getClazz() {
        return clazz;
    }

    public void setClazz(Integer clazz) {
        this.clazz = clazz;
    }
}
