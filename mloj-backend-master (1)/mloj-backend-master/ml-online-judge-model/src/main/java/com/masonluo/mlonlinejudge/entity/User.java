package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import java.time.LocalDateTime;
import java.util.HashMap;

/**
 * 用户信息类，保存用户的账号密码等信息
 *
 * @author masonluo
 * @date 2020/12/26 3:33 下午
 */
@TableName("t_user")
public class User extends BaseEntity {
    /**
     * 用户的账号，默认为邮箱
     */
    private String username;

    private String password;

    /**
     * 用户状态，对应着数据库状态，0为false，1为true
     * <p>
     * 如果用户可用，则为true
     * 否则，为false
     */
    private boolean status;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", status=" + status +
                ", createdTime=" + createdTime +
                ", updatedTime=" + updatedTime +
                '}';
    }

    public static final class Builder {
        protected Integer id;
        protected LocalDateTime createdTime;
        protected LocalDateTime updatedTime;
        private String username;
        private String password;
        private boolean status;

        private Builder() {
        }

        public static Builder anUser() {
            return new Builder();
        }

        public Builder withId(Integer id) {
            this.id = id;
            return this;
        }

        public Builder withCreatedTime(LocalDateTime createdTime) {
            this.createdTime = createdTime;
            return this;
        }

        public Builder withUpdatedTime(LocalDateTime updatedTime) {
            this.updatedTime = updatedTime;
            return this;
        }

        public Builder withUsername(String username) {
            this.username = username;
            return this;
        }

        public Builder withPassword(String password) {
            this.password = password;
            return this;
        }

        public Builder withStatus(boolean status) {
            this.status = status;
            return this;
        }

        public User build() {
            User user = new User();
            user.setId(id);
            user.setCreatedTime(createdTime);
            user.setUpdatedTime(updatedTime);
            user.setUsername(username);
            user.setPassword(password);
            user.setStatus(status);
            return user;
        }
    }
}
