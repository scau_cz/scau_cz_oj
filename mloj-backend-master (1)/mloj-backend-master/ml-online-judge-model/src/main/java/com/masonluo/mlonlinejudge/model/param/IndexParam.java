package com.masonluo.mlonlinejudge.model.param;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author masonluo
 * @date 2021/3/28 2:51 下午
 */
public class IndexParam {

    @NotNull
    private Integer id;

    @NotNull
    private Integer bookId;

    @NotBlank
    private String name;

    @Min(value = 0, message = "The order should be large than 0")
    private Integer order;

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
