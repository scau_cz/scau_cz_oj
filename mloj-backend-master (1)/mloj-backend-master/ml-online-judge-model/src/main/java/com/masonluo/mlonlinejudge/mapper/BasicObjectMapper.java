package com.masonluo.mlonlinejudge.mapper;

import org.mapstruct.*;
import org.springframework.boot.context.properties.bind.Name;

import java.util.Collection;
import java.util.List;

/**
 * @author masonluo
 * @date 2020/12/28 3:41 下午
 */
public interface BasicObjectMapper<SOURCE, TARGET> {
    @Mappings({})
    @InheritConfiguration
    @Named("to")
    TARGET to(SOURCE source);

    @InheritConfiguration
    @Named("collection-to")
    List<TARGET> to(Collection<SOURCE> source);

    @InheritInverseConfiguration
    @Named("from")
    SOURCE from(TARGET source);

    @InheritInverseConfiguration
    @Named("collection-from")
    List<SOURCE> from(Collection<TARGET> source);
}