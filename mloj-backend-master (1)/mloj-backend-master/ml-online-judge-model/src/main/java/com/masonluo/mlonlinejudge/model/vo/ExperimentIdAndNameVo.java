package com.masonluo.mlonlinejudge.model.vo;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2022-04-13 12:10
 **/
public class ExperimentIdAndNameVo {
    private Integer id;

    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}


