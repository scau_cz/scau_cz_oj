package com.masonluo.mlonlinejudge.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.masonluo.mlonlinejudge.model.compile.LanguageConfig;

/**
 * 用来和Judge Server进行交互的Dto对象，向Judge Server传输数据
 *
 * @author masonluo
 * @date 2021/2/5 9:33 下午
 */
public class JudgeParamDto {
    private String src;

    @JsonProperty("language_config")
    private LanguageConfig languageConfig;

    @JsonProperty("max_cpu_time")
    private long maxCpuTime;

    @JsonProperty("max_memory")
    private long maxMemory;

    @JsonProperty("test_case_id")
    private String testCaseId;

    private boolean output;

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public LanguageConfig getLanguageConfig() {
        return languageConfig;
    }

    public void setLanguageConfig(LanguageConfig languageConfig) {
        this.languageConfig = languageConfig;
    }

    public long getMaxCpuTime() {
        return maxCpuTime;
    }

    public void setMaxCpuTime(long maxCpuTime) {
        this.maxCpuTime = maxCpuTime;
    }

    public long getMaxMemory() {
        return maxMemory;
    }

    public void setMaxMemory(long maxMemory) {
        this.maxMemory = maxMemory;
    }

    public String getTestCaseId() {
        return testCaseId;
    }

    public void setTestCaseId(String testCaseId) {
        this.testCaseId = testCaseId;
    }

    public boolean isOutput() {
        return output;
    }

    public void setOutput(boolean output) {
        this.output = output;
    }

    @Override
    public String toString() {
        return "JudgeParamDto{" +
                "src='" + src + '\'' +
                ", languageConfig=" + languageConfig +
                ", maxCpuTime=" + maxCpuTime +
                ", maxMemory=" + maxMemory +
                ", testCaseId=" + testCaseId +
                ", output=" + output +
                '}';
    }
}
