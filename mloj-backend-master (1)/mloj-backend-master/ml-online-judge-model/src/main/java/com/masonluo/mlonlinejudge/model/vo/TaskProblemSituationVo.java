package com.masonluo.mlonlinejudge.model.vo;

import com.masonluo.mlonlinejudge.entity.Solution;

public class TaskProblemSituationVo {

    private String situationInfo;

    private Integer problemId;


    public String getSituationInfo() {
        return situationInfo;
    }

    public void setSituationInfo(String situationInfo) {
        this.situationInfo = situationInfo;
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }
}
