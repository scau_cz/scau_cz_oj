package com.masonluo.mlonlinejudge.entity;


import java.time.LocalDateTime;

public class SolutionResultJoinTestWithTime extends SolutionResultJoinTest{
    private LocalDateTime createTime;
    private LocalDateTime updateTime;

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public SolutionResultJoinTestWithTime() {
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public SolutionResultJoinTestWithTime(Integer userId, Integer mode, Integer testId, Integer problemId, Integer solutionId, Integer resultId, LocalDateTime createTime, LocalDateTime updateTime) {
        super(userId, mode, testId, problemId, solutionId, resultId);
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public SolutionResultJoinTestWithTime(LocalDateTime createTime, LocalDateTime updateTime) {
        this.createTime = createTime;
        this.updateTime = updateTime;
    }
}
