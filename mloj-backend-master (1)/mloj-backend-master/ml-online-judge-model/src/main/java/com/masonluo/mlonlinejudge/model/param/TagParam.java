package com.masonluo.mlonlinejudge.model.param;

/**
 * @author masonluo
 * @date 2021/1/25 2:57 下午
 */
public class TagParam {
    private Integer id;

    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TagVo{" +
                "id=" + id +
                ", name=" + name +
                '}';
    }
}
