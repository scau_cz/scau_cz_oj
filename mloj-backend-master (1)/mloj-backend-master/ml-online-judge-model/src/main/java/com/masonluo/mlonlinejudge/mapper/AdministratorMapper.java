package com.masonluo.mlonlinejudge.mapper;

import com.masonluo.mlonlinejudge.entity.Administrator;
import com.masonluo.mlonlinejudge.model.bo.AdministratorBo;
import com.masonluo.mlonlinejudge.model.dto.AdministratorDto;
import com.masonluo.mlonlinejudge.model.param.AdministratorParam;
import org.mapstruct.Mapper;

/**
 *
 *@author: yangqi
 *@date: 2021-09-10
 */
@Mapper(componentModel = "spring")
public interface AdministratorMapper {
    Administrator createDtoToDo(AdministratorDto administratorDto);

    AdministratorBo doToBo(Administrator administrator);

    AdministratorDto paramToCreateDto(AdministratorParam administratorParam);
}
