package com.masonluo.mlonlinejudge.enums;

import java.util.Objects;

/**
 * @author masonluo
 * @date 2020/12/26 3:52 下午
 */
public enum Gender {
    UNKNOWN("unknown", 0),
    MALE("male", 1),
    FEMALE("female", 2);

    Gender(String description, int id) {
        this.description = description;
        this.id = id;
    }

    public final String description;

    public final int id;

    public String getDescription() {
        return description;
    }

    public static int getId(Gender e) {
        return e.id;
    }


    public static Gender getByName(String name){
        for(Gender e:values()){
            if(Objects.equals(e.name(),name)){
                return e;
            }
        }
        return null;
    }

    public static Gender getById(int id) {
        for (Gender gender : Gender.values()) {
            if (Objects.equals(id, gender.id)) {
                return gender;
            }
        }
        return null;
    }
}
