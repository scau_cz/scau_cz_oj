package com.masonluo.mlonlinejudge.mapper.decorator;

import com.masonluo.mlonlinejudge.mapper.SolutionMapper;

/**
 * @author masonluo
 * @date 2021/2/7 3:43 下午
 */
public abstract class SolutionMapperDecorator implements SolutionMapper {
}
