package com.masonluo.mlonlinejudge.mapper;

import com.masonluo.mlonlinejudge.entity.ExperimentScore;
import com.masonluo.mlonlinejudge.entity.ExperimentScoreResult;
import com.masonluo.mlonlinejudge.model.dto.ExperimentScoreResultDto;
import com.masonluo.mlonlinejudge.model.vo.ExperimentScoreResultVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ExperimentResultMapper {

    @Mapping(target = "experimentScore", expression = "java(com.masonluo.mlonlinejudge.entity.ExperimentScore.toStrJson(experimentScoreResult.getExperimentScore()))")
    @Mapping(target = "experimentProblemResult", expression = "java(com.masonluo.mlonlinejudge.entity.ExperimentProblemResult.toStrJson(experimentScoreResult.getExperimentProblemResult()))")
    ExperimentScoreResultDto doToDto(ExperimentScoreResult experimentScoreResult);

    @Mapping(target = "experimentScore", expression = "java(com.masonluo.mlonlinejudge.entity.ExperimentScore.toJsonStr(experimentScoreResultDto.getExperimentScore()))")
    @Mapping(target = "experimentProblemResult", expression = "java(com.masonluo.mlonlinejudge.entity.ExperimentProblemResult.toJsonStr(experimentScoreResultDto.getExperimentProblemResult()))")
    ExperimentScoreResult dtoToDo(ExperimentScoreResultDto experimentScoreResultDto);

    @Mapping(target = "experimentScore", expression = "java(com.masonluo.mlonlinejudge.entity.ExperimentScore.toStrJson(experimentScoreResult.getExperimentScore()))")
    @Mapping(target = "experimentProblemResult", expression = "java(com.masonluo.mlonlinejudge.entity.ExperimentProblemResult.toStrJson(experimentScoreResult.getExperimentProblemResult()))")
    List<ExperimentScoreResultDto> doToDto(List<ExperimentScoreResult> experimentScoreResult);

    @Mapping(target = "experimentScore", expression = "java(com.masonluo.mlonlinejudge.entity.ExperimentScore.toJsonStr(experimentScoreResultDto.getExperimentScore()))")
    @Mapping(target = "experimentProblemResult", expression = "java(com.masonluo.mlonlinejudge.entity.ExperimentProblemResult.toJsonStr(experimentScoreResultDto.getExperimentProblemResult()))")
    List<ExperimentScoreResult> dtoToDo(List<ExperimentScoreResultDto> experimentScoreResultDto);

    List<ExperimentScoreResultVo> dtoToVo(List<ExperimentScoreResultDto> experimentScoreResultDto);

}
