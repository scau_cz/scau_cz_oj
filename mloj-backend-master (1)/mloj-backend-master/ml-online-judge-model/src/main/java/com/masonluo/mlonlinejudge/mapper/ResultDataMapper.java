package com.masonluo.mlonlinejudge.mapper;

import com.masonluo.mlonlinejudge.enums.ExecError;
import com.masonluo.mlonlinejudge.enums.ExecResult;
import com.masonluo.mlonlinejudge.mapper.decorator.ResultDataMapperDecorator;
import com.masonluo.mlonlinejudge.mapper.decorator.UserInfoMapperDecorator;
import com.masonluo.mlonlinejudge.model.judge.JudgeResult;
import com.masonluo.mlonlinejudge.model.judge.ResultData;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import java.util.Map;

/**
 * @author masonluo
 * @date 2021/2/5 10:37 下午
 */
@Mapper(componentModel = "spring")
@DecoratedWith(ResultDataMapperDecorator.class)
public interface ResultDataMapper {

    ResultData map(Map<String, Object> map);

    List<ResultData> map(List<Map<String, Object>> map);
}
