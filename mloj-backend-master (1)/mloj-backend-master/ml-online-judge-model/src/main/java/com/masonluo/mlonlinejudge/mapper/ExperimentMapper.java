package com.masonluo.mlonlinejudge.mapper;

import com.masonluo.mlonlinejudge.entity.Experiment;
import com.masonluo.mlonlinejudge.enums.Level;
import com.masonluo.mlonlinejudge.model.bo.ExperimentBo;
import com.masonluo.mlonlinejudge.model.dto.ExperimentUploadDto;
import com.masonluo.mlonlinejudge.model.param.ExperimentParam;
import com.masonluo.mlonlinejudge.model.param.ExperimentUploadParam;
import com.masonluo.mlonlinejudge.model.vo.ExperimentVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;
import java.util.List;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2021-10-06 01:50
 **/
@Mapper(componentModel = "spring")
public interface ExperimentMapper {

    Experiment uploadParamToDo(ExperimentUploadParam experimentUploadParam);

    Experiment voToDo(ExperimentVo experimentVo);

    @Mapping(source = "creatorId", target = "creator")
    ExperimentVo doToVo(Experiment experiment);

    @Mapping(source = "creatorId", target = "creator")
    List<ExperimentVo> doToVo(List<Experiment> experiments);

    ExperimentBo voToBo(ExperimentVo experimentVo);

    //@Mapping(target = "startTime" , expression = "java(java.time.format.DateTimeFormatter.ofPattern(\"yyyy-MM-dd HH:mm:ss\").format(experiment.getStartTime()))")
    //@Mapping(target = "endTime" , expression = "java(java.time.format.DateTimeFormatter.ofPattern(\"yyyy-MM-dd HH:mm:ss\").format(experiment.getEndTime()))")
    @Mapping(source = "creatorId", target = "creator")
    ExperimentBo doToBo(Experiment experiment);

    //@Mapping(target = "startTime" , expression = "java(java.time.format.DateTimeFormatter.ofPattern(\"yyyy-MM-dd HH:mm:ss\").format(experiment.getStartTime()))")
    //@Mapping(target = "endTime" , expression = "java(java.time.format.DateTimeFormatter.ofPattern(\"yyyy-MM-dd HH:mm:ss\").format(experiment.getEndTime()))")
    @Mapping(source = "creatorId", target = "creator")
    List<ExperimentBo> doToBo(List<Experiment> experiments);


    Experiment from(ExperimentBo ExperimentBo);

    List<Experiment> from(Collection<ExperimentBo> ExperimentBos);



    //@Mapping(target = "startTime", source = "startTime" , dateFormat = "yyyy-MM-dd HH:mm:ss")
    //@Mapping(target = "endTime",source = "endTime", dateFormat = "yyyy-MM-dd HH:mm:ss")
    Experiment experimentUploadDtoToExperiment(ExperimentUploadDto experimentUploadDto);

    ExperimentUploadDto experimentToExperimentUploadDto(Experiment experiment);

    ExperimentUploadDto paramToDto(ExperimentParam experimentParam);

    //@Mapping(target = "startTime", source = "startTime" , dateFormat = "yyyy-MM-dd HH:mm:ss")
    //@Mapping(target = "endTime",source = "endTime", dateFormat = "yyyy-MM-dd HH:mm:ss")
    Experiment experimentParamExperiment(ExperimentParam experimentParam);

    ExperimentParam doToParam(Experiment experiment);
}


