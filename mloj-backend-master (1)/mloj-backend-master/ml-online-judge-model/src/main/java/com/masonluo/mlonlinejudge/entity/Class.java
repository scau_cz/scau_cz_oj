package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * <p>
 * 班级表
 * </p>
 *
 * @author testjava
 * @since 2021-08-18
 */
@TableName("t_class")
@ApiModel(value = "Class对象", description = "班级表")
public class Class extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    public Class() {

    }

    @ApiModelProperty(value = "班级id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "高校id")
    private Integer schoolId;

    @ApiModelProperty(value = "班级名称")
    private String className;

    @ApiModelProperty(value = "年级")
    private Integer grade;

    @ApiModelProperty(value = "专业")
    private String major;

    @ApiModelProperty(value = "班别")
    private Integer clazz;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Integer getClazz() {
        return clazz;
    }

    public void setClazz(Integer clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toString() {
        return "Class{" +
                "id=" + id +
                ", schoolId=" + schoolId +
                ", className='" + className + '\'' +
                ", grade=" + grade +
                ", major='" + major + '\'' +
                ", clazz='" + clazz + '\'' +
                '}';
    }
}
