package com.masonluo.mlonlinejudge.model.compile.factory;

import com.masonluo.mlonlinejudge.enums.Language;
import com.masonluo.mlonlinejudge.model.compile.LanguageConfig;

/**
 * @author masonluo
 * @date 2021/2/6 4:10 下午
 */
public final class LanguageConfigUtils {
    public static LanguageConfig getLanguageConfig(Language language) {
        LanguageConfigFactory factory = null;
        switch (language) {
            case C:
                factory = new CLanguageConfigFactory();
                break;
            case JAVA:
                factory = new JavaLanguageConfigFactory();
                break;
                // TODO 支持的编程语言中添加Python
            case PYTHON:
                factory = new PythonLanguageConfigFactory();
                break;
                // TODO 支持的编程语言中添加C++
            case CPP:
                factory = new CppLanguageConfigFactory();
                break;
        }
        return factory == null ? null : factory.getLanguageConfig();
    }
}
