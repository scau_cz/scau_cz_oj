package com.masonluo.mlonlinejudge.model.vo;

/**
 * @author masonluo
 * @date 2021/2/11 1:12 下午
 */
public class LoginVo {
    private TokenVo normalToken;

    private TokenVo refreshToken;

    private String username;

    public TokenVo getNormalToken() {
        return normalToken;
    }

    public void setNormalToken(TokenVo normalToken) {
        this.normalToken = normalToken;
    }

    public TokenVo getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(TokenVo refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
