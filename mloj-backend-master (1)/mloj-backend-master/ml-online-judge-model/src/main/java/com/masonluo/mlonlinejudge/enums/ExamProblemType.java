package com.masonluo.mlonlinejudge.enums;

import java.util.Objects;

/**
 * @author 罗有冠
 * @date 2022/2/16 22:53
 */
public enum ExamProblemType {
    PROGRAMMING("programming", 1),
    CHOICE("choice", 2),
    TRUEORFALSE("trueorfalse", 3);


    public final int id;

    public final String description;

    ExamProblemType(String description, int id) {
        this.id = id;
        this.description = description;
    }

    public static ExamProblemType getByName(String name){
        for(ExamProblemType e:values()){
            if(Objects.equals(e.name(),name)){
                return e;
            }
        }
        return null;
    }

    public static ExamProblemType getById(int id) {
        for (ExamProblemType examProblemType : ExamProblemType.values()) {
            if (Objects.equals(id, examProblemType.id)) {
                return examProblemType;
            }
        }
        return null;
    }


}
