package com.masonluo.mlonlinejudge.model.param;


import com.masonluo.mlonlinejudge.model.bo.IoSampleBo;
import com.masonluo.mlonlinejudge.model.bo.TagBo;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author masonluo
 * @date 2021/2/7 5:41 下午
 */
public class ProblemUploadParam {

    @NotBlank(message = "userId不能为空")
    private Integer userId;

    @NotBlank
    private String title;

    @NotBlank
    private String description;

    @NotBlank
    private String inputDescription;

    @NotBlank
    private String outputDescription;

    @Max(1000000)
    @Min(0)
    private Integer timeLimit;

    @Max(100000000)
    @Min(0)
    private Integer memoryLimit;

    @Min(0)
    @Max(4)
    private Integer level;

    private String hint;

    private boolean visible;

    private List<TagParam> tags;

    private List<IoSampleParam> ioSamples;

    @NotBlank(message = "Test case id should not be empty")
    private String testCaseId;

    private String answer;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInputDescription() {
        return inputDescription;
    }

    public void setInputDescription(String inputDescription) {
        this.inputDescription = inputDescription;
    }

    public String getOutputDescription() {
        return outputDescription;
    }

    public void setOutputDescription(String outputDescription) {
        this.outputDescription = outputDescription;
    }

    public Integer getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(Integer timeLimit) {
        this.timeLimit = timeLimit;
    }

    public Integer getMemoryLimit() {
        return memoryLimit;
    }

    public void setMemoryLimit(Integer memoryLimit) {
        this.memoryLimit = memoryLimit;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public List<TagParam> getTags() {
        return tags;
    }

    public void setTags(List<TagParam> tags) {
        this.tags = tags;
    }

    public List<IoSampleParam> getIoSamples() {
        return ioSamples;
    }

    public void setIoSamples(List<IoSampleParam> ioSamples) {
        this.ioSamples = ioSamples;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getTestCaseId() {
        return testCaseId;
    }

    public void setTestCaseId(String testCaseId) {
        this.testCaseId = testCaseId;
    }
}
