package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author 罗有冠
 * @date 2022/3/2 22:31
 */
@TableName("t_ac_record")
public class AcRecord {

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "编程题id")
    private Integer problemId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public AcRecord() {
    }
}
