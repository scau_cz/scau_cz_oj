package com.masonluo.mlonlinejudge.mapper;

import com.masonluo.mlonlinejudge.entity.TestCaseResult;
import com.masonluo.mlonlinejudge.model.judge.ResultData;
import org.mapstruct.Mapper;

/**
 * @author masonluo
 * @date 2021/2/7 12:47 上午
 */
@Mapper(componentModel = "spring")
public interface TestCaseResultMapper {
    TestCaseResult resultDataToTestCaseResult(ResultData resultData);
}
