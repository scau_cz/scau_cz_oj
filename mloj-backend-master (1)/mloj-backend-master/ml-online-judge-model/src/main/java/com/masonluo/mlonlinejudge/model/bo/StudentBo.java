package com.masonluo.mlonlinejudge.model.bo;

public class StudentBo {
    private Integer userId;
    private String userName;
    private String nickName;
    private String phone;
    private Integer gender;
    private Integer schoolId;
    private String password;


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    @Override
    public String toString() {
        return "StudentParam{" +
                "userId=" + userId +
                ", username='" + userName + '\'' +
                ", nickname='" + nickName + '\'' +
                ", phone='" + phone + '\'' +
                ", gender=" + gender +
                ", schoolId=" + schoolId +
                ", password='" + password + '\'' +
                '}';
    }

}
