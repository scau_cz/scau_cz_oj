package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
//import lombok.Data;
//import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 学生成绩表
 * </p>
 *
 * @author yangqi
 * @since 2021-08-19
 */
//@Data
//@EqualsAndHashCode(callSuper = false)
@TableName("t_score")
@ApiModel(value="Score对象", description="学生成绩表")
public class Score implements Serializable {



    @ApiModelProperty(value = "成绩id")
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "测验id")
    private Integer examId;

    @ApiModelProperty(value = "分数")
    private Integer grade;

//    @ApiModelProperty(value = "学生名字")
//    private String studentName;

    @ApiModelProperty(value = "班级id")
    private Integer classId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public Integer getExamId() {
        return examId;
    }

    public void setExamId(Integer examId) {
        this.examId = examId;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }
//
//    public String getStudentName() {
//        return studentName;
//    }
//
//    public void setStudentName(String studentName) {
//        this.studentName = studentName;
//    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    @Override
    public String toString() {
        return "Score{" +
                "id=" + id +
                ", studentId=" + studentId +
                ", examId=" + examId +
                ", grade=" + grade +
//                ", studentName='" + studentName + '\'' +
                ", classId=" + classId +
                '}';
    }
}
