package com.masonluo.mlonlinejudge.model.param;

import com.masonluo.mlonlinejudge.entity.AnswerContent;

import java.util.List;

public class SubmitParam {

    private Integer userId;

    private Integer examId;

    private Integer classId;

    private List<AnswerContent> answerContent;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }


    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public List<AnswerContent> getAnswerContent() {
        return answerContent;
    }

    public void setAnswerContent(List<AnswerContent> answerContent) {
        this.answerContent = answerContent;
    }

    public Integer getExamId() {
        return examId;
    }

    public void setExamId(Integer examId) {
        this.examId = examId;
    }
}
