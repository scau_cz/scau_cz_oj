package com.masonluo.mlonlinejudge.model.param;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

/**
 * @program: mloj-backend
 * @description:
 * @author: LOTP
 * @create: 2021-08-13 12:37
 **/
public class SchoolParam {

    @NotBlank
    private String schoolName;

    private String email;

    /*private LocalDate createTime;


    private LocalDate updateTime;*/

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    /*
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }



    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }*/

    /*public LocalDate getCreateTime() {
        return createTime;
    }

    public LocalDate getUpdateTime() {
        return updateTime;
    }*/
}


