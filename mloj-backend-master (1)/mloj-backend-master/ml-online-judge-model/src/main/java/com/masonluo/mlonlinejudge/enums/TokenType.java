package com.masonluo.mlonlinejudge.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.util.ObjectUtils;

/**
 * @author masonluo
 * @date 2020/12/31 8:45 下午
 */
public enum TokenType {
    NORMAL(0, "normal"),
    REFRESH(1, "refresh");

    private int id;

    private String type;

    TokenType(int id, String type) {
        this.id = id;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @JsonValue
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonCreator
    public static TokenType getByType(String type) {
        for (TokenType tokenType : TokenType.values()) {
            if (ObjectUtils.nullSafeEquals(type, tokenType.getType())) {
                return tokenType;
            }
        }
        return null;
    }
}
