package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @author LZC
 * @create 2021-12-20 22:43
 */
@TableName("t_program_answer")
public class ProgramAnswerEntity extends BaseEntity{
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String test_case_id;
    private String answer_content;
    private Integer problem_id;
    private String input_content;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getTest_case_id() {
        return test_case_id;
    }

    public void setTest_case_id(String test_case_id) {
        this.test_case_id = test_case_id;
    }

    public String getAnswer_content() {
        return answer_content;
    }

    public void setAnswer_content(String answer_content) {
        this.answer_content = answer_content;
    }

    public String getInput_content() {
        return input_content;
    }

    public void setInput_content(String input_content) {
        this.input_content = input_content;
    }

    public Integer getProblem_id() {
        return problem_id;
    }

    public void setProblem_id(Integer problem_id) {
        this.problem_id = problem_id;
    }
}
