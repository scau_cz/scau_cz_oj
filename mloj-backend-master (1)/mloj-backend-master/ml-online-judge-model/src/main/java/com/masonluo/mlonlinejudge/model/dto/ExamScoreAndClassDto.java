package com.masonluo.mlonlinejudge.model.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;

@ColumnWidth(17)
public class ExamScoreAndClassDto {

    @ExcelProperty("考试ID")
    private Integer examId;
    @ExcelProperty("考试标题")
    private String examName;

    private Integer classId;
    @ExcelProperty("班级名称")
    private String className;
    @ExcelProperty("学号")
    private Integer userId;

    private String userName;
    @ExcelProperty("姓名")
    private String nickName;
    @ExcelProperty("单选题")
    private Integer singleChoiceScore;
    @ExcelProperty("判断题")
    private Integer trueFalseQuestionScore;
    @ExcelProperty("编程题")
    private Integer programQuestionScore;
    @ExcelProperty("总成绩")
    private Integer totalScore;

    @Override
    public String toString() {
        return "ExamScoreAndClassDto{" +
                "examId=" + examId +
                ", examName='" + examName + '\'' +
                ", classId=" + classId +
                ", className='" + className + '\'' +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                ", nickName='" + nickName + '\'' +
                ", singleChoiceScore=" + singleChoiceScore +
                ", trueFalseQuestionScore=" + trueFalseQuestionScore +
                ", programQuestionScore=" + programQuestionScore +
                ", totalScore=" + totalScore +
                '}';
    }

    public Integer getExamId() {
        return examId;
    }

    public void setExamId(Integer examId) {
        this.examId = examId;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getSingleChoiceScore() {
        return singleChoiceScore;
    }

    public void setSingleChoiceScore(Integer singleChoiceScore) {
        this.singleChoiceScore = singleChoiceScore;
    }

    public Integer getTrueFalseQuestionScore() {
        return trueFalseQuestionScore;
    }

    public void setTrueFalseQuestionScore(Integer trueFalseQuestionScore) {
        this.trueFalseQuestionScore = trueFalseQuestionScore;
    }

    public Integer getProgramQuestionScore() {
        return programQuestionScore;
    }

    public void setProgramQuestionScore(Integer programQuestionScore) {
        this.programQuestionScore = programQuestionScore;
    }

    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
