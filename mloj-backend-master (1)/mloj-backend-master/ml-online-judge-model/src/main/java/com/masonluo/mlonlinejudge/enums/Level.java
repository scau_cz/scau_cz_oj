package com.masonluo.mlonlinejudge.enums;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.masonluo.mlonlinejudge.entity.Answer;
import com.masonluo.mlonlinejudge.entity.ExperimentScore;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/**
 * 问题难度等级
 *
 * @author masonluo
 * @date 2020/12/26 4:02 下午
 */
public enum Level {

    EASY(0, 0),
    MEDIUM(1, 1),
    HARD(2, 2),
    INSUPERABLE(3,3),
    HELL(4,4);



    Level(int description, int id) {
        this.description = description;
        this.id = id;
    }

    public final int description;

    public final int id;

    public int getDescription() {
        return description;
    }

    public int getId() {
        return id;
    }

    public static Level getById(int id) {
        for (Level level : Level.values()) {
            if (Objects.equals(id, level.id)) {
                return level;
            }
        }
        return null;
    }

    public static List<Answer> conversion(String str){
        List<Answer> answers = JSON.parseArray(str, Answer.class);
        return answers;
    }
    public static String toJsonStr(List<Answer> answer){
        return JSONUtil.toJsonStr(answer);
    }

    public static List<ExperimentScore> strToJson(String str){
        List<ExperimentScore> experimentScores = JSON.parseArray(str, ExperimentScore.class);
        return experimentScores;
    }
    public static String jsonToStr(List<ExperimentScore> experimentScores){
        return JSONUtil.toJsonStr(experimentScores);
    }

    public static LocalDateTime toDateStr(String time){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return  LocalDateTime.parse(time, formatter);
    }
}
