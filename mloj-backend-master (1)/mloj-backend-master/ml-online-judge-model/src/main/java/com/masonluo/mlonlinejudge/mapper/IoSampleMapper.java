package com.masonluo.mlonlinejudge.mapper;

import com.masonluo.mlonlinejudge.entity.IoSample;
import com.masonluo.mlonlinejudge.model.bo.IoSampleBo;
import org.mapstruct.Mapper;

/**
 * @author masonluo
 * @date 2021/1/22 5:50 下午
 */
@Mapper(componentModel = "spring")
public interface IoSampleMapper extends BasicObjectMapper<IoSample, IoSampleBo> {
}
