package com.masonluo.mlonlinejudge.model.dto;

import com.masonluo.mlonlinejudge.enums.Language;

/**
 * 用户问题解决方案Dto，用来保存到消息队列中
 *
 * @author masonluo
 * @date 2021/2/6 1:35 下午
 */
public class SolutionDto {

    private Integer id;

    private Integer userId;

    private String src;

    private Language language;

    private Integer problemId;

    private Integer examAndExperimentId;

    private String userToken;


    public String getSrc() {
        return src;
    }

    public Integer getExamAndExperimentId() {
        return examAndExperimentId;
    }

    public void setExamAndExperimentId(Integer examAndExperimentId) {
        this.examAndExperimentId = examAndExperimentId;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "SolutionDto{" +
                "id=" + id +
                ", userId=" + userId +
                ", src='" + src + '\'' +
                ", language=" + language +
                ", problemId=" + problemId +
                ", userToken='" + userToken + '\'' +
                '}';
    }
}
