package com.masonluo.mlonlinejudge.model.bo;

public class AcRecordBo {
    private Integer userId;
    private Integer problemId;

    public AcRecordBo() {
    }

    public AcRecordBo(Integer userId, Integer problemId) {
        this.userId = userId;
        this.problemId = problemId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    @Override
    public String toString() {
        return "AcRecordBo{" +
                "userId=" + userId +
                ", problemId=" + problemId +
                '}';
    }
}
