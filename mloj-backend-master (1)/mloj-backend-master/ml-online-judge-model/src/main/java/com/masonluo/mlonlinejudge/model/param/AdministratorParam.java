package com.masonluo.mlonlinejudge.model.param;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class AdministratorParam {
    @NotNull(message = "管理员id不能为空")
    private Integer id;
    @NotBlank(message = "管理员邮箱不能为空")
    private String username;
    @NotBlank(message = "管理员密码不能为空")
    private String password;
//    private Integer status;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    //    public Integer getStatus() {
//        return status;
//    }
//
//    public void setStatus(Integer status) {
//        this.status = status;
//    }
}
