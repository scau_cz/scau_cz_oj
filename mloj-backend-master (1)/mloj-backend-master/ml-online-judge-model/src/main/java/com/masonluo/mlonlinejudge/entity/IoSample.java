package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 输入输出示例
 * @author masonluo
 * @date 2020/12/26 4:13 下午
 */
@TableName("t_io_sample")
public class IoSample extends BaseEntity{
    private Integer problemId;

    private String inputSample;

    private String outputSample;

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public String getInputSample() {
        return inputSample;
    }

    public void setInputSample(String inputSample) {
        this.inputSample = inputSample;
    }

    public String getOutputSample() {
        return outputSample;
    }

    public void setOutputSample(String outputSample) {
        this.outputSample = outputSample;
    }

    @Override
    public String toString() {
        return "IoSample{" +
                "id=" + id +
                ", createdTime=" + createdTime +
                ", updatedTime=" + updatedTime +
                ", problemId=" + problemId +
                ", inputSample='" + inputSample + '\'' +
                ", outputSample='" + outputSample + '\'' +
                '}';
    }
}
