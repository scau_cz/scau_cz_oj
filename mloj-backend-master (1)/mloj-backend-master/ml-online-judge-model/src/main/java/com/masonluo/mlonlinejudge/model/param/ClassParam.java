package com.masonluo.mlonlinejudge.model.param;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 作者: 吴彦祖
 * 日期: 2024年4月25日20:30:46
 */
public class ClassParam {

    @NotBlank
    private String className;

    @NotNull
    private Integer schoolId;

    @NotNull
    private Integer teacherId;

    @NotBlank
    private Integer grade;
    @NotBlank
    private String major;
    @NotBlank
    private Integer clazz;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Integer getClazz() {
        return clazz;
    }

    public void setClazz(Integer clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toString() {
        return "ClassParam{" +
                "className='" + className + '\'' +
                ", schoolId=" + schoolId +
                ", teacherId=" + teacherId +
                ", grade=" + grade +
                ", major='" + major + '\'' +
                ", clazz=" + clazz +
                '}';
    }
}
