package com.masonluo.mlonlinejudge.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Answer implements Serializable {

    private String prefix;

    private String content;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
