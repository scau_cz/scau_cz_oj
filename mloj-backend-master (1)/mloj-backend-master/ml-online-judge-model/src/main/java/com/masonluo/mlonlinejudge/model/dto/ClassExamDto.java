package com.masonluo.mlonlinejudge.model.dto;

public class ClassExamDto {
    private Integer classId;

    private Integer examId;

    private String examName;

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getExamId() {
        return examId;
    }

    public void setExamId(Integer examId) {
        this.examId = examId;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    @Override
    public String toString() {
        return "ClassExamDto{" +
                "classId=" + classId +
                ", examId=" + examId +
                ", examName='" + examName + '\'' +
                '}';
    }
}
