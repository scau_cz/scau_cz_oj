package com.masonluo.mlonlinejudge.model.dto;

/**
 * @author masonluo
 * @date 2021/1/20 4:05 下午
 */
public class LoginDto {
    private TokenDto normalToken;

    private TokenDto refreshToken;

    private String username;

    public LoginDto() {
    }

    public LoginDto(TokenDto normalToken, TokenDto refreshToken, String username) {
        this.normalToken = normalToken;
        this.refreshToken = refreshToken;
        this.username = username;
    }

    public TokenDto getNormalToken() {
        return normalToken;
    }

    public void setNormalToken(TokenDto normalToken) {
        this.normalToken = normalToken;
    }

    public TokenDto getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(TokenDto refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "LoginDto{" +
                "normalToken=" + normalToken +
                ", refreshToken=" + refreshToken +
                ", username='" + username + '\'' +
                '}';
    }
}
