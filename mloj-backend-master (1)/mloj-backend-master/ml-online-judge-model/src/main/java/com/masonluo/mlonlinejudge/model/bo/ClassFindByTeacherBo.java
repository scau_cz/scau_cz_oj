package com.masonluo.mlonlinejudge.model.bo;

/**
 * 作者: 吴彦祖
 * 日期: 2021-10-28
 */
public class ClassFindByTeacherBo {
    private Integer id;// 班级id

    private String className;

    private Integer schoolId;// 学校id

    private Integer grade; // 年级

    private String major; // 专业

    private Integer clazz; // 班别

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Integer getClazz() {
        return clazz;
    }

    public void setClazz(Integer clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toString() {
        return "ClassFindByTeacherBo{" +
                "id=" + id +
                ", className='" + className + '\'' +
                ", schoolId=" + schoolId +
                ", grade=" + grade +
                ", major='" + major + '\'' +
                ", clazz=" + clazz +
                '}';
    }
}
