package com.masonluo.mlonlinejudge.model.bo;

import com.masonluo.mlonlinejudge.entity.Answer;
import com.masonluo.mlonlinejudge.entity.Tag;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: molj
 * @description:
 * @author: LOTP
 * @create: 2021-08-28 23:27
 **/
public class SelectProblemBo {
    private Integer id;

    /**
     *  0代表选择题，1代表判断题，2代表程序题
     */
    private Integer type;

    /**
     * 题目
     */
    private String title;

    /**
     * 题目列表
     */
    private List<Answer> answer;

    /**
     * 答案
     */
    private String correct;

    /**
     * 题目难度：0代表低级  1代表中级  2代表高级
     */
    private Integer level;

    /**
     * 标识
     */
    private List<TagBo> tags = new ArrayList<>();


    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 章节id
     */
    private Integer indexId;

    public SelectProblemBo(Integer id, Integer type, String title, List<Answer> answer, String correct, Integer level,List<TagBo> tags, Integer userId, Integer indexId){
        this.id = id;
        this.type = type;
        this.title = title;
        this.answer = answer;
        this.correct = correct;
        this.level = level;
        this.tags = tags;
        this.userId = userId;
        this.indexId = indexId;
    }

    public List<TagBo> getTags() {
        return tags;
    }

    public void setTags(List<TagBo> tags) {
        this.tags = tags;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Answer> getAnswer() {
        return answer;
    }

    public void setAnswer(List<Answer> answer) {
        this.answer = answer;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getIndexId() {
        return indexId;
    }

    public void setIndexId(Integer indexId) {
        this.indexId = indexId;
    }
}


