package com.masonluo.mlonlinejudge.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author masonluo
 * @date 2021/1/25 11:20 下午
 */
public class TestCase {
    /**
     * output的md5值，去除所有的空白字符
     */
    @JsonProperty("stripped_output_md5")
    private String strippedOutputMd5;

    @JsonProperty("input_name")
    private String inputName;

    @JsonProperty("output_name")
    private String outputName;

    @JsonProperty("input_size")
    private long inputSize;

    @JsonProperty("output_size")
    private long outputSize;

    public String getStrippedOutputMd5() {
        return strippedOutputMd5;
    }

    public void setStrippedOutputMd5(String strippedOutputMd5) {
        this.strippedOutputMd5 = strippedOutputMd5;
    }

    public String getInputName() {
        return inputName;
    }

    public void setInputName(String inputName) {
        this.inputName = inputName;
    }

    public String getOutputName() {
        return outputName;
    }

    public void setOutputName(String outputName) {
        this.outputName = outputName;
    }

    public long getInputSize() {
        return inputSize;
    }

    public void setInputSize(long inputSize) {
        this.inputSize = inputSize;
    }

    public long getOutputSize() {
        return outputSize;
    }

    public void setOutputSize(long outputSize) {
        this.outputSize = outputSize;
    }
}