package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.masonluo.mlonlinejudge.enums.JudgeMode;
import com.masonluo.mlonlinejudge.enums.Language;

/**
 * 用户提交的问题解决方案
 *
 * @author masonluo
 * @date 2020/12/26 9:08 下午
 */
@TableName("t_solution")
public class Solution extends BaseEntity {

    private Integer problemId;
    /**
     * 提交该解决方案的用户id
     */
    private Integer userId;
    /**
     * 源代码
     */
    private String sourceCode;
    /**
     * 语言
     */
    private Language language;

    /**
     * 评判模式
     */
    private JudgeMode mode;
    /**
     * 代码运行结果id， 如果resultId = -1，那么当前代码正在等待执行
     */
    private Integer resultId;

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public JudgeMode getMode() {
        return mode;
    }

    public void setMode(JudgeMode mode) {
        this.mode = mode;
    }

    public Integer getResultId() {
        return resultId;
    }

    public void setResultId(Integer resultId) {
        this.resultId = resultId;
    }

    @Override
    public String toString() {
        return "Solution{" +
                "id=" + id +
                ", createdTime=" + createdTime +
                ", updatedTime=" + updatedTime +
                ", problemId=" + problemId +
                ", userId=" + userId +
                ", sourceCode='" + sourceCode + '\'' +
                ", language=" + language +
                ", mode=" + mode +
                ", resultId=" + resultId +
                '}';
    }
}
