/**
 * 用户发送给MQ的对象，统一存放在这个包
 *
 * @author masonluo
 * @date 2021/3/30 7:44 下午
 */
package com.masonluo.mlonlinejudge.model.mq;