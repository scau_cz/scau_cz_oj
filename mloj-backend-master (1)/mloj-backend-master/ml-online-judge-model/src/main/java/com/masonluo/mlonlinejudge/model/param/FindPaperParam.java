package com.masonluo.mlonlinejudge.model.param;

public class FindPaperParam {
    private Integer current;

    private Integer limit;

    private String name;

    public Integer getCurrent() {
        return current;
    }

    public void setCurrent(Integer current) {
        this.current = current;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "FindPaperParam{" +
                "current=" + current +
                ", limit=" + limit +
                ", name='" + name + '\'' +
                '}';
    }
}
