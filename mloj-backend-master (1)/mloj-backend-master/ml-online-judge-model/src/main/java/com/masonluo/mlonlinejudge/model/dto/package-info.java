/**
 * Dto(Data Transfer Object), 数据传输对象，一般为无状态，可在各个层次之间参数的传输或者返回
 * 跟Vo并无太大的差别，可以直接在各个层（Controller、Service）返回这个对象，而无需进行过多的转换
 *
 * @author masonluo
 * @date 2021/3/30 7:41 下午
 */
package com.masonluo.mlonlinejudge.model.dto;