/**
 * 各层方法的入参，如果过多参数的话，统一使用xxxParam进行封装使用
 *
 * @author masonluo
 * @date 2021/3/30 7:44 下午
 */
package com.masonluo.mlonlinejudge.model.param;