package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 每一本书的章节索引（目录）
 *
 * @author masonluo
 * @date 2021/2/10 3:15 下午
 */
@TableName("t_index")
public class Index extends BaseEntity {
    private String name;

    /**
     * 排序顺序，值越大，排序越往后。 比如第一章，这个值就为1，排序顺序在最前面
     */
    private Integer order;

    private Integer bookId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    @Override
    public String toString() {
        return "Index{" +
                "id=" + id +
                ", createdTime=" + createdTime +
                ", updatedTime=" + updatedTime +
                ", name='" + name + '\'' +
                ", order=" + order +
                ", bookId=" + bookId +
                '}';
    }
}
