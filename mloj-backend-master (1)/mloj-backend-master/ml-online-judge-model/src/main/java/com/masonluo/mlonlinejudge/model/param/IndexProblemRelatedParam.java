package com.masonluo.mlonlinejudge.model.param;

import java.util.ArrayList;
import java.util.List;

/**
 * @author masonluo
 * @date 2021/2/10 3:53 下午
 */
public class IndexProblemRelatedParam {
    private List<Integer> problemId = new ArrayList<>();

    public List<Integer> getProblemId() {
        return problemId;
    }

    public void setProblemId(List<Integer> problemId) {
        this.problemId = problemId;
    }
}
