package com.masonluo.mlonlinejudge.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.masonluo.mlonlinejudge.entity.Result;
import com.masonluo.mlonlinejudge.enums.ExecResult;

import java.time.LocalDateTime;

public class ResultVo {
    /**
     * 最大Cpu时间，因为有多组测试数据，当运行结果正确({@link ExecResult#SUCCESS})时不为空
     */
    private Integer cpuTime = 0;
    /**
     * 最大内存使用，因为有多组测试数据，当运行结果正确({@link ExecResult#SUCCESS})时不为空
     */
    private Integer memory = 0;
    /**
     * 编译结果， 是否编译通过
     * TODO 这个应该变为多种类型的结果
     */
    private boolean compilerResult;
    /**
     * 编译错误提示信息，只有当{@link Result##compilerResult}为false时，才会有值
     */
    private String errorData;
    /**
     * 运行结果，只有当{@link Result##compilerResult}为true时，才有效
     */
    private ExecResult processResult;
    /***
     * 测试用例的正确答案
     */
    private String trueAnswer;//正确的答案
    private String userAnswer;//用户输出的答案
    private Integer testCaseIndex;//第几个测试用例出错了
    private String inputContent;//输入的内容
    private Integer userId; //该记录提交的用户id
    private String userName; //该记录提交的用户名字
    private String problem_title;//题目标题

    @TableId(value = "id", type = IdType.AUTO)
    protected Integer id;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    protected LocalDateTime createdTime;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    protected LocalDateTime updatedTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDateTime getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(LocalDateTime updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Integer getCpuTime() {
        return cpuTime;
    }

    public void setCpuTime(Integer cpuTime) {
        this.cpuTime = cpuTime;
    }

    public Integer getMemory() {
        return memory;
    }

    public void setMemory(Integer memory) {
        this.memory = memory;
    }

    public boolean isCompilerResult() {
        return compilerResult;
    }

    public void setCompilerResult(boolean compilerResult) {
        this.compilerResult = compilerResult;
    }

    public String getErrorData() {
        return errorData;
    }

    public void setErrorData(String errorData) {
        this.errorData = errorData;
    }

    public ExecResult getProcessResult() {
        return processResult;
    }

    public void setProcessResult(ExecResult processResult) {
        this.processResult = processResult;
    }

    public String getTrueAnswer() {
        return trueAnswer;
    }

    public void setTrueAnswer(String trueAnswer) {
        this.trueAnswer = trueAnswer;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public Integer getTestCaseIndex() {
        return testCaseIndex;
    }

    public void setTestCaseIndex(Integer testCaseIndex) {
        this.testCaseIndex = testCaseIndex;
    }

    public String getInputContent() {
        return inputContent;
    }

    public void setInputContent(String inputContent) {
        this.inputContent = inputContent;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProblem_title() {
        return problem_title;
    }

    public void setProblem_title(String problem_title) {
        this.problem_title = problem_title;
    }
}
