package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @program: mloj-backend
 * @description:保存高校的表
 * @author: LOTP
 * @create: 2021-08-13 12:45
 **/
@TableName("t_school")
public class School extends BaseEntity{
    private String schoolName;

    private String email;

    private String schoolBackground;

    private String schoolEnglishName;

    public String getSchoolEnglishName() {
        return schoolEnglishName;
    }

    public void setSchoolEnglishName(String schoolEnglishName) {
        this.schoolEnglishName = schoolEnglishName;
    }
/*private String updateTime;

    private String createTime;*/

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSchoolBackground() {
        return schoolBackground;
    }

    public void setSchoolBackground(String schoolBackground) {
        this.schoolBackground = schoolBackground;
    }

    @Override
    public String toString() {
        return "School{" +
                "schoolName='" + schoolName + '\'' +
                ", email='" + email + '\'' +
                ", schoolBackground='" + schoolBackground + '\'' +
                ", schoolEnglishName='" + schoolEnglishName + '\'' +
                '}';
    }
}


