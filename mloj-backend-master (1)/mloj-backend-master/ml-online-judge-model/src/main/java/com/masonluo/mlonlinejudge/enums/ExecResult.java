package com.masonluo.mlonlinejudge.enums;

import java.util.Objects;

/**
 * 评判服务器返回的执行结果
 *
 * @author masonluo
 * @date 2020/12/26 4:19 下午
 */
public enum ExecResult {
    WRONG_ANSWER("wrong answer", -1),
    SUCCESS("success", 0),
    CPU_TIME_LIMIT_EXCEEDED("cpu time limit exceeded", 1),
    REAL_TIME_LIMIT_EXCEEDED("real time limit exceeded", 2),
    MEMORY_LIMIT_EXCEEDED("memory time limit exceeded", 3),
    RUNTIME_ERROR("runtime error", 4),
    SYSTEM_ERROR("system error", 5);

    ExecResult(String description, int id) {
        this.id = id;
        this.description = description;
    }

    public final int id;

    public final String description;

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public static ExecResult getById(int id) {
        for (ExecResult execResult : ExecResult.values()) {
            if (Objects.equals(id, execResult.id)) {
                return execResult;
            }
        }
        return null;
    }
}
