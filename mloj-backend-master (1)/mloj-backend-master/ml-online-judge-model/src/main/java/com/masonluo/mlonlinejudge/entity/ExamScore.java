package com.masonluo.mlonlinejudge.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 考试得分表
 * </p>
 *
 * @author yangqi
 * @since 2021-10-10
 */
@TableName("t_exam_score")
@ApiModel(value="TExamScore对象", description="考试得分表")
public class ExamScore extends BaseEntity {

//    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "答卷id")
    private Integer answerSheetId;

    @ApiModelProperty(value = "考试id")
    private Integer examId;

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "班级id")
    private Integer classId;

    @ApiModelProperty(value = "单选题得分")
    private Integer singleChoiceScore;

    @ApiModelProperty(value = "判断题得分")
    private Integer trueFalseQuestionScore;

    @ApiModelProperty(value = "编程题得分")
    private Integer programQuestionScore;


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAnswerSheetId() {
        return answerSheetId;
    }

    public void setAnswerSheetId(Integer answerSheetId) {
        this.answerSheetId = answerSheetId;
    }

    public Integer getExamId() {
        return examId;
    }

    public void setExamId(Integer examId) {
        this.examId = examId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getSingleChoiceScore() {
        return singleChoiceScore;
    }

    public void setSingleChoiceScore(Integer singleChoiceScore) {
        this.singleChoiceScore = singleChoiceScore;
    }

    public Integer getTrueFalseQuestionScore() {
        return trueFalseQuestionScore;
    }

    public void setTrueFalseQuestionScore(Integer trueFalseQuestionScore) {
        this.trueFalseQuestionScore = trueFalseQuestionScore;
    }

    public Integer getProgramQuestionScore() {
        return programQuestionScore;
    }
    public void setProgramQuestionScore(Integer programQuestionScore) {
        this.programQuestionScore = programQuestionScore;
    }
    @Override
    public String toString() {
        return "ExamScore{" +
                "id=" + id +
                ", answerSheetId=" + answerSheetId +
                ", examId=" + examId +
                ", classId=" + classId +
                ", singleChoiceScore=" + singleChoiceScore +
                ", trueFalseQuestionScore=" + trueFalseQuestionScore +
                ", programQuestionScore=" + programQuestionScore +
                '}';
    }


}
